#ifndef CNE_NOTIFICATIONS_H
#define CNE_NOTIFICATIONS_H

#include <functional>
#include <list>
#include <memory>
#include <utility>

#include "utils.h"

namespace cne { namespace notifications {
    /**
     * @brief Class deriving from specified notifications
     * Provides interface for work with all notifiers
     *
     * @tparam Notifications Pack of requested notifications
     */
    template<typename... Notifications>
    class notifications_base : public Notifications...
    {
    public:
        /**
         * @brief Disconnects all connections and clears listeners of all notifications
         */
        void clear_listeners() { clear_listeners_impl<Notifications...>(); }

        /**
         * @brief Swaps all listeners of all notifications
         *
         * @param other Notification mixin to swap listeners with
         */
        void swap_listeners(notifications_base& other) { swap_listeners_impl<Notifications...>(other); }

        /**
         * @brief Swaps all listeners of all notifications
         *
         * @param other Notification mixin to swap listeners with
         */
        void swap_listeners(notifications_base&& other) { swap_listeners_impl<Notifications...>(other); }

    private:
        template<typename Last>
        void clear_listeners_impl() {
            Last::clear_listeners();
        }

        template<typename First, typename Second, typename... Rest>
        void clear_listeners_impl() {
            First::clear_listeners();
            clear_listeners_impl<Second, Rest...>();
        }

        template<typename Last>
        void swap_listeners_impl(notifications_base& other) {
            Last::swap_listeners(dynamic_cast<Last&>(other));
        }

        template<typename First, typename Second, typename... Rest>
        void swap_listeners_impl(notifications_base& other) {
            First::swap_listeners(dynamic_cast<First&>(other));
            swap_listeners_impl<Second, Rest...>(other);
        }
    };

    /**
     * @brief Base class for work with listeners
     * Provides only functionality, should be inherited privately
     *
     * @tparam Args Types which will be forwarded to listeners
     */
    template<typename... Args>
    class notifier
    {
    protected:
        using op_listener_type = std::function<bool(Args...)>;
        using undo_listener_type = std::function<void(Args...)>;
        using listeners_pair = std::pair<op_listener_type, undo_listener_type>;

    public:
        using listeners_type = std::list<listeners_pair>;
        using shared_proxy = utils::shared_proxy<listeners_type>;
        using connection_type = connection<notifier>;

        notifier() { initialize_proxy(); }

        notifier(const notifier& other) : m_listeners(other.m_listeners) { initialize_proxy(); }

        notifier(notifier&& other) : m_listeners(std::move(other)) { initialize_proxy(); }

        notifier& operator=(const notifier& other) {
            m_listeners = other.m_listeners;

            return *this;
        }

        notifier& operator=(const notifier&& other) {
            m_listeners = std::move(other.m_listeners);

            return *this;
        }

        ~notifier() { *m_proxy = nullptr; }

        /**
         * @brief Adds listeners and returns object representing this connection
         *
         * @param listener Notification listener
         * @param undo_listener Listener called when subsequent notifications failed
         * @return auto Object representing the connection
         */
        auto add_listener(op_listener_type&& listener, undo_listener_type&& undo_listener = nullptr) {
            m_listeners.emplace_back(listener, undo_listener);
            return connection_type(m_proxy, std::prev(m_listeners.end()));
        }

        /**
         * @brief Calls all listeners, if any of them fails calls previous undo listeners
         *
         * @param args Arguments to forward to listener
         * @return true Notifications succeeded
         * @return false Notifications failed
         */
        bool notify(Args... args) const {
            auto fail_it = notify_operation(std::forward<Args>(args)...);

            if (!check_success(fail_it)) {
                notify_undo(fail_it, std::forward<Args>(args)...);
                return false;
            }

            return true;
        }

        /**
         * @brief Calls all listeners
         *
         * @param args Arguments to forward to listener
         * @return listeners_type::const_iterator Iterator to failed listener or end iterator of the list of listeners
         */
        typename listeners_type::const_iterator notify_operation(Args... args) const {
            auto it = m_listeners.begin();
            try {
                for (; it != m_listeners.end(); ++it) {
                    if (!it->first(std::forward<Args>(args)...)) { break; }
                }
            } catch (...) {
                notify_undo(it, std::forward<Args>(args)...);
                throw;
            }

            return it;
        }

        /**
         * @brief Checks if notify_operation called previously succeeded
         *
         * @param it Iterator returned from notify_operation
         * @return true Notifications succeeded
         * @return false Notifications failed
         */
        bool check_success(typename listeners_type::const_iterator it) const { return it == m_listeners.end(); }

        /**
         * @brief Calls all undo listeners preceding the failed connection
         * Calls are made in reverse order to notify_operation
         *
         * @param failed_listener Iterator to the failed listener
         * @param args Arguments to forward to listener
         */
        void notify_undo(typename listeners_type::const_iterator failed_listener, Args... args) const {
            auto reverse_it = std::reverse_iterator(failed_listener);
            auto reverse_end = std::reverse_iterator(m_listeners.begin());
            for (; reverse_it != reverse_end; ++reverse_it) {
                if (reverse_it->second) { reverse_it->second(std::forward<Args>(args)...); }
            }
        }

        /**
         * @brief Calls all undo listeners
         * Calls are made in reverse order to notify_operation
         *
         * @param args Arguments to forward to listener
         */
        void notify_undo(Args... args) const { notify_undo(m_listeners.cend(), std::forward<Args>(args)...); }

        /**
         * @brief Disconnects all connections and clears the list of listeners
         */
        void clear_listeners() {
            m_listeners.clear();

            *m_proxy = nullptr;
            initialize_proxy();
        }

        /**
         * @brief Swaps all listeners
         *
         * @param other Notifier to swap listeners with
         */
        void swap_listeners(notifier& other) {
            m_listeners.swap(other.m_listeners);
            m_proxy.swap(other.m_proxy);
        }

    private:
        void initialize_proxy() { m_proxy = std::make_shared<listeners_type*>(&m_listeners); }

        listeners_type m_listeners;
        shared_proxy   m_proxy; // proxies used for connections
    };

    /**
     * @brief Notifier for operations that try to insert new element into container
     *
     * @tparam Container Container type
     * @tparam T Value type
     * @tparam ConstIterator Const iterator type or empty pack
     */
    template<typename Container, typename T, typename... ConstIterator>
    class insert : protected notifier<const Container&, const T&, ConstIterator...>
    {
    private:
        using base = notifier<const Container&, const T&, ConstIterator...>;
        using op_listener_type = typename base::op_listener_type;
        using undo_listener_type = typename base::undo_listener_type;

    public:
        /**
         * @brief Adds listeners to insert notifications and returns object representing this connection
         *
         * @param listener Notification listener
         * @param undo_listener Listener called when subsequent notifications failed
         * @return auto Object representing the connection
         */
        auto insert_check(op_listener_type&& listener, undo_listener_type&& undo_listener = nullptr) {
            return this->add_listener(std::move(listener), std::move(undo_listener));
        }

        /**
         * @brief Disconnects all connections and clears the list of insert listeners
         */
        void clear_insert_listeners() { this->clear_listeners(); }

        /**
         * @brief Swaps all insert listeners
         *
         * @param other Notifier to swap listeners with
         */
        void swap_insert_listeners(insert& other) { this->swap_listeners(other); }

        /**
         * @brief Swaps all insert listeners
         *
         * @param other Notifier to swap listeners with
         */
        void swap_insert_listeners(insert&& other) { this->swap_listeners(other); }

    protected:
        /**
         * @brief Calls all listeners of insert notification, if any of them fails calls previous undo listeners
         *
         * @param val Value to be inserted
         * @param pos Position where will the value be inserted
         * @return true Notifications succeeded
         * @return false Notifications failed
         */
        bool insert_called(const T& val, ConstIterator... pos) const {
            return this->notify(*static_cast<const Container*>(this), val, pos...);
        }

        /**
         * @brief Calls all undo listeners of insert notification
         * Calls are made in reverse order to insert_called
         *
         * @param val Value notified to be inserted
         * @param pos Notified position where will the value be inserted
         */
        void insert_undo(const T& val, ConstIterator... pos) const {
            this->notify_undo(*static_cast<const Container*>(this), val, pos...);
        }

        /**
         * @brief Calls insert_called for all values and positions in range [val_first, val_last)
         * Range of positions must be at least as long as range of values
         * This overload participates in overload only if ConstIterator isn't an empty pack
         *
         * @tparam PosIterator Bidirectional iterator
         * @tparam ValIterator Bidirectional iterator
         * @param val_first First iterator to value
         * @param val_last Last iterator to value
         * @param pos_first First iterator to position
         * @return true Notifications succeeded
         * @return false Notifications failed
         */
        template<typename PosIterator, typename ValIterator>
        bool insert_called(ValIterator val_first, ValIterator val_last, PosIterator pos_first) {
            auto val_it = val_first;
            auto pos_it = pos_first;
            try {
                for (; val_it != val_last; ++val_it, ++pos_it) {
                    if (!insert_called(*val_it, *pos_it)) { break; }
                }
            } catch (...) {
                insert_undo(val_it, val_first, pos_it);

                throw;
            }

            if (val_it != val_last) {
                insert_undo(val_it, val_first, pos_it);

                return false;
            }

            return true;
        }

        /**
         * @brief Calls insert_undo for all values and positions in range [val_first, val_fail)
         * Calls are made in reverse order to insert_called
         * This overload participates in overload only if ConstIterator isn't an empty pack
         *
         * @tparam PosIterator Bidirectional iterator
         * @tparam ValIterator Bidirectional iterator
         * @param val_fail Iterator to value that caused notification to fail
         * @param val_first First iterator to value
         * @param pos_fail Iterator to position that caused notification to fail
         */
        template<typename PosIterator, typename ValIterator>
        void insert_undo(ValIterator val_fail, ValIterator val_first, PosIterator pos_fail) {
            auto val_rit = std::reverse_iterator(val_fail);
            auto val_rend = std::reverse_iterator(val_first);
            for (; val_rit != val_rend; ++val_rit) {
                --pos_fail;
                insert_undo(*val_rit, *pos_fail);
            }
        }

        /**
         * @brief Calls insert_called for all values in range [val_first, val_last)
         * This overload participates in overload only if ConstIterator is an empty pack
         *
         * @tparam ValIterator Bidirectional iterator
         * @param val_first First iterator to value
         * @param val_last Last iterator to value
         * @return true Notifications succeeded
         * @return false Notifications failed
         */
        template<typename ValIterator>
        bool insert_called(ValIterator val_first, ValIterator val_last) {
            auto val_it = val_first;
            try {
                for (; val_it != val_last; ++val_it) {
                    if (!insert_called(*val_it)) { break; }
                }
            } catch (...) {
                insert_undo(val_it, val_first);

                throw;
            }

            if (val_it != val_last) {
                insert_undo(val_it, val_first);

                return false;
            }

            return true;
        }

        /**
         * @brief Calls insert_undo for all values and positions in range [val_first, val_fail)
         * Calls are made in reverse order to insert_called
         * This overload participates in overload only if ConstIterator is an empty pack
         *
         * @tparam ValIterator Bidirectional iterator
         * @param val_fail Iterator to value that caused notification to fail
         * @param val_first First iterator to value
         */
        template<typename ValIterator>
        void insert_undo(ValIterator val_fail, ValIterator val_first) {
            auto val_rit = std::reverse_iterator(val_fail);
            auto val_rend = std::reverse_iterator(val_first);
            for (; val_rit != val_rend; ++val_rit) { insert_undo(*val_rit); }
        }
    };

    /**
     * @brief Notifier for operations that try to erase elements from container
     *
     * @tparam Container Container type
     * @tparam ConstIterator Const iterator of container
     */
    template<typename Container, typename ConstIterator>
    class erase : protected notifier<const Container&, ConstIterator, ConstIterator>
    {
    private:
        using base = notifier<const Container&, ConstIterator, ConstIterator>;
        using op_listener_type = typename base::op_listener_type;
        using undo_listener_type = typename base::undo_listener_type;

    public:
        /**
         * @brief Adds listeners to erase notifications and returns object representing this connection
         *
         * @param listener Notification listener
         * @param undo_listener Listener called when subsequent notifications failed
         * @return auto Object representing the connection
         */
        auto erase_check(op_listener_type&& listener, undo_listener_type&& undo_listener = nullptr) {
            return this->add_listener(std::move(listener), std::move(undo_listener));
        }

        /**
         * @brief Disconnects all connections and clears the list of erase listeners
         */
        void clear_erase_listeners() { this->clear_listeners(); }

        /**
         * @brief Swaps all erase listeners
         *
         * @param other Notifier to swap listeners with
         */
        void swap_erase_listeners(erase& other) { this->swap_listeners(other); }

        /**
         * @brief Swaps all erase listeners
         *
         * @param other Notifier to swap listeners with
         */
        void swap_erase_listeners(erase&& other) { this->swap_listeners(other); }

    protected:
        /**
         * @brief Calls all listeners of erase notification, if any of them fails calls previous undo listeners
         * Attempted erased elemets are in range [first, last)
         *
         * @param first First iterator of erase range
         * @param last Last iterator of erase range
         * @return true Notifications succeeded
         * @return false Notifications failed
         */
        bool erase_called(ConstIterator first, ConstIterator last) const {
            if (first == last) { return true; }

            return this->notify(*static_cast<const Container*>(this), first, last);
        }

        /**
         * @brief Calls all undo listeners of erase notification
         *
         * @param first First iterator of notified erase range
         * @param last Last iterator of notified erase range
         */
        void erase_undo(ConstIterator first, ConstIterator last) const {
            this->notify_undo(*static_cast<const Container*>(this), first, last);
        }
    };

    /**
     * @brief Notifier for operations that try to replace value of element in container
     *
     * @tparam Container Container type
     * @tparam ConstIterator Const iterator of container
     * @tparam T Type of value to be replaced
     * In case of a map, T is mapped_type
     */
    template<typename Container, typename ConstIterator, typename T>
    class replace : protected notifier<const Container&, ConstIterator, const T&>
    {
    private:
        using base = notifier<const Container&, ConstIterator, const T&>;
        using op_listener_type = typename base::op_listener_type;
        using undo_listener_type = typename base::undo_listener_type;

    public:
        template<typename C, typename B>
        friend class replaceable_reference_wrapper;

        /**
         * @brief Adds listeners to replace notifications and returns object representing this connection
         *
         * @param listener Notification listener
         * @param undo_listener Listener called when subsequent notifications failed
         * @return auto Object representing the connection
         */
        auto replace_check(op_listener_type&& listener, undo_listener_type&& undo_listener = nullptr) {
            return this->add_listener(std::move(listener), std::move(undo_listener));
        }

        /**
         * @brief Disconnects all connections and clears the list of replace listeners
         */
        void clear_replace_listeners() { this->clear_listeners(); }

        /**
         * @brief Swaps all replace listeners
         *
         * @param other Notifier to swap listeners with
         */
        void swap_replace_listeners(replace& other) { this->swap_listeners(other); }

        /**
         * @brief Swaps all replace listeners
         *
         * @param other Notifier to swap listeners with
         */
        void swap_replace_listeners(replace&& other) { this->swap_listeners(other); }

    protected:
        /**
         * @brief Calls all listeners of replace notification, if any of them fails calls previous undo listeners
         *
         * @param it Iterator to element to be replaced
         * @param val Value that will replace the element
         * @return true Notifications succeeded
         * @return false Notifications failed
         */
        bool replace_called(ConstIterator it, const T& val) const {
            return this->notify(*static_cast<const Container*>(this), it, val);
        }

        /**
         * @brief Calls all undo listeners of replace notification
         *
         * @param it Iterator to element notified to be replaced
         * @param val Value that notified to replace the element
         */
        void replace_undo(ConstIterator it, const T& val) const {
            this->notify_undo(*static_cast<const Container*>(this), it, val);
        }

        /**
         * @brief Calls replace_called for all positions and values in range [val_first, val_last)
         * Range of positions must be at least as long as range of values
         *
         * @tparam PosIterator Bidirectional iterator
         * @tparam ValIterator Bidirectional iterator
         * @param pos_first First iterator to position
         * @param val_first First iterator to value
         * @param val_last Last iterator to value
         * @return true Notifications succeeded
         * @return false Notifications failed
         */
        template<typename PosIterator, typename ValIterator>
        bool replace_called(PosIterator pos_first, ValIterator val_first, ValIterator val_last) {
            auto val_it = val_first;
            auto pos_it = pos_first;
            try {
                for (; val_it != val_last; ++val_it, ++pos_it) {
                    if (!replace_called(*pos_it, *val_it)) { break; }
                }
            } catch (...) {
                replace_undo(pos_it, val_it, val_first);

                throw;
            }

            if (val_it != val_last) {
                replace_undo(pos_it, val_it, val_first);

                return false;
            }

            return true;
        }

        /**
         * @brief Calls replace_undo for all values and positions in range [val_first, val_fail)
         * Calls are made in reverse order to insert_called
         *
         * @tparam PosIterator Bidirectional iterator
         * @tparam ValIterator Bidirectional iterator
         * @param pos_fail Iterator to position that caused notification to fail
         * @param val_fail Iterator to value that caused notification to fail
         * @param val_first First iterator to value
         */
        template<typename PosIterator, typename ValIterator>
        void replace_undo(PosIterator pos_fail, ValIterator val_fail, ValIterator val_first) {
            auto val_rit = std::reverse_iterator(val_fail);
            auto val_rend = std::reverse_iterator(val_first);
            for (; val_rit != val_rend; ++val_rit) {
                --pos_fail;
                replace_undo(*pos_fail, *val_rit);
            }
        }
    };

    /**
     * @brief Notifier for value change of elements in container through reference wrapper
     * The only notification to be called after the operation
     * Reference wrapper provides way to pass rollback operation that is called if notifications fail
     *
     * @tparam Container Container type
     * @tparam ConstIterator Const iterator of container
     */
    template<typename Container, typename ConstIterator>
    class value_change : protected notifier<const Container&, ConstIterator>
    {
    private:
        using base = notifier<const Container&, ConstIterator>;
        using op_listener_type = typename base::op_listener_type;
        using undo_listener_type = typename base::undo_listener_type;

        template<typename C, typename B>
        friend class reference_wrapper;

    public:
        /**
         * @brief Adds listeners to value change notifications and returns object representing this connection
         *
         * @param listener Notification listener
         * @param undo_listener Listener called when subsequent notifications failed
         * @return auto Object representing the connection
         */
        auto value_change_check(op_listener_type&& listener, undo_listener_type&& undo_listener = nullptr) {
            return this->add_listener(std::move(listener), std::move(undo_listener));
        }

        /**
         * @brief Disconnects all connections and clears the list of value change listeners
         */
        void clear_value_change_listeners() { this->clear_listeners(); }

        /**
         * @brief Swaps all value change listeners
         *
         * @param other Notifier to swap listeners with
         */
        void swap_value_change_listeners(value_change& other) { this->swap_listeners(other); }

        /**
         * @brief Swaps all value change listeners
         *
         * @param other Notifier to swap listeners with
         */
        void swap_value_change_listeners(value_change&& other) { this->swap_listeners(other); }

    protected:
        /**
         * @brief Calls all listeners of value change notification, if any of them fails calss rollback and previous
         * undo listeners
         *
         * @param it Iterator to changed element
         * @param rollback Invokable object that takes no arguments that rollback the change
         * @return true Notifications succeeded
         * @return false Notifications failed
         */
        bool value_changed(ConstIterator it, std::function<void()> rollback) const {
            typename base::listeners_type::const_iterator failed_it;
            try {
                failed_it = this->notify_operation(*static_cast<const Container*>(this), it);
            } catch (...) {
                rollback();
                this->notify_undo(failed_it, *static_cast<const Container*>(this), it);

                throw;
            }

            if (!this->check_success(failed_it)) {
                rollback();
                this->notify_undo(failed_it, *static_cast<const Container*>(this), it);

                return false;
            }

            return true;
        }
    };
}} // namespace cne::notifications

#endif // CNE_NOTIFICATIONS_H
