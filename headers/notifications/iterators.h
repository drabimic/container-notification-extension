#ifndef CNE_ITERATORS_H
#define CNE_ITERATORS_H

namespace cne {
/**
 * @brief Namespace containing iterator wrappers supporting value_change and replace notifications from returned
 * references and pointers
 *
 * Iterators doesn't satisfy all requirements given by their iterator category due to the need to return custom
 * objects wrapping the references and pointers
 */
namespace iterators {
    template<typename Traits>
    class const_forward_iterator
    {
    private:
        using container = typename Traits::container;
        using const_iterator = typename Traits::wrapped_const_iterator;

    public:
        using real_reference = typename std::iterator_traits<const_iterator>::reference;
        using difference_type = typename container::difference_type;
        using value_type = typename container::value_type;
        using reference = typename container::const_reference;
        using pointer = typename container::const_pointer;
        using iterator_category = std::forward_iterator_tag;

        const_forward_iterator() = default;

        explicit const_forward_iterator(const_iterator it) : m_it(it) {}

        void swap(const_forward_iterator& other) { m_it.swap(other.m_it); }

        [[nodiscard]] reference operator*() const { return reference(*this); }

        [[nodiscard]] pointer operator->() const { return pointer(reference(*this)); }

        const_forward_iterator& operator++() {
            ++m_it;
            return *this;
        }

        const_forward_iterator operator++(int) {
            const_forward_iterator tmp = *this;
            ++(*this);
            return tmp;
        }

        [[nodiscard]] bool operator==(const const_forward_iterator& right) const { return (m_it == right.m_it); }

        [[nodiscard]] bool operator!=(const const_forward_iterator& right) const { return !(*this == right); }

        friend void swap(const_forward_iterator<Traits>& lhs, const_forward_iterator<Traits>& rhs) { lhs.swap(rhs); }

        /**
         * @brief Get reference from wrapped iterator
         *
         * @return real_reference Reference retrieved by dereferencing wrapped iterator
         */
        real_reference get_reference() const { return *m_it; }

        /**
         * @brief Get the wrapped iterator
         *
         * @return const_iterator Wrapped iterator
         */
        const_iterator get_iterator() const { return m_it; }

    private:
        const_iterator m_it;
    };

    template<typename Traits>
    class const_local_fw_iterator
    {
    private:
        using container = typename Traits::container;
        using const_local_iterator = typename Traits::wrapped_const_local_iterator;

    public:
        using real_reference = typename std::iterator_traits<const_local_iterator>::reference;
        using difference_type = typename container::difference_type;
        using value_type = typename container::value_type;
        using reference = typename container::const_reference;
        using pointer = typename container::const_pointer;
        using iterator_category = std::forward_iterator_tag;

        const_local_fw_iterator() = default;

        explicit const_local_fw_iterator(const_local_iterator it) : m_it(it) {}

        void swap(const_local_fw_iterator& other) { m_it.swap(other.m_it); }

        [[nodiscard]] reference operator*() const { return reference(*this); }

        [[nodiscard]] pointer operator->() const { return pointer(reference(*this)); }

        const_local_fw_iterator& operator++() {
            ++m_it;
            return *this;
        }

        const_local_fw_iterator operator++(int) {
            const_local_fw_iterator tmp = *this;
            ++(*this);
            return tmp;
        }

        [[nodiscard]] bool operator==(const const_local_fw_iterator& right) const { return (m_it == right.m_it); }

        [[nodiscard]] bool operator!=(const const_local_fw_iterator& right) const { return !(*this == right); }

        friend void swap(const_local_fw_iterator<Traits>& lhs, const_local_fw_iterator<Traits>& rhs) { lhs.swap(rhs); }

        /**
         * @brief Get reference from wrapped iterator
         *
         * @return real_reference Reference retrieved by dereferencing wrapped iterator
         */
        real_reference get_reference() const { return *m_it; }

        /**
         * @brief Get the wrapped iterator
         *
         * @return const_local_iterator Wrapped iterator
         */
        const_local_iterator get_iterator() const { return m_it; }

    private:
        const_local_iterator m_it;
    };

    template<typename Traits>
    class forward_iterator
    {
    private:
        using container = typename Traits::container;
        using iterator = typename Traits::wrapped_iterator;
        using shared_proxy = typename container::shared_proxy;

    public:
        using real_reference = typename std::iterator_traits<iterator>::reference;
        using difference_type = typename container::difference_type;
        using value_type = typename container::value_type;
        using reference = typename container::reference;
        using pointer = typename container::pointer;
        using iterator_category = std::forward_iterator_tag;

        forward_iterator() = default;

        forward_iterator(const shared_proxy& proxy, iterator it) : m_proxy(proxy), m_it(it) {}

        operator const_forward_iterator<Traits>() { return const_forward_iterator<Traits>(m_it); }

        void swap(forward_iterator& other) {
            if (std::addressof(other) != this) {
                m_proxy.swap(other.m_proxy);
                m_it.swap(other.m_it);
            }
        }

        [[nodiscard]] reference operator*() { return reference(m_proxy, *this); }

        [[nodiscard]] pointer operator->() { return pointer(reference(m_proxy, *this)); }

        forward_iterator& operator++() {
            ++m_it;
            return *this;
        }

        forward_iterator operator++(int) {
            forward_iterator tmp = *this;
            ++(*this);
            return tmp;
        }

        [[nodiscard]] bool operator==(const forward_iterator& right) const { return (m_it == right.m_it); }

        [[nodiscard]] bool operator!=(const forward_iterator& right) const { return !(*this == right); }

        friend void swap(forward_iterator<Traits>& lhs, forward_iterator<Traits>& rhs) { lhs.swap(rhs); }

        /**
         * @brief Get reference from wrapped iterator
         *
         * @return real_reference Reference retrieved by dereferencing wrapped iterator
         */
        real_reference get_reference() const { return *m_it; }

        /**
         * @brief Get the wrapped iterator
         *
         * @return iterator Wrapped iterator
         */
        iterator get_iterator() const { return m_it; }

    private:
        shared_proxy m_proxy;
        iterator     m_it;
    };

    template<typename Traits>
    class const_bidirectional_iterator
    {
    private:
        using container = typename Traits::container;
        using const_iterator = typename Traits::wrapped_const_iterator;

    public:
        using real_reference = typename std::iterator_traits<const_iterator>::reference;
        using difference_type = typename container::difference_type;
        using value_type = typename container::value_type;
        using reference = typename container::const_reference;
        using pointer = typename container::const_pointer;
        using iterator_category = std::bidirectional_iterator_tag;

        const_bidirectional_iterator() = default;

        explicit const_bidirectional_iterator(const_iterator it) : m_it(it) {}

        void swap(const_bidirectional_iterator& other) { m_it.swap(other.m_it); }

        [[nodiscard]] reference operator*() const { return reference(*this); }

        [[nodiscard]] pointer operator->() const { return pointer(reference(*this)); }

        const_bidirectional_iterator& operator++() {
            ++m_it;
            return *this;
        }

        const_bidirectional_iterator operator++(int) {
            const_bidirectional_iterator tmp = *this;
            ++(*this);
            return tmp;
        }

        const_bidirectional_iterator& operator--() {
            --m_it;
            return *this;
        }

        const_bidirectional_iterator operator--(int) {
            const_bidirectional_iterator tmp = *this;
            --(*this);
            return tmp;
        }

        [[nodiscard]] bool operator==(const const_bidirectional_iterator& right) const { return (m_it == right.m_it); }

        [[nodiscard]] bool operator!=(const const_bidirectional_iterator& right) const { return !(*this == right); }

        friend void swap(const_bidirectional_iterator<Traits>& lhs, const_bidirectional_iterator<Traits>& rhs) {
            lhs.swap(rhs);
        }

        /**
         * @brief Get reference from wrapped iterator
         *
         * @return real_reference Reference retrieved by dereferencing wrapped iterator
         */
        real_reference get_reference() const { return *m_it; }

        /**
         * @brief Get the wrapped iterator
         *
         * @return const_iterator Wrapped iterator
         */
        const_iterator get_iterator() const { return m_it; }

    private:
        const_iterator m_it;
    };

    template<typename Traits>
    class bidirectional_iterator
    {
    private:
        using container = typename Traits::container;
        using iterator = typename Traits::wrapped_iterator;
        using shared_proxy = typename container::shared_proxy;

    public:
        using real_reference = typename std::iterator_traits<iterator>::reference;
        using difference_type = typename container::difference_type;
        using value_type = typename container::value_type;
        using reference = typename container::reference;
        using pointer = typename container::pointer;
        using iterator_category = std::bidirectional_iterator_tag;

        bidirectional_iterator() = default;

        bidirectional_iterator(const shared_proxy& proxy, iterator it) : m_proxy(proxy), m_it(it) {}

        operator const_bidirectional_iterator<Traits>() { return const_bidirectional_iterator<Traits>(m_it); }

        void swap(bidirectional_iterator& other) {
            if (std::addressof(other) != this) {
                m_proxy.swap(other.m_proxy);
                m_it.swap(other.m_it);
            }
        }

        [[nodiscard]] reference operator*() { return reference(m_proxy, *this); }

        [[nodiscard]] pointer operator->() { return pointer(reference(m_proxy, *this)); }

        bidirectional_iterator& operator++() {
            ++m_it;
            return *this;
        }

        bidirectional_iterator operator++(int) {
            bidirectional_iterator tmp = *this;
            ++(*this);
            return tmp;
        }

        bidirectional_iterator& operator--() {
            --m_it;
            return *this;
        }

        bidirectional_iterator operator--(int) {
            bidirectional_iterator tmp = *this;
            --(*this);
            return tmp;
        }

        [[nodiscard]] bool operator==(const bidirectional_iterator& right) const { return (m_it == right.m_it); }

        [[nodiscard]] bool operator!=(const bidirectional_iterator& right) const { return !(*this == right); }

        friend void swap(bidirectional_iterator<Traits>& lhs, bidirectional_iterator<Traits>& rhs) { lhs.swap(rhs); }

        /**
         * @brief Get reference from wrapped iterator
         *
         * @return real_reference Reference retrieved by dereferencing wrapped iterator
         */
        real_reference get_reference() const { return *m_it; }

        /**
         * @brief Get the wrapped iterator
         *
         * @return iterator Wrapped iterator
         */
        iterator get_iterator() const { return m_it; }

    private:
        shared_proxy m_proxy;
        iterator     m_it;
    };

    template<typename Traits>
    class const_random_access_iterator
    {
    private:
        using container = typename Traits::container;
        using const_iterator = typename Traits::wrapped_const_iterator;

    public:
        using real_reference = typename std::iterator_traits<const_iterator>::reference;
        using difference_type = typename container::difference_type;
        using value_type = typename container::value_type;
        using reference = typename container::const_reference;
        using pointer = typename container::const_pointer;
        using iterator_category = std::random_access_iterator_tag;

        const_random_access_iterator() = default;

        explicit const_random_access_iterator(const_iterator it) : m_it(it) {}

        void swap(const_random_access_iterator& other) { m_it.swap(other.m_it); }

        [[nodiscard]] reference operator*() const { return reference(*this); }

        [[nodiscard]] pointer operator->() const { return pointer(reference(*this)); }

        [[nodiscard]] reference operator[](const difference_type offset) const { return (*(*this + offset)); }

        const_random_access_iterator& operator++() {
            ++m_it;
            return *this;
        }

        const_random_access_iterator operator++(int) {
            const_random_access_iterator tmp = *this;
            ++(*this);
            return tmp;
        }

        const_random_access_iterator& operator--() {
            --m_it;
            return *this;
        }

        const_random_access_iterator operator--(int) {
            const_random_access_iterator tmp = *this;
            --(*this);
            return tmp;
        }

        const_random_access_iterator& operator+=(const difference_type offset) {
            m_it += offset;
            return *this;
        }

        [[nodiscard]] const_random_access_iterator operator+(const difference_type offset) const {
            const_random_access_iterator tmp = *this;
            return tmp += offset;
        }

        const_random_access_iterator& operator-=(const difference_type offset) { return *this += (-offset); }

        [[nodiscard]] const_random_access_iterator operator-(const difference_type offset) const {
            const_random_access_iterator tmp = *this;
            return tmp -= offset;
        }

        [[nodiscard]] difference_type operator-(const const_random_access_iterator right) const {
            return m_it - right.m_it;
        }

        [[nodiscard]] bool operator==(const const_random_access_iterator& right) const { return (m_it == right.m_it); }

        [[nodiscard]] bool operator!=(const const_random_access_iterator& right) const { return !(*this == right); }

        [[nodiscard]] bool operator<(const const_random_access_iterator& right) const { return (m_it < right.m_it); }

        [[nodiscard]] bool operator>(const const_random_access_iterator& right) const { return (m_it > right.m_it); }

        [[nodiscard]] bool operator<=(const const_random_access_iterator& right) const { return (m_it <= right.m_it); }

        [[nodiscard]] bool operator>=(const const_random_access_iterator& right) const { return (m_it >= right.m_it); }

        friend void swap(const_random_access_iterator<Traits>& lhs, const_random_access_iterator<Traits>& rhs) {
            lhs.swap(rhs);
        }

        /**
         * @brief Get reference from wrapped iterator
         *
         * @return real_reference Reference retrieved by dereferencing wrapped iterator
         */
        real_reference get_reference() const { return *m_it; }

        /**
         * @brief Get the wrapped iterator
         *
         * @return const_iterator Wrapped iterator
         */
        const_iterator get_iterator() const { return m_it; }

    private:
        const_iterator m_it;
    };

    template<typename Traits>
    class random_access_iterator
    {
    private:
        using container = typename Traits::container;
        using iterator = typename Traits::wrapped_iterator;
        using shared_proxy = typename container::shared_proxy;

    public:
        using real_reference = typename std::iterator_traits<iterator>::reference;
        using difference_type = typename container::difference_type;
        using value_type = typename container::value_type;
        using reference = typename container::reference;
        using pointer = typename container::pointer;
        using iterator_category = std::random_access_iterator_tag;

        random_access_iterator() = default;

        random_access_iterator(const shared_proxy& proxy, iterator it) : m_proxy(proxy), m_it(it) {}

        operator const_random_access_iterator<Traits>() { return const_random_access_iterator<Traits>(m_it); }

        void swap(random_access_iterator& other) {
            if (std::addressof(other) != this) {
                m_proxy.swap(other.m_proxy);
                m_it.swap(other.m_it);
            }
        }

        [[nodiscard]] reference operator*() { return reference(m_proxy, *this); }

        [[nodiscard]] pointer operator->() { return pointer(reference(m_proxy, *this)); }

        [[nodiscard]] reference operator[](const difference_type offset) const { return (*(*this + offset)); }

        random_access_iterator& operator++() {
            ++m_it;
            return *this;
        }

        random_access_iterator operator++(int) {
            random_access_iterator tmp = *this;
            ++(*this);
            return tmp;
        }

        random_access_iterator& operator--() {
            --m_it;
            return *this;
        }

        random_access_iterator operator--(int) {
            random_access_iterator tmp = *this;
            --(*this);
            return tmp;
        }

        random_access_iterator& operator+=(const difference_type offset) {
            m_it += offset;
            return *this;
        }

        [[nodiscard]] random_access_iterator operator+(const difference_type offset) const {
            random_access_iterator tmp = *this;
            return tmp += offset;
        }

        random_access_iterator& operator-=(const difference_type offset) { return *this += (-offset); }

        [[nodiscard]] random_access_iterator operator-(const difference_type offset) const {
            random_access_iterator tmp = *this;
            return tmp -= offset;
        }

        [[nodiscard]] difference_type operator-(const random_access_iterator right) const { return m_it - right.m_it; }

        [[nodiscard]] bool operator==(const random_access_iterator& right) const { return (m_it == right.m_it); }

        [[nodiscard]] bool operator!=(const random_access_iterator& right) const { return !(*this == right); }

        [[nodiscard]] bool operator<(const random_access_iterator& right) const { return (m_it < right.m_it); }

        [[nodiscard]] bool operator>(const random_access_iterator& right) const { return (m_it > right.m_it); }

        [[nodiscard]] bool operator<=(const random_access_iterator& right) const { return (m_it <= right.m_it); }

        [[nodiscard]] bool operator>=(const random_access_iterator& right) const { return (m_it >= right.m_it); }

        friend void swap(random_access_iterator<Traits>& lhs, random_access_iterator<Traits>& rhs) { lhs.swap(rhs); }

        /**
         * @brief Get reference from wrapped iterator
         *
         * @return real_reference Reference retrieved by dereferencing wrapped iterator
         */
        real_reference get_reference() const { return *m_it; }

        /**
         * @brief Get the wrapped iterator
         *
         * @return iterator Wrapped iterator
         */
        iterator get_iterator() const { return m_it; }

    private:
        shared_proxy m_proxy;
        iterator     m_it;
    };
} // namespace iterators
} // namespace cne

#endif // CNE_ITERATORS_H
