#ifndef CNE_REFERENCE_WRAPPER_H
#define CNE_REFERENCE_WRAPPER_H

namespace cne { namespace notifications {
    /**
     * @brief Proxy for pointers returned from iterator
     *
     * @tparam Wrapper Type of reference wrapper
     */
    template<typename Wrapper>
    class pointer_proxy
    {
    public:
        pointer_proxy(const Wrapper& wrapper) : m_wrapper(wrapper) {}

        Wrapper* operator->() { return &m_wrapper; }

    private:
        Wrapper m_wrapper;
    };

    namespace getters {
        template<typename It>
        class key
        {
        public:
            using value_type = typename It::value_type::first_type;
            const value_type& operator()(It it) const { return it->first; }
        };
        template<typename It>
        class mapped_value
        {
        public:
            using value_type = typename It::value_type::second_type;
            const value_type& operator()(It it) const { return it->second; }
        };
        template<typename It>
        class value
        {
        public:
            using value_type = typename It::value_type;
            const value_type& operator()(It it) const { return it->get_reference(); }
        };
    } // namespace getters

    template<typename Container, template<typename> typename Retriever = getters::value>
    class const_reference_wrapper
    {
    protected:
        using iterator = typename Container::const_iterator;
        using retriever = Retriever<iterator>;
        using value_type = typename retriever::value_type;

    public:
        const_reference_wrapper(iterator it) : m_iterator(it) {}

        /**
         * @brief Invokes callable with wrapped object as its only parameter
         *
         * @tparam Ty Callable type that takes in exacly one argument of type:
         * const value_type&, const value_type or value_type
         * @param callable Callable that works with wrapped object
         */
        template<typename Ty>
        void invoke(Ty&& callable) const {
            auto function = std::function(std::forward<Ty>(callable));
            static_assert(
              std::is_same_v<
                decltype(function),
                std::function<void(
                  const value_type&)>> || std::is_same_v<decltype(function), std::function<void(const value_type)>> || std::is_same_v<decltype(function), std::function<void(value_type)>>,
              "In const_reference_wrapper::invoke(Ty) const: Ty can only be Callable with signature void(const "
              "value_type&), void(const value_type) or void(value_type)");

            function(data());
        }

        /**
         * @brief Get const reference of wrapped object
         *
         * @return const value_type& Wrapped object
         */
        const value_type& data() const { return m_retriever(m_iterator); };

        operator const value_type&() const { return data(); }

    protected:
        iterator  m_iterator;
        retriever m_retriever;
    };

    template<typename Map>
    using key_wrapper = const_reference_wrapper<Map, getters::key>;

    template<typename Map>
    using const_mapped_wrapper = const_reference_wrapper<Map, getters::mapped_value>;

    template<typename Map>
    class const_map_reference_wrapper : public const_reference_wrapper<Map, getters::value>
    {
    protected:
        using base = const_reference_wrapper<Map, getters::value>;
        using iterator = typename base::iterator;

    public:
        const_map_reference_wrapper(iterator it) : base(it), first(it), second(it) {}

        key_wrapper<Map>          first;
        const_mapped_wrapper<Map> second;
    };

    template<typename Container, typename ConstBase = const_reference_wrapper<Container>>
    class reference_wrapper : public ConstBase
    {
    protected:
        using iterator = typename ConstBase::iterator;
        using value_type = typename ConstBase::value_type;
        using shared_proxy = typename Container::shared_proxy;

    public:
        reference_wrapper(const shared_proxy& proxy, iterator it) : ConstBase(it), m_proxy(proxy) {}

        using ConstBase::invoke;

        /**
         * @brief Invokes callable with wrapped object as its only parameter, if notifications fail rollback is called
         *
         * @tparam Ty Callable type that takes in exacly one argument of type value_type&
         * @param callable Callable that works with wrapped object
         * @param rollback Callable that rollbacks changes made by callable
         * @return true Notifications succeeded
         * @return false Notifications failed
         */
        template<typename Ty1, typename Ty2>
        bool invoke(Ty1&& callable, Ty2&& rollback) {
            std::function function(std::forward<Ty1>(callable));
            std::function rollback_function(std::forward<Ty2>(rollback));
            static_assert(std::is_same_v<decltype(function), std::function<void(value_type&)>>,
                          "reference_wrapper::invoke(Ty1, Ty2): Ty can only Callable with signature void(value_type&)");
            static_assert(std::is_same_v<decltype(rollback_function), std::function<void(value_type&)>>,
                          "reference_wrapper::invoke(Ty1, Ty2): Ty can only Callable with signature void(value_type&)");

            return invoke_nonconst(std::move(function), std::move(rollback_function));
        }

    protected:
        /**
         * @brief Get reference of wrapped object
         *
         * @return value_type& Wrapped object
         */
        value_type& nonconst_data() { return const_cast<value_type&>(this->data()); }

        bool invoke_nonconst(std::function<void(value_type&)> function,
                             std::function<void(value_type&)> rollback = nullptr) {
            value_type& ref = nonconst_data();
            function(ref);

            return (*m_proxy)->value_changed(this->m_iterator, [&ref, &rollback]() { rollback(ref); });
        }

        shared_proxy m_proxy;
    };

    template<typename Container, typename ConstBase = const_reference_wrapper<Container>>
    class replaceable_reference_wrapper : public reference_wrapper<Container, ConstBase>
    {
        using base = reference_wrapper<Container, ConstBase>;
        using iterator = typename base::iterator;
        using value_type = typename base::value_type;
        using shared_proxy = typename base::shared_proxy;

    public:
        replaceable_reference_wrapper(const shared_proxy& proxy, iterator it) : base(proxy, it) {}

        /**
         * @brief Assigns new_value to wrapped object
         * Notifies replace notifications
         * value_type must be copy assignable
         *
         * @param new_value Value to be assigned
         * @return replaceable_reference_wrapper& returns *this
         */
        replaceable_reference_wrapper& operator=(const value_type& new_value) {
            replace_value(new_value);
            return *this;
        }

        /**
         * @brief Assigns new_value to wrapped object
         * Notifies replace notifications
         * value_type must be move assignable
         *
         * @param new_value Value to be assigned
         * @return replaceable_reference_wrapper& returns *this
         */
        replaceable_reference_wrapper& operator=(value_type&& new_value) {
            replace_value(std::move(new_value));
            return *this;
        }

        /**
         * @brief Assigns new_value to wrapped object
         * Notifies replace notifications
         * value_type must be copy assignable
         *
         * @param new_value Value to be assigned
         * @return true Notifications succeeded
         * @return false Notifications failed
         */
        bool replace_value(const value_type& new_value) { return p_replace_value(new_value); }

        /**
         * @brief Assigns new_value to wrapped object
         * Notifies replace notifications
         * value_type must be move assignable
         *
         * @param new_value Value to be assigned
         * @return true Notifications succeeded
         * @return false Notifications failed
         */
        bool replace_value(value_type&& new_value) { return p_replace_value(std::move(new_value)); }

    private:
        /**
         * @brief Perfect forwarding for replace_value
         *
         * @tparam V const value_type& or value_type&&
         * @param new_value Value to be assigned
         * @return true Notifications succeeded
         * @return false Notifications failed
         */
        template<typename V>
        bool p_replace_value(V&& new_value) {
            value_type& ref = this->nonconst_data();
            if (!(*(this->m_proxy))->replace_called(this->m_iterator, new_value)) { return false; }

            ref = std::forward<V>(new_value);
            return true;
        }
    };

    template<typename Map>
    using mapped_reference_wrapper = replaceable_reference_wrapper<Map, const_mapped_wrapper<Map>>;

    template<typename Map>
    class map_reference_wrapper : public reference_wrapper<Map>
    {
    protected:
        using base = reference_wrapper<Map>;
        using iterator = typename base::iterator;
        using value_type = typename base::value_type;
        using shared_proxy = typename base::shared_proxy;

    public:
        map_reference_wrapper(const shared_proxy& container, iterator it)
          : base(container, it), first(it), second(container, it) {}

        operator const_map_reference_wrapper<Map>() { return const_map_reference_wrapper<Map>(this->m_iterator); }

        key_wrapper<Map>              first;
        mapped_reference_wrapper<Map> second;
    };
}} // namespace cne::notifications

#endif // CNE_REFERENCE_WRAPPER_H
