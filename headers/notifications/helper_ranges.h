#ifndef CNE_HELPER_RANGES_H
#define CNE_HELPER_RANGES_H

#include "helper_iterators.h"

namespace cne {
/**
 * @brief Ranges providing methods begin() and end()
 */
namespace helper_ranges {

    /**
     * @brief Range of elements with the same value
     * Stores size of range and const reference to value
     * Returns random access iterators
     * No constructor, members are public
     *
     * @tparam Size Size type
     * @tparam Difference Difference type
     * @tparam T Value type
     */
    template<typename Size, typename Difference, typename T>
    struct count_ref_range
    {
        using iterator = helper_iterators::count_ref_iterator<Size, Difference, T>;

        iterator begin() const { return iterator{0, m_ref}; }
        iterator end() const { return iterator{m_count, m_ref}; }
        Size     size() const { return m_count; }

        Size     m_count;
        const T& m_ref;
    };

    /**
     * @brief Wraps a container and returns reverse iteratiors
     * Stores reference to the container
     *
     * @tparam Ty Type of container
     */
    template<typename Ty>
    class reverse_range
    {
    public:
        reverse_range(Ty& container) : m_underlying{container} {}
        auto begin() { return m_underlying.rbegin(); }
        auto end() { return m_underlying.rend(); }

    private:
        Ty& m_underlying;
    };

    template<typename Ty>
    reverse_range(Ty&)->reverse_range<Ty>;

} // namespace helper_ranges
} // namespace cne

#endif // CNE_HELPER_RANGES_H
