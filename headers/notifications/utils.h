#ifndef CNE_UTILS_H
#define CNE_UTILS_H

#include <memory>

namespace cne {

/**
 * @brief Custom hashing functors
 */
namespace hash {
    /**
     * @brief Hashing functors for map iterators
     * 
     * @tparam Map Map type
     */
	template<typename Map>
    class map_iterator_hash
    {
    public:
        size_t operator()(const typename Map::const_iterator& it) const {
            return m_hash(&(it->first.data()));
        }

	private:
        std::hash<const typename Map::key_type*> m_hash;
    };

    /**
     * @brief Hashing functor for set iterators
     * 
     * @tparam Set Set type
     */
	template<typename Set>
    class set_iterator_hash
    {
    public:
        size_t operator()(const typename Set::const_iterator& it) const {
            return m_hash(&*it);
        }

    private:
        std::hash<const typename Set::key_type*> m_hash;
    };
}


namespace utils {
    /**
     * @brief Shared pointer to pointer of object
     * 
     * @tparam Ty Type of object to proxy
     */
    template<typename Ty>
    using shared_proxy = std::shared_ptr<Ty*>;

	template<class Iter, class NodeType>
    struct insert_return_type
    {
        Iter     position;
        bool     inserted;
        NodeType node;
    };
} // namespace utils

namespace notifications {
    /**
     * @brief Class representing connection of listener to notification
     * Move only
     * 
     * @tparam Notifier Notifying class
     */
    template<typename Notifier>
    class connection
    {
    private:
        using shared_proxy = typename Notifier::shared_proxy;
        using iterator = typename Notifier::listeners_type::iterator;
        using undo_listener_type = typename Notifier::listeners_type::value_type::second_type;

    public:
        connection(const shared_proxy& proxy, iterator listener) : m_proxy(proxy), m_listener(listener) {}

        connection(connection&&) = default;
        connection& operator=(connection&&) = default;

        bool active() { return m_proxy != nullptr && *m_proxy != nullptr; }

        void disconnect() {
			if (active()) {
                (*m_proxy)->erase(m_listener);
                m_proxy = nullptr;
			}
        }

        void change_undo_listener(undo_listener_type&& undo_listener) {
            if (active()) { m_listener->value()->second = undo_listener; }
        }

    private:
        shared_proxy m_proxy;
        iterator     m_listener;
    };
} // namespace notifications
} // namespace cne

#endif // CNE_UTILS_H
