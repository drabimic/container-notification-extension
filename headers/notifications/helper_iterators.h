#ifndef CNE_HELPER_ITERATORS_H
#define CNE_HELPER_ITERATORS_H

#include <iterator>

namespace cne {
/**
 * @brief Iterators for easier handling of values sent to notifications
 */
namespace helper_iterators {
    /**
     * @brief Bidirectional iterator
     * Iterating calls are not forwarded to wrapped iterator - iterator still points to the same element
     * Other operations are forwarded to the wrapped iterator and the results are returned
     *
     * @tparam Iterator Iterator type to be wrapped
     */
    template<typename Iterator>
    class constant_iterator_wrapper
    {
    public:
        using difference_type = typename Iterator::difference_type;
        using value_type = Iterator;
        using reference = Iterator&;
        using pointer = Iterator*;
        using iterator_category = std::bidirectional_iterator_tag;

        [[nodiscard]] reference operator*() { return m_it; }

        [[nodiscard]] pointer operator->() { return &m_it; }

        constant_iterator_wrapper& operator++() { return *this; }

        constant_iterator_wrapper operator++(int) {
            constant_iterator_wrapper tmp = *this;
            return tmp;
        }

        constant_iterator_wrapper& operator--() { return *this; }

        constant_iterator_wrapper operator--(int) {
            constant_iterator_wrapper tmp = *this;
            return tmp;
        }

        [[nodiscard]] bool operator==(const constant_iterator_wrapper& right) const {
            // check for vector and idx is redundant
            return (m_it == right.m_it);
        }

        [[nodiscard]] bool operator!=(const constant_iterator_wrapper& right) const { return !(*this == right); }

        Iterator m_it;
    };

    /**
     * @brief Adding another level of indirection to iterator
     * Iterating calls are forwarded to the wrapped iterator
     * Dereferencing results in the wrapped iterator
     *
     * @tparam Iterator
     */
    template<typename Iterator>
    class iterator_wrapper
    {
    public:
        using difference_type = typename Iterator::difference_type;
        using value_type = Iterator;
        using reference = Iterator&;
        using pointer = Iterator*;
        using iterator_category = std::bidirectional_iterator_tag;

        [[nodiscard]] reference operator*() { return m_it; }

        [[nodiscard]] pointer operator->() { return &m_it; }

        iterator_wrapper& operator++() {
            // preincrement
            ++m_it;
            return *this;
        }

        iterator_wrapper operator++(int) {
            // postincrement
            iterator_wrapper tmp = *this;
            ++(*this);
            return tmp;
        }

        iterator_wrapper& operator--() {
            --m_it;
            return *this;
        }

        iterator_wrapper operator--(int) {
            iterator_wrapper tmp = *this;
            --(*this);
            return tmp;
        }

        [[nodiscard]] bool operator==(const iterator_wrapper& right) const {
            // check for vector and idx is redundant
            return (m_it == right.m_it);
        }

        [[nodiscard]] bool operator!=(const iterator_wrapper& right) const { return !(*this == right); }

        Iterator m_it;
    };

    /**
     * @brief Random access iterator storing const reference to value and index used for comparison
     * Used to simulate range of elements with the same value
     * No constructor, members are public
     *
     * @tparam Size Size type
     * @tparam Difference Difference type
     * @tparam T Value type
     */
    template<typename Size, typename Difference, typename T>
    struct count_ref_iterator
    {
        using difference_type = Difference;
        using value_type = T;
        using reference = const T&;
        using pointer = const T*;
        using iterator_category = std::random_access_iterator_tag;

        [[nodiscard]] reference operator*() const { return m_ref; }

        [[nodiscard]] pointer operator->() const { return &m_ref; }

        [[nodiscard]] reference operator[](const difference_type offset) const { return (*(*this + offset)); }

        count_ref_iterator& operator++() {
            // preincrement
            ++m_count;
            return *this;
        }

        count_ref_iterator operator++(int) {
            // postincrement
            count_ref_iterator tmp = *this;
            ++(*this);
            return tmp;
        }

        count_ref_iterator& operator--() {
            --m_count;
            return *this;
        }

        count_ref_iterator operator--(int) {
            count_ref_iterator tmp = *this;
            --(*this);
            return tmp;
        }

        count_ref_iterator& operator+=(const difference_type offset) {
            m_count += offset;
            return *this;
        }

        [[nodiscard]] count_ref_iterator operator+(const difference_type offset) const {
            count_ref_iterator tmp = *this;
            return tmp += offset;
        }

        count_ref_iterator& operator-=(const difference_type offset) { return *this += (-offset); }

        [[nodiscard]] count_ref_iterator operator-(const difference_type offset) const {
            count_ref_iterator tmp = *this;
            return tmp -= offset;
        }

        [[nodiscard]] difference_type operator-(const count_ref_iterator right) const {
            return m_count - right.m_count;
        }

        [[nodiscard]] bool operator==(const count_ref_iterator& right) const { return (m_count == right.m_count); }

        [[nodiscard]] bool operator!=(const count_ref_iterator& right) const { return !(*this == right); }

        [[nodiscard]] bool operator<(const count_ref_iterator& right) const { return (m_count < right.m_count); }

        [[nodiscard]] bool operator>(const count_ref_iterator& right) const { return (m_count > right.m_count); }

        [[nodiscard]] bool operator<=(const count_ref_iterator& right) const { return (m_count <= right.m_count); }

        [[nodiscard]] bool operator>=(const count_ref_iterator& right) const { return (m_count >= right.m_count); }

        Size     m_count;
        const T& m_ref;
    };
} // namespace helper_iterators
} // namespace cne

#endif // CNE_HELPER_ITERATORS_H
