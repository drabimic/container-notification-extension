#ifndef CNE_DEQUE_H
#define CNE_DEQUE_H

#include <initializer_list>
#include <iterator>
#include <optional>
#include <utility>

#include "../notifications/helper_iterators.h"
#include "../notifications/helper_ranges.h"
#include "../notifications/iterators.h"
#include "../notifications/notifications.h"
#include "../notifications/reference_wrapper.h"
#include "../notifications/utils.h"

namespace cne {
template<typename WrappedDeque>
class deque;

// for iterators
template<typename WrappedDeque>
class deque_traits
{
public:
    using container = deque<WrappedDeque>;
    using wrapped_type = WrappedDeque;
    using wrapped_iterator = typename wrapped_type::iterator;
    using wrapped_const_iterator = typename wrapped_type::const_iterator;
};

// for class definitions
template<typename WrappedDeque>
class deque_types
{
private:
    using traits = deque_traits<WrappedDeque>;

public:
    using wrapped_type = typename traits::wrapped_type;
    using iterator = iterators::random_access_iterator<traits>;
    using const_iterator = iterators::const_random_access_iterator<traits>;
    using value_type = typename WrappedDeque::value_type;
};

template<typename WrappedDeque>
using deque_notifications = notifications::notifications_base<
  notifications::insert<deque<WrappedDeque>, typename deque_types<WrappedDeque>::value_type,
                        typename deque_types<WrappedDeque>::const_iterator>,
  notifications::erase<deque<WrappedDeque>, typename deque_types<WrappedDeque>::const_iterator>,
  notifications::replace<deque<WrappedDeque>, typename deque_types<WrappedDeque>::const_iterator,
                         typename deque_types<WrappedDeque>::value_type>,
  notifications::value_change<deque<WrappedDeque>, typename deque_types<WrappedDeque>::const_iterator>>;

template<typename WrappedDeque>
class deque : public deque_notifications<WrappedDeque>
{
private:
    using base_class = deque_notifications<WrappedDeque>;
    using types = deque_types<WrappedDeque>;

public:
    using wrapped_type = typename types::wrapped_type;
    using value_type = typename wrapped_type::value_type;
    using allocator_type = typename wrapped_type::allocator_type;
    using size_type = typename wrapped_type::size_type;
    using difference_type = typename wrapped_type::difference_type;

    using iterator = typename types::iterator;
    using const_iterator = typename types::const_iterator;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    using reference = notifications::replaceable_reference_wrapper<deque>;
    using const_reference = notifications::const_reference_wrapper<deque>;
    using pointer = notifications::pointer_proxy<reference>;
    using const_pointer = notifications::pointer_proxy<const_reference>;

    using shared_proxy = utils::shared_proxy<deque>;

private:
    using constant_iterator_wrapper = helper_iterators::constant_iterator_wrapper<const_iterator>;
    using iterator_wrapper = helper_iterators::iterator_wrapper<const_iterator>;
    using count_ref_iterator = helper_iterators::count_ref_iterator<size_type, difference_type, value_type>;
    using count_ref_range = helper_ranges::count_ref_range<size_type, difference_type, value_type>;

public:
    deque() noexcept(noexcept(allocator_type())) { initialize_proxy(); }

    explicit deque(const allocator_type& alloc) noexcept : m_data(alloc) { initialize_proxy(); }

    deque(size_type count, const value_type& value, const allocator_type& alloc = allocator_type())
      : m_data(count, value, alloc) {
        initialize_proxy();
    }

    explicit deque(size_type count, const allocator_type& alloc = allocator_type()) : m_data(count, alloc) {
        initialize_proxy();
    }

    template<typename InputIt>
    deque(InputIt first, InputIt last, const allocator_type& alloc = allocator_type()) : m_data(first, last, alloc) {
        initialize_proxy();
    }

    deque(const deque& other) : base_class(other), m_data(other.m_data) { initialize_proxy(); }

    explicit deque(const wrapped_type& data) : m_data(data) { initialize_proxy(); }

    deque(const deque& other, const allocator_type& alloc) : base_class(other), m_data(other.m_data, alloc) {
        initialize_proxy();
    }

    deque(const wrapped_type& data, const allocator_type& alloc) : m_data(data, alloc) { initialize_proxy(); }

    deque(deque&& other) noexcept : base_class(std::move(other)), m_data(std::move(other.m_data)) {
        initialize_proxy();
    }

    explicit deque(wrapped_type&& data) noexcept : m_data(std::move(data)) { initialize_proxy(); }

    deque(deque&& other, const allocator_type& alloc)
      : base_class(std::move(other)), m_data(std::move(other.m_data), alloc) {
        initialize_proxy();
    }

    deque(wrapped_type&& data, const allocator_type& alloc) : m_data(std::move(data), alloc) { initialize_proxy(); }

    deque(std::initializer_list<value_type> init, const allocator_type& alloc = allocator_type())
      : m_data(init, alloc) {
        initialize_proxy();
    }

    ~deque() { *m_proxy = nullptr; }

    /**
     * @brief Copies content from other to this including listeners
     * Doesn't call any notifications
     *
     * @param other Container to copy from
     * @return deque& Returns *this
     */
    deque& operator=(const deque& other) {
        base_class::operator=(other);
        m_data = other.m_data;

        return *this;
    }

    /**
     * @brief Copies content from data to this
     * Notifies replace and either insert or erase listeners depending on size of data
     *
     * @param data Container to copy from
     * @return deque& Returns *this
     */
    deque& operator=(const wrapped_type& data) {
        if (notify_assign(data)) { m_data = data; }

        return *this;
    }

    /**
     * @brief Moves content from other to this including listeners
     * Doesn't call any notifications
     *
     * @param other Container to move from
     * @return deque& Returns *this
     */
    deque& operator=(deque&& other) noexcept(std::allocator_traits<allocator_type>::is_always_equal::value) {
        base_class::operator=(std::move(other));
        m_data = std::move(m_data);

        return *this;
    }

    /**
     * @brief Moves content from data to this
     * Notifies replace and either insert or erase listeners depending on size of data
     *
     * @param data Container to move from
     * @return deque& Returns *this
     */
    deque& operator=(wrapped_type&& data) {
        if (notify_assign(data)) { m_data = std::move(data); }

        return *this;
    }

    /**
     * @brief Copies content from initializer list to this
     * Notifies replace and either insert or erase listeners depending on size of initializer list
     *
     * @param ilist Initializer list to copy from
     * @return deque& Returns *this
     */
    deque& operator=(std::initializer_list<value_type> ilist) {
        if (notify_assign(ilist)) { m_data = ilist; }

        return *this;
    }

    /**
     * @brief Assigns count copies of value to this
     * Notifies replace and either insert or erase listeners depending on count
     *
     * @param count Number of elements to assign
     * @param value Value to copy
     * @return true Notifications succeeded, assign happened
     * @return false Notifications failed, assign didn't happen
     */
    bool assign(size_type count, const value_type& value) {
        bool res = notify_assign(count_ref_range{count, value});

        if (res) { m_data.assign(count, value); }
        return res;
    }

    /**
     * @brief Assigns values from range [first, last) to this
     * Notifies replace and either insert or erase listeners depending on distance between first and last
     * Internally constructs temporary container and then swaps with current contents
     * Values are constructed from range even if notifications fail
     *
     * @tparam InputIt Type of iterator to values, must satisfy at least InputIterator
     * @param first First iterator of range
     * @param last Last iterator of range
     * @return true Notifications succeeded, assign happened
     * @return false Notifications failed, assign didn't happen
     */
    template<typename InputIt>
    bool assign(InputIt first, InputIt last) {
        wrapped_type tmp(first, last);

        bool res = notify_assign(tmp);
        if (res) { m_data.swap(tmp); }

        return res;
    }

    /**
     * @brief Assigns values from range initializer list to this
     * Notifies replace and either insert or erase listeners depending on distance between first and last
     * Internally constructs temporary container and then swaps with current contents
     *
     * @param ilist Initializer list to copy from
     * @return true Notifications succeeded, assign happened
     * @return false Notifications failed, assign didn't happen
     */
    bool assign(std::initializer_list<value_type> ilist) {
        bool res = notify_assign(ilist);
        if (res) { m_data = ilist; };

        return res;
    }

    allocator_type get_allocator() const { return m_data.get_allocator(); }

    reference at(size_type pos) {
        if (pos >= size()) { throw std::out_of_range("Index out of range"); }

        return *(begin() + pos);
    }

    const_reference at(size_type pos) const {
        if (pos >= size()) { throw std::out_of_range("Index out of range"); }

        return *(begin() + pos);
    }

    reference operator[](size_type pos) { return *(begin() + pos); }

    const_reference operator[](size_type pos) const { return *(begin() + pos); }

    reference front() { return *begin(); }

    const_reference front() const { return *begin(); }

    reference back() { return *(std::prev(end())); }

    const_reference back() const { return *(std::prev(end())); }

    iterator begin() noexcept { return iterator(m_proxy, m_data.begin()); }

    const_iterator begin() const noexcept { return const_iterator(m_data.begin()); }

    const_iterator cbegin() const noexcept { return const_iterator(m_data.cbegin()); }

    iterator end() noexcept { return iterator(m_proxy, m_data.end()); }

    const_iterator end() const noexcept { return const_iterator(m_data.end()); }

    const_iterator cend() const noexcept { return const_iterator(m_data.cend()); }

    reverse_iterator rbegin() noexcept { return reverse_iterator(end()); }

    const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator(end()); }

    const_reverse_iterator crbegin() const noexcept { return const_reverse_iterator(cend()); }

    reverse_iterator rend() noexcept { return reverse_iterator(begin()); }

    const_reverse_iterator rend() const noexcept { return const_reverse_iterator(begin()); }

    const_reverse_iterator crend() const noexcept { return const_reverse_iterator(cbegin()); }

    [[nodiscard]] bool empty() const noexcept { return m_data.empty(); }

    size_type size() const noexcept { return m_data.size(); }

    size_type max_size() const noexcept { return m_data.max_size(); }

    void shrink_to_fit() { m_data.shrink_to_fit(); }

    /**
     * @brief Clears the content of container
     * Notifies erase listeners
     *
     * @return true Notifications succeeded, clear happened
     * @return false Notifications failed, clear didn't happen
     */
    bool clear() {
        if (empty()) { return true; }

        if (!base_class::erase_called(cbegin(), cend())) { return false; }

        m_data.clear();
        return true;
    }

    /**
     * @brief Inserts value before pos
     * Notifies insert listeners
     *
     * @param pos Iterator before which the value will be inserted
     * @param value Value to be copied from
     * @return iterator Iterator to the inserted element or end iterator if notifications failed
     */
    iterator insert(const_iterator pos, const value_type& value) { return p_insert(pos, value); }

    /**
     * @brief Inserts value before pos
     * Notifies insert listeners
     * Doesn't move from value if notifications fail
     *
     * @param pos Iterator before which the value will be inserted
     * @param value Value to be moved from
     * @return iterator Iterator to the inserted element or end iterator if notifications failed
     */
    iterator insert(const_iterator pos, value_type&& value) { return p_insert(pos, std::move(value)); }

    /**
     * @brief Inserts count copies of value before pos
     * Notifies insert listeners
     *
     * @param pos Iterator before which values will be inserted
     * @param count Number of copies of value to be inserted
     * @param value Value to be copied from
     * @return iterator Iterator to first inserted element or end iterator if notifications failed
     */
    iterator insert(const_iterator pos, size_type count, const value_type& value) {
        if (!notify_count_insert(pos, count, value)) { return end(); }

        return iterator(m_proxy, m_data.insert(pos.get_iterator(), count, value));
    }

    /**
     * @brief Inserts values from range [first, last) before pos
     * Notifies insert listeners
     * Internally constructs temporary container and then moves the values to this
     * Values are constructed from range even if notifications fail
     *
     * @tparam InputIt Type of iterator to values, must satisfy at least InputIterator
     * @param pos Iterator before which values will be inserted
     * @param first First iterator of range
     * @param last Last iterator of range
     * @return iterator Iterator to first inserted element or end iterator if notifications failed
     */
    template<typename InputIt>
    iterator insert(const_iterator pos, InputIt first, InputIt last) {
        using move_iterator = std::move_iterator<typename wrapped_type::iterator>;
        wrapped_type tmp;
        tmp.insert(tmp.cend(), first, last);

        return p_insert(pos, move_iterator(tmp.begin()), move_iterator(tmp.end()));
    }

    /**
     * @brief Inserts values from initializer list before pos
     * Notifies insert listeners
     *
     * @param pos Iterator before which values will be inserted
     * @param ilist Initializer list to copy from
     * @return iterator Iterator to first inserted element or end iterator if notifications failed
     */
    iterator insert(const_iterator pos, std::initializer_list<value_type> ilist) {
        return p_insert(pos, ilist.begin(), ilist.end());
    }

    /**
     * @brief Inserts value constructed from args before pos
     * Notifies insert listeners
     * Value is constructed from args even if notifications fail
     *
     * @tparam Args Argument pack
     * @param pos Iterator before which the value will be inserted
     * @param args Arguments to forward to the constructor of the element
     * @return iterator Iterator to inserted element or end iterator if notifications failed
     */
    template<typename... Args>
    iterator emplace(const_iterator pos, Args&&... args) {
        value_type value(std::forward<Args>(args)...);

        return p_insert(pos, std::move(value));
    }

    /**
     * @brief Erases element from container
     * Notifies erase listeners
     *
     * @param pos Iterator to the element to be erased
     * @return std::optional<iterator> Iterator following the erased element or empty optional if notifications failed
     */
    std::optional<iterator> erase(const_iterator pos) {
        if (!base_class::erase_called(pos, std::next(pos))) { return std::nullopt; }

        return iterator(m_proxy, m_data.erase(pos.get_iterator()));
    }

    /**
     * @brief Erases elements from range [first, last)
     * Notifies erase listeners
     *
     * @param first First iterator of range
     * @param last Last iterator of range
     * @return std::optional<iterator> Iterator following the last erased element or empty optional if notifications
     * failed
     */
    std::optional<iterator> erase(const_iterator first, const_iterator last) {
        if (!base_class::erase_called(first, last)) { return std::nullopt; }

        return iterator(m_proxy, m_data.erase(first.get_iterator(), last.get_iterator()));
    }

    /**
     * @brief Inserts value to the front of the container
     * Notifies insert listeners
     *
     * @param value Value to be copied from
     * @return true Notifications succeeded, push_front happened
     * @return false Notifications failed, push_front didn't happen
     */
    bool push_front(const value_type& value) { return p_push_front(value); }

    /**
     * @brief Inserts value to the front of the container
     * Notifies insert listeners
     *
     * @param value Value to be moved from from
     * @return true Notifications succeeded, push_front happened
     * @return false Notifications failed, push_front didn't happen
     */
    bool push_front(value_type&& value) { return p_push_front(std::move(value)); }

    /**
     * @brief Inserts value constructed from args to the front of the container
     * Notifies insert listeners
     * Value is constructed from args even if notifications fail
     * value_type must be MoveInsertable
     *
     * @tparam Args Argument pack
     * @param args Arguments to forward to the constructor of the element
     * @return std::optional<reference> Reference to the inserted element or empty optional if notifications failed
     */
    template<typename... Args>
    std::optional<reference> emplace_front(Args&&... args) {
        if (push_front(value_type(std::forward<Args>(args)...))) {
            return *begin();
        } else {
            return std::nullopt;
        }
    }

    /**
     * @brief Erases first element of the container
     * Notifies erase listeners
     *
     * @return true Notifications succeeded, pop_front happened
     * @return false Notifications failed, pop_front didn't happen
     */
    bool pop_front() {
        if (!base_class::erase_called(cbegin(), std::next(cbegin()))) { return false; }

        m_data.pop_front();
        return true;
    }

    /**
     * @brief Inserts value to the end of the container
     * Notifies insert listeners
     *
     * @param value Value to be copied from
     * @return true Notifications succeeded, push_back happened
     * @return false Notifications failed, push_back didn't happen
     */
    bool push_back(const value_type& value) { return p_push_back(value); }

    /**
     * @brief Inserts value to the end of the container
     * Notifies insert listeners
     *
     * @param value Value to be moved from from
     * @return true Notifications succeeded, push_back happened
     * @return false Notifications failed, push_back didn't happen
     */
    bool push_back(value_type&& value) { return p_push_back(std::move(value)); }

    /**
     * @brief Inserts value constructed from args to the end of the container
     * Notifies insert listeners
     * Value is constructed from args even if notifications fail
     * value_type must be MoveInsertable
     *
     * @tparam Args Argument pack
     * @param args Arguments to forward to the constructor of the element
     * @return std::optional<reference> Reference to the inserted element or empty optional if notifications failed
     */
    template<typename... Args>
    std::optional<reference> emplace_back(Args&&... args) {
        if (push_back(value_type(std::forward<Args>(args)...))) {
            return *std::prev(end());
        } else {
            return std::nullopt;
        }
    }

    /**
     * @brief Erases last element of the container
     * Notifies erase listeners
     *
     * @return true Notifications succeeded, pop_back happened
     * @return false Notifications failed, pop_back didn't happen
     */
    bool pop_back() {
        if (!base_class::erase_called(std::prev(cend()), cend())) { return false; }

        m_data.pop_back();
        return true;
    }

    /**
     * @brief Resizes the contianer to contain count elements
     * If the current size is greater than count, the container is reduced to its first count elements
     * If the current size is less than count, additional default inserted values are appended
     * Notifies erase or insert listeners depending on count
     *
     * @param count New size
     * @return true Notifications succeeded, resize happened
     * @return false Notifications failed, resize didn't happen
     */
    bool resize(size_type count) {
        if (count > size()) {
            // Can't straight up use insert due to type limitations
            if (notify_count_insert(cend(), count - size())) {
                m_data.resize(count);
            } else {
                return false;
            }
        } else if (count < size()) {
            return resize_erase(count);
        }

        return true;
    }

    /**
     * @brief Resizes the contianer to contain count elements
     * If the current size is greater than count, the container is reduced to its first count elements
     * If the current size is less than count, additional copies of value are appended
     * Notifies erase or insert listeners depending on count
     *
     * @param count New size
     * @param value Value to be copied from
     * @return true Notifications succeeded, resize happened
     * @return false Notifications failed, resize didn't happen
     */
    bool resize(size_type count, const value_type& value) {
        if (count > size()) {
            // Can't straight up use insert due to type limitations
            if (notify_count_insert(cend(), count - size(), value)) {
                m_data.resize(count, value);
            } else {
                return false;
            }
        } else if (count < size()) {
            return resize_erase(count, value);
        }

        return true;
    }

    /**
     * @brief Swaps contents of this and other, not including listeners
     * Notifies replace and insert or erase listeners on either container
     *
     * @param other Container to exchange contents with
     * @return true Notifications succeeded, swap happened
     * @return false Notifications failed, swap didn't happen
     */
    bool swap(deque& other) {
        if (!notify_assign(other)) { return false; }

        bool res;
        try {
            res = other.notify_assign(*this);
        } catch (...) {
            notify_undo_assign(other);
            throw;
        }

        if (!res) {
            notify_undo_assign(other);
        } else {
            m_data.swap(other.m_data);

            auto tmp = m_proxy;
            *m_proxy = *other.m_proxy;
            *other.m_proxy = *tmp;
        }

        return res;
    }

    /**
     * @brief Returns reference to the underlying container
     * Useful for operations that should be done without notifications and for compability with older code
     *
     * @return wrapped_type& Reference to the underlying container
     */
    wrapped_type& container() { return m_data; }

    friend bool operator==(const cne::deque<WrappedDeque>& lhs, const cne::deque<WrappedDeque>& rhs) {
        return lhs.m_data == rhs.m_data;
    }

    friend bool operator!=(const cne::deque<WrappedDeque>& lhs, const cne::deque<WrappedDeque>& rhs) {
        return lhs.m_data != rhs.m_data;
    }

    friend bool operator<(const cne::deque<WrappedDeque>& lhs, const cne::deque<WrappedDeque>& rhs) {
        return lhs.m_data < rhs.m_data;
    }

    friend bool operator<=(const cne::deque<WrappedDeque>& lhs, const cne::deque<WrappedDeque>& rhs) {
        return lhs.m_data <= rhs.m_data;
    }

    friend bool operator>(const cne::deque<WrappedDeque>& lhs, const cne::deque<WrappedDeque>& rhs) {
        return lhs.m_data > rhs.m_data;
    }

    friend bool operator>=(const cne::deque<WrappedDeque>& lhs, const cne::deque<WrappedDeque>& rhs) {
        return lhs.m_data >= rhs.m_data;
    }

    friend bool swap(cne::deque<WrappedDeque>& lhs, cne::deque<WrappedDeque>& rhs) { return lhs.swap(rhs); }

private:
    /**
     * @brief Erases last elements in containers to resize it to count
     * Notifies erase listeners
     *
     * @param count New size of container
     * @return true Notifications succeeded, erase happened
     * @return false Notifications failed, erase didn't happen
     */
    bool resize_erase(size_type count) {
        bool res = base_class::erase_called(cbegin() + count, cend());
        if (res) { m_data.resize(count); }

        return res;
    }

    /**
     * @brief Erases last elements in containers to resize it to count
     * Notifies erase listeners
     *
     * @param count New size of container
     * @param value Needed to invoke the right overload of resize
     * @return true Notifications succeeded, erase happened
     * @return false Notifications failed, erase didn't happen
     */
    bool resize_erase(size_type count, const value_type& value) {
        bool res = base_class::erase_called(cbegin() + count, cend());
        if (res) { m_data.resize(count, value); }

        return res;
    }

    /**
     * @brief Perfect forwarding of value for push_back
     * Notifies insert listeners
     *
     * @tparam V Only const value_type& or value_type&&
     * @param value Value to be forwarded
     * @return true Notifications succeeded, push_back happened
     * @return false Notifications failed, push_back didn't happen
     */
    template<typename V>
    bool p_push_back(V&& value) {
        if (!base_class::insert_called(value, cend())) { return false; }

        m_data.push_back(std::forward<V>(value));
        return true;
    }

    /**
     * @brief Perfect forwarding of value for push_front
     * Notifies insert listeners
     *
     * @tparam V Only const value_type& or value_type&&
     * @param value Value to be forwarded
     * @return true Notifications succeeded, push_front happened
     * @return false Notifications failed, push_front didn't happen
     */
    template<typename V>
    bool p_push_front(V&& value) {
        if (!base_class::insert_called(value, cbegin())) { return false; }

        m_data.push_front(std::forward<V>(value));
        return true;
    }

    /**
     * @brief Notifies insertion of count copies of value before pos
     * Notifies insert listeners
     *
     * @param pos Iterator before which the values will be inserted
     * @param count Number of copies
     * @param value Value to be inserted
     * @return true Notifications succeeded
     * @return false Notifications failed
     */
    bool notify_count_insert(const_iterator pos, size_type count, const value_type& value = value_type()) {
        return base_class::insert_called(count_ref_iterator{0, value}, count_ref_iterator{count, value},
                                         constant_iterator_wrapper{pos});
    }

    /**
     * @brief Perfect forwarding of value for insert
     * Notifies insert listeners
     *
     * @tparam V Only const value_type& or value_type&&
     * @param pos Iterator before which the values will be inserted
     * @param value Value to be forwarded
     * @return iterator Iterator to the inserted element or end iterator if notifications failed
     */
    template<typename V>
    iterator p_insert(const_iterator pos, V&& value) {
        if (!base_class::insert_called(value, pos)) { return end(); }

        return iterator(m_proxy, m_data.insert(pos.get_iterator(), std::forward<V>(value)));
    }

    /**
     * @brief Inserts elements from range [first, last) before pos
     * Notifies insert listeners
     *
     * @tparam BidirectionalIt Type of iterator to values, must satisfy at least BidirectionalIterator
     * @param pos Iterator before which the values will be inserted
     * @param first First iterator of range
     * @param last Last iterator of range
     * @return iterator Iterator to the first inserted element or end iterator if notifications failed
     */
    template<typename BidirectionalIt>
    iterator p_insert(const_iterator pos, BidirectionalIt first, BidirectionalIt last) {
        if (!base_class::insert_called(first, last, constant_iterator_wrapper{pos})) { return end(); }

        return iterator(m_proxy, m_data.insert(pos.get_iterator(), first, last));
    }

    /**
     * @brief Notifies replace and either insert or erase listeners depending on size of data
     *
     * @tparam Ty Type of container or range to assign from
     * Iterators of Ty must satisfy RandomAccessIterator
     * Iterators of Ty must be dereferenceable to value_type or type convertible to value_type
     *
     * @param data Container or range of elements to assign
     * @return true Notifications succeeded
     * @return false Notifications failed
     */
    template<typename Ty>
    bool notify_assign(const Ty& data) {
        auto it = cbegin();
        auto data_it = data.begin();
        bool res = true;
        bool is_bigger = size() > data.size();
        bool is_smaller = size() < data.size();

        if (is_bigger) {
            it += data.size();
            data_it += data.size();
        } else {
            it += size();
            data_it += size();
        }

        if (!base_class::replace_called(iterator_wrapper{cbegin()}, data.begin(), data_it)) { return false; }

        try {
            if (is_bigger) {
                res = res && base_class::erase_called(it, cend());
            } else if (is_smaller) {
                res = res && base_class::insert_called(data_it, data.end(), constant_iterator_wrapper{cend()});
            }
        } catch (...) {
            base_class::replace_undo(iterator_wrapper{it}, data_it, data.begin());
            throw;
        }

        if (!res) { base_class::replace_undo(iterator_wrapper{it}, data_it, data.begin()); }
        return res;
    }

    /**
     * @brief Rollbacks notifications called by notify_assign
     * Iterators of Ty must satisfy RandomAccessIterator
     * Iterators of Ty must be dereferenceable to value_type or type convertible to value_type
     *
     * @tparam Ty Type of container or range to assign from
     * @param data Container or range of elements to assign
     */
    template<typename Ty>
    void notify_undo_assign(const Ty& data) {
        auto it = cbegin();
        auto data_it = data.begin();
        bool is_bigger = size() > data.size();
        bool is_smaller = size() < data.size();

        if (is_bigger) {
            it += data.size();
            data_it += data.size();
        } else {
            it += size();
            data_it += size();
        }

        if (is_bigger) {
            base_class::erase_undo(it, cend());
        } else if (is_smaller) {
            base_class::insert_undo(data.end(), data_it, iterator_wrapper{cend()});
        }

        base_class::replace_undo(iterator_wrapper{it}, data_it, data.begin());
    }

    void initialize_proxy() { m_proxy = std::make_shared<deque*>(this); }

    shared_proxy m_proxy;
    wrapped_type m_data;
};
} // namespace cne

#endif // CNE_DEQUE_H
