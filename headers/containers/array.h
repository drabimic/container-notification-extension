#ifndef CNE_ARRAY_H
#define CNE_ARRAY_H

#include <iterator>
#include <optional>
#include <utility>

#include "../notifications/helper_iterators.h"
#include "../notifications/helper_ranges.h"
#include "../notifications/iterators.h"
#include "../notifications/notifications.h"
#include "../notifications/reference_wrapper.h"
#include "../notifications/utils.h"

namespace cne {
template<typename WrappedArray>
class array;

// for iterators
template<typename WrappedArray>
class array_traits
{
public:
    using container = array<WrappedArray>;
    using wrapped_type = WrappedArray;
    using wrapped_iterator = typename wrapped_type::iterator;
    using wrapped_const_iterator = typename wrapped_type::const_iterator;
};

// for class definitions
template<typename WrappedArray>
class array_types
{
private:
    using traits = array_traits<WrappedArray>;

public:
    using wrapped_type = typename traits::wrapped_type;
    using iterator = iterators::random_access_iterator<traits>;
    using const_iterator = iterators::const_random_access_iterator<traits>;
    using value_type = typename WrappedArray::value_type;
};

template<typename WrappedArray>
using array_notifications = notifications::notifications_base<
  notifications::replace<array<WrappedArray>, typename array_types<WrappedArray>::const_iterator,
                         typename array_types<WrappedArray>::value_type>,
  notifications::value_change<array<WrappedArray>, typename array_types<WrappedArray>::const_iterator>>;

template<typename WrappedArray>
class array : public array_notifications<WrappedArray>
{
private:
    using base_class = array_notifications<WrappedArray>;
    using types = array_types<WrappedArray>;

public:
    using wrapped_type = typename types::wrapped_type;
    using value_type = typename wrapped_type::value_type;
    using size_type = typename wrapped_type::size_type;
    using difference_type = typename wrapped_type::difference_type;

    using iterator = typename types::iterator;
    using const_iterator = typename types::const_iterator;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    using reference = notifications::replaceable_reference_wrapper<array>;
    using const_reference = notifications::const_reference_wrapper<array>;
    using pointer = notifications::pointer_proxy<reference>;
    using const_pointer = notifications::pointer_proxy<const_reference>;

    using shared_proxy = utils::shared_proxy<array>;

private:
    using iterator_wrapper = helper_iterators::iterator_wrapper<const_iterator>;
    using count_ref_range = helper_ranges::count_ref_range<size_type, difference_type, value_type>;

public:
    array() { initialize_proxy(); }

    array(const array& other) : base_class(other), m_data(other.m_data) { initialize_proxy(); }

    explicit array(const wrapped_type& data) : m_data(data) { initialize_proxy(); }

    array(array&& other) : base_class(std::move(other)), m_data(std::move(other.m_data)) { initialize_proxy(); }

    explicit array(wrapped_type&& data) : m_data(std::move(data)) { initialize_proxy(); }

    ~array() { *m_proxy = nullptr; }

    /**
     * @brief Copies content from other to this including listeners
     * Doesn't call any notifications
     *
     * @param other Container to copy from
     * @return array& Returns *this
     */
    array& operator=(const array& other) {
        base_class::operator=(other);
        m_data = other.m_data;

        return *this;
    }

    /**
     * @brief Copies content from data to this
     * Notifies replace listeners
     *
     * @param data Container to copy from
     * @return array& Returns *this
     */
    array& operator=(const wrapped_type& data) {
        if (notify_assign(data)) { m_data = data; }

        return *this;
    }

    /**
     * @brief Moves content from other to this including listeners
     * Doesn't call any notifications
     *
     * @param other Container to move from
     * @return array& Returns *this
     */
    array& operator=(array&& other) {
        base_class::operator=(std::move(other));
        m_data = std::move(m_data);

        return *this;
    }

    /**
     * @brief Moves content from data to this
     * Notifies replace listeners
     *
     * @param data Container to move from
     * @return array& Returns *this
     */
    array& operator=(wrapped_type&& data) {
        if (notify_assign(data)) { m_data = std::move(data); }

        return *this;
    }

    reference at(size_type pos) {
        if (pos >= size()) { throw std::out_of_range("Index out of range"); }

        return *(begin() + pos);
    }

    const_reference at(size_type pos) const {
        if (pos >= size()) { throw std::out_of_range("Index out of range"); }

        return *(begin() + pos);
    }

    reference operator[](size_type pos) { return *(begin() + pos); }

    const_reference operator[](size_type pos) const { return *(begin() + pos); }

    reference front() { return *begin(); }

    const_reference front() const { return *begin(); }

    reference back() { return *(end() - 1); }

    const_reference back() const { return *(end() - 1); }

    value_type* data() noexcept { return m_data.data(); }

    const value_type* data() const noexcept { return m_data.data(); }

    iterator begin() noexcept { return iterator(m_proxy, m_data.begin()); }

    const_iterator begin() const noexcept { return const_iterator(m_data.begin()); }

    const_iterator cbegin() const noexcept { return const_iterator(m_data.cbegin()); }

    iterator end() noexcept { return iterator(m_proxy, m_data.end()); }

    const_iterator end() const noexcept { return const_iterator(m_data.end()); }

    const_iterator cend() const noexcept { return const_iterator(m_data.cend()); }

    reverse_iterator rbegin() noexcept { return reverse_iterator(end()); }

    const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator(end()); }

    const_reverse_iterator crbegin() const noexcept { return const_reverse_iterator(cend()); }

    reverse_iterator rend() noexcept { return reverse_iterator(begin()); }

    const_reverse_iterator rend() const noexcept { return const_reverse_iterator(begin()); }

    const_reverse_iterator crend() const noexcept { return const_reverse_iterator(cbegin()); }

    [[nodiscard]] bool empty() const noexcept { return m_data.empty(); }

    size_type size() const noexcept { return m_data.size(); }

    size_type max_size() const noexcept { return m_data.max_size(); }

    /**
     * @brief Assigns the given value to all elements in the container
     * Notifies replace listeners
     *
     * @param value Value to be copied from
     * @return true Notifications succeeded, fill happened
     * @return false Notifications failed, fill didn't happen
     */
    bool fill(const value_type& value) {
        bool res = notify_assign(count_ref_range{size(), value});

        if (res) { m_data.fill(value); }
        return res;
    }

    /**
     * @brief Swaps contents of this and other, not including listeners
     * Notifies replace on both containers
     *
     * @param other Container to exchange contents with
     * @return true Notifications succeeded, swap happened
     * @return false Notifications failed, swap didn't happen
     */
    bool swap(array& other) {
        if (!notify_assign(other)) { return false; }

        bool res;
        try {
            res = other.notify_assign(*this);
        } catch (...) {
            notify_undo_assign(other);
            throw;
        }

        if (!res) {
            notify_undo_assign(other);
        } else {
            m_data.swap(other.m_data);

            auto tmp = m_proxy;
            *m_proxy = *other.m_proxy;
            *other.m_proxy = *tmp;
        }

        return res;
    }

    /**
     * @brief Returns reference to the underlying container
     * Useful for operations that should be done without notifications and for compability with older code
     *
     * @return wrapped_type& Reference to the underlying container
     */
    wrapped_type& container() { return m_data; }

    friend bool operator==(const cne::array<WrappedArray>& lhs, const cne::array<WrappedArray>& rhs) {
        return lhs.m_data == rhs.m_data;
    }

    friend bool operator!=(const cne::array<WrappedArray>& lhs, const cne::array<WrappedArray>& rhs) {
        return lhs.m_data != rhs.m_data;
    }

    friend bool operator<(const cne::array<WrappedArray>& lhs, const cne::array<WrappedArray>& rhs) {
        return lhs.m_data < rhs.m_data;
    }

    friend bool operator<=(const cne::array<WrappedArray>& lhs, const cne::array<WrappedArray>& rhs) {
        return lhs.m_data <= rhs.m_data;
    }

    friend bool operator>(const cne::array<WrappedArray>& lhs, const cne::array<WrappedArray>& rhs) {
        return lhs.m_data > rhs.m_data;
    }

    friend bool operator>=(const cne::array<WrappedArray>& lhs, const cne::array<WrappedArray>& rhs) {
        return lhs.m_data >= rhs.m_data;
    }

    friend bool swap(cne::array<WrappedArray>& lhs, cne::array<WrappedArray>& rhs) { return lhs.swap(rhs); }

private:
    /**
     * @brief Notifies replace listeners
     *
     * @tparam Ty Type of container or range to assign from
     * Iterators of Ty must satisfy BidirectionalIterator
     * Iterators of Ty must be dereferenceable to value_type or type convertible to value_type
     *
     * @param data Container or range of elements to assign
     * @return true Notifications succeeded
     * @return false Notifications failed
     */
    template<typename Ty>
    bool notify_assign(const Ty& data) {
        return base_class::replace_called(iterator_wrapper{begin()}, data.begin(), data.end());
    }

    /**
     * @brief Rollbacks notifications called by notify_assign
     * Iterators of Ty must satisfy BidirectionalIterator
     * Iterators of Ty must be dereferenceable to value_type or type convertible to value_type
     *
     * @tparam Ty Type of container or range to assign from
     * @param data Container or range of elements to assign
     */
    template<typename Ty>
    void notify_undo_assign(const Ty& data) {
        base_class::replace_undo(iterator_wrapper{end()}, data.end(), data.begin());
    }

    void initialize_proxy() { m_proxy = std::make_shared<array*>(this); }

    shared_proxy m_proxy;
    wrapped_type m_data;
};
} // namespace cne

#endif // CNE_ARRAY_H
