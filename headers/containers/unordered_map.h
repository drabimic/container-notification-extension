#ifndef CNE_UNORDERED_MAP_H
#define CNE_UNORDERED_MAP_H

#include <initializer_list>
#include <iterator>
#include <optional>
#include <tuple>
#include <type_traits>
#include <unordered_set>
#include <utility>
#include <vector>

#include "../notifications/helper_ranges.h"
#include "../notifications/iterators.h"
#include "../notifications/notifications.h"
#include "../notifications/reference_wrapper.h"
#include "../notifications/utils.h"

namespace cne {
template<typename WrappedMap>
class unordered_map;

// for iterators
template<typename WrappedMap>
class unordered_map_traits
{
public:
    using container = unordered_map<WrappedMap>;
    using wrapped_type = WrappedMap;
    using wrapped_iterator = typename wrapped_type::iterator;
    using wrapped_const_iterator = typename wrapped_type::const_iterator;
    using wrapped_const_local_iterator = typename wrapped_type::const_local_iterator;
};

// for class definitions
template<typename WrappedMap>
class unordered_map_types
{
public:
    using traits = unordered_map_traits<WrappedMap>;
    using wrapped_type = typename traits::wrapped_type;
    using iterator = iterators::forward_iterator<traits>;
    using const_iterator = iterators::const_forward_iterator<traits>;
    using const_local_iterator = iterators::const_local_fw_iterator<traits>;
    using value_type = typename wrapped_type::value_type;
    using mapped_type = typename wrapped_type::mapped_type;
};

template<typename WrappedMap>
using unordered_map_notifications = notifications::notifications_base<
  notifications::insert<unordered_map<WrappedMap>, typename unordered_map_types<WrappedMap>::value_type>,
  notifications::erase<unordered_map<WrappedMap>, typename unordered_map_types<WrappedMap>::const_iterator>,
  notifications::replace<unordered_map<WrappedMap>, typename unordered_map_types<WrappedMap>::const_iterator,
                         typename unordered_map_types<WrappedMap>::mapped_type>,
  notifications::value_change<unordered_map<WrappedMap>, typename unordered_map_types<WrappedMap>::const_iterator>>;

template<typename WrappedMultimap>
class unordered_multimap;

// for iterators
template<typename WrappedMultimap>
class unordered_multimap_traits
{
public:
    using container = unordered_multimap<WrappedMultimap>;
    using wrapped_type = WrappedMultimap;
    using wrapped_iterator = typename wrapped_type::iterator;
    using wrapped_const_iterator = typename wrapped_type::const_iterator;
    using wrapped_const_local_iterator = typename wrapped_type::const_local_iterator;
};

// for class definitions
template<typename WrappedMultimap>
class unordered_multimap_types
{
public:
    using traits = unordered_multimap_traits<WrappedMultimap>;
    using wrapped_type = typename traits::wrapped_type;
    using iterator = iterators::forward_iterator<traits>;
    using const_iterator = iterators::const_forward_iterator<traits>;
    using const_local_iterator = iterators::const_local_fw_iterator<traits>;
    using value_type = typename wrapped_type::value_type;
    using mapped_type = typename wrapped_type::mapped_type;
};

template<typename WrappedMultimap>
using unordered_multimap_notifications = notifications::notifications_base<
  notifications::insert<unordered_multimap<WrappedMultimap>,
                        typename unordered_multimap_types<WrappedMultimap>::value_type>,
  notifications::erase<unordered_multimap<WrappedMultimap>,
                       typename unordered_multimap_types<WrappedMultimap>::const_iterator>,
  notifications::replace<unordered_multimap<WrappedMultimap>,
                         typename unordered_multimap_types<WrappedMultimap>::const_iterator,
                         typename unordered_multimap_types<WrappedMultimap>::mapped_type>,
  notifications::value_change<unordered_multimap<WrappedMultimap>,
                              typename unordered_multimap_types<WrappedMultimap>::const_iterator>>;

template<typename WrappedMap>
class unordered_map : public unordered_map_notifications<WrappedMap>
{
private:
    using base_class = unordered_map_notifications<WrappedMap>;
    using types = unordered_map_types<WrappedMap>;

    using iterator_hash = hash::map_iterator_hash<unordered_map>;

public:
    using wrapped_type = WrappedMap;
    using key_type = typename wrapped_type::key_type;
    using mapped_type = typename types::mapped_type;
    using value_type = typename types::value_type;
    using size_type = typename wrapped_type::size_type;
    using difference_type = typename wrapped_type::difference_type;

    using hasher = typename wrapped_type::hasher;
    using key_equal = typename wrapped_type::key_equal;

    using allocator_type = typename wrapped_type::allocator_type;

    using iterator = typename types::iterator;
    using const_iterator = typename types::const_iterator;
    // local_iterator - Not implementable with notifications
    using const_local_iterator = typename types::const_local_iterator;

    using reference = notifications::map_reference_wrapper<unordered_map>;
    using const_reference = notifications::const_map_reference_wrapper<unordered_map>;
    using pointer = notifications::pointer_proxy<reference>;
    using const_pointer = notifications::pointer_proxy<const_reference>;

    using mapped_reference = notifications::mapped_reference_wrapper<unordered_map>;
    using const_mapped_reference = notifications::const_mapped_wrapper<unordered_map>;

    using node_type = typename wrapped_type::node_type;
    using insert_return_type = utils::insert_return_type<iterator, node_type>;

    using shared_proxy = utils::shared_proxy<unordered_map>;

    template<typename W>
    friend class unordered_map;

    template<typename W>
    friend class unordered_multimap;

public:
    unordered_map() = default;

    explicit unordered_map(size_type bucket_count, const hasher& hash = hasher(), const key_equal& equal = key_equal(),
                           const allocator_type& alloc = allocator_type())
      : m_data(bucket_count, hash, equal, alloc) {
        initialize_proxy();
    }

    unordered_map(size_type bucket_count, const allocator_type& alloc)
      : m_data(bucket_count, hasher(), key_equal(), alloc) {
        initialize_proxy();
    }

    unordered_map(size_type bucket_count, const hasher& hash, const allocator_type& alloc)
      : m_data(bucket_count, hash, key_equal(), alloc) {
        initialize_proxy();
    }

    explicit unordered_map(const allocator_type& alloc) : m_data(alloc) { initialize_proxy(); }

    template<typename InputIt>
    unordered_map(InputIt first, InputIt last) : m_data(first, last) {
        initialize_proxy();
    }

    template<typename InputIt>
    unordered_map(InputIt first, InputIt last, size_type bucket_count, const hasher& hash = hasher(),
                  const key_equal& equal = key_equal(), const allocator_type& alloc = allocator_type())
      : m_data(first, last, bucket_count, hash, equal, alloc) {
        initialize_proxy();
    }

    template<typename InputIt>
    unordered_map(InputIt first, InputIt last, size_type bucket_count, const allocator_type& alloc)
      : m_data(first, last, bucket_count, hasher(), key_equal(), alloc) {
        initialize_proxy();
    }

    template<typename InputIt>
    unordered_map(InputIt first, InputIt last, size_type bucket_count, const hasher& hash, const allocator_type& alloc)
      : m_data(first, last, bucket_count, hash, key_equal(), alloc) {
        initialize_proxy();
    }

    unordered_map(const unordered_map& other) : base_class(other), m_data(other.m_data) { initialize_proxy(); }

    explicit unordered_map(const wrapped_type& data) : m_data(data) { initialize_proxy(); }

    unordered_map(const unordered_map& other, const allocator_type& alloc)
      : base_class(other), m_data(other.m_data, alloc) {
        initialize_proxy();
    }

    unordered_map(const wrapped_type& data, const allocator_type& alloc) : m_data(data, alloc) { initialize_proxy(); }

    unordered_map(unordered_map&& other) : base_class(std::move(other)), m_data(std::move(other.m_data)) {
        initialize_proxy();
    }

    explicit unordered_map(wrapped_type&& data) : m_data(std::move(data)) { initialize_proxy(); }

    unordered_map(unordered_map&& other, const allocator_type& alloc)
      : base_class(std::move(other)), m_data(std::move(other.m_data), alloc) {
        initialize_proxy();
    }

    unordered_map(wrapped_type&& data, const allocator_type& alloc) : m_data(std::move(data), alloc) {
        initialize_proxy();
    }

    unordered_map(std::initializer_list<value_type> init) : m_data(init) { initialize_proxy(); }

    unordered_map(std::initializer_list<value_type> init, size_type bucket_count, const hasher& hash = hasher(),
                  const key_equal& equal = key_equal(), const allocator_type& alloc = allocator_type())
      : m_data(init, bucket_count, hash, equal, alloc) {
        initialize_proxy();
    }

    unordered_map(std::initializer_list<value_type> init, size_type bucket_count, const allocator_type& alloc)
      : m_data(init, bucket_count, hasher(), key_equal(), alloc) {
        initialize_proxy();
    }

    unordered_map(std::initializer_list<value_type> init, size_type bucket_count, const hasher& hash,
                  const allocator_type& alloc)
      : m_data(init, bucket_count, hash, key_equal(), alloc) {
        initialize_proxy();
    }

    ~unordered_map() = default;

    /**
     * @brief Copies content from other to this including listeners
     * Doesn't call any notifications
     *
     * @param other Container to copy from
     * @return unordered_map& Returns *this
     */
    unordered_map& operator=(const unordered_map& other) {
        base_class::operator=(other);
        m_data = other.m_data;

        return *this;
    }

    /**
     * @brief Copies content from data to this
     * Notifies replace, insert and erase listeners
     *
     * @param data Container to copy from
     * @return unordered_map& Returns *this
     */
    unordered_map& operator=(const wrapped_type& data) {
        p_assign(data);

        return *this;
    }

    /**
     * @brief Moves content from other to this including listeners
     * Doesn't call any notifications
     *
     * @param other Container to move from
     * @return unordered_map& Returns *this
     */
    unordered_map& operator=(unordered_map&& other) noexcept(
      std::allocator_traits<allocator_type>::is_always_equal::value&& std::is_nothrow_move_assignable<hasher>::value&&
                                                                      std::is_nothrow_move_assignable<key_equal>::value) {
        base_class::operator=(std::move(other));
        m_data = std::move(other.m_data);

        return *this;
    }

    /**
     * @brief Moves content from data to this
     * Notifies replace, insert and erase listeners
     *
     * @param data Container to move from
     * @return unordered_map& Returns *this
     */
    unordered_map& operator=(wrapped_type&& data) {
        p_assign(std::move(data));

        return *this;
    }

    /**
     * @brief Copies content from initializer list to this
     * Notifies replace, insert and erase listeners
     * Internally constructs temporary container and then swaps with current contents
     *
     * @param ilist Initializer list to copy from
     * @return unordered_map& Returns *this
     */
    unordered_map& operator=(std::initializer_list<value_type> ilist) {
        wrapped_type tmp(ilist);

        bool res = notify_assign(tmp);
        if (res) { m_data.swap(tmp); }

        return *this;
    }

    allocator_type get_allocator() const { return m_data.get_allocator(); }

    mapped_reference at(const key_type& key) {
        auto it = find(key);
        if (it == end()) { throw std::out_of_range("Invalid key"); }

        return it->second;
    }

    const_mapped_reference at(const key_type& key) const {
        auto it = find(key);
        if (it == end()) { throw std::out_of_range("Invalid key"); }

        return it->second;
    }

    std::optional<reference> operator[](const key_type& key) { return operator_at(key); }

    std::optional<reference> operator[](key_type&& key) { return operator_at(std::move(key)); }

    iterator begin() noexcept { return iterator(m_proxy, m_data.begin()); }

    const_iterator begin() const noexcept { return const_iterator(m_data.begin()); }

    const_iterator cbegin() const noexcept { return const_iterator(m_data.cbegin()); }

    iterator end() noexcept { return iterator(m_proxy, m_data.end()); }

    const_iterator end() const noexcept { return const_iterator(m_data.end()); }

    const_iterator cend() const noexcept { return const_iterator(m_data.cend()); }

    [[nodiscard]] bool empty() const noexcept { return m_data.empty(); }

    size_type size() const noexcept { return m_data.size(); }

    size_type max_size() const noexcept { return m_data.max_size(); }

    /**
     * @brief Clears the content of container
     * Notifies erase listeners
     *
     * @return true Notifications succeeded, clear happened
     * @return false Notifications failed, clear didn't happen
     */
    bool clear() {
        if (empty()) { return true; }

        if (!base_class::erase_called(begin(), end())) { return false; }

        m_data.clear();
        return true;
    }

    /**
     * @brief Inserts element into the container, if the container doesn't already contain
     * value with the same key
     * Notifies insert listeners
     *
     * @param value Value to be copied from
     * @return std::pair<iterator, bool> Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     * and bool set to true if insertion took place
     */
    std::pair<iterator, bool> insert(const value_type& value) { return p_insert(value); }

    /**
     * @brief Inserts element into the container, if the container doesn't already contain
     * value with the same key
     * Notifies insert listeners
     *
     * @param value Value to be moved from
     * @return std::pair<iterator, bool> Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     * and bool set to true if insertion took place
     */
    std::pair<iterator, bool> insert(value_type&& value) { return p_insert(std::move(value)); }

    /**
     * @brief Inserts element into the container, if the container doesn't already contain
     * value with the same key
     * Notifies insert listeners
     *
     * @param value Value to be copied from
     * @return iterator Iterator to inserted element or element that prevented insertion or end iterator if
     * notifications failed
     */
    iterator insert(const_iterator, const value_type& value) { return p_insert(value).first; }

    /**
     * @brief Inserts element into the container, if the container doesn't already contain
     * value with the same key
     * Notifies insert listeners
     *
     * @param value Value to be moved from
     * @return iterator Iterator to inserted element or element that prevented insertion or end iterator if
     * notifications failed
     */
    iterator insert(const_iterator, value_type&& value) { return p_insert(std::move(value)).first; }

    /**
     * @brief Inserts elements from range [first, last) into the container, if the container doesn't already contain
     * value with the same key
     * Notifies insert listeners
     * Internally constructs temporary container and then merges it into this
     * Values are constructed from range even if notifications fail
     *
     * @tparam InputIt Type of iterator to values, must satisfy at least InputIterator
     * @param first First iterator of range
     * @param last Last iterator of range
     * @return true Notifications succeeded, insert happened
     * @return false Notifications failed, insert didn't happen
     */
    template<typename InputIt>
    bool insert(InputIt first, InputIt last) {
        wrapped_type to_merge;
        to_merge.insert(first, last);

        return merge(to_merge);
    }

    /**
     * @brief Inserts elements from initializer list into the container, if the container doesn't already contain
     * value with the same key
     * Notifies insert listeners
     * Internally constructs temporary container and then merges it into this
     *
     * @param ilist Initializer list to copy from
     * @return true Notifications succeeded, insert happened
     * @return false Notifications failed, insert didn't happen
     */
    bool insert(std::initializer_list<value_type> ilist) { return insert(ilist.begin(), ilist.end()); }

    /**
     * @brief Inserts element into the container, if the container doesn't already contain
     * value with the same key
     * Notifies insert listeners
     *
     * @param nh Compatible node handle
     * @return insert_return_type Returns an insert_return_type with the members initialized as follows: if nh is
     * empty, inserted is false, position is end(), and node is empty. Otherwise if the insertion took place,
     * inserted is true, position points to the inserted element, and node is empty. If the insertion failed,
     * inserted is false, node has the previous value of nh, and position points to an element with a key equivalent
     * to nh.key() or to end if notifications failed
     */
    insert_return_type insert(node_type&& nh) {
        if (nh.empty()) { return {end(), false, node_type()}; }
        wrapped_type tmp;
        tmp.insert(std::move(nh));

        auto [position, check] = insert_checker(*(tmp.begin()));
        if (!check) { return {position, check, tmp.extract(tmp.begin())}; }

        return {iterator(m_proxy, m_data.insert(position.get_iterator(), tmp.extract(tmp.begin()))), true, node_type()};
    }

    /**
     * @brief Inserts element into the container, if the container doesn't already contain
     * value with the same key
     * Notifies insert listeners
     *
     * @param nh Compatible node handle
     * @return std::optional<iterator> End iterator if nh was empty, iterator pointing to the inserted element if
     * insertion took place, iterator pointing to an element with a key equivalent to nh.key() if it failed and
     * empty optional if notifications failed
     */
    std::optional<iterator> insert(const_iterator, node_type&& nh) {
        auto res = insert(std::move(nh));

        if (!res.inserted) {
            nh = std::move(res.node);
            if (res.position == end()) { return std::nullopt; }
        }

        return res.position;
    }

    /**
     * @brief If a key equivalent to k already exists in the container, assigns std::forward<M>(obj) to the mapped_type
     * corresponding to the key k. If the key does not exist, inserts the new value
     * Notifies insert or replace listeners
     * Requires mapped_type to be move assignable
     * obj is moved from even if notifications fail
     * Internally calls emplace_hint on insert
     *
     * @tparam M Type to perfect forward into mapped_type
     * @param k Key used both to look up and to insert if not found
     * k is copied on insert
     * @param obj Value to insert or assign
     * @return tuple<iterator, bool, bool> Iterator to inserted or assigned element or end iterator if insert
     * notifications failed
     * First bool is true if insert was attempted, false if assign was attempted
     * Second bool is
     * true if notifications succeeded, false if notifications failed
     */
    template<typename M>
    std::tuple<iterator, bool, bool> insert_or_assign(const key_type& k, M&& obj) {
        return p_insert_or_assign(k, std::forward<M>(obj));
    }

    /**
     * @brief If a key equivalent to k already exists in the container, assigns std::forward<M>(obj) to the mapped_type
     * corresponding to the key k. If the key does not exist, inserts the new value
     * Notifies insert or replace listeners
     * Requires mapped_type to be move assignable
     * obj is moved from even if notifications fail
     * Internally calls emplace_hint on insert
     *
     * @tparam M Type to perfect forward into mapped_type
     * @param k Key used both to look up and to insert if not found
     * k is moved from on insert
     * @param obj Value to insert or assign
     * @return tuple<iterator, bool, bool> Iterator to inserted or assigned element or end iterator if insert
     * notifications failed
     * First bool is true if insert was attempted, false if assign was attempted
     * Second bool is
     * true if notifications succeeded, false if notifications failed
     */
    template<typename M>
    std::tuple<iterator, bool, bool> insert_or_assign(key_type&& k, M&& obj) {
        return p_insert_or_assign(std::move(k), std::forward<M>(obj));
    }

    /**
     * @brief If a key equivalent to k already exists in the container, assigns std::forward<M>(obj) to the mapped_type
     * corresponding to the key k. If the key does not exist, inserts the new value
     * Notifies insert or replace listeners
     * Requires mapped_type to be move assignable
     * obj is moved from even if notifications fail
     * Internally calls emplace_hint on insert
     *
     * @tparam M Type to perfect forward into mapped_type
     * @param k Key used both to look up and to insert if not found
     * k is copied on insert
     * @param obj Value to insert or assign
     * @return pair<iterator, bool> Iterator to inserted or assigned element or end iterator if insert
     * notifications failed bool is true if notifications succeeded, fail if notifications failed
     */
    template<typename M>
    std::pair<iterator, bool> insert_or_assign(const_iterator, const key_type& k, M&& obj) {
        auto res = p_insert_or_assign(k, std::forward<M>(obj));
        return {std::get<0>(res), std::get<2>(res)};
    }

    /**
     * @brief If a key equivalent to k already exists in the container, assigns std::forward<M>(obj) to the mapped_type
     * corresponding to the key k. If the key does not exist, inserts the new value
     * Notifies insert or replace listeners
     * Requires mapped_type to be move assignable
     * obj is moved from even if notifications fail
     * Internally calls emplace_hint on insert
     *
     * @tparam M Type to perfect forward into mapped_type
     * @param k Key used both to look up and to insert if not found
     * k is moved from on insert
     * @param obj Value to insert or assign
     * @return pair<iterator, bool> Iterator to inserted or assigned element or end iterator if insert
     * notifications failed bool is true if notifications succeeded, fail if notifications failed
     */
    template<typename M>
    std::pair<iterator, bool> insert_or_assign(const_iterator, key_type&& k, M&& obj) {
        auto res = p_insert_or_assign(std::move(k), std::forward<M>(obj));
        return {std::get<0>(res), std::get<2>(res)};
    }

    /**
     * @brief Inserts a new element into the container constructed in-place with the given args if there is no
     * element with the key in the container
     * Notifies insert listeners
     * Value is constructed from args even if notifications fail
     * Internally constructs temprorary container, emplaces into it then extracts the node and tries to insert it
     *
     * @tparam Args Argument pack
     * @param args Arguments to forward to the constructor of the element
     * @return std::pair<iterator, bool> Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     * and bool set to true if insertion took place
     */
    template<typename... Args>
    std::pair<iterator, bool> emplace(Args&&... args) {
        wrapped_type tmp;
        tmp.emplace(std::forward<Args>(args)...);

        auto res = insert(tmp.extract(tmp.begin()));
        return {res.position, res.inserted};
    }

    /**
     * @brief Inserts a new element into the container constructed in-place with the given args if there is no
     * element with the key in the container
     * Notifies insert listeners
     * Value is constructed from args even if notifications fail
     * Internally constructs temprorary container, emplaces into it then extracts the node and tries to insert it
     *
     * @tparam Args Argument pack
     * @param args Arguments to forward to the constructor of the element
     * @return iterator Iterator to the newly inserted element, element that prevented the insertion or end iterator
     * if notifications failed
     */
    template<typename... Args>
    iterator emplace_hint(const_iterator hint, Args&&... args) {
        return emplace(std::forward<Args>(args)...).first;
    }

    /**
     * @brief If a key equivalent to k already exists in the container, does nothing. Otherwise, behaves like
     * emplace_hint except that the element is constructed with std::piecewise_construct
     * Notifies insert listeners
     * Construct the value even when notifications fail
     *
     * @tparam Args Argument pack
     * @param k Key used both to look up and to insert if not found
     * @param args Arguments to forward to the constructor of the mapped_type
     * @return pair<iterator, bool> Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     * and bool set to true if insertion took place
     */
    template<typename... Args>
    std::pair<iterator, bool> try_emplace(const key_type& k, Args&&... args) {
        return p_try_emplace(k, std::forward<Args>(args)...);
    }

    /**
     * @brief If a key equivalent to k already exists in the container, does nothing. Otherwise, behaves like
     * emplace_hint except that the element is constructed with std::piecewise_construct
     * Notifies insert listeners
     * Construct the value even when notifications fail
     *
     * @tparam Args Argument pack
     * @param k Key used both to look up and to insert if not found
     * @param args Arguments to forward to the constructor of the mapped_type
     * @return pair<iterator, bool> Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     * and bool set to true if insertion took place
     */
    template<typename... Args>
    std::pair<iterator, bool> try_emplace(key_type&& k, Args&&... args) {
        return p_try_emplace(std::move(k), std::forward<Args>(args)...);
    }

    /**
     * @brief If a key equivalent to k already exists in the container, does nothing. Otherwise, behaves like
     * emplace_hint except that the element is constructed with std::piecewise_construct
     * Notifies insert listeners
     * Construct the value even when notifications fail
     *
     * @tparam Args Argument pack
     * @param k Key used both to look up and to insert if not found
     * @param args Arguments to forward to the constructor of the mapped_type
     * @return pair<iterator, bool> Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     * and bool set to true if insertion took place
     */
    template<typename... Args>
    iterator try_emplace(const_iterator, const key_type& k, Args&&... args) {
        return p_try_emplace(k, std::forward<Args>(args)...).first;
    }

    /**
     * @brief If a key equivalent to k already exists in the container, does nothing. Otherwise, behaves like
     * emplace_hint except that the element is constructed with std::piecewise_construct
     * Notifies insert listeners
     * Construct the value even when notifications fail
     *
     * @tparam Args Argument pack
     * @param k Key used both to look up and to insert if not found
     * @param args Arguments to forward to the constructor of the mapped_type
     * @return pair<iterator, bool> Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     * and bool set to true if insertion took place
     */
    template<typename... Args>
    std::pair<iterator, bool> try_emplace(const_iterator hint, key_type&& k, Args&&... args) {
        return p_try_emplace(std::move(k), std::forward<Args>(args)...).first;
    }

    /**
     * @brief Erases element from container
     * Notifies erase listeners
     *
     * @param pos Iterator to the element to be erased
     * @return std::optional<iterator> Iterator following the erased element or empty optional if notifications
     * failed
     */
    std::optional<iterator> erase(const_iterator pos) {
        if (!base_class::erase_called(pos, std::next(pos))) { return std::nullopt; }

        return iterator(m_proxy, m_data.erase(pos.get_iterator()));
    }

    /**
     * @brief Erases elements from range [first, last)
     * Notifies erase listeners
     *
     * @param first First iterator of range
     * @param last Last iterator of range
     * @return std::optional<iterator> Iterator following the last erased element or empty optional if notifications
     * failed
     */
    std::optional<iterator> erase(const_iterator first, const_iterator last) {
        if (!base_class::erase_called(first, last)) { return std::nullopt; }

        return iterator(m_proxy, m_data.erase(first.get_iterator(), last.get_iterator()));
    }

    /**
     * @brief Erases elements with key equal to key
     * Notifies erase listeners
     *
     * @param key Key value of elements to remove
     * @return size_type Number of elements erased
     */
    size_type erase(const key_type& key) {
        auto it = find(key);
        if (it == end()) { return 0; }

        return erase(it).has_value();
    }

    /**
     * @brief Swaps contents of this and other, not including listeners
     * Notifies replace, insert and erase listeners on both container
     *
     * @param other Container to exchange contents with
     * @return true Notifications succeeded, swap happened
     * @return false Notifications failed, swap didn't happen
     */
    bool swap(unordered_map& other) {
        if (!notify_assign(other)) { return false; }

        bool res;
        try {
            res = other.notify_assign(*this);
        } catch (...) {
            notify_undo_assign(other);
            throw;
        }

        if (!res) {
            notify_undo_assign(other);
        } else {
            m_data.swap(other.m_data);

            auto tmp = m_proxy;
            *m_proxy = *other.m_proxy;
            *other.m_proxy = *tmp;
        }

        return res;
    }

    /**
     * @brief Unlinks the node that contains the element and returns node handle that owns it
     * Notifies erase listeners
     *
     * @param pos Iterator to element
     * @return node_type Node handle that owns unlinked element or empty node if notifications failed
     */
    node_type extract(const_iterator pos) {
        if (!base_class::erase_called(pos, std::next(pos))) { return node_type(); }

        return m_data.extract(pos.get_iterator());
    }

    /**
     * @brief Unlinks the node that contains element with key equal to key and returns node handle that owns it
     * Notifies erase listeners
     *
     * @param x Key to identify node to be extracted
     * @return std::optional<node_type> Node handle that owns unlinked element, empty node if element with key doesn't
     * exist or empty optional if notifications failed
     */
    std::optional<node_type> extract(const key_type& x) {
        iterator it = find(x);
        if (it == end()) { return node_type(); }

        auto res = extract(it);
        if (res.empty()) {
            return std::nullopt;
        } else {
            return std::move(res);
        }
    }

    /**
     * @brief Tries to splice each element from source into this
     * Notifies insert listeners in this and erase listeners in other
     *
     * @tparam W2 wrapped_type of source map
     * @param source Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename W2>
    bool merge(unordered_map<W2>& source) {
        static_assert(
          std::is_same_v<typename unordered_map<W2>::key_type,
                         key_type> && std::is_same_v<typename unordered_map<W2>::allocator_type, allocator_type>);

        if (this == std::addressof(source)) { return true; }

        // processed elements from source
        std::vector<typename unordered_map<W2>::const_iterator> processed;

        auto rollback = [this, &source, &processed]() {
            for (auto& val_it : helper_ranges::reverse_range(processed)) {
                this->insert_undo(*val_it);
                source.erase_undo(val_it, std::next(val_it));
            }
        };

        std::pair<iterator, bool> insert_res;
        for (auto it = source.cbegin(); it != source.cend(); ++it) {
            try {
                insert_res = insert_checker(*it);
            } catch (...) {
                rollback();
                throw;
            }

            if (!insert_res.second && insert_res.first == end()) {
                rollback();
                return false;
            }

            bool erase_res;
            if (insert_res.second) {
                try {
                    erase_res = source.erase_called(it, std::next(it));
                } catch (...) {
                    base_class::insert_undo(*it);
                    rollback();

                    throw;
                }

                if (!erase_res) {
                    base_class::insert_undo(*it);
                    rollback();

                    return false;
                }

                processed.emplace_back(it);
            }
        }

        for (auto& val_it : processed) { m_data.insert(source.extract(val_it)); }
        return true;
    }

    /**
     * @brief Tries to splice each element from source into this
     * Notifies insert listeners in this and erase listeners in other
     *
     * @tparam W2 wrapped_type of source map
     * @param source Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename W2>
    bool merge(unordered_map<W2>&& source) {
        return merge(source);
    }

    /**
     * @brief Tries to splice each element from source into this
     * Notifies insert listeners in this and erase listeners in other
     *
     * @tparam W2 wrapped_type of source multimap
     * @param source Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename W2>
    bool merge(unordered_multimap<W2>& source) {
        static_assert(
          std::is_same_v<typename unordered_multimap<W2>::key_type,
                         key_type> && std::is_same_v<typename unordered_multimap<W2>::allocator_type, allocator_type>);

        if (this == std::addressof(source)) { return true; }

        // processed elements from source
        std::vector<typename unordered_multimap<W2>::const_iterator> processed;

        auto rollback = [this, &source, &processed]() {
            for (auto& val_it : helper_ranges::reverse_range(processed)) {
                this->insert_undo(*val_it);
                source.erase_undo(val_it, std::next(val_it));
            }
        };

        std::pair<iterator, bool> insert_res;
        for (auto it = source.cbegin(); it != source.cend(); ++it) {
            if (std::next(it) != end() && key_eq()(*it, *std::next(it))) { continue; }

            try {
                insert_res = insert_checker(*it);
            } catch (...) {
                rollback();
                throw;
            }

            if (!insert_res.second && insert_res.first == end()) {
                rollback();
                return false;
            }

            bool erase_res;
            if (insert_res.second) {
                try {
                    erase_res = source.erase_called(it, std::next(it));
                } catch (...) {
                    base_class::insert_undo(*it);
                    rollback();

                    throw;
                }

                if (!erase_res) {
                    base_class::insert_undo(*it);
                    rollback();

                    return false;
                }

                processed.emplace_back(it);
            }
        }

        for (auto& val_it : processed) { m_data.insert(source.extract(val_it)); }
        return true;
    }

    /**
     * @brief Tries to splice each element from source into this
     * Notifies insert listeners in this and erase listeners in other
     *
     * @tparam W2 wrapped_type of source multimap
     * @param source Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename W2>
    bool merge(unordered_multimap<W2>&& source) {
        return merge(source);
    }

    /**
     * @brief Tries to splice each element from data into this
     * Notifies insert listeners
     *
     * @param data Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    bool merge(wrapped_type& data) {
        // processed elements from data
        std::vector<typename wrapped_type::const_iterator> processed;

        auto rollback = [this, &processed]() {
            for (auto& val_it : helper_ranges::reverse_range(processed)) { this->insert_undo(*val_it); }
        };

        std::pair<iterator, bool> insert_res;
        for (auto it = data.cbegin(); it != data.cend(); ++it) {
            try {
                insert_res = insert_checker(*it);
            } catch (...) {
                rollback();
                throw;
            }

            if (!insert_res.second && insert_res.first == end()) {
                rollback();
                return false;
            }

            if (insert_res.second) { processed.emplace_back(it); }
        }

        for (auto& val_it : processed) { m_data.insert(data.extract(val_it)); }
        return true;
    }

    /**
     * @brief Tries to splice each element from data into this
     * Notifies insert listeners
     *
     * @param data Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    bool merge(wrapped_type&& data) { return merge(data); }

    size_type count(const key_type& key) const { return m_data.count(key); }

    iterator find(const key_type& key) { return iterator(m_proxy, m_data.find(key)); }

    const_iterator find(const key_type& key) const { return const_iterator(m_data.find(key)); }

    std::pair<iterator, iterator> equal_range(const key_type& key) {
        auto res = m_data.equal_range(key);
        return {iterator(m_proxy, res.first), iterator(m_proxy, res.second)};
    }

    std::pair<const_iterator, const_iterator> equal_range(const key_type& key) const {
        auto res = m_data.equal_range(key);
        return {const_iterator(res.first), const_iterator(res.second)};
    }

    const_local_iterator begin(size_type n) const { return const_local_iterator(m_data.begin(n)); }

    const_local_iterator cbegin(size_type n) const { return const_local_iterator(m_data.cbegin(n)); }

    const_local_iterator end(size_type n) const { return const_local_iterator(m_data.end(n)); }

    const_local_iterator cend(size_type n) const { return const_local_iterator(m_data.cend(n)); }

    size_type bucket_count() const { return m_data.bucket_count(); }

    size_type max_bucket_count() const { return m_data.max_bucket_count(); }

    size_type bucket_size(size_type n) const { return m_data.bucket_size(); }

    size_type bucket(const key_type& key) const { return m_data.bucket(key); }

    float load_factor() const { return m_data.load_factor(); }

    float max_load_factor() const { return m_data.max_load_factor(); }

    void max_load_factor(float ml) { m_data.max_load_factor(ml); }

    void rehash(size_type count) { return m_data.rehash(count); }

    void reserve(size_type count) { return m_data.reserve(count); }

    hasher hash_function() const { return m_data.hash_function(); }

    key_equal key_eq() const { return m_data.key_eq(); }

    /**
     * @brief Returns reference to the underlying container
     * Useful for operations that should be done without notifications and for compability with older code
     *
     * @return wrapped_type& Reference to the underlying container
     */
    wrapped_type& container() { return m_data; }

    friend bool operator==(const unordered_map<WrappedMap>& lhs, const unordered_map<WrappedMap>& rhs) {
        return lhs.m_data == rhs.m_data;
    }

    friend bool operator!=(const unordered_map<WrappedMap>& lhs, const unordered_map<WrappedMap>& rhs) {
        return lhs.m_data != rhs.m_data;
    }

    friend bool operator<(const unordered_map<WrappedMap>& lhs, const unordered_map<WrappedMap>& rhs) {
        return lhs.m_data < rhs.m_data;
    }

    friend bool operator<=(const unordered_map<WrappedMap>& lhs, const unordered_map<WrappedMap>& rhs) {
        return lhs.m_data <= rhs.m_data;
    }

    friend bool operator>(const unordered_map<WrappedMap>& lhs, const unordered_map<WrappedMap>& rhs) {
        return lhs.m_data > rhs.m_data;
    }

    friend bool operator>=(const unordered_map<WrappedMap>& lhs, const unordered_map<WrappedMap>& rhs) {
        return lhs.m_data >= rhs.m_data;
    }

    friend bool swap(unordered_map<WrappedMap>& lhs, unordered_map<WrappedMap>& rhs) { return lhs.swap(rhs); }

private:
    /**
     * @brief Perfect forwarding for operator[]
     * Notifies insert listeners
     *
     * @tparam K const key_type& or key_type&&
     * @param key Key used both to look up and to insert if not found
     */
    template<typename K>
    std::optional<reference> operator_at(K&& key) {
        auto res = try_emplace(std::forward<K>(key));

        if (!res.second && res.first == end()) { return std::nullopt; }

        return res.first->second;
    }

    /**
     * @brief Perfect forwarding for operator=
     * Notifies replace, insert and erase listeners
     *
     * @tparam Ty const wrapped_type& or wrapped_type&&
     * @param data Container to forward to operator=
     */
    template<typename Ty>
    void p_assign(Ty&& data) {
        if (notify_assign(data)) { m_data = std::forward<Ty>(data); }
    }

    /**
     * @brief Notifies replace, insert and erase listeners
     *
     * @tparam Map Map type
     * Iterators of Map must satisfy ForwardIterator
     * Iterators of Map must not be invalidated on erase
     * Iterators of Map must be dereferenceable to value_type or type convertible to value_type
     *
     * @param data Map to assign from
     * @return true Notifications succeeded
     * @return false Notifications failed
     */
    template<typename Map>
    bool notify_assign(const Map& data) {
        std::vector<const_iterator>                                          to_replace;
        std::vector<std::reference_wrapper<const typename Map::mapped_type>> replacing;
        std::vector<std::reference_wrapper<const typename Map::value_type>>  to_insert;
        std::vector<const_iterator>                                          to_erase;
        std::unordered_set<const_iterator, iterator_hash>                    processed;
        bool                                                                 res;

        for (auto it = data.begin(); it != data.end(); ++it) {
            auto pos = find(it->first);
            if (pos != end()) {
                to_replace.push_back(pos);
                replacing.emplace_back(it->second);

                processed.insert(pos);
            } else {
                to_insert.emplace_back(*it);
            }
        }

        if (!base_class::replace_called(to_replace.begin(), replacing.begin(), replacing.end())) { return false; }

        try {
            res = base_class::insert_called(to_insert.begin(), to_insert.end());
        } catch (...) {
            base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());

            throw;
        }

        if (!res) {
            base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());

            return false;
        }

        try {
            for (auto it = begin(); it != end(); ++it) {
                if (processed.count(it) == 0) {
                    res = base_class::erase_called(it, std::next(it));
                    if (!res) { break; }
                    to_erase.push_back(it);
                }
            }
        } catch (...) {
            for (auto it : to_erase) { base_class::erase_undo(it, std::next(it)); }
            base_class::insert_undo(to_insert.end(), to_insert.begin());
            base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());

            throw;
        }

        if (!res) {
            for (auto it : to_erase) { base_class::erase_undo(it, std::next(it)); }
            base_class::insert_undo(to_insert.end(), to_insert.begin());
            base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());
        }

        return res;
    }

    /**
     * @brief Rollbacks notifications called by notify_assign
     *
     * @tparam Map Map type
     * Iterators of Map must satisfy ForwardIterator
     * Iterators of Map must be dereferenceable to value_type or type convertible to value_type
     * @param data Map to assign from
     */
    template<typename Map>
    void notify_undo_assign(const Map& data) {
        std::vector<const_iterator>                                          to_replace;
        std::vector<std::reference_wrapper<const typename Map::mapped_type>> replacing;
        std::vector<std::reference_wrapper<const typename Map::value_type>>  to_insert;
        std::unordered_set<const_iterator, iterator_hash>                    processed;

        for (auto it = data.begin(); it != data.end(); ++it) {
            auto pos = find(it->first);
            if (it != end()) {
                to_replace.push_back(pos);
                replacing.emplace_back(it->second);

                processed.insert(pos);
            } else {
                to_insert.emplace_back(*it);
            }
        }

        for (auto it = begin(); it != end(); ++it) {
            if (processed.count(it) == 0) { base_class::erase_undo(it, std::next(it)); }
        }

        base_class::insert_undo(to_insert.end(), to_insert.begin());
        base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());
    }

    /**
     * @brief If a key equivalent to k already exists in the container, assigns std::forward<M>(obj) to the mapped_type
     * corresponding to the key k. If the key does not exist, inserts the new value
     * Notifies insert or replace listeners
     * Requires mapped_type to be move assignable
     * Value is constructed from obj if notifications fail
     * Internally calls emplace_hint on insert
     *
     * @tparam K Type to perfect forward into key_type
     * @tparam M Type to perfect forward into mapped_type
     * @param k Key used both to look up and to insert if not found
     * @param obj Value to insert or assign
     * @return tuple<iterator, bool, bool> Iterator to inserted or assigned element or end iterator if insert
     * notifications failed First bool is true if insert was attempted, false if assign was attempted Second bool is
     * true if notifications succeeded, false if notifications failed
     */
    template<typename K, typename M>
    std::tuple<iterator, bool, bool> p_insert_or_assign(K&& k, M&& obj) {
        auto pos = find(k);
        if (pos == end()) {
            auto res = emplace(std::forward<K>(k), std::forward<M>(obj)).first;

            return std::make_tuple(res, true, res == end());
        } else {
            mapped_type tmp(std::forward<M>(obj));
            auto        res = pos->second.replace_value(std::move(tmp));

            return std::make_tuple(pos, false, res);
        }
    }

    /**
     * @brief Perfect forwarding for try_emplace
     * Notifies insert listeners
     * Value is constructed from args if notifications fail
     *
     * @tparam K Only const key_type& or key_type&&
     * @tparam Args Argument pack
     * @param k Key used both to look up and to insert if not found
     * @param args Arguments to forward to the constructor of the mapped_type
     * @return pair<iterator, bool> Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     * and bool set to true if insertion took place
     */
    template<typename K, typename... Args>
    std::pair<iterator, bool> p_try_emplace(K&& k, Args&&... args) {
        auto pos = find(k);

        if (pos != end()) { return {pos, false}; }

        wrapped_type tmp;
        tmp.try_emplace(std::forward<K>(k), std::forward<Args>(args)...);
        auto res = insert(tmp.extract(tmp.begin()));

        if (res.has_value()) {
            return {*res, true};
        } else {
            return {end(), false};
        }
    }

    /**
     * @brief Perfect forwarding of value to insert
     * Notifies insert listeners
     *
     * @tparam V Only const value_type& or value_type&&
     * @param value Value to be forwarded
     * @return std::pair<iterator, bool> Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     * and bool set to true if insertion took place
     */
    template<typename V>
    std::pair<iterator, bool> p_insert(V&& value) {
        auto res = insert_checker(value);

        if (res.second) { iterator(m_proxy, m_data.insert(std::forward<V>(value))); }
        return res;
    }

    /**
     * @brief Checks if container contains value with the same key and calls insert listeners
     *
     *
     * @param value Value to be inserted
     * @return std::pair<iterator, bool> If insert can happen returns end iterator and true
     * If insert can't happen due to already existing element, returns iterator to the element and false.
     * If insert can't happen due to callback, returns end iterator and false
     */
    std::pair<iterator, bool> insert_checker(const value_type& value) {
        auto it = find(value.first);
        if (it != end()) { return {it, false}; }

        if (!base_class::insert_called(value)) { return {it, false}; }

        return {it, true};
    }

    void initialize_proxy() { m_proxy = std::make_shared<unordered_map*>(this); }

    shared_proxy m_proxy;
    wrapped_type m_data;
};

template<typename WrappedMultimap>
class unordered_multimap : public unordered_multimap_notifications<WrappedMultimap>
{
private:
    using base_class = unordered_multimap_notifications<WrappedMultimap>;
    using types = unordered_multimap_types<WrappedMultimap>;

    using iterator_hash = hash::map_iterator_hash<unordered_multimap>;

public:
    using wrapped_type = WrappedMultimap;
    using key_type = typename wrapped_type::key_type;
    using mapped_type = typename types::mapped_type;
    using value_type = typename types::value_type;
    using size_type = typename wrapped_type::size_type;
    using difference_type = typename wrapped_type::difference_type;

    using hasher = typename wrapped_type::hasher;
    using key_equal = typename wrapped_type::key_equal;

    using allocator_type = typename wrapped_type::allocator_type;

    using iterator = typename types::iterator;
    using const_iterator = typename types::const_iterator;
    // local_iterator - Not implementable with notifications
    using const_local_iterator = typename types::const_local_iterator;

    using reference = notifications::map_reference_wrapper<unordered_multimap>;
    using const_reference = notifications::const_map_reference_wrapper<unordered_multimap>;
    using pointer = notifications::pointer_proxy<reference>;
    using const_pointer = notifications::pointer_proxy<const_reference>;

    using node_type = typename wrapped_type::node_type;

    using shared_proxy = utils::shared_proxy<unordered_multimap>;

    template<typename W>
    friend class unordered_map;

    template<typename W>
    friend class unordered_multimap;

public:
    unordered_multimap() = default;

    explicit unordered_multimap(size_type bucket_count, const hasher& hash = hasher(),
                                const key_equal& equal = key_equal(), const allocator_type& alloc = allocator_type())
      : m_data(bucket_count, hash, equal, alloc) {
        initialize_proxy();
    }

    unordered_multimap(size_type bucket_count, const allocator_type& alloc)
      : m_data(bucket_count, hasher(), key_equal(), alloc) {
        initialize_proxy();
    }

    unordered_multimap(size_type bucket_count, const hasher& hash, const allocator_type& alloc)
      : m_data(bucket_count, hash, key_equal(), alloc) {
        initialize_proxy();
    }

    explicit unordered_multimap(const allocator_type& alloc) : m_data(alloc) { initialize_proxy(); }

    template<typename InputIt>
    unordered_multimap(InputIt first, InputIt last) : m_data(first, last) {
        initialize_proxy();
    }

    template<typename InputIt>
    unordered_multimap(InputIt first, InputIt last, size_type bucket_count, const hasher& hash = hasher(),
                       const key_equal& equal = key_equal(), const allocator_type& alloc = allocator_type())
      : m_data(first, last, bucket_count, hash, equal, alloc) {
        initialize_proxy();
    }

    template<typename InputIt>
    unordered_multimap(InputIt first, InputIt last, size_type bucket_count, const allocator_type& alloc)
      : m_data(first, last, bucket_count, hasher(), key_equal(), alloc) {
        initialize_proxy();
    }

    template<typename InputIt>
    unordered_multimap(InputIt first, InputIt last, size_type bucket_count, const hasher& hash,
                       const allocator_type& alloc)
      : m_data(first, last, bucket_count, hash, key_equal(), alloc) {
        initialize_proxy();
    }

    unordered_multimap(const unordered_multimap& other) : base_class(other), m_data(other.m_data) {
        initialize_proxy();
    }

    explicit unordered_multimap(const wrapped_type& data) : m_data(data) { initialize_proxy(); }

    unordered_multimap(const unordered_multimap& other, const allocator_type& alloc)
      : base_class(other), m_data(other.m_data, alloc) {
        initialize_proxy();
    }

    unordered_multimap(const wrapped_type& data, const allocator_type& alloc) : m_data(data, alloc) {
        initialize_proxy();
    }

    unordered_multimap(unordered_multimap&& other) : base_class(std::move(other)), m_data(std::move(other.m_data)) {
        initialize_proxy();
    }

    explicit unordered_multimap(wrapped_type&& data) : m_data(std::move(data)) { initialize_proxy(); }

    unordered_multimap(unordered_multimap&& other, const allocator_type& alloc)
      : base_class(std::move(other)), m_data(std::move(other.m_data), alloc) {
        initialize_proxy();
    }

    unordered_multimap(wrapped_type&& data, const allocator_type& alloc) : m_data(std::move(data), alloc) {
        initialize_proxy();
    }

    unordered_multimap(std::initializer_list<value_type> init) : m_data(init) { initialize_proxy(); }

    unordered_multimap(std::initializer_list<value_type> init, size_type bucket_count, const hasher& hash = hasher(),
                       const key_equal& equal = key_equal(), const allocator_type& alloc = allocator_type())
      : m_data(init, bucket_count, hash, equal, alloc) {
        initialize_proxy();
    }

    unordered_multimap(std::initializer_list<value_type> init, size_type bucket_count, const allocator_type& alloc)
      : m_data(init, bucket_count, hasher(), key_equal(), alloc) {
        initialize_proxy();
    }

    unordered_multimap(std::initializer_list<value_type> init, size_type bucket_count, const hasher& hash,
                       const allocator_type& alloc)
      : m_data(init, bucket_count, hash, key_equal(), alloc) {
        initialize_proxy();
    }

    ~unordered_multimap() = default;

    /**
     * @brief Copies content from other to this including listeners
     * Doesn't call any notifications
     *
     * @param other Container to copy from
     * @return unordered_multimap& Returns *this
     */
    unordered_multimap& operator=(const unordered_multimap& other) {
        base_class::operator=(other);
        m_data = other.m_data;

        return *this;
    }

    /**
     * @brief Copies content from data to this
     * Notifies replace, insert and erase listeners
     *
     * @param data Container to copy from
     * @return unordered_multimap& Returns *this
     */
    unordered_multimap& operator=(const wrapped_type& data) {
        p_assign(data);

        return *this;
    }

    /**
     * @brief Moves content from other to this including listeners
     * Doesn't call any notifications
     *
     * @param other Container to move from
     * @return unordered_multimap& Returns *this
     */
    unordered_multimap& operator=(unordered_multimap&& other) noexcept(
      std::allocator_traits<allocator_type>::is_always_equal::value&& std::is_nothrow_move_assignable<hasher>::value&&
                                                                      std::is_nothrow_move_assignable<key_equal>::value) {
        base_class::operator=(std::move(other));
        m_data = std::move(other.m_data);

        return *this;
    }

    /**
     * @brief Moves content from data to this
     * Notifies replace, insert and erase listeners
     *
     * @param data Container to move from
     * @return unordered_multimap& Returns *this
     */
    unordered_multimap& operator=(wrapped_type&& data) {
        p_assign(std::move(data));

        return *this;
    }

    /**
     * @brief Copies content from initializer list to this
     * Notifies replace, insert and erase listeners
     * Internally constructs temporary container and then swaps with current contents
     *
     * @param ilist Initializer list to copy from
     * @return unordered_multimap& Returns *this
     */
    unordered_multimap& operator=(std::initializer_list<value_type> ilist) {
        wrapped_type tmp(ilist);

        bool res = notify_assign(tmp);
        if (res) { m_data.swap(tmp); }

        return *this;
    }

    iterator begin() noexcept { return iterator(m_proxy, m_data.begin()); }

    const_iterator begin() const noexcept { return const_iterator(m_data.begin()); }

    const_iterator cbegin() const noexcept { return const_iterator(m_data.cbegin()); }

    iterator end() noexcept { return iterator(m_proxy, m_data.end()); }

    const_iterator end() const noexcept { return const_iterator(m_data.end()); }

    const_iterator cend() const noexcept { return const_iterator(m_data.cend()); }

    [[nodiscard]] bool empty() const noexcept { return m_data.empty(); }

    size_type size() const noexcept { return m_data.size(); }

    size_type max_size() const noexcept { return m_data.max_size(); }

    /**
     * @brief Clears the content of container
     * Notifies erase listeners
     *
     * @return true Notifications succeeded, clear happened
     * @return false Notifications failed, clear didn't happen
     */
    bool clear() {
        if (empty()) { return true; }

        if (!base_class::erase_called(begin(), end())) { return false; }

        m_data.clear();
        return true;
    }

    /**
     * @brief Inserts element into the container
     * Notifies insert listeners
     *
     * @param value Value to be copied from
     * @return iterator Iterator to inserted element or end iterator if notifications failed
     */
    iterator insert(const value_type& value) { return p_insert(value); }

    /**
     * @brief Inserts element into the container
     * Notifies insert listeners
     *
     * @param value Value to be moved from
     * @return iterator Iterator to inserted element or end iterator if notifications failed
     */
    iterator insert(value_type&& value) { return p_insert(std::move(value)); }

    /**
     * @brief Inserts element into the container
     * Notifies insert listeners
     *
     * @param value Value to be copied from
     * @return iterator Iterator to inserted element or end iterator if notifications failed
     */
    iterator insert(const_iterator, const value_type& value) { return p_insert(value); }

    /**
     * @brief Inserts element into the container
     * Notifies insert listeners
     *
     *  @param value Value to be moved from
     * @return iterator Iterator to inserted element or end iterator if notifications failed
     */
    iterator insert(const_iterator, value_type&& value) { return p_insert(std::move(value)); }

    /**
     * @brief Inserts elements from range [first, last) into the container
     * Notifies insert listeners
     * Internally constructs temporary container and then merges it into this
     * Values are constructed from range even if notifications fail
     *
     * @tparam InputIt Type of iterator to values, must satisfy at least InputIterator
     * @param first First iterator of range
     * @param last Last iterator of range
     * @return true Notifications succeeded, insert happened
     * @return false Notifications failed, insert didn't happen
     */
    template<typename InputIt>
    bool insert(InputIt first, InputIt last) {
        wrapped_type to_merge;
        to_merge.insert(first, last);

        return merge(to_merge);
    }

    /**
     * @brief Inserts elements from initializer list into the container, if the container doesn't already contain
     * value with the same key
     * Notifies insert listeners
     * Internally constructs temporary container and then merges it into this
     *
     * @param ilist Initializer list to copy from
     * @return true Notifications succeeded, insert happened
     * @return false Notifications failed, insert didn't happen
     */
    bool insert(std::initializer_list<value_type> ilist) { return insert(ilist.begin(), ilist.end()); }

    /**
     * @brief Inserts element into the container
     * Notifies insert listeners
     *
     * @param nh Compatible node handle
     * @return std::optional<iterator> Iterator to inserted element, end iterator if nh was empty or empty optional if
     * notifications failed
     */
    std::optional<iterator> insert(node_type&& nh) {
        if (nh.empty()) { return end(); }
        wrapped_type tmp;
        tmp.insert(std::move(nh));

        if (!base_class::insert_called(nh.value())) {
            nh = tmp.extract(tmp.begin());
            return std::nullopt;
        }

        return iterator(m_proxy, m_data.insert(tmp.extract(tmp.begin())));
    }

    /**
     * @brief Inserts element into the container
     * Notifies insert listeners
     *
     * @param nh Compatible node handle
     * @return std::optional<iterator> Iterator to inserted element, end iterator if nh was empty or empty optional if
     * notifications failed
     */
    std::optional<iterator> insert(const_iterator, node_type&& nh) { return insert(std::move(nh)); }

    /**
     * @brief Inserts a new element into the container constructed in-place with the given args
     * Notifies insert listeners
     * Value is constructed from args even if notifications fail
     * Internally constructs temprorary container, emplaces into it then extracts the node and tries to insert it
     *
     * @tparam Args Argument pack
     * @param args Arguments to forward to the constructor of the element
     * @return iterator Iterator to inserted element or end iterator if notifications failed
     */
    template<typename... Args>
    iterator emplace(Args&&... args) {
        wrapped_type tmp;
        tmp.emplace(std::forward<Args>(args)...);

        auto res = insert(tmp.extract(tmp.begin()));
        if (!res.has_value()) {
            return end();
        } else {
            return *res;
        }
    }

    /**
     * @brief Inserts a new element into the container constructed in-place with the given args
     * Notifies insert listeners
     * Value is constructed from args even if notifications fail
     * Internally constructs temprorary container, emplaces into it then extracts the node and tries to insert it
     *
     * @tparam Args Argument pack
     * @param args Arguments to forward to the constructor of the element
     * @return iterator Iterator to inserted element or end iterator if notifications failed
     */
    template<typename... Args>
    iterator emplace_hint(const_iterator, Args&&... args) {
        return emplace(std::forward<Args>(args)...);
    }

    /**
     * @brief Erases element from container
     * Notifies erase listeners
     *
     * @param pos Iterator to the element to be erased
     * @return std::optional<iterator> Iterator following the erased element or empty optional if notifications
     * failed
     */
    std::optional<iterator> erase(const_iterator pos) {
        if (!base_class::erase_called(pos, std::next(pos))) { return std::nullopt; }

        return iterator(m_proxy, m_data.erase(pos.get_iterator()));
    }

    /**
     * @brief Erases elements from range [first, last)
     * Notifies erase listeners
     *
     * @param first First iterator of range
     * @param last Last iterator of range
     * @return std::optional<iterator> Iterator following the last erased element or empty optional if notifications
     * failed
     */
    std::optional<iterator> erase(const_iterator first, const_iterator last) {
        if (!base_class::erase_called(first, last)) { return std::nullopt; }

        return iterator(m_proxy, m_data.erase(first.get_iterator(), last.get_iterator()));
    }

    /**
     * @brief Erases elements with key equal to key
     * Notifies erase listeners
     *
     * @param key Key value of elements to remove
     * @return size_type Number of elements erased
     */
    size_type erase(const key_type& key) {
        auto      range = equal_range(key);
        size_type count = std::distance(range.first, range.second);

        if (!erase(range.first, range.second).has_value()) { count = 0; }
        return count;
    }

    /**
     * @brief Swaps contents of this and other, not including listeners
     * Notifies replace, insert and erase listeners on both container
     *
     * @param other Container to exchange contents with
     * @return true Notifications succeeded, swap happened
     * @return false Notifications failed, swap didn't happen
     */
    bool swap(unordered_multimap& other) {
        if (!notify_assign(other)) { return false; }

        bool res;
        try {
            res = other.notify_assign(*this);
        } catch (...) {
            notify_undo_assign(other);
            throw;
        }

        if (!res) {
            notify_undo_assign(other);
        } else {
            m_data.swap(other.m_data);

            auto tmp = m_proxy;
            *m_proxy = *other.m_proxy;
            *other.m_proxy = *tmp;
        }

        return res;
    }

    /**
     * @brief Unlinks the node that contains the element and returns node handle that owns it
     * Notifies erase listeners
     *
     * @param pos Iterator to element
     * @return node_type Node handle that owns unlinked element or empty node of notifications failed
     */
    node_type extract(const_iterator pos) {
        if (!base_class::erase_called(pos, std::next(pos))) { return node_type(); }

        return m_data.extract(pos.get_iterator());
    }

    /**
     * @brief Unlinks node that contains element with key equal to key and returns node handle that owns it
     * Notifies erase listeners
     *
     * @param x Key to identify node to be extracted
     * @return std::optional<node_type> Node handle that owns unlinked element, empty node if element with key doesn't
     * exist or empty optional if notifications failed
     */
    std::optional<node_type> extract(const key_type& x) {
        iterator it = find(x);
        if (it == end()) { return node_type(); }

        auto res = extract(it);
        if (res.empty()) {
            return std::nullopt;
        } else {
            return std::move(res);
        }
    }

    /**
     * @brief Tries to splice each element from source into this
     * Notifies insert listeners in this and erase listeners in other
     *
     * @tparam W2 wrapped_type of source multimap
     * @param source Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename W2>
    bool merge(unordered_multimap<W2>& source) {
        return p_merge(source);
    }

    /**
     * @brief Tries to splice each element from source into this
     * Notifies insert listeners in this and erase listeners in other
     *
     * @tparam W2 wrapped_type of source multimap
     * @param source Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename W2>
    bool merge(unordered_multimap<W2>&& source) {
        return p_merge(source);
    }

    /**
     * @brief Tries to splice each element from source into this
     * Notifies insert listeners in this and erase listeners in other
     *
     * @tparam W2 wrapped_type of source map
     * @param source Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename W2>
    bool merge(unordered_map<W2>& source) {
        return p_merge(source);
    }

    /**
     * @brief Tries to splice each element from source into this
     * Notifies insert listeners in this and erase listeners in other
     *
     * @tparam W2 wrapped_type of source map
     * @param source Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename W2>
    bool merge(unordered_map<W2>&& source) {
        return p_merge(source);
    }

    /**
     * @brief Tries to splice each element from data into this
     * Notifies insert listeners
     *
     * @param data Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    bool merge(wrapped_type& data) {
        std::vector<typename wrapped_type::const_iterator> processed;

        auto rollback = [this, &processed]() {
            for (auto& val_it : helper_ranges::reverse_range(processed)) { this->insert_undo(*val_it); }
        };

        bool insert_res;
        for (auto it = data.cbegin(); it != data.cend(); ++it) {
            try {
                insert_res = base_class::insert_called(*it);
            } catch (...) {
                rollback();
                throw;
            }

            if (!insert_res) {
                rollback();
                return false;
            }

            processed.emplace_back(it);
        }

        for (auto& val_it : processed) { m_data.insert(data.extract(val_it)); }
        return true;
    }

    /**
     * @brief Tries to splice each element from data into this
     * Notifies insert listeners
     *
     * @param data Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    bool merge(wrapped_type&& data) { return merge(data); }

    size_type count(const key_type& key) const { return m_data.count(key); }

    iterator find(const key_type& key) { return iterator(m_proxy, m_data.find(key)); }

    const_iterator                find(const key_type& key) const { return const_iterator(m_data.find(key)); }
    std::pair<iterator, iterator> equal_range(const key_type& key) {
        auto res = m_data.equal_range(key);
        return {iterator(m_proxy, res.first), iterator(m_proxy, res.second)};
    }

    std::pair<const_iterator, const_iterator> equal_range(const key_type& key) const {
        auto res = m_data.equal_range(key);
        return {const_iterator(res.first), const_iterator(res.second)};
    }

    const_local_iterator begin(size_type n) const { return const_local_iterator(m_data.begin(n)); }

    const_local_iterator cbegin(size_type n) const { return const_local_iterator(m_data.cbegin(n)); }

    const_local_iterator end(size_type n) const { return const_local_iterator(m_data.end(n)); }

    const_local_iterator cend(size_type n) const { return const_local_iterator(m_data.cend(n)); }

    size_type bucket_count() const { return m_data.bucket_count(); }

    size_type max_bucket_count() const { return m_data.max_bucket_count(); }

    size_type bucket_size(size_type n) const { return m_data.bucket_size(); }

    size_type bucket(const key_type& key) const { return m_data.bucket(key); }

    float load_factor() const { return m_data.load_factor(); }

    float max_load_factor() const { return m_data.max_load_factor(); }

    void max_load_factor(float ml) { m_data.max_load_factor(ml); }

    void rehash(size_type count) { return m_data.rehash(count); }

    void reserve(size_type count) { return m_data.reserve(count); }

    hasher hash_function() const { return m_data.hash_function(); }

    key_equal key_eq() const { return m_data.key_eq(); }

    /**
     * @brief Returns reference to the underlying container
     * Useful for operations that should be done without notifications and for compability with older code
     *
     * @return wrapped_type& Reference to the underlying container
     */
    wrapped_type& container() { return m_data; }

    friend bool operator==(const unordered_multimap<WrappedMultimap>& lhs,
                           const unordered_multimap<WrappedMultimap>& rhs) {
        return lhs.m_data == rhs.m_data;
    }

    friend bool operator!=(const unordered_multimap<WrappedMultimap>& lhs,
                           const unordered_multimap<WrappedMultimap>& rhs) {
        return lhs.m_data != rhs.m_data;
    }

    friend bool operator<(const unordered_multimap<WrappedMultimap>& lhs,
                          const unordered_multimap<WrappedMultimap>& rhs) {
        return lhs.m_data < rhs.m_data;
    }

    friend bool operator<=(const unordered_multimap<WrappedMultimap>& lhs,
                           const unordered_multimap<WrappedMultimap>& rhs) {
        return lhs.m_data <= rhs.m_data;
    }

    friend bool operator>(const unordered_multimap<WrappedMultimap>& lhs,
                          const unordered_multimap<WrappedMultimap>& rhs) {
        return lhs.m_data > rhs.m_data;
    }

    friend bool operator>=(const unordered_multimap<WrappedMultimap>& lhs,
                           const unordered_multimap<WrappedMultimap>& rhs) {
        return lhs.m_data >= rhs.m_data;
    }

    friend bool swap(unordered_multimap<WrappedMultimap>& lhs, unordered_multimap<WrappedMultimap>& rhs) {
        return lhs.swap(rhs);
    }

private:
    /**
     * @brief Perfect forwarding for operator=
     * Notifies replace, insert and erase listeners
     *
     * @tparam Ty const wrapped_type& or wrapped_type&&
     * @param data Container to forward to operator=
     */
    template<typename Ty>
    void p_assign(Ty&& data) {
        if (notify_assign(data)) { m_data = std::forward<Ty>(data); }
    }

    /**
     * @brief Notifies replace, insert and erase listeners
     *
     * @tparam Map Map type
     * Iterators of Map must satisfy ForwardIterator
     * Iterators of Map must not be invalidated on erase
     * Iterators of Map must be dereferenceable to value_type or type convertible to value_type
     * @param data Map to assign from
     * @return true Notifications succeeded
     * @return false Notifications failed
     */
    template<typename Map>
    bool notify_assign(const Map& data) {
        std::vector<const_iterator>                                          to_replace;
        std::vector<std::reference_wrapper<const typename Map::mapped_type>> replacing;
        std::vector<std::reference_wrapper<const typename Map::value_type>>  to_insert;
        std::vector<const_iterator>                                          to_erase;
        std::unordered_set<const_iterator, iterator_hash>                    processed;
        bool                                                                 res;

        for (auto it = data.begin(); it != data.end();) {
            auto eq_range = equal_range(it->first);
            auto data_eq_range = data.equal_range(it->first);

            iterator                     this_it;
            typename Map::const_iterator data_it;
            for (this_it = eq_range.first, data_it = data_eq_range.first;
                 this_it != eq_range.second && data_it != data_eq_range.second; ++this_it, ++data_it) {
                to_replace.push_back(this_it);
                replacing.emplace_back(data_it->second);

                processed.insert(this_it);
            }

            while (data_it != data_eq_range.second) {
                to_insert.emplace_back(*data_it);
                ++data_it;
            }

            it = data_it;
        }

        if (!base_class::replace_called(to_replace.begin(), replacing.begin(), replacing.end())) { return false; }

        try {
            res = base_class::insert_called(to_insert.begin(), to_insert.end());
        } catch (...) {
            base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());

            throw;
        }

        if (!res) {
            base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());

            return false;
        }

        try {
            for (auto it = begin(); it != end(); ++it) {
                if (processed.count(it) == 0) {
                    res = base_class::erase_called(it, std::next(it));
                    if (!res) { break; }
                    to_erase.push_back(it);
                }
            }
        } catch (...) {
            for (auto it : to_erase) { base_class::erase_undo(it, std::next(it)); }
            base_class::insert_undo(to_insert.end(), to_insert.begin());
            base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());

            throw;
        }

        if (!res) {
            for (auto it : to_erase) { base_class::erase_undo(it, std::next(it)); }
            base_class::insert_undo(to_insert.end(), to_insert.begin());
            base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());
        }

        return res;
    }

    /**
     * @brief Rollbacks notifications called by notify_assign
     *
     * @tparam Map Map type
     * Iterators of Map must satisfy ForwardIterator
     * Iterators of Map must be dereferenceable to value_type or type convertible to value_type
     * @param data Map to assign from
     */
    template<typename Map>
    void notify_undo_assign(const Map& data) {
        std::vector<const_iterator>                                          to_replace;
        std::vector<std::reference_wrapper<const typename Map::mapped_type>> replacing;
        std::vector<std::reference_wrapper<const typename Map::value_type>>  to_insert;
        std::vector<const_iterator>                                          to_erase;
        std::unordered_set<const_iterator, iterator_hash>                    processed;

        for (auto it = data.begin(); it != data.end();) {
            auto eq_range = equal_range(it->first);
            auto data_eq_range = data.equal_range(it->first);

            iterator                     this_it;
            typename Map::const_iterator data_it;
            for (this_it = eq_range.first, data_it = data_eq_range.first;
                 this_it != eq_range.second && data_it != data_eq_range.second; ++this_it, ++data_it) {
                to_replace.push_back(this_it);
                replacing.emplace_back(data_it->second);

                processed.insert(this_it);
            }

            while (data_it != data_eq_range.second) {
                to_insert.emplace_back(*data_it);
                ++data_it;
            }

            it = data_it;
        }

        for (auto it = begin(); it != end(); ++it) {
            if (processed.count(it) == 0) { base_class::erase_undo(it, std::next(it)); }
        }

        base_class::insert_undo(to_insert.end(), to_insert.begin());
        base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());
    }

    /**
     * @brief Perfect forwarding of value to insert
     * Notifies insert listeners
     *
     * @tparam V Only const value_type& or value_type&&
     * @param value Value to be forwarded
     * @return iterator Iterator to inserted element or end iterator if notifications failed
     */
    template<typename V>
    iterator p_insert(V&& value) {
        if (!base_class::insert_called(value)) { return end(); }

        return m_data.insert(std::forward<V>(value));
    }

    /**
     * @brief Perfect forwarding of maps and multimaps
     * Notifies insert listeners in this and erase listeners in other
     *
     * @tparam Ty unordered_map&, unordered_map&&, unordered_multimap&, unordered_multimap&&
     * @param source Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename Ty>
    bool p_merge(Ty& source) {
        static_assert(std::is_same_v<typename Ty::key_type,
                                     key_type> && std::is_same_v<typename Ty::allocator_type, allocator_type>);

        std::vector<typename Ty::const_iterator> processed;

        auto rollback = [this, &source, &processed]() {
            for (auto& val_it : helper_ranges::reverse_range(processed)) {
                this->insert_undo(*val_it);
                source.erase_undo(val_it, std::next(val_it));
            }
        };

        bool insert_res;
        for (auto it = source.cbegin(); it != source.cend(); ++it) {
            try {
                insert_res = base_class::insert_called(*it);
            } catch (...) {
                rollback();
                throw;
            }

            if (!insert_res) {
                rollback();
                return false;
            }

            bool erase_res;
            try {
                erase_res = source.erase_called(it, std::next(it));
            } catch (...) {
                base_class::insert_undo(*it);
                rollback();

                throw;
            }

            if (!erase_res) {
                base_class::insert_undo(*it);
                rollback();

                return false;
            }

            processed.emplace_back(it);
        }

        for (auto& val_it : processed) { m_data.insert(source.extract(val_it)); }
        return true;
    }

    void initialize_proxy() { m_proxy = std::make_shared<unordered_multimap*>(this); }

    shared_proxy m_proxy;
    wrapped_type m_data;
};
} // namespace cne

#endif // CNE_UNORDERED_MAP_H
