#ifndef CNE_MAP_H
#define CNE_MAP_H

#include <functional>
#include <initializer_list>
#include <iterator>
#include <optional>
#include <tuple>
#include <type_traits>
#include <unordered_set>
#include <utility>
#include <vector>

#include "../notifications/helper_ranges.h"
#include "../notifications/iterators.h"
#include "../notifications/notifications.h"
#include "../notifications/reference_wrapper.h"

namespace cne {
template<typename WrappedMap>
class map;

// for iterators
template<typename WrappedMap>
class map_traits
{
public:
    using container = map<WrappedMap>;
    using wrapped_type = WrappedMap;
    using wrapped_iterator = typename wrapped_type::iterator;
    using wrapped_const_iterator = typename wrapped_type::const_iterator;
};

// for class definitions
template<typename WrappedMap>
class map_types
{
public:
    using traits = map_traits<WrappedMap>;
    using wrapped_type = typename traits::wrapped_type;
    using iterator = iterators::bidirectional_iterator<traits>;
    using const_iterator = iterators::const_bidirectional_iterator<traits>;
    using value_type = typename wrapped_type::value_type;
    using mapped_type = typename wrapped_type::mapped_type;
};

template<typename WrappedMap>
using map_notifications = notifications::notifications_base<
  notifications::insert<map<WrappedMap>, typename map_types<WrappedMap>::value_type,
                        typename map_types<WrappedMap>::const_iterator>,
  notifications::erase<map<WrappedMap>, typename map_types<WrappedMap>::const_iterator>,
  notifications::replace<map<WrappedMap>, typename map_types<WrappedMap>::const_iterator,
                         typename map_types<WrappedMap>::mapped_type>,
  notifications::value_change<map<WrappedMap>, typename map_types<WrappedMap>::const_iterator>>;

template<typename WrappedMultimap>
class multimap;

// for iterators
template<typename WrappedMultimap>
class multimap_traits
{
public:
    using container = multimap<WrappedMultimap>;
    using wrapped_type = WrappedMultimap;
    using wrapped_iterator = typename wrapped_type::iterator;
    using wrapped_const_iterator = typename wrapped_type::const_iterator;
};

// for class definitions
template<typename WrappedMultimap>
class multimap_types
{
public:
    using traits = multimap_traits<WrappedMultimap>;
    using wrapped_type = typename traits::wrapped_type;
    using iterator = iterators::bidirectional_iterator<traits>;
    using const_iterator = iterators::const_bidirectional_iterator<traits>;
    using value_type = typename wrapped_type::value_type;
    using mapped_type = typename wrapped_type::mapped_type;
};

template<typename WrappedMultimap>
using multimap_notifications = notifications::notifications_base<
  notifications::insert<multimap<WrappedMultimap>, typename multimap_types<WrappedMultimap>::value_type,
                        typename multimap_types<WrappedMultimap>::const_iterator>,
  notifications::erase<multimap<WrappedMultimap>, typename multimap_types<WrappedMultimap>::const_iterator>,
  notifications::replace<multimap<WrappedMultimap>, typename multimap_types<WrappedMultimap>::const_iterator,
                         typename multimap_types<WrappedMultimap>::mapped_type>,
  notifications::value_change<multimap<WrappedMultimap>, typename multimap_types<WrappedMultimap>::const_iterator>>;

template<typename WrappedMap>
class map : public map_notifications<WrappedMap>
{
private:
    using base_class = map_notifications<WrappedMap>;
    using types = map_types<WrappedMap>;

    using iterator_hash = hash::map_iterator_hash<map>;

public:
    using wrapped_type = WrappedMap;
    using key_type = typename wrapped_type::key_type;
    using mapped_type = typename types::mapped_type;
    using value_type = typename types::value_type;
    using size_type = typename wrapped_type::size_type;
    using difference_type = typename wrapped_type::difference_type;

    using key_compare = typename wrapped_type::key_compare;

    using allocator_type = typename wrapped_type::allocator_type;

    using iterator = typename types::iterator;
    using const_iterator = typename types::const_iterator;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    using reference = notifications::map_reference_wrapper<map>;
    using const_reference = notifications::const_map_reference_wrapper<map>;
    using pointer = notifications::pointer_proxy<reference>;
    using const_pointer = notifications::pointer_proxy<const_reference>;

    using mapped_reference = notifications::mapped_reference_wrapper<map>;
    using const_mapped_reference = notifications::const_mapped_wrapper<map>;

    using node_type = typename wrapped_type::node_type;
    using insert_return_type = utils::insert_return_type<iterator, node_type>;

    using value_compare = typename wrapped_type::value_compare;

    using shared_proxy = utils::shared_proxy<map>;

    template<typename W>
    friend class map;

    template<typename W>
    friend class multimap;

public:
    map() { initialize_proxy(); }

    explicit map(const key_compare& comp, const allocator_type& alloc = allocator_type()) : m_data(comp, alloc) {
        initialize_proxy();
    }

    explicit map(const allocator_type& alloc) : m_data(alloc) { initialize_proxy(); }

    template<typename InputIt>
    map(InputIt first, InputIt last, const key_compare& comp = key_compare(),
        const allocator_type& alloc = allocator_type())
      : m_data(first, last, comp, alloc) {
        initialize_proxy();
    }

    template<typename InputIt>
    map(InputIt first, InputIt last, const allocator_type& alloc) : m_data(first, last, key_compare(), alloc) {
        initialize_proxy();
    }

    map(const map& other) : base_class(other), m_data(other.m_data) { initialize_proxy(); }

    explicit map(const wrapped_type& data) : m_data(data) { initialize_proxy(); }

    map(const map& other, const allocator_type& alloc) : base_class(other), m_data(other.m_data, alloc) {
        initialize_proxy();
    }

    map(const wrapped_type& data, const allocator_type& alloc) : m_data(data, alloc) { initialize_proxy(); }

    map(map&& other) : base_class(std::move(other)), m_data(std::move(other.m_data)) { initialize_proxy(); }

    explicit map(wrapped_type&& data) : m_data(std::move(data)) { initialize_proxy(); }

    map(map&& other, const allocator_type& alloc)
      : base_class(std::move(other)), m_data(std::move(other.m_data), alloc) {
        initialize_proxy();
    }

    map(wrapped_type&& data, const allocator_type& alloc) : m_data(std::move(data), alloc) { initialize_proxy(); }

    map(std::initializer_list<value_type> init, const key_compare& comp = key_compare(),
        const allocator_type& alloc = allocator_type())
      : m_data(init, comp, alloc) {
        initialize_proxy();
    }

    map(std::initializer_list<value_type> init, const allocator_type& alloc) : map(init, key_compare(), alloc) {
        initialize_proxy();
    }

    ~map() = default;

    /**
     * @brief Copies content from other to this including listeners
     * Doesn't call any notifications
     *
     * @param other Container to copy from
     * @return map& Returns *this
     */
    map& operator=(const map& other) {
        base_class::operator=(other);
        m_data = other.m_data;

        return *this;
    }

    /**
     * @brief Copies content from data to this
     * Notifies replace, insert and erase listeners
     *
     * @param data Container to copy from
     * @return map& Returns *this
     */
    map& operator=(const wrapped_type& data) {
        p_assign(data);
        return *this;
    }

    /**
     * @brief Moves content from other to this including listeners
     * Doesn't call any notifications
     *
     * @param other Container to move from
     * @return map& Returns *this
     */
    map& operator=(map&& other) noexcept(std::allocator_traits<allocator_type>::is_always_equal::value&&
                                           std::is_nothrow_move_assignable<key_compare>::value) {
        base_class::operator=(std::move(other));
        m_data = std::move(other.m_data);

        return *this;
    }

    /**
     * @brief Moves content from data to this
     * Notifies replace, insert and erase listeners
     *
     * @param data Container to move from
     * @return map& Returns *this
     */
    map& operator=(wrapped_type&& data) {
        p_assign(std::move(data));
        return *this;
    }

    /**
     * @brief Copies content from initializer list to this
     * Notifies replace, insert and erase listeners
     * Internally constructs temporary container and then swaps with current contents
     *
     * @param ilist Initializer list to copy from
     * @return map& Returns *this
     */
    map& operator=(std::initializer_list<value_type> ilist) {
        wrapped_type tmp(ilist);

        bool res = notify_assign(tmp);
        if (res) { m_data.swap(tmp); }

        return *this;
    }

    allocator_type get_allocator() const { return m_data.get_allocator(); }

    mapped_reference at(const key_type& key) {
        auto it = find(key);
        if (it == end()) { throw std::out_of_range("Invalid key"); }

        return it->second;
    }

    const_mapped_reference at(const key_type& key) const {
        auto it = find(key);
        if (it == end()) { throw std::out_of_range("Invalid key"); }

        return it->second;
    }

    std::optional<reference> operator[](const key_type& key) { return operator_at(key); }

    std::optional<reference> operator[](key_type&& key) { return operator_at(std::move(key)); }

    iterator begin() noexcept { return iterator(m_proxy, m_data.begin()); }

    const_iterator begin() const noexcept { return const_iterator(m_data.begin()); }

    const_iterator cbegin() const noexcept { return const_iterator(m_data.cbegin()); }

    iterator end() noexcept { return iterator(m_proxy, m_data.end()); }

    const_iterator end() const noexcept { return const_iterator(m_data.end()); }

    const_iterator cend() const noexcept { return const_iterator(m_data.cend()); }

    reverse_iterator rbegin() noexcept { return reverse_iterator(end()); }

    const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator(end()); }

    const_reverse_iterator crbegin() const noexcept { return const_reverse_iterator(cend()); }

    reverse_iterator rend() noexcept { return reverse_iterator(begin()); }

    const_reverse_iterator rend() const noexcept { return const_reverse_iterator(begin()); }

    const_reverse_iterator crend() const noexcept { return const_reverse_iterator(cbegin()); }

    [[nodiscard]] bool empty() const noexcept { return m_data.empty(); }

    size_type size() const noexcept { return m_data.size(); }

    size_type max_size() const noexcept { return m_data.max_size(); }

    /**
     * @brief Clears the content of container
     * Notifies erase listeners
     *
     * @return true Notifications succeeded, clear happened
     * @return false Notifications failed, clear didn't happen
     */
    bool clear() {
        if (empty()) { return true; }

        if (!base_class::erase_called(begin(), end())) { return false; }

        m_data.clear();
        return true;
    }

    /**
     * @brief Inserts element into the container, if the container doesn't already contain
     * value with the same key
     * Notifies insert listeners
     *
     * @param value Value to be copied from
     * @return std::pair<iterator, bool> Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     * and bool set to true if insertion took place
     */
    std::pair<iterator, bool> insert(const value_type& value) { return p_insert(value); }

    /**
     * @brief Inserts element into the container, if the container doesn't already contain
     * value with the same key
     * Notifies insert listeners
     *
     * @param value Value to be moved from
     * @return std::pair<iterator, bool> Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     * and bool set to true if insertion took place
     */
    std::pair<iterator, bool> insert(value_type&& value) { return p_insert(std::move(value)); }

    /**
     * @brief Inserts element into the container, if the container doesn't already contain
     * value with the same key
     * Notifies insert listeners
     *
     * @param hint Iterator to the position before which the new element will be inserted
     * @param value Value to be copied from
     * @return iterator Iterator to inserted element or element that prevented insertion or end iterator if
     * notifications failed
     */
    iterator insert(const_iterator hint, const value_type& value) { return p_insert(hint, value); }

    /**
     * @brief Inserts element into the container, if the container doesn't already contain
     * value with the same key
     *
     * @param hint Iterator to the position before which the new element will be inserted
     * @param value Value to be moved from
     * @return iterator Iterator to inserted element or element that prevented insertion or end iterator if
     * notifications failed
     */
    iterator insert(const_iterator hint, value_type&& value) { return p_insert(hint, std::move(value)); }

    /**
     * @brief Inserts elements from range [first, last) into the container, if the container doesn't already contain
     * value with the same key
     * Notifies insert listeners
     * Internally constructs temporary container and then merges it into this
     * Values are constructed from range even if notifications fail
     *
     * @tparam InputIt Type of iterator to values, must satisfy at least InputIterator
     * @param first First iterator of range
     * @param last Last iterator of range
     * @return true Notifications succeeded, insert happened
     * @return false Notifications failed, insert didn't happen
     */
    template<typename InputIt>
    bool insert(InputIt first, InputIt last) {
        wrapped_type to_merge;
        to_merge.insert(first, last);

        return merge(to_merge);
    }

    /**
     * @brief Inserts elements from initializer list into the container, if the container doesn't already contain
     * value with the same key
     * Notifies insert listeners
     * Internally constructs temporary container and then merges it into this
     *
     * @param ilist Initializer list to copy from
     * @return true Notifications succeeded, insert happened
     * @return false Notifications failed, insert didn't happen
     */
    bool insert(std::initializer_list<value_type> ilist) { return insert(ilist.begin(), ilist.end()); }

    /**
     * @brief Inserts element into the container, if the container doesn't already contain
     * value with the same key
     * Notifies insert listeners
     *
     * @param nh Compatible node handle
     * @return insert_return_type Returns an insert_return_type with the members initialized as follows: if nh is
     * empty, inserted is false, position is end(), and node is empty. Otherwise if the insertion took place,
     * inserted is true, position points to the inserted element, and node is empty. If the insertion failed,
     * inserted is false, node has the previous value of nh, and position points to an element with a key equivalent
     * to nh.key() or to end if notifications failed
     */
    insert_return_type insert(node_type&& nh) {
        if (nh.empty()) { return {end(), false, node_type()}; }
        wrapped_type tmp;
        tmp.insert(std::move(nh));

        auto [position, check] = insert_checker(*(tmp.begin()));
        if (!check) { return {position, check, tmp.extract(tmp.begin())}; }

        return {iterator(m_proxy, m_data.insert(position.get_iterator(), tmp.extract(tmp.begin()))), true, node_type()};
    }

    /**
     * @brief Inserts element into the container, if the container doesn't already contain
     * value with the same key
     * Notifies insert listeners
     *
     * @param hint Iterator to the position before which the new element will be inserted
     * @param nh Compatible node handle
     * @return std::optional<iterator> End iterator if nh was empty, iterator pointing to the inserted element if
     * insertion took place, iterator pointing to an element with a key equivalent to nh.key() if it failed and
     * empty optional if notifications failed
     */
    std::optional<iterator> insert(const_iterator hint, node_type&& nh) {
        if (nh.empty()) { return end(); }
        wrapped_type tmp;
        tmp.insert(std::move(nh));

        auto [position, check] = insert_hint_checker(hint, *(tmp.begin()));
        if (!check) {
            nh = tmp.extract(tmp.begin());
            if (position == end()) {
                return std::nullopt;
            } else {
                return position;
            }
        }

        return iterator(m_proxy, m_data.insert(position.get_iterator(), tmp.extract(tmp.begin())));
    }

    /**
     * @brief If a key equivalent to k already exists in the container, assigns std::forward<M>(obj) to the mapped_type
     * corresponding to the key k. If the key does not exist, inserts the new value
     * Notifies insert or replace listeners
     * Requires mapped_type to be move assignable
     * Value is constructed from obj if notifications fail
     * Internally calls emplace_hint on insert
     *
     * @tparam M Type to perfect forward into mapped_type
     * @param k Key used both to look up and to insert if not found
     * k is copied on insert
     * @param obj Value to insert or assign
     * @return tuple<iterator, bool, bool> Iterator to inserted or assigned element or end iterator if insert
     * notifications failed
     * First bool is true if insert was attempted, false if assign was attempted
     * Second bool is
     * true if notifications succeeded, false if notifications failed
     */
    template<typename M>
    std::tuple<iterator, bool, bool> insert_or_assign(const key_type& k, M&& obj) {
        return p_insert_or_assign(k, std::forward<M>(obj));
    }

    /**
     * @brief If a key equivalent to k already exists in the container, assigns std::forward<M>(obj) to the mapped_type
     * corresponding to the key k. If the key does not exist, inserts the new value
     * Notifies insert or replace listeners
     * Requires mapped_type to be move assignable
     * Value is constructed from obj if notifications fail
     * Internally calls emplace_hint on insert
     *
     * @tparam M Type to perfect forward into mapped_type
     * @param k Key used both to look up and to insert if not found
     * k is moved from on insert
     * @param obj Value to insert or assign
     * @return tuple<iterator, bool, bool> Iterator to inserted or assigned element or end iterator if insert
     * notifications failed
     * First bool is true if insert was attempted, false if assign was attempted
     * Second bool is
     * true if notifications succeeded, false if notifications failed
     */
    template<typename M>
    std::tuple<iterator, bool, bool> insert_or_assign(key_type&& k, M&& obj) {
        return p_insert_or_assign(std::move(k), std::forward<M>(obj));
    }

    /**
     * @brief If a key equivalent to k already exists in the container, assigns std::forward<M>(obj) to the mapped_type
     * corresponding to the key k. If the key does not exist, inserts the new value
     * Notifies insert or replace listeners
     * Requires mapped_type to be move assignable
     * Value is constructed from obj if notifications fail
     * Internally calls emplace_hint on insert
     *
     * @tparam M Type to perfect forward into mapped_type
     * @param hint Iterator to the position before which the new element will be inserted
     * @param k Key used both to look up and to insert if not found
     * k is copied on insert
     * @param obj Value to insert or assign
     * @return pair<iterator, bool> Iterator to inserted or assigned element or end iterator if insert
     * notifications failed bool is true if notifications succeeded, fail if notifications failed
     */
    template<typename M>
    std::pair<iterator, bool> insert_or_assign(const_iterator hint, const key_type& k, M&& obj) {
        return p_insert_or_assign(hint, k, std::forward<M>(obj));
    }

    /**
     * @brief If a key equivalent to k already exists in the container, assigns std::forward<M>(obj) to the mapped_type
     * corresponding to the key k. If the key does not exist, inserts the new value
     * Notifies insert or replace listeners
     * Requires mapped_type to be move assignable
     * Value is constructed from obj if notifications fail
     * Internally calls emplace_hint on insert
     *
     * @tparam M Type to perfect forward into mapped_type
     * @param hint Iterator to the position before which the new element will be inserted
     * @param k Key used both to look up and to insert if not found
     * k is moved from on insert
     * @param obj Value to insert or assign
     * @return pair<iterator, bool> Iterator to inserted or assigned element or end iterator if insert
     * notifications failed bool is true if notifications succeeded, fail if notifications failed
     */
    template<typename M>
    std::pair<iterator, bool> insert_or_assign(const_iterator hint, key_type&& k, M&& obj) {
        return p_insert_or_assign(hint, std::move(k), std::forward<M>(obj));
    }

    /**
     * @brief Inserts a new element into the container constructed in-place with the given args if there is no
     * element with the key in the container
     * Notifies insert listeners
     * Value is constructed from args even if notifications fail
     * Internally constructs temporary container, emplaces into it then extracts the node and tries to insert it
     *
     * @tparam Args Argument pack
     * @param args Arguments to forward to the constructor of the element
     * @return std::pair<iterator, bool> Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     * and bool set to true if insertion took place
     */
    template<typename... Args>
    std::pair<iterator, bool> emplace(Args&&... args) {
        wrapped_type tmp;
        tmp.emplace(std::forward<Args>(args)...);

        auto res = insert(tmp.extract(tmp.begin()));
        return {res.position, res.inserted};
    }

    /**
     * @brief Inserts a new element into the container constructed in-place with the given args if there is no
     * element with the key in the container
     * Notifies insert listeners
     * Value is constructed from args even if notifications fail
     * Internally constructs temporary container, emplaces into it then extracts the node and tries to insert it
     *
     * @tparam Args Argument pack
     * @param hint Iterator to the position before which the new element will be inserted
     * @param args Arguments to forward to the constructor of the element
     * @return iterator Iterator to the newly inserted element, element that prevented the insertion or end iterator
     * if notifications failed
     */
    template<typename... Args>
    iterator emplace_hint(const_iterator hint, Args&&... args) {
        wrapped_type tmp;
        tmp.emplace(std::forward<Args>(args)...);

        auto res = insert(hint, tmp.extract(tmp.begin()));
        if (!res.has_value()) {
            return end();
        } else {
            return *res;
        }
    }

    /**
     * @brief If a key equivalent to k already exists in the container, does nothing. Otherwise, behaves like
     * emplace_hint except that the element is constructed with std::piecewise_construct
     * Notifies insert listeners
     * Value is constructed from obj if notifications fail
     *
     * @tparam Args Argument pack
     * @param k Key used both to look up and to insert if not found
     * @param args Arguments to forward to the constructor of the mapped_type
     * @return pair<iterator, bool> Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     * and bool set to true if insertion took place
     */
    template<typename... Args>
    std::pair<iterator, bool> try_emplace(const key_type& k, Args&&... args) {
        return p_try_emplace(k, std::forward<Args>(args)...);
    }

    /**
     * @brief If a key equivalent to k already exists in the container, does nothing. Otherwise, behaves like
     * emplace_hint except that the element is constructed with std::piecewise_construct
     * Notifies insert listeners
     * Value is constructed from obj if notifications fail
     *
     * @tparam Args Argument pack
     * @param k Key used both to look up and to insert if not found
     * @param args Arguments to forward to the constructor of the mapped_type
     * @return pair<iterator, bool> Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     * and bool set to true if insertion took place
     */
    template<typename... Args>
    std::pair<iterator, bool> try_emplace(key_type&& k, Args&&... args) {
        return p_try_emplace(std::move(k), std::forward<Args>(args)...);
    }

    /**
     * @brief If a key equivalent to k already exists in the container, does nothing. Otherwise, behaves like
     * emplace_hint except that the element is constructed with std::piecewise_construct
     * Notifies insert listeners
     * Value is constructed from obj if notifications fail
     *
     * @tparam Args Argument pack
     * @param hint Iterator to the position before which the new element will be inserted
     * @param k Key used both to look up and to insert if not found
     * @param args Arguments to forward to the constructor of the mapped_type
     * @return pair<iterator, bool> Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     * and bool set to true if insertion took place
     */
    template<typename... Args>
    iterator try_emplace(const_iterator hint, const key_type& k, Args&&... args) {
        return p_try_emplace(hint, k, std::forward<Args>(args)...);
    }

    /**
     * @brief If a key equivalent to k already exists in the container, does nothing. Otherwise, behaves like
     * emplace_hint except that the element is constructed with std::piecewise_construct
     * Notifies insert listeners
     * Value is constructed from obj if notifications fail
     *
     * @tparam Args Argument pack
     * @param hint Iterator to the position before which the new element will be inserted
     * @param k Key used both to look up and to insert if not found
     * @param args Arguments to forward to the constructor of the mapped_type
     * @return pair<iterator, bool> Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     * and bool set to true if insertion took place
     */
    template<typename... Args>
    std::pair<iterator, bool> try_emplace(const_iterator hint, key_type&& k, Args&&... args) {
        return p_try_emplace(hint, std::move(k), std::forward<Args>(args)...);
    }

    /**
     * @brief Erases element from container
     * Notifies erase listeners
     *
     * @param pos Iterator to the element to be erased
     * @return std::optional<iterator> Iterator following the erased element or empty optional if notifications
     * failed
     */
    std::optional<iterator> erase(const_iterator pos) {
        if (!base_class::erase_called(pos, std::next(pos))) { return std::nullopt; }

        return iterator(m_proxy, m_data.erase(pos.get_iterator()));
    }

    /**
     * @brief Erases elements from range [first, last)
     * Notifies erase listeners
     *
     * @param first First iterator of range
     * @param last Last iterator of range
     * @return std::optional<iterator> Iterator following the last erased element or empty optional if notifications
     * failed
     */
    std::optional<iterator> erase(const_iterator first, const_iterator last) {
        if (!base_class::erase_called(first, last)) { return std::nullopt; }

        return iterator(m_proxy, m_data.erase(first.get_iterator(), last.get_iterator()));
    }

    /**
     * @brief Erases elements with key equal to key
     * Notifies erase listeners
     *
     * @param key Key value of elements to remove
     * @return size_type Number of elements erased
     */
    size_type erase(const key_type& key) {
        auto it = find(key);
        if (it == end()) { return 0; }

        return erase(it).has_value();
    }

    /**
     * @brief Swaps contents of this and other, not including listeners
     * Notifies replace, insert and erase listeners on both container
     *
     * @param other Container to exchange contents with
     * @return true Notifications succeeded, swap happened
     * @return false Notifications failed, swap didn't happen
     */
    bool swap(map& other) {
        if (!notify_assign(other)) { return false; }

        bool res;
        try {
            res = other.notify_assign(*this);
        } catch (...) {
            notify_undo_assign(other);

            throw;
        }

        if (!res) {
            notify_undo_assign(other);
        } else {
            m_data.swap(other.m_data);

            auto tmp = m_proxy;
            *m_proxy = *other.m_proxy;
            *other.m_proxy = *tmp;
        }

        return res;
    }

    /**
     * @brief Unlinks the node that contains the element and returns node handle that owns it
     * Notifies erase listeners
     *
     * @param pos Iterator to element
     * @return node_type Node handle that owns unlinked element or empty node if notifications failed
     */
    node_type extract(const_iterator pos) {
        if (!base_class::erase_called(pos, std::next(pos))) { return node_type(); }

        return m_data.extract(pos.get_iterator());
    }

    /**
     * @brief Unlinks the node that contains element with key equal to key and returns node handle that owns it
     * Notifies erase listeners
     *
     * @param x Key to identify node to be extracted
     * @return std::optional<node_type> Node handle that owns unlinked element, empty node if element with key doesn't
     * exist or empty optional if notifications failed
     */
    std::optional<node_type> extract(const key_type& x) {
        iterator it = find(x);
        if (it == end()) { return node_type(); }

        auto res = extract(it);
        if (res.empty()) {
            return std::nullopt;
        } else {
            return std::move(res);
        }
    }

    /**
     * @brief Tries to splice each element from source into this
     * Notifies insert listeners in this and erase listeners in other
     * Requires iterator not to be invalidated on insert
     *
     * @tparam W2 wrapped_type of source map
     * @param source Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename W2>
    bool merge(map<W2>& source) {
        static_assert(std::is_same_v<typename map<W2>::value_type,
                                     value_type> && std::is_same_v<typename map<W2>::allocator_type, allocator_type>);

        if (this == std::addressof(source)) { return true; }

        // mapping insert pos, what to insert it
        std::vector<std::pair<const_iterator, typename map<W2>::const_iterator>> mapping;

        auto rollback = [this, &source, &mapping]() {
            for (auto& [pos, val_it] : helper_ranges::reverse_range(mapping)) {
                this->insert_undo(*val_it, pos);
                source.erase_undo(val_it, std::next(val_it));
            }
        };

        std::pair<iterator, bool> insert_res;
        for (auto it = source.cbegin(); it != source.cend(); ++it) {
            try {
                insert_res = insert_checker(*it);
            } catch (...) {
                rollback();

                throw;
            }

            if (!insert_res.second && insert_res.first == end()) {
                rollback();

                return false;
            }

            bool erase_res;
            if (insert_res.second) {
                try {
                    erase_res = source.erase_called(it, std::next(it));
                } catch (...) {
                    base_class::insert_undo(*it, insert_res.first);
                    rollback();

                    throw;
                }

                if (!erase_res) {
                    base_class::insert_undo(*it, insert_res.first);
                    rollback();

                    return false;
                }

                mapping.emplace_back(insert_res.first, it);
            }
        }

        for (auto& [pos, val_it] : mapping) { m_data.insert(pos.get_iterator(), source.extract(val_it)); }
        return true;
    }

    /**
     * @brief Tries to splice each element from source into this
     * Notifies insert listeners in this and erase listeners in other
     * Requires iterator not to be invalidated on insert
     *
     * @tparam W2 wrapped_type of source map
     * @param source Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename W2>
    bool merge(map<W2>&& source) {
        return merge(source);
    }

    /**
     * @brief Tries to splice each element from source into this
     * Notifies insert listeners in this and erase listeners in other
     * Requires iterator not to be invalidated on insert
     *
     * @tparam W2 wrapped_type of source multimap
     * @param source Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename W2>
    bool merge(multimap<W2>& source) {
        static_assert(
          std::is_same_v<typename multimap<W2>::value_type,
                         value_type> && std::is_same_v<typename multimap<W2>::allocator_type, allocator_type>);

        if (this == std::addressof(source)) { return true; }

        // mapping insert pos, what to insert pos
        std::vector<std::pair<const_iterator, typename multimap<W2>::const_iterator>> mapping;

        auto rollback = [this, &source, &mapping]() {
            for (auto& [pos, val_it] : helper_ranges::reverse_range(mapping)) {
                this->insert_undo(*val_it, pos);
                source.erase_undo(val_it, std::next(val_it));
            }
        };

        std::pair<iterator, bool> insert_res;
        for (auto it = source.cbegin(); it != source.cend(); ++it) {
            if (std::next(it) != end() && equals(it->first, std::next(it)->first)) { continue; }

            try {
                insert_res = insert_checker(*it);
            } catch (...) {
                rollback();

                throw;
            }

            if (!insert_res.second && insert_res.first == end()) {
                rollback();

                return false;
            }

            bool erase_res;
            if (insert_res.second) {
                try {
                    erase_res = source.erase_called(it, std::next(it));
                } catch (...) {
                    base_class::insert_undo(*it, insert_res.first);
                    rollback();

                    throw;
                }

                if (!erase_res) {
                    base_class::insert_undo(*it, insert_res.first);
                    rollback();

                    return false;
                }

                mapping.emplace_back(insert_res.first, it);
            }
        }

        for (auto& [pos, val_it] : mapping) { m_data.insert(pos, source.extract(val_it)); }
        return true;
    }

    /**
     * @brief Tries to splice each element from source into this
     * Notifies insert listeners in this and erase listeners in other
     * Requires iterator not to be invalidated on insert
     *
     * @tparam W2 wrapped_type of source map
     * @param source Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename W2>
    bool merge(multimap<W2>&& source) {
        return merge(source);
    }

    /**
     * @brief Tries to splice each element from data into this
     * Notifies insert listeners
     * Requires iterator not to be invalidated on insert
     *
     * @param data Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    bool merge(wrapped_type& data) {
        // mapping insert pos, what to insert it
        std::vector<std::pair<const_iterator, typename wrapped_type::const_iterator>> mapping;

        auto rollback = [this, &mapping]() {
            for (auto& [pos, val_it] : helper_ranges::reverse_range(mapping)) { this->insert_undo(*val_it, pos); }
        };

        std::pair<iterator, bool> insert_res;
        for (auto it = data.cbegin(); it != data.cend(); ++it) {
            try {
                insert_res = insert_checker(*it);
            } catch (...) {
                rollback();
                throw;
            }

            if (!insert_res.second && insert_res.first == end()) {
                rollback();
                return false;
            }

            if (insert_res.second) { mapping.emplace_back(insert_res.first, it); }
        }

        for (auto& [pos, val_it] : mapping) { m_data.insert(pos.get_iterator(), data.extract(val_it)); }
        return true;
    }

    /**
     * @brief Tries to splice each element from data into this
     * Notifies insert listeners
     * Requires iterator not to be invalidated on insert
     *
     * @param data Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    bool merge(wrapped_type&& data) { return merge(data); }

    size_type count(const key_type& key) const { return m_data.count(key); }

    template<typename K>
    size_type count(const K& x) const {
        return m_data.count(x);
    }

    iterator find(const key_type& key) { return iterator(m_proxy, m_data.find(key)); }

    const_iterator find(const key_type& key) const { return const_iterator(m_data.find(key)); }

    template<typename K>
    iterator find(const K& x) {
        return iterator(m_proxy, m_data.find(x));
    }

    template<typename K>
    const_iterator find(const K& x) const {
        return const_iterator(m_data.find(x));
    }

    std::pair<iterator, iterator> equal_range(const key_type& key) {
        auto res = m_data.equal_range(key);
        return {iterator(m_proxy, res.first), iterator(m_proxy, res.second)};
    }

    std::pair<const_iterator, const_iterator> equal_range(const key_type& key) const {
        auto res = m_data.equal_range(key);
        return {const_iterator(res.first), const_iterator(res.second)};
    }

    template<typename K>
    std::pair<iterator, iterator> equal_range(const K& x) {
        auto res = m_data.equal_range(x);
        return {iterator(m_proxy, res.first), iterator(m_proxy, res.second)};
    }

    template<typename K>
    std::pair<const_iterator, const_iterator> equal_range(const K& x) const {
        auto res = m_data.equal_range(x);
        return {const_iterator(res.first), const_iterator(res.second)};
    }

    iterator lower_bound(const key_type& key) { return iterator(m_proxy, m_data.lower_bound(key)); }

    const_iterator lower_bound(const key_type& key) const { return const_iterator(m_data.lower_bound(key)); }

    template<typename K>
    iterator lower_bound(const K& x) {
        return iterator(m_proxy, m_data.lower_bound(x));
    }

    template<typename K>
    const_iterator lower_bound(const K& x) const {
        return const_iterator(m_data.lower_bound(x));
    }

    iterator upper_bound(const key_type& key) { return iterator(m_proxy, m_data.upper_bound(key)); }

    const_iterator upper_bound(const key_type& key) const { return const_iterator(m_data.upper_bound(key)); }

    template<typename K>
    iterator upper_bound(const K& x) {
        return iterator(m_proxy, m_data.upper_bound(x));
    }

    template<typename K>
    const_iterator upper_bound(const K& x) const {
        return const_iterator(m_data.upper_bound(x));
    }

    key_compare key_comp() const { return m_data.key_comp(); }

    value_compare value_comp() const { return m_data.value_comp(); }

    /**
     * @brief Returns reference to the underlying container
     * Useful for operations that should be done without notifications and for compability with older code
     *
     * @return wrapped_type& Reference to the underlying container
     */
    wrapped_type& container() { return m_data; }

    friend bool operator==(const map<WrappedMap>& lhs, const map<WrappedMap>& rhs) { return lhs.m_data == rhs.m_data; }

    friend bool operator!=(const map<WrappedMap>& lhs, const map<WrappedMap>& rhs) { return lhs.m_data != rhs.m_data; }

    friend bool operator<(const map<WrappedMap>& lhs, const map<WrappedMap>& rhs) { return lhs.m_data < rhs.m_data; }

    friend bool operator<=(const map<WrappedMap>& lhs, const map<WrappedMap>& rhs) { return lhs.m_data <= rhs.m_data; }

    friend bool operator>(const map<WrappedMap>& lhs, const map<WrappedMap>& rhs) { return lhs.m_data > rhs.m_data; }

    friend bool operator>=(const map<WrappedMap>& lhs, const map<WrappedMap>& rhs) { return lhs.m_data >= rhs.m_data; }

    friend bool swap(map<WrappedMap>& lhs, map<WrappedMap>& rhs) { return lhs.swap(rhs); }

private:
    iterator non_const_iterator(const_iterator it) {
        return iterator(m_proxy, m_data.erase(it.get_iterator(), it.get_iterator()));
    }

    /**
     * @brief Perfect forwarding for operator[]
     * Notifies insert listeners
     *
     * @tparam K const key_type& or key_type&&
     * @param key Key used both to look up and to insert if not found
     */
    template<typename K>
    std::optional<reference> operator_at(K&& key) {
        auto res = try_emplace(std::forward<K>(key));

        if (!res.second && res.first == end()) { return std::nullopt; }

        return res.first->second;
    }

    /**
     * @brief Perfect forwarding for operator=
     * Notifies replace, insert and erase listeners
     *
     * @tparam Ty const wrapped_type& or wrapped_type&&
     * @param data Container to forward to operator=
     */
    template<typename Ty>
    void p_assign(Ty&& data) {
        if (notify_assign(data)) { m_data = std::forward<Ty>(data); }
    }

    /**
     * @brief Notifies replace, insert and erase listeners
     *
     * @tparam Map Map type
     * Iterators of Map must satisfy ForwardIterator
     * Iterators of Map must not be invalidated on erase
     * Iterators of Map must be dereferenceable to value_type or type convertible to value_type
     *
     * @param data Map to assign from
     * @return true Notifications succeeded
     * @return false Notifications failed
     */
    template<typename Map>
    bool notify_assign(const Map& data) {
        std::vector<const_iterator>                                          to_replace;
        std::vector<std::reference_wrapper<const typename Map::mapped_type>> replacing;
        std::vector<const_iterator>                                          where_insert;
        std::vector<std::reference_wrapper<const typename Map::value_type>>  to_insert;
        std::vector<const_iterator>                                          to_erase;
        std::unordered_set<const_iterator, iterator_hash>                    processed;
        bool                                                                 res;

        for (auto it = data.begin(); it != data.end(); ++it) {
            auto pos = lower_bound(it->first);
            if (pos != end() && equals(pos->first, it->first)) {
                to_replace.push_back(pos);
                replacing.emplace_back(it->second);

                processed.insert(pos);
            } else {
                where_insert.push_back(pos);
                to_insert.emplace_back(*it);
            }
        }

        if (!base_class::replace_called(to_replace.begin(), replacing.begin(), replacing.end())) { return false; }

        try {
            res = base_class::insert_called(to_insert.begin(), to_insert.end(), where_insert.begin());
        } catch (...) {
            base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());

            throw;
        }

        if (!res) {
            base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());

            return false;
        }

        try {
            for (auto it = begin(); it != end(); ++it) {
                if (processed.count(it) == 0) {
                    res = base_class::erase_called(it, std::next(it));
                    if (!res) { break; }
                    to_erase.push_back(it);
                }
            }
        } catch (...) {
            for (auto it : to_erase) { base_class::erase_undo(it, std::next(it)); }
            base_class::insert_undo(to_insert.end(), to_insert.begin(), where_insert.end());
            base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());

            throw;
        }

        if (!res) {
            for (auto it : to_erase) { base_class::erase_undo(it, std::next(it)); }
            base_class::insert_undo(to_insert.end(), to_insert.begin(), where_insert.end());
            base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());
        }

        return res;
    }

    /**
     * @brief Rollbacks notifications called by notify_assign
     *
     * @tparam Map Map type
     * Iterators of Map must satisfy ForwardIterator
     * Iterators of Map must be dereferenceable to value_type or type convertible to value_type
     * @param data Map to assign from
     */
    template<typename Map>
    void notify_undo_assign(const Map& data) {
        std::vector<const_iterator>                                          to_replace;
        std::vector<std::reference_wrapper<const typename Map::mapped_type>> replacing;
        std::vector<const_iterator>                                          where_insert;
        std::vector<std::reference_wrapper<const typename Map::value_type>>  to_insert;
        std::unordered_set<const_iterator, iterator_hash>                    processed;

        for (auto it = data.begin(); it != data.end(); ++it) {
            auto pos = lower_bound(it->first);
            if (pos != end() && equals(pos->first, it->first)) {
                to_replace.push_back(pos);
                replacing.emplace_back(it->second);

                processed.insert(pos);
            } else {
                where_insert.push_back(pos);
                to_insert.emplace_back(*it);
            }
        }

        for (auto it = begin(); it != end(); ++it) {
            if (processed.count(it) == 0) { base_class::erase_undo(it, std::next(it)); }
        }

        base_class::insert_undo(to_insert.end(), to_insert.begin(), where_insert.end());
        base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());
    }

    /**
     * @brief If a key equivalent to k already exists in the container, assigns std::forward<M>(obj) to the mapped_type
     * corresponding to the key k. If the key does not exist, inserts the new value
     * Notifies insert or replace listeners
     * Requires mapped_type to be move assignable
     * Value is constructed from obj if notifications fail
     * Internally calls emplace_hint on insert
     *
     * @tparam K Type to perfect forward into key_type
     * @tparam M Type to perfect forward into mapped_type
     * @param k Key used both to look up and to insert if not found
     * @param obj Value to insert or assign
     * @return tuple<iterator, bool, bool> Iterator to inserted or assigned element or end iterator if insert
     * notifications failed First bool is true if insert was attempted, false if assign was attempted Second bool is
     * true if notifications succeeded, false if notifications failed
     */
    template<typename K, typename M>
    std::tuple<iterator, bool, bool> p_insert_or_assign(K&& k, M&& obj) {
        auto hint = lower_bound(k);
        if (hint == end() || !equals(hint->first, k)) {
            auto res = emplace_hint(std::forward<K>(k), std::forward<M>(obj));

            return std::make_tuple(res, true, res == end());
        } else {
            mapped_type tmp(std::forward<M>(obj));
            auto        res = hint->second.replace_value(std::move(tmp));

            return std::make_tuple(hint, false, res);
        }
    }

    /**
     * @brief If a key equivalent to k already exists in the container, assigns std::forward<M>(obj) to the mapped_type
     * corresponding to the key k. If the key does not exist, inserts the new value
     * Notifies insert or replace listeners
     * Requires mapped_type to be move assignable
     * Value is constructed from obj if notifications fail
     * Internally calls emplace_hint on insert
     *
     * @tparam K Type to perfect forward into key_type
     * @tparam M Type to perfect forward into mapped_type
     * @param hint Iterator to the position before which the new element will be inserted
     * @param k Key used both to look up and to insert if not found
     * @param obj Value to insert or assign
     * @return pair<iterator, bool> Iterator to inserted or assigned element or end iterator if insert
     * notifications failed bool is true if notifications succeeded, fail if notifications failed
     */
    template<typename K, typename M>
    std::pair<iterator, bool> p_insert_or_assign(const_iterator hint, K&& k, M&& obj) {
        auto nc_hint = non_const_iterator(hint);
        auto check_res = hint_checker(hint, k);

        if (!check_res.second && check_res.first != end()) {
            mapped_type tmp(std::forward<M>(obj));
            auto        res = check_res.first->second.replace_value(std::move(tmp));

            return {check_res.first, res};
        }

        if (check_res.second) {
            auto res = emplace_hint(nc_hint, std::forward<K>(k), std::forward<M>(obj));

            return {res, res != end()};
        }

        auto res = insert_or_assign(std::forward<K>(k), std::move(obj));
        return {std::get<0>(res), std::get<2>(res)};
    }

    /**
     * @brief Perfect forwarding for try_emplace
     * Notifies insert listeners
     * Value is constructed from args if notifications fail
     *
     * @tparam K Only const key_type& or key_type&&
     * @tparam Args Argument pack
     * @param k Key used both to look up and to insert if not found
     * @param args Arguments to forward to the constructor of the mapped_type
     * @return pair<iterator, bool> Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     * and bool set to true if insertion took place
     */
    template<typename K, typename... Args>
    std::pair<iterator, bool> p_try_emplace(K&& k, Args&&... args) {
        auto hint = lower_bound(k);
        auto check_res = hint_checker(hint, k);

        if (!check_res.second) { return {check_res.first, false}; }

        wrapped_type tmp;
        tmp.try_emplace(std::forward<K>(k), std::forward<Args>(args)...);
        auto res = insert(hint, tmp.extract(tmp.begin()));

        if (res.has_value()) {
            return {*res, true};
        } else {
            return {end(), false};
        }
    }

    /**
     * @brief Perfect forwarding for try_emplace
     * Notifies insert listeners
     * Value is constructed from args if notifications fail
     *
     * @tparam K Only const key_type& or key_type&&
     * @tparam Args Argument pack
     * @param hint Iterator to the position before which the new element will be inserted
     * @param k Key used both to look up and to insert if not found
     * @param args Arguments to forward to the constructor of the mapped_type
     * @return iterator, bool Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     */
    template<typename K, typename... Args>
    iterator p_try_emplace(const_iterator hint, K&& k, Args&&... args) {
        auto check_res = hint_checker(hint, k);
        if (!check_res.second) {
            if (check_res.first == end()) {
                hint = lower_bound(k);
            } else {
                return check_res.first;
            }
        }

        wrapped_type tmp;
        tmp.try_emplace(std::forward<K>(k), std::forward<Args>(args)...);
        auto res = insert(hint, tmp.extract(tmp.begin()));
        if (res.has_value()) {
            return *res;
        } else {
            return end();
        }
    }

    /**
     * @brief Uses key_comp to determine if two values are equal
     *
     * @param first First key for comparison
     * @param second Second key for comparison
     * @return true Keys are equal
     * @return false Keys are not equal
     */
    bool equals(const key_type& first, const key_type& second) {
        return !key_comp()(first, second) && !key_comp()(second, first);
    }

    /**
     * @brief Perfect forwarding of value to insert
     * Notifies insert listeners
     *
     * @tparam V Only const value_type& or value_type&&
     * @param value Value to be forwarded
     * @return std::pair<iterator, bool> Iterator to inserted element or
     * element that prevented insertion or end iterator if notifications failed
     * and bool set to true if insertion took place
     */
    template<typename V>
    std::pair<iterator, bool> p_insert(V&& value) {
        auto res = insert_checker(value);

        if (res.second) { res.first = iterator(m_proxy, m_data.insert(std::forward<V>(value)).first); }
        return res;
    }

    /**
     * @brief Perfect forwarding of value to insert
     * Notifies insert listeners
     *
     * @tparam V Only const value_type& or value_type&&
     * @param hint Iterator to the position before which the new element will be inserted
     * @param value Value to be forwarded
     * @return iterator Iterator to inserted element or element that prevented insertion or end iterator if
     * notifications failed
     */
    template<typename V>
    iterator p_insert(const_iterator hint, V&& value) {
        auto [position, check] = insert_hint_checker(hint, value);
        if (!check) { return position; }

        return iterator(m_proxy, m_data.insert(position.get_iterator(), std::forward<V>(value)));
    }

    /**
     * @brief Checks if hint is correct, checks if container contains value with the same key and calls insert listeners
     *
     * @param hint Iterator to the position before which the new element will be inserted
     * @param value Value to be inserted
     * @return std::pair<iterator, bool> If insert can happen returns true along with hint where should value be
     * inserted.
     * If insert can't happen due to already existing element, returns iterator to the element and false.
     * If insert can't happen due to callback, returns end iterator and false
     */
    std::pair<iterator, bool> insert_hint_checker(const_iterator hint, const value_type& value) {
        auto res = hint_checker(hint, value.first);

        if (res.second) {
            if (!base_class::insert_called(value, res.first)) { return {end(), false}; }

            return {res.first, true};
        }

        if (res.first != end()) { return res; }

        return insert_checker(value);
    }

    /**
     * @brief Checks if container contains value with the same key and calls insert listeners
     *
     *
     * @param value Value to be inserted
     * @return std::pair<iterator, bool> If insert can happen returns true along with hint where should value be
     * inserted.
     * If insert can't happen due to already existing element, returns iterator to the element and false.
     * If insert can't happen due to callback, returns end iterator and false
     */
    std::pair<iterator, bool> insert_checker(const value_type& value) {
        iterator hint = lower_bound(value.first);
        if (hint != end() && equals(hint->first, value.first)) { return {hint, false}; }

        if (!base_class::insert_called(value, hint)) { return {end(), false}; }

        return {hint, true};
    }

    /**
     * @brief Checks if supplied hint is correct
     *
     * @param hint Hint to verify
     * @param key Key to compare
     * @return std::pair<iterator, bool> If the hint is correct, returns it along with true
     * If hint is incorrect returns end iterator along with false
     * If hint is correct but existing element is preventing insertion returns iterator to element with key equal to key
     * and false
     */
    std::pair<iterator, bool> hint_checker(const_iterator hint, const key_type& key) {
        iterator nc_hint = non_const_iterator(hint);

        if (nc_hint != end() && equals(nc_hint->first, key)) { return {nc_hint, false}; }
        if (nc_hint != begin() && equals(std::prev(nc_hint)->first, key)) { return {std::prev(nc_hint), false}; }

        bool gt_last = false, lt_first = false, lt_hint = false, gt_prev = false;

        if (!empty()) {
            gt_last = (nc_hint == end() && key_comp()(std::prev(nc_hint)->first, key));
            lt_first = (nc_hint == begin() && key_comp()(key, nc_hint->first));
            lt_hint = (nc_hint == end() || key_comp()(key, nc_hint->first));
            gt_prev = (nc_hint == begin() || key_comp()(std::prev(nc_hint)->first, key));
        }

        if (empty() || gt_last || lt_first || (lt_hint && gt_prev)) { return {nc_hint, true}; }

        return {end(), false};
    }

    void initialize_proxy() { m_proxy = std::make_shared<map*>(this); }

    shared_proxy m_proxy;
    wrapped_type m_data;
};

template<typename WrappedMultimap>
class multimap : public multimap_notifications<WrappedMultimap>
{
private:
    using base_class = multimap_notifications<WrappedMultimap>;
    using types = multimap_types<WrappedMultimap>;

    using iterator_hash = hash::map_iterator_hash<multimap>;

public:
    using wrapped_type = WrappedMultimap;
    using key_type = typename wrapped_type::key_type;
    using mapped_type = typename types::mapped_type;
    using value_type = typename types::value_type;
    using size_type = typename wrapped_type::size_type;
    using difference_type = typename wrapped_type::difference_type;

    using key_compare = typename wrapped_type::key_compare;

    using allocator_type = typename wrapped_type::allocator_type;

    using iterator = typename types::iterator;
    using const_iterator = typename types::const_iterator;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    using reference = notifications::map_reference_wrapper<multimap>;
    using const_reference = notifications::const_map_reference_wrapper<multimap>;
    using pointer = notifications::pointer_proxy<reference>;
    using const_pointer = notifications::pointer_proxy<const_reference>;

    using node_type = typename wrapped_type::node_type;

    using value_compare = typename wrapped_type::value_compare;

    using shared_proxy = utils::shared_proxy<multimap>;

    template<typename W>
    friend class multimap;

    template<typename W>
    friend class map;

public:
    multimap() { initialize_proxy(); }

    explicit multimap(const key_compare& comp, const allocator_type& alloc = allocator_type()) : m_data(comp, alloc) {
        initialize_proxy();
    }

    explicit multimap(const allocator_type& alloc) : m_data(alloc) { initialize_proxy(); }

    template<typename InputIt>
    multimap(InputIt first, InputIt last, const key_compare& comp = key_compare(),
             const allocator_type& alloc = allocator_type())
      : m_data(first, last, comp, alloc) {
        initialize_proxy();
    }

    template<typename InputIt>
    multimap(InputIt first, InputIt last, const allocator_type& alloc) : m_data(first, last, key_compare(), alloc) {
        initialize_proxy();
    }

    multimap(const multimap& other) : base_class(other), m_data(other.m_data) { initialize_proxy(); }

    explicit multimap(const wrapped_type& data) : m_data(data) { initialize_proxy(); }

    multimap(const multimap& other, const allocator_type& alloc) : base_class(other), m_data(other.m_data, alloc) {
        initialize_proxy();
    }

    multimap(const wrapped_type& data, const allocator_type& alloc) : m_data(data, alloc) { initialize_proxy(); }

    multimap(multimap&& other) : base_class(std::move(other)), m_data(std::move(other.m_data)) { initialize_proxy(); }

    explicit multimap(wrapped_type&& data) : m_data(std::move(data)) { initialize_proxy(); }

    multimap(multimap&& other, const allocator_type& alloc)
      : base_class(std::move(other)), m_data(std::move(other.m_data), alloc) {
        initialize_proxy();
    }

    multimap(wrapped_type&& data, const allocator_type& alloc) : m_data(std::move(data), alloc) { initialize_proxy(); }

    multimap(std::initializer_list<value_type> init, const key_compare& comp = key_compare(),
             const allocator_type& alloc = allocator_type())
      : m_data(init, comp, alloc) {
        initialize_proxy();
    }

    multimap(std::initializer_list<value_type> init, const allocator_type& alloc)
      : multimap(init, key_compare(), alloc) {
        initialize_proxy();
    }

    ~multimap() = default;

    /**
     * @brief Copies content from other to this including listeners
     * Doesn't call any notifications
     *
     * @param other Container to copy from
     * @return multimap& Returns *this
     */
    multimap& operator=(const multimap& other) {
        base_class::operator=(other);
        m_data = other.m_data;

        return *this;
    }

    /**
     * @brief Copies content from data to this
     * Notifies replace, insert and erase listeners
     *
     * @param data Container to copy from
     * @return multimap& Returns *this
     */
    multimap& operator=(const wrapped_type& data) {
        p_assign(data);
        return *this;
    }

    /**
     * @brief Moves content from other to this including listeners
     * Doesn't call any notifications
     *
     * @param other Container to move from
     * @return multimap& Returns *this
     */
    multimap& operator=(multimap&& other) noexcept(std::allocator_traits<allocator_type>::is_always_equal::value&&
                                                     std::is_nothrow_move_assignable<key_compare>::value) {
        base_class::operator=(std::move(other));
        m_data = std::move(other.m_data);

        return *this;
    }

    /**
     * @brief Moves content from data to this
     * Notifies replace, insert and erase listeners
     *
     * @param data Container to move from
     * @return multimap& Returns *this
     */
    multimap& operator=(wrapped_type&& data) {
        p_assign(std::move(data));
        return *this;
    }

    /**
     * @brief Copies content from initializer list to this
     * Notifies replace, insert and erase listeners
     * Internally constructs temporary container and then swaps with current contents
     *
     * @param ilist Initializer list to copy from
     * @return multimap& Returns *this
     */
    multimap& operator=(std::initializer_list<value_type> ilist) {
        wrapped_type tmp(ilist);

        bool res = notify_assign(tmp);
        if (res) { m_data.swap(tmp); }

        return *this;
    }

    allocator_type get_allocator() const { return m_data.get_allocator(); }

    iterator begin() noexcept { return iterator(m_proxy, m_data.begin()); }

    const_iterator begin() const noexcept { return const_iterator(m_data.begin()); }

    const_iterator cbegin() const noexcept { return const_iterator(m_data.cbegin()); }

    iterator end() noexcept { return iterator(m_proxy, m_data.end()); }

    const_iterator end() const noexcept { return const_iterator(m_data.end()); }

    const_iterator cend() const noexcept { return const_iterator(m_data.cend()); }

    reverse_iterator rbegin() noexcept { return reverse_iterator(end()); }

    const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator(end()); }

    const_reverse_iterator crbegin() const noexcept { return const_reverse_iterator(cend()); }

    reverse_iterator rend() noexcept { return reverse_iterator(begin()); }

    const_reverse_iterator rend() const noexcept { return const_reverse_iterator(begin()); }

    const_reverse_iterator crend() const noexcept { return const_reverse_iterator(cbegin()); }

    [[nodiscard]] bool empty() const noexcept { return m_data.empty(); }

    size_type size() const noexcept { return m_data.size(); }

    size_type max_size() const noexcept { return m_data.max_size(); }

    /**
     * @brief Clears the content of container
     * Notifies erase listeners
     *
     * @return true Notifications succeeded, clear happened
     * @return false Notifications failed, clear didn't happen
     */
    bool clear() {
        if (empty()) { return true; }

        if (!base_class::erase_called(begin(), end())) { return false; }

        m_data.clear();
        return true;
    }

    /**
     * @brief Inserts element into the container
     * Notifies insert listeners
     *
     * @param value Value to be copied from
     * @return iterator Iterator to inserted element or end iterator if notifications failed
     */
    iterator insert(const value_type& value) { return p_insert(value); }

    /**
     * @brief Inserts element into the container
     * Notifies insert listeners
     *
     * @param value Value to be moved from
     * @return iterator Iterator to inserted element or end iterator if notifications failed
     */
    iterator insert(value_type&& value) { return p_insert(std::move(value)); }

    /**
     * @brief Inserts element into the container
     * Notifies insert listeners
     *
     * @param hint Iterator to the position before which the new element will be inserted
     * @param value Value to be copied from
     * @return iterator Iterator to inserted element or end iterator if notifications failed
     */
    iterator insert(const_iterator hint, const value_type& value) { return p_insert(hint, value); }

    /**
     * @brief Inserts element into the container
     * Notifies insert listeners
     *
     * @param hint Iterator to the position before which the new element will be inserted
     * @param value Value to be moved from
     * @return iterator Iterator to inserted element or end iterator if notifications failed
     */
    iterator insert(const_iterator hint, value_type&& value) { return p_insert(hint, std::move(value)); }

    /**
     * @brief Inserts elements from range [first, last) into the container
     * Notifies insert listeners
     * Internally constructs temporary container and then merges it into this
     * Values are constructed from range even if notifications fail
     *
     * @tparam InputIt Type of iterator to values, must satisfy at least InputIterator
     * @param first First iterator of range
     * @param last Last iterator of range
     * @return true Notifications succeeded, insert happened
     * @return false Notifications failed, insert didn't happen
     */
    template<typename InputIt>
    bool insert(InputIt first, InputIt last) {
        wrapped_type to_merge;
        to_merge.insert(first, last);

        return merge(to_merge);
    }

    /**
     * @brief Inserts elements from initializer list into the container, if the container doesn't already contain
     * value with the same key
     * Notifies insert listeners
     * Internally constructs temporary container and then merges it into this
     *
     * @param ilist Initializer list to copy from
     * @return true Notifications succeeded, insert happened
     * @return false Notifications failed, insert didn't happen
     */
    bool insert(std::initializer_list<value_type> ilist) { return insert(ilist.begin(), ilist.end()); }

    /**
     * @brief Inserts element into the container
     * Notifies insert listeners
     *
     * @param nh Compatible node handle
     * @return std::optional<iterator> Iterator to inserted element, end iterator if nh was empty or empty optional if
     * notifications failed
     */
    std::optional<iterator> insert(node_type&& nh) {
        if (nh.empty()) { return end(); }
        wrapped_type tmp;
        tmp.insert(std::move(nh));

        auto hint = insert_checker(*(tmp.begin()));
        if (!hint.has_value()) {
            nh = tmp.extract(tmp.begin());
            return std::nullopt;
        }

        return iterator(m_proxy, m_data.insert(hint->get_iterator(), tmp.extract(tmp.begin())));
    }

    /**
     * @brief Inserts element into the container
     * Notifies insert listeners
     *
     * @param hint Iterator to the position before which the new element will be inserted
     * @param nh Compatible node handle
     * @return std::optional<iterator> Iterator to inserted element, end iterator if nh was empty or empty optional if
     * notifications failed
     */
    std::optional<iterator> insert(const_iterator hint, node_type&& nh) {
        if (nh.empty()) { return end(); }
        wrapped_type tmp;
        tmp.insert(std::move(nh));

        auto res = insert_hint_checker(hint, *(tmp.begin()));
        if (!res.has_value()) {
            nh = tmp.extract(tmp.begin());
            return std::nullopt;
        }

        return iterator(m_proxy, m_data.insert(res->get_iterator(), tmp.extract(tmp.begin())));
    }

    /**
     * @brief Inserts a new element into the container constructed in-place with the given args
     * Notifies insert listeners
     * Value is constructed from args even if notifications fail
     * Internally constructs temporary container, emplaces into it then extracts the node and tries to insert it
     *
     * @tparam Args Argument pack
     * @param args Arguments to forward to the constructor of the element
     * @return iterator Iterator to inserted element or end iterator if notifications failed
     */
    template<typename... Args>
    iterator emplace(Args&&... args) {
        wrapped_type tmp;
        tmp.emplace(std::forward<Args>(args)...);

        auto res = insert(tmp.extract(tmp.begin()));
        if (!res.has_value()) {
            return end();
        } else {
            return *res;
        }
    }

    /**
     * @brief Inserts a new element into the container constructed in-place with the given args
     * Notifies insert listeners
     * Value is constructed from args even if notifications fail
     * Internally constructs temprorary container, emplaces into it then extracts the node and tries to insert it
     *
     * @tparam Args Argument pack
     * @param hint Iterator to the position before which the new element will be inserted
     * @param args Arguments to forward to the constructor of the element
     * @return iterator Iterator to inserted element or end iterator if notifications failed
     */
    template<typename... Args>
    iterator emplace_hint(const_iterator hint, Args&&... args) {
        wrapped_type tmp;
        tmp.emplace(std::forward<Args>(args)...);

        auto res = insert(hint, tmp.extract(tmp.begin()));
        if (!res.has_value()) {
            return end();
        } else {
            return *res;
        }
    }

    /**
     * @brief Erases element from container
     * Notifies erase listeners
     *
     * @param pos Iterator to the element to be erased
     * @return std::optional<iterator> Iterator following the erased element or empty optional if notifications
     * failed
     */
    std::optional<iterator> erase(const_iterator pos) {
        if (!base_class::erase_called(pos, std::next(pos))) { return std::nullopt; }

        return iterator(m_proxy, m_data.erase(pos.get_iterator()));
    }

    /**
     * @brief Erases elements from range [first, last)
     * Notifies erase listeners
     *
     * @param first First iterator of range
     * @param last Last iterator of range
     * @return std::optional<iterator> Iterator following the last erased element or empty optional if notifications
     * failed
     */
    std::optional<iterator> erase(const_iterator first, const_iterator last) {
        if (!base_class::erase_called(first, last)) { return std::nullopt; }

        return iterator(m_proxy, m_data.erase(first.get_iterator(), last.get_iterator()));
    }

    /**
     * @brief Erases elements with key equal to key
     * Notifies erase listeners
     *
     * @param key Key value of elements to remove
     * @return size_type Number of elements erased
     */
    size_type erase(const key_type& key) {
        auto      range = equal_range(key);
        size_type count = std::distance(range.first, range.second);

        if (!erase(range.first, range.second).has_value()) { count = 0; }
        return count;
    }

    /**
     * @brief Swaps contents of this and other, not including listeners
     * Notifies replace, insert and erase listeners on both container
     *
     * @param other Container to exchange contents with
     * @return true Notifications succeeded, swap happened
     * @return false Notifications failed, swap didn't happen
     */
    bool swap(multimap& other) {
        if (!notify_assign(other)) { return false; }

        bool res;
        try {
            res = other.notify_assign(*this);
        } catch (...) {
            notify_undo_assign(other);

            throw;
        }

        if (!res) {
            notify_undo_assign(other);
        } else {
            m_data.swap(other.m_data);

            auto tmp = m_proxy;
            *m_proxy = *other.m_proxy;
            *other.m_proxy = *tmp;
        }

        return res;
    }

    /**
     * @brief Unlinks the node that contains the element and returns node handle that owns it
     * Notifies erase listeners
     *
     * @param pos Iterator to element
     * @return node_type Node handle that owns unlinked element or empty node if notifications failed
     */
    node_type extract(const_iterator pos) {
        if (!base_class::erase_called(pos, std::next(pos))) { return node_type(); }

        return m_data.extract(pos.get_iterator());
    }

    /**
     * @brief Unlinks the node that contains element with key equal to key and returns node handle that owns it
     * Notifies erase listeners
     *
     * @param x Key to identify node to be extracted
     * @return std::optional<node_type> Node handle that owns unlinked element, empty node if element with key doesn't
     * exist or empty optional if notifications failed
     */
    std::optional<node_type> extract(const key_type& x) {
        iterator it = find(x);
        if (it == end()) { return node_type(); }

        auto res = extract(it);
        if (res.empty()) {
            return std::nullopt;
        } else {
            return std::move(res);
        }
    }

    /**
     * @brief Tries to splice each element from source into this
     * Notifies insert listeners in this and erase listeners in other
     * Requires iterator not to be invalidated on insert
     *
     * @tparam W2 wrapped_type of source multimap
     * @param source Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename W2>
    bool merge(multimap<W2>& source) {
        return p_merge(source);
    }

    /**
     * @brief Tries to splice each element from source into this
     * Notifies insert listeners in this and erase listeners in other
     * Requires iterator not to be invalidated on insert
     *
     * @tparam W2 wrapped_type of source multimap
     * @param source Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename W2>
    bool merge(multimap<W2>&& source) {
        return p_merge(source);
    }

    /**
     * @brief Tries to splice each element from source into this
     * Notifies insert listeners in this and erase listeners in other
     * Requires iterator not to be invalidated on insert
     *
     * @tparam W2 wrapped_type of source map
     * @param source Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename W2>
    bool merge(map<W2>& source) {
        return p_merge(source);
    }

    /**
     * @brief Tries to splice each element from source into this
     * Notifies insert listeners in this and erase listeners in other
     * Requires iterator not to be invalidated on insert
     *
     * @tparam W2 wrapped_type of source multimap
     * @param source Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename W2>
    bool merge(map<W2>&& source) {
        return p_merge(source);
    }

    /**
     * @brief Tries to splice each element from data into this
     * Notifies insert listeners
     * Requires iterator not to be invalidated on insert
     *
     * @param data Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    bool merge(wrapped_type& data) {
        // mapping insert pos, what to insert pos
        std::vector<std::pair<const_iterator, typename wrapped_type::const_iterator>> mapping;

        auto rollback = [this, &mapping]() {
            for (auto& [pos, val_it] : helper_ranges::reverse_range(mapping)) { this->insert_undo(*val_it, pos); }
        };

        std::optional<iterator> insert_res;
        for (auto it = data.cbegin(); it != data.cend(); ++it) {
            try {
                insert_res = insert_checker(*it);
            } catch (...) {
                rollback();
                throw;
            }

            if (!insert_res.has_value()) {
                rollback();
                return false;
            }

            mapping.emplace_back(insert_res.value(), it);
        }

        // this should be faster, than calling merge
        for (auto& [pos, val_it] : mapping) { m_data.insert(pos.get_iterator(), data.extract(val_it)); }
        return true;
    }

    /**
     * @brief Tries to splice each element from data into this
     * Notifies insert listeners
     * Requires iterator not to be invalidated on insert
     *
     * @param data Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    bool merge(wrapped_type&& data) { return merge(data); }

    size_type count(const key_type& key) const { return m_data.count(key); }

    template<typename K>
    size_type count(const K& x) const {
        return m_data.count(x);
    }

    iterator find(const key_type& key) { return iterator(m_proxy, m_data.find(key)); }

    const_iterator find(const key_type& key) const { return const_iterator(m_data.find(key)); }

    template<typename K>
    iterator find(const K& x) {
        return iterator(m_proxy, m_data.find(x));
    }

    template<typename K>
    const_iterator find(const K& x) const {
        return const_iterator(m_data.find(x));
    }

    std::pair<iterator, iterator> equal_range(const key_type& key) {
        auto res = m_data.equal_range(key);
        return {iterator(m_proxy, res.first), iterator(m_proxy, res.second)};
    }

    std::pair<const_iterator, const_iterator> equal_range(const key_type& key) const {
        auto res = m_data.equal_range(key);
        return {const_iterator(res.first), const_iterator(res.second)};
    }

    template<typename K>
    std::pair<iterator, iterator> equal_range(const K& x) {
        auto res = m_data.equal_range(x);
        return {iterator(m_proxy, res.first), iterator(m_proxy, res.second)};
    }

    template<typename K>
    std::pair<const_iterator, const_iterator> equal_range(const K& x) const {
        auto res = m_data.equal_range(x);
        return {const_iterator(res.first), const_iterator(res.second)};
    }

    iterator lower_bound(const key_type& key) { return iterator(m_proxy, m_data.lower_bound(key)); }

    const_iterator lower_bound(const key_type& key) const { return const_iterator(m_data.lower_bound(key)); }

    template<typename K>
    iterator lower_bound(const K& x) {
        return iterator(m_proxy, m_data.lower_bound(x));
    }

    template<typename K>
    const_iterator lower_bound(const K& x) const {
        return const_iterator(m_data.lower_bound(x));
    }

    iterator upper_bound(const key_type& key) { return iterator(m_proxy, m_data.upper_bound(key)); }

    const_iterator upper_bound(const key_type& key) const { return const_iterator(m_data.upper_bound(key)); }

    template<typename K>
    iterator upper_bound(const K& x) {
        return iterator(m_proxy, m_data.upper_bound(x));
    }

    template<typename K>
    const_iterator upper_bound(const K& x) const {
        return const_iterator(m_data.upper_bound(x));
    }

    key_compare key_comp() const { return m_data.key_comp(); }

    value_compare value_comp() const { return m_data.value_comp(); }

    /**
     * @brief Returns reference to the underlying container
     * Useful for operations that should be done without notifications and for compability with older code
     *
     * @return wrapped_type& Reference to the underlying container
     */
    wrapped_type& container() { return m_data; }

    friend bool operator==(const multimap<WrappedMultimap>& lhs, const multimap<WrappedMultimap>& rhs) {
        return lhs.m_data == rhs.m_data;
    }

    friend bool operator!=(const multimap<WrappedMultimap>& lhs, const multimap<WrappedMultimap>& rhs) {
        return lhs.m_data != rhs.m_data;
    }

    friend bool operator<(const multimap<WrappedMultimap>& lhs, const multimap<WrappedMultimap>& rhs) {
        return lhs.m_data < rhs.m_data;
    }

    friend bool operator<=(const multimap<WrappedMultimap>& lhs, const multimap<WrappedMultimap>& rhs) {
        return lhs.m_data <= rhs.m_data;
    }

    friend bool operator>(const multimap<WrappedMultimap>& lhs, const multimap<WrappedMultimap>& rhs) {
        return lhs.m_data > rhs.m_data;
    }

    friend bool operator>=(const multimap<WrappedMultimap>& lhs, const multimap<WrappedMultimap>& rhs) {
        return lhs.m_data >= rhs.m_data;
    }

    friend bool swap(multimap<WrappedMultimap>& lhs, multimap<WrappedMultimap>& rhs) { return lhs.swap(rhs); }

private:
    iterator non_const_iterator(const_iterator it) {
        return iterator(m_proxy, m_data.erase(it.get_iterator(), it.get_iterator()));
    }

    /**
     * @brief Perfect forwarding for operator=
     * Notifies replace, insert and erase listeners
     *
     * @tparam Ty const wrapped_type& or wrapped_type&&
     * @param data Container to forward to operator=
     */
    template<typename Ty>
    void p_assign(Ty&& data) {
        if (notify_assign(data)) { m_data = std::forward<Ty>(data); }
    }

    /**
     * @brief Notifies replace, insert and erase listeners
     *
     * @tparam Map Map type
     * Iterators of Map must satisfy ForwardIterator
     * Iterators of Map must not be invalidated on erase
     * Iterators of Map must be dereferenceable to value_type or type convertible to value_type
     * @param data Map to assign from
     * @return true Notifications succeeded
     * @return false Notifications failed
     */
    template<typename Map>
    bool notify_assign(const Map& data) {
        std::vector<const_iterator>                                          to_replace;
        std::vector<std::reference_wrapper<const typename Map::mapped_type>> replacing;
        std::vector<const_iterator>                                          where_insert;
        std::vector<std::reference_wrapper<const typename Map::value_type>>  to_insert;
        std::vector<const_iterator>                                          to_erase;
        std::unordered_set<const_iterator, iterator_hash>                    processed;
        bool                                                                 res;

        for (auto it = data.begin(); it != data.end();) {
            auto eq_range = equal_range(it->first);
            auto data_eq_range = data.equal_range(it->first);

            iterator                     this_it;
            typename Map::const_iterator data_it;
            for (this_it = eq_range.first, data_it = data_eq_range.first;
                 this_it != eq_range.second && data_it != data_eq_range.second; ++this_it, ++data_it) {
                to_replace.push_back(this_it);
                replacing.emplace_back(data_it->second);

                processed.insert(this_it);
            }

            while (data_it != data_eq_range.second) {
                where_insert.push_back(this_it);
                to_insert.emplace_back(*data_it);
                ++data_it;
            }

            it = data_it;
        }

        if (!base_class::replace_called(to_replace.begin(), replacing.begin(), replacing.end())) { return false; }

        try {
            res = base_class::insert_called(to_insert.begin(), to_insert.end(), where_insert.begin());
        } catch (...) {
            base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());

            throw;
        }

        if (!res) {
            base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());

            return false;
        }

        try {
            for (auto it = begin(); it != end(); ++it) {
                if (processed.count(it) == 0) {
                    res = base_class::erase_called(it, std::next(it));
                    if (!res) { break; }
                    to_erase.push_back(it);
                }
            }
        } catch (...) {
            for (auto it : to_erase) { base_class::erase_undo(it, std::next(it)); }
            base_class::insert_undo(to_insert.end(), to_insert.begin(), where_insert.end());
            base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());

            throw;
        }

        if (!res) {
            for (auto it : to_erase) { base_class::erase_undo(it, std::next(it)); }
            base_class::insert_undo(to_insert.end(), to_insert.begin(), where_insert.end());
            base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());
        }

        return res;
    }

    /**
     * @brief Rollbacks notifications called by notify_assign
     *
     * @tparam Map Map type
     * Iterators of Map must satisfy ForwardIterator
     * Iterators of Map must be dereferenceable to value_type or type convertible to value_type
     * @param data Map to assign from
     */
    template<typename Map>
    void notify_undo_assign(const Map& data) {
        std::vector<const_iterator>                                          to_replace;
        std::vector<std::reference_wrapper<const typename Map::mapped_type>> replacing;
        std::vector<const_iterator>                                          where_insert;
        std::vector<std::reference_wrapper<const typename Map::value_type>>  to_insert;
        std::vector<const_iterator>                                          to_erase;
        std::unordered_set<const_iterator, iterator_hash>                    processed;

        for (auto it = data.begin(); it != data.end();) {
            auto eq_range = equal_range(it->first);
            auto data_eq_range = data.equal_range(it->first);

            iterator                     this_it;
            typename Map::const_iterator data_it;
            for (this_it = eq_range.first, data_it = data_eq_range.first;
                 this_it != eq_range.second && data_it != data_eq_range.second; ++this_it, ++data_it) {
                to_replace.push_back(this_it);
                replacing.emplace_back(data_it->second);

                processed.insert(this_it);
            }

            while (data_it != data_eq_range.second) {
                where_insert.push_back(this_it);
                to_insert.emplace_back(*data_it);
                ++data_it;
            }

            it = data_it;
        }

        for (auto it = begin(); it != end(); ++it) {
            if (processed.count(it) == 0) { base_class::erase_undo(it, std::next(it)); }
        }

        base_class::insert_undo(to_insert.end(), to_insert.begin(), where_insert.end());
        base_class::replace_undo(to_replace.end(), replacing.end(), replacing.begin());
    }

    /**
     * @brief Perfect forwarding of value to insert
     * Notifies insert listeners
     *
     * @tparam V Only const value_type& or value_type&&
     * @param value Value to be forwarded
     * @return iterator Iterator to inserted element or end iterator if notifications failed
     */
    template<typename V>
    iterator p_insert(V&& value) {
        auto hint = upper_bound(value.first);
        return p_insert(hint, std::forward<V>(value));
    }

    /**
     * @brief Perfect forwarding of value to insert
     * Notifies insert listeners
     *
     * @tparam V Only const value_type& or value_type&&
     * @param hint Iterator to the position before which the new element will be inserted
     * @param value Value to be forwarded
     * @return iterator Iterator to inserted element or end iterator if notifications failed
     */
    template<typename V>
    iterator p_insert(const_iterator hint, V&& value) {
        auto res = insert_hint_checker(hint, value);
        if (!res.has_value()) { return end(); }

        return iterator(m_proxy, m_data.insert(res->get_iterator(), std::forward<V>(value)));
    }

    /**
     * @brief Perfect forwarding of maps and multimaps
     * Notifies insert listeners in this and erase listeners in other
     * Requires iterator not to be invalidated on insert
     *
     * @tparam Ty map&, map&&, multimap&, multimap&&
     * @param source Container to merge into this
     * @return true Notifications succeeded, marge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename Ty>
    bool p_merge(Ty& source) {
        static_assert(std::is_same_v<typename Ty::value_type,
                                     value_type> && std::is_same_v<typename Ty::allocator_type, allocator_type>);

        if (this == std::addressof(source)) { return true; }

        // mapping insert pos, what to insert it
        std::vector<std::pair<const_iterator, typename Ty::const_iterator>> mapping;

        auto rollback = [this, &source, &mapping]() {
            for (auto& [pos, val_it] : helper_ranges::reverse_range(mapping)) {
                this->insert_undo(*val_it, pos);
                source.erase_undo(val_it, std::next(val_it));
            }
        };

        std::optional<iterator> insert_res;
        for (auto it = source.cbegin(); it != source.cend(); ++it) {
            try {
                insert_res = insert_checker(*it);
            } catch (...) {
                rollback();
                throw;
            }

            if (!insert_res.has_value()) {
                rollback();

                return false;
            }

            bool erase_res;
            try {
                erase_res = source.erase_called(it, std::next(it));
            } catch (...) {
                base_class::insert_undo(*it, *insert_res);
                rollback();

                throw;
            }

            if (!erase_res) {
                base_class::insert_undo(*it, *insert_res);
                rollback();

                return false;
            }

            mapping.emplace_back(*insert_res, it);
        }

        for (auto& [pos, val_it] : mapping) { m_data.insert(pos.get_iterator(), source.extract(val_it)); }
        return true;
    }

    /**
     * @brief Uses key_comp to determine if two values are equal
     *
     * @param first First key for comparison
     * @param second Second key for comparison
     * @return true Keys are equal
     * @return false Keys are not equal
     */
    bool equals(const key_type& first, const key_type& second) {
        return !key_comp()(first, second) && !key_comp()(second, first);
    }

    /**
     * @brief Checks if hint is correct calls insert listeners
     *
     *
     * @param hint Iterator to the position before which the new element will be inserted
     * @param value Value to be inserted
     * @return std::optional<iterator> Hint where should value be inserted or empty optional if notifications failed
     */
    std::optional<iterator> insert_hint_checker(const_iterator hint, const value_type& value) {
        iterator nc_hint = non_const_iterator(hint);

        bool ge_last = false, lt_first = false, lt_hint = false, ge_prev = false;

        if (!empty()) {
            ge_last = (nc_hint == end() && !value_comp()(value, *std::prev(nc_hint)));
            lt_first = (nc_hint == begin() && value_comp()(value, *nc_hint));
            lt_hint = (nc_hint == end() || value_comp()(value, *nc_hint));
            ge_prev = (nc_hint == begin() || !value_comp()(value, *std::prev(nc_hint)));
        }

        if (empty() || ge_last || lt_first || (lt_hint && ge_prev)) {
            if (!base_class::insert_called(value, hint)) { return std::nullopt; }

            return nc_hint;
        }

        return insert_checker(value);
    }

    /**
     * @brief Calls insert listeners
     *
     *
     * @param value Value to be inserted
     * @return std::optional<iterator> Hint where should value be inserted or empty optional if notifications failed
     */
    std::optional<iterator> insert_checker(const value_type& value) {
        iterator hint = upper_bound(value.first);

        if (!base_class::insert_called(value, hint)) { return std::nullopt; }

        return hint;
    }

    void initialize_proxy() { m_proxy = std::make_shared<multimap*>(this); }

    shared_proxy m_proxy;
    wrapped_type m_data;
};
} // namespace cne

#endif // CNE_MAP_H
