#ifndef CNE_LIST_H
#define CNE_LIST_H

#include <initializer_list>
#include <iterator>
#include <optional>
#include <utility>
#include <vector>

#include "../notifications/helper_iterators.h"
#include "../notifications/helper_ranges.h"
#include "../notifications/iterators.h"
#include "../notifications/notifications.h"
#include "../notifications/reference_wrapper.h"
#include "../notifications/utils.h"

namespace cne {
template<typename WrappedList>
class list;

// for iterators
template<typename WrappedList>
class list_traits
{
public:
    using container = list<WrappedList>;
    using wrapped_type = WrappedList;
    using wrapped_iterator = typename wrapped_type::iterator;
    using wrapped_const_iterator = typename wrapped_type::const_iterator;
};

// for class definitions
template<typename WrappedList>
class list_types
{
private:
    using traits = list_traits<WrappedList>;

public:
    using wrapped_type = typename traits::wrapped_type;
    using iterator = iterators::bidirectional_iterator<traits>;
    using const_iterator = iterators::const_bidirectional_iterator<traits>;
    using value_type = typename WrappedList::value_type;
};

template<typename WrappedList>
using list_notifications = notifications::notifications_base<
  notifications::insert<list<WrappedList>, typename list_types<WrappedList>::value_type,
                        typename list_types<WrappedList>::const_iterator>,
  notifications::erase<list<WrappedList>, typename list_types<WrappedList>::const_iterator>,
  notifications::replace<list<WrappedList>, typename list_types<WrappedList>::const_iterator,
                         typename list_types<WrappedList>::value_type>,
  notifications::value_change<list<WrappedList>, typename list_types<WrappedList>::const_iterator>>;

template<typename WrappedList>
class list : public list_notifications<WrappedList>
{
private:
    using base_class = list_notifications<WrappedList>;
    using types = list_types<WrappedList>;

public:
    using wrapped_type = typename types::wrapped_type;
    using value_type = typename wrapped_type::value_type;
    using allocator_type = typename wrapped_type::allocator_type;
    using size_type = typename wrapped_type::size_type;
    using difference_type = typename wrapped_type::difference_type;

    using iterator = typename types::iterator;
    using const_iterator = typename types::const_iterator;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    using reference = notifications::replaceable_reference_wrapper<list>;
    using const_reference = notifications::const_reference_wrapper<list>;
    using pointer = notifications::pointer_proxy<reference>;
    using const_pointer = notifications::pointer_proxy<const_reference>;

    using shared_proxy = utils::shared_proxy<list>;

private:
    using constant_iterator_wrapper = helper_iterators::constant_iterator_wrapper<const_iterator>;
    using iterator_wrapper = helper_iterators::iterator_wrapper<const_iterator>;
    using count_ref_iterator = helper_iterators::count_ref_iterator<size_type, difference_type, value_type>;
    using count_ref_range = helper_ranges::count_ref_range<size_type, difference_type, value_type>;

public:
    list() noexcept(noexcept(allocator_type())) { initialize_proxy(); }

    explicit list(const allocator_type& alloc) noexcept : m_data(alloc) { initialize_proxy(); }

    list(size_type count, const value_type& value, const allocator_type& alloc = allocator_type())
      : m_data(count, value, alloc) {
        initialize_proxy();
    }

    explicit list(size_type count, const allocator_type& alloc = allocator_type()) : m_data(count, alloc) {
        initialize_proxy();
    }

    template<typename InputIt>
    list(InputIt first, InputIt last, const allocator_type& alloc = allocator_type()) : m_data(first, last, alloc) {
        initialize_proxy();
    }

    list(const list& other) : base_class(other), m_data(other.m_data) { initialize_proxy(); }

    explicit list(const wrapped_type& data) : m_data(data) { initialize_proxy(); }

    list(const list& other, const allocator_type& alloc) : base_class(other), m_data(other.m_data, alloc) {
        initialize_proxy();
    }

    list(const wrapped_type& data, const allocator_type& alloc) : m_data(data, alloc) { initialize_proxy(); }

    list(list&& other) noexcept : base_class(std::move(other)), m_data(std::move(other.m_data)) { initialize_proxy(); }

    explicit list(wrapped_type&& data) noexcept : m_data(std::move(data)) { initialize_proxy(); }

    list(list&& other, const allocator_type& alloc)
      : base_class(std::move(other)), m_data(std::move(other.m_data), alloc) {
        initialize_proxy();
    }

    list(wrapped_type&& data, const allocator_type& alloc) : m_data(std::move(data), alloc) { initialize_proxy(); }

    list(std::initializer_list<value_type> init, const allocator_type& alloc = allocator_type()) : m_data(init, alloc) {
        initialize_proxy();
    }

    ~list() { *m_proxy = nullptr; }

    /**
     * @brief Copies content from other to this including listeners
     * Doesn't call any notifications
     *
     * @param other Contaier to copy from
     * @return list& Returns *this
     */
    list& operator=(const list& other) {
        base_class::operator=(other);
        m_data = other.m_data;

        return *this;
    }

    /**
     * @brief Copies content from data to this
     * Notifies replace and either insert or erase listeners depending on size of data
     *
     * @param data Container to copy from
     * @return list& Returns *this
     */
    list& operator=(const wrapped_type& data) {
        if (notify_assign(data)) { m_data = data; }

        return *this;
    }

    /**
     * @brief Moves content from other to this including listeners
     * Doesn't call any notifications
     *
     * @param other Container to move from
     * @return list& Returns *this
     */
    list& operator=(list&& other) noexcept(std::allocator_traits<allocator_type>::is_always_equal::value) {
        base_class::operator=(std::move(other));
        m_data = std::move(m_data);

        return *this;
    }

    /**
     * @brief Moves content from data to this
     * Notifies replace and either insert or erase listeners depending on size of data
     *
     * @param data Container to move from
     * @return list& Returns *this
     */
    list& operator=(wrapped_type&& data) {
        if (notify_assign(data)) { m_data = std::move(data); }

        return *this;
    }

    /**
     * @brief Copies content from initializer list to this
     * Notifies replace and either insert or erase listeners depending on size of initializer list
     *
     * @param ilist Initializer list to copy from
     * @return list& Returns *this
     */
    list& operator=(std::initializer_list<value_type> ilist) {
        if (notify_assign(ilist)) { m_data = ilist; }

        return *this;
    }

    /**
     * @brief Assigns count copies of value to this
     * Notifies replace and either insert or erase listeners depending on count
     *
     * @param count Number of elements to assign
     * @param value Value to copy
     * @return true Notifications succeeded, assign happened
     * @return false Notifications failed, assign didn't happen
     */
    bool assign(size_type count, const value_type& value) {
        bool res = notify_assign(count_ref_range{count, value});

        if (res) { m_data.assign(count, value); }
        return res;
    }

    /**
     * @brief Assigns values from range [first, last) to this
     * Notifies replace and either insert or erase listeners depending on distance between first and last
     * Internally constructs temporary container and then swaps with current contents
     * Values are constructed from range even if notifications fail
     *
     * @tparam InputIt Type of iterator to values, must satisfy at least InputIterator
     * @param first First iterator of range
     * @param last Last iterator of range
     * @return true Notifications succeeded, assign happened
     * @return false Notifications failed, assign didn't happen
     */
    template<typename InputIt>
    bool assign(InputIt first, InputIt last) {
        wrapped_type tmp(first, last);

        bool res = notify_assign(tmp);
        if (res) { m_data.swap(tmp); }

        return res;
    }

    /**
     * @brief Assigns values from range initializer list to this
     * Notifies replace and either insert or erase listeners depending on distance between first and last
     * Internally constructs temporary container and then swaps with current contents
     *
     * @param ilist Initializer list to copy from
     * @return true Notifications succeeded, assign happened
     * @return false Notifications failed, assign didn't happen
     */
    bool assign(std::initializer_list<value_type> ilist) {
        bool res = notify_assign(ilist);
        if (res) { m_data = ilist; };

        return res;
    }

    allocator_type get_allocator() const { return m_data.get_allocator(); }

    reference front() { return *begin(); }

    const_reference front() const { return *begin(); }

    reference back() { return *(std::prev(end())); }

    const_reference back() const { return *(std::prev(end())); }

    iterator begin() noexcept { return iterator(m_proxy, m_data.begin()); }

    const_iterator begin() const noexcept { return const_iterator(m_data.begin()); }

    const_iterator cbegin() const noexcept { return const_iterator(m_data.cbegin()); }

    iterator end() noexcept { return iterator(m_proxy, m_data.end()); }

    const_iterator end() const noexcept { return const_iterator(m_data.end()); }

    const_iterator cend() const noexcept { return const_iterator(m_data.cend()); }

    reverse_iterator rbegin() noexcept { return reverse_iterator(end()); }

    const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator(end()); }

    const_reverse_iterator crbegin() const noexcept { return const_reverse_iterator(cend()); }

    reverse_iterator rend() noexcept { return reverse_iterator(begin()); }

    const_reverse_iterator rend() const noexcept { return const_reverse_iterator(begin()); }

    const_reverse_iterator crend() const noexcept { return const_reverse_iterator(cbegin()); }

    [[nodiscard]] bool empty() const noexcept { return m_data.empty(); }

    size_type size() const noexcept { return m_data.size(); }

    size_type max_size() const noexcept { return m_data.max_size(); }

    /**
     * @brief Clears the content of container
     * Notifies erase listeners
     *
     * @return true Notifications succeeded, clear happened
     * @return false Notifications failed, clear didn't happen
     */
    bool clear() {
        if (empty()) { return true; }

        if (!base_class::erase_called(cbegin(), cend())) { return false; }

        m_data.clear();
        return true;
    }

    /**
     * @brief Inserts value before pos
     * Notifies insert listeners
     *
     * @param pos Iterator before which the value will be inserted
     * @param value Value to be copied from
     * @return iterator Iterator to the inserted element or end iterator if notifications failed
     */
    iterator insert(const_iterator pos, const value_type& value) { return p_insert(pos, value); }

    /**
     * @brief Inserts value before pos
     * Notifies insert listeners
     * Doesn't move from value if notifications fail
     *
     * @param pos Iterator before which the value will be inserted
     * @param value Value to be moved from
     * @return iterator Iterator to the inserted element or end iterator if notifications failed
     */
    iterator insert(const_iterator pos, value_type&& value) { return p_insert(pos, std::move(value)); }

    /**
     * @brief Inserts count copies of value before pos
     * Notifies insert listeners
     *
     * @param pos Iterator before which values will be inserted
     * @param count Number of copies of value to be inserted
     * @param value Value to be copied from
     * @return iterator Iterator to first inserted element or end iterator if notifications failed
     */
    iterator insert(const_iterator pos, size_type count, const value_type& value) {
        if (!notify_count_insert(pos, count, value)) { return end(); }

        return iterator(m_proxy, m_data.insert(pos.get_iterator(), count, value));
    }

    /**
     * @brief Inserts values from range [first, last) before pos
     * Notifies insert listeners
     * Internally constructs temporary container and then splices it into this
     * Values are constructed from range even if notifications fail
     *
     * @tparam InputIt Type of iterator to values, must satisfy at least InputIterator
     * @param pos Iterator before which values will be inserted
     * @param first First iterator of range
     * @param last Last iterator of range
     * @return iterator Iterator to first inserted element or end iterator if notifications failed
     */
    template<typename InputIt>
    iterator insert(const_iterator pos, InputIt first, InputIt last) {
        wrapped_type tmp;
        tmp.insert(tmp.cbegin(), first, last);

        return p_insert_splice(pos, tmp);
    }

    /**
     * @brief Inserts values from initializer list before pos
     * Notifies insert listeners
     *
     * @param pos Iterator before which values will be inserted
     * @param ilist Initializer list to copy from
     * @return iterator Iterator to first inserted element or end iterator if notifications failed
     */
    iterator insert(const_iterator pos, std::initializer_list<value_type> ilist) {
        return p_insert(pos, ilist.begin(), ilist.end());
    }

    /**
     * @brief Inserts value constructed from args before pos
     * Notifies insert listeners
     * Internally constructs temporary container and then splices it into this
     * Value is constructed from args even if notifications fail
     *
     * @tparam Args Argument pack
     * @param pos Iterator before which the value will be inserted
     * @param args Arguments to forward to the constructor of the element
     * @return iterator Iterator to inserted element or end iterator if notifications failed
     */
    template<typename... Args>
    iterator emplace(const_iterator pos, Args&&... args) {
        wrapped_type tmp;
        tmp.emplace_back(std::forward<Args>(args)...);

        return p_insert_splice(pos, tmp);
    }

    /**
     * @brief Erases element from container
     * Notifies erase listeners
     *
     * @param pos Iterator to the element to be erased
     * @return std::optional<iterator> Iterator following the erased element or empty optional if notifications failed
     */
    std::optional<iterator> erase(const_iterator pos) {
        if (!base_class::erase_called(pos, std::next(pos))) { return std::nullopt; }

        return iterator(m_proxy, m_data.erase(pos.get_iterator()));
    }

    /**
     * @brief Erases elements from range [first, last)
     * Notifies erase listeners
     *
     * @param first First iterator of range
     * @param last Last iterator of range
     * @return std::optional<iterator> Iterator following the last erased element or empty optional if notifications
     * failed
     */
    std::optional<iterator> erase(const_iterator first, const_iterator last) {
        if (!base_class::erase_called(first, last)) { return std::nullopt; }

        return iterator(m_proxy, m_data.erase(first.get_iterator(), last.get_iterator()));
    }

    /**
     * @brief Inserts value to the front of the container
     * Notifies insert listeners
     *
     * @param value Value to be copied from
     * @return true Notifications succeeded, push_front happened
     * @return false Notifications failed, push_front didn't happen
     */
    bool push_front(const value_type& value) { return p_insert(begin(), value) != end(); }

    /**
     * @brief Inserts value to the front of the container
     * Notifies insert listeners
     *
     * @param value Value to be moved from from
     * @return true Notifications succeeded, push_front happened
     * @return false Notifications failed, push_front didn't happen
     */
    bool push_front(value_type&& value) { return p_insert(begin(), std::move(value)) != end(); }

    /**
     * @brief Inserts value constructed from args to the front of the container
     * Notifies insert listeners
     * Internally constructs temporary container and then splices it into this
     * Value is constructed from args even if notifications fail
     *
     * @tparam Args Argument pack
     * @param args Arguments to forward to the constructor of the element
     * @return std::optional<reference> Reference to the inserted element or empty optional if notifications failed
     */
    template<typename... Args>
    std::optional<reference> emplace_front(Args&&... args) {
        auto res = emplace(cbegin(), std::forward<Args>(args)...);
        if (res != end()) {
            return *res;
        } else {
            return std::nullopt;
        }
    }

    /**
     * @brief Erases first element of the container
     * Notifies erase listeners
     *
     * @return true Notifications succeeded, pop_front happened
     * @return false Notifications failed, pop_front didn't happen
     */
    bool pop_front() { return erase(cbegin()).has_value(); }

    /**
     * @brief Inserts value to the end of the container
     * Notifies insert listeners
     *
     * @param value Value to be copied from
     * @return true Notifications succeeded, push_back happened
     * @return false Notifications failed, push_back didn't happen
     */
    bool push_back(const value_type& value) { return p_insert(cend(), value) != end(); }

    /**
     * @brief Inserts value to the end of the container
     * Notifies insert listeners
     *
     * @param value Value to be moved from from
     * @return true Notifications succeeded, push_back happened
     * @return false Notifications failed, push_back didn't happen
     */
    bool push_back(value_type&& value) { return p_insert(cend(), std::move(value)) != end(); }

    /**
     * @brief Inserts value constructed from args to the end of the container
     * Notifies insert listeners
     * Internally constructs temporary container and then splices it into this
     * Value is constructed from args even if notifications fail
     *
     * @tparam Args Argument pack
     * @param args Arguments to forward to the constructor of the element
     * @return std::optional<reference> Reference to the inserted element or empty optional if notifications failed
     */
    template<typename... Args>
    std::optional<reference> emplace_back(Args&&... args) {
        auto res = emplace(cend(), std::forward<Args>(args)...);
        if (res != end()) {
            return *res;
        } else {
            return std::nullopt;
        }
    }

    /**
     * @brief Erases last element of the container
     * Notifies erase listeners
     *
     * @return true Notifications succeeded, pop_back happened
     * @return false Notifications failed, pop_back didn't happen
     */
    bool pop_back() { return erase(std::prev(cend())).has_value(); }

    /**
     * @brief Resizes the contianer to contain count elements
     * If the current size is greater than count, the container is reduced to its first count elements
     * If the current size is less than count, additional default inserted values are appended
     * Notifies erase or insert listeners depending on count
     *
     * @param count New size
     * @return true Notifications succeeded, resize happened
     * @return false Notifications failed, resize didn't happen
     */
    bool resize(size_type count) {
        if (count > size()) {
            if (notify_count_insert(cend(), count - size())) {
                m_data.resize(count);
            } else {
                return false;
            }
        } else if (count < size()) {
            return resize_erase(count);
        }

        return true;
    }

    /**
     * @brief Resizes the contianer to contain count elements
     * If the current size is greater than count, the container is reduced to its first count elements
     * If the current size is less than count, additional copies of value are appended
     * Notifies erase or insert listeners depending on count
     *
     * @param count New size
     * @param value Value to be copied from
     * @return true Notifications succeeded, resize happened
     * @return false Notifications failed, resize didn't happen
     */
    bool resize(size_type count, const value_type& value) {
        if (count > size()) {
            // Can't straight up use insert due to type limitations
            if (notify_count_insert(cend(), count - size(), value)) {
                m_data.resize(count, value);
            } else {
                return false;
            }
        } else if (count < size()) {
            return resize_erase(count, value);
        }

        return true;
    }

    /**
     * @brief Swaps contents of this and other, not including listeners
     * Notifies replace and insert or erase listeners on either container
     *
     * @param other Container to exchange contents with
     * @return true Notifications succeeded, swap happened
     * @return false Notifications failed, swap didn't happen
     */
    bool swap(list& other) {
        if (!notify_assign(other)) { return false; }
        bool res;

        try {
            res = other.notify_assign(*this);
        } catch (...) {
            notify_undo_assign(other);
            throw;
        }

        if (!res) {
            notify_undo_assign(other);
        } else {
            m_data.swap(other.m_data);

            auto tmp = m_proxy;
            *m_proxy = *other.m_proxy;
            *other.m_proxy = *tmp;
        }

        return res;
    }

    /**
     * @brief Merges two sorted lists into one. Lists should be sorted into ascending order
     * Notifies insert listeners on this and erase listeners on other
     * Insert is notified one by one, this saves time on merges that fail early
     * Requires iterator not to be invalidated on insert
     * Iterators to merged elements are invalidated
     *
     * @param other Container to merge
     * @return true Notifications succeeded, merge happened
     * @return false Notifications failed, merge didn't happen
     */
    bool merge(list& other) { return merge(other, std::less<value_type>()); }

    /**
     * @brief Merges two sorted lists into one. Lists should be sorted into ascending order
     * Notifies insert listeners on this and erase listeners on other
     * Insert is notified one by one, this saves time on merges that fail early
     * Requires iterator not to be invalidated on insert
     * Iterators to merged elements are invalidated
     *
     * @param other Container to merge
     * @return true Notifications succeeded, merge happened
     * @return false Notifications failed, merge didn't happen
     */
    bool merge(list&& other) { return merge(other); }

    /**
     * @brief Merges two sorted lists into one. Lists should be sorted into ascending order
     * Notifies insert listeners on this and erase listeners on other
     * Insert is notified one by one, this saves time on merges that fail early
     * Requires iterator not to be invalidated on insert
     * Iterators to merged elements are invalidated
     *
     * @tparam Compare Type of comparison object
     * @param other Container to merge
     * @param comp Comparison object
     * @return true Notifications succeeded, merge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename Compare>
    bool merge(list& other, Compare comp) {
        if (this == std::addressof(other)) { return true; }

        if (!other.erase_called(other.cbegin(), other.cend())) { return false; }

        // mapping insert pos, what to insert it
        std::vector<std::pair<const_iterator, const_iterator>> mapping;

        auto     it = cbegin();
        iterator other_it = other.begin();

        auto rollback = [this, &other, &mapping]() {
            for (auto& [pos, val_it] : helper_ranges::reverse_range(mapping)) { this->insert_undo(*val_it, pos); }
            other.erase_undo(other.begin(), other.end());
        };

        try {
            while (it != cend() && other_it != other.end()) {
                if (comp(*other_it, *it)) {
                    if (base_class::insert_called(*other_it, it)) {
                        mapping.emplace_back(it, other_it);
                        ++other_it;
                    } else {
                        break;
                    }
                } else {
                    ++it;
                }
            }
        } catch (...) {
            rollback();

            throw;
        }

        if (it != cend() && other_it != other.end()) {
            rollback();

            return false;
        }

        bool res;
        try {
            res = base_class::insert_called(other_it, other.end(), constant_iterator_wrapper{it});
        } catch (...) {
            rollback();

            throw;
        }

        if (!res) {
            rollback();
        } else {
            m_data.merge(other.m_data, comp);
        }

        return res;
    }

    /**
     * @brief Merges two sorted lists into one. Lists should be sorted into ascending order
     * Notifies insert listeners on this and erase listeners on other
     * Insert is notified one by one, this saves time on merges that fail early
     * Requires iterator not to be invalidated on insert
     * Iterators to merged elements are invalidated
     *
     * @tparam Compare Type of comparison object
     * @param other Container to merge
     * @param comp Comparison object
     * @return true Notifications succeeded, merge happened
     * @return false Notifications failed, merge didn't happen
     */
    template<typename Compare>
    bool merge(list&& other, Compare comp) {
        return merge(other, comp);
    }

    /**
     * @brief Transfers all elements from other to this before pos
     * Notifies insert listeners on this and erase listeners on other
     * Iterators to transfered elements are invalidated
     *
     * @param pos Iterator before which elements will be transfered
     * @param other Container to transfer elements from
     * @return true Notifications succeeded, splice happened
     * @return false Notifications failed, splice didn't happen
     */
    bool splice(const_iterator pos, list& other) { return splice(pos, other, other.begin(), other.end()); }

    /**
     * @brief Transfers all elements from other to this before pos
     * Notifies insert listeners on this and erase listeners on other
     * Iterators to transfered elements are invalidated
     *
     * @param pos Iterator before which elements will be transfered
     * @param other Container to transfer elements from
     * @return true Notifications succeeded, splice happened
     * @return false Notifications failed, splice didn't happen
     */
    bool splice(const_iterator pos, list&& other) { return splice(pos, other); }

    /**
     * @brief Transfers element from other to this before pos
     * Notifies insert listeners on this and erase listeners on other
     * Iterators to transfered elements are invalidated
     *
     * @param pos Iterator before which elements will be transfered
     * @param other Container to transfer elements from
     * @param it Iterator to element to be transfered
     * @return true Notifications succeeded, splice happened
     * @return false Notifications failed, splice didn't happen
     */
    bool splice(const_iterator pos, list& other, const_iterator it) { return splice(pos, other, it, std::next(it)); }

    /**
     * @brief Transfers element from other to this before pos
     * Notifies insert listeners on this and erase listeners on other
     * Iterators to transfered elements are invalidated
     *
     * @param pos Iterator before which elements will be transfered
     * @param other Container to transfer elements from
     * @param it Iterator to element to be transfered
     * @return true Notifications succeeded, splice happened
     * @return false Notifications failed, splice didn't happen
     */
    bool splice(const_iterator pos, list&& other, const_iterator it) { return splice(pos, other, it); }

    /**
     * @brief Transfers elements in range [first, last) from other to this before pos
     * Notifies insert listeners on this and erase listeners on other
     * Iterators to transfered elements are invalidated
     *
     * @param pos Iterator before which elements will be transfered
     * @param other Container to transfer elements from
     * @param first First iterator of range
     * @param last Last iterator of range
     * @return true Notifications succeeded, splice happened
     * @return false Notifications failed, splice didn't happen
     */
    bool splice(const_iterator pos, list& other, const_iterator first, const_iterator last) {
        if (this == std::addressof(other) || first == last) { return true; }

        if (!other.erase_called(first, last)) { return false; }

        bool res;
        try {
            res = base_class::insert_called(first, last, constant_iterator_wrapper{pos});
        } catch (...) {
            other.erase_undo(first, last);
            throw;
        }

        if (!res) {
            other.erase_undo(first, last);
        } else {
            m_data.splice(pos.get_iterator(), other.m_data, first.get_iterator(), last.get_iterator());
        }

        return res;
    }

    /**
     * @brief Transfers elements in range [first, last) from other to this before pos
     * Notifies insert listeners on this and erase listeners on other
     * Iterators to transfered elements are invalidated
     *
     * @param pos Iterator before which elements will be transfered
     * @param other Container to transfer elements from
     * @param first First iterator of range
     * @param last Last iterator of range
     * @return true Notifications succeeded, splice happened
     * @return false Notifications failed, splice didn't happen
     */
    bool splice(const_iterator pos, list&& other, const_iterator first, const_iterator last) {
        return splice(pos, other, first, last);
    }

    /**
     * @brief Erases all elements that equal to value
     * Notifies erase listeners
     *
     * @param value Value of elements to remove
     * @return size_type Number of elements erased
     */
    size_type remove(const value_type& value) {
        return remove_if([&value](const value_type& other) { return value == other; });
    }

    /**
     * @brief Erases all elements satisfying specified criteria
     * Notifies erase listeners
     * Notifies erase one by one, this saves time on removes that fail early
     *
     * @tparam UnaryPredicate Type of unary predicate
     * @param p Unary predicate specifying criteria
     * @return size_type Number of elements erased
     */
    template<typename UnaryPredicate>
    size_type remove_if(UnaryPredicate p) {
        std::vector<const_iterator> to_remove;
        bool                        res = true;
        size_type                   ret_val = 0;

        try {
            for (auto it = cbegin(); it != cend(); ++it) {
                if (p(*it)) {
                    if (!base_class::erase_called(it, std::next(it))) { break; }
                    to_remove.push_back(it);
                    ++ret_val;
                }
            }
        } catch (...) {
            for (auto it : helper_ranges::reverse_range(to_remove)) { base_class::erase_undo(it, std::next(it)); }
            throw;
        }

        if (!res) {
            for (auto it : helper_ranges::reverse_range(to_remove)) { base_class::erase_undo(it, std::next(it)); }
            return 0;
        }

        for (auto it : to_remove) { m_data.erase(it.get_iterator()); }
        return ret_val;
    }

    void reverse() noexcept { m_data.reverse(); }

    /**
     * @brief Removes all consecutive duplicate elements
     * Notifies erase listeners
     * Notifies erase one by one, this saves time on removes that fail early
     *
     * @return size_type Number of elements erased
     */
    size_type unique() { return unique(std::equal_to<value_type>()); }

    /**
     * @brief Removes all consecutive duplicate elements according to binary predicate
     * Notifies erase listeners
     * Notifies erase one by one, this saves time on removes that fail early
     *
     * @tparam BinaryPredicate Type of binary predicate
     * @param p Binary predicate that returs true, when elements should be treated equal
     * @return size_type Number of elements erased
     */
    template<typename BinaryPredicate>
    size_type unique(BinaryPredicate p) {
        auto                        it = cbegin();
        size_type                   ret_val = 0;
        std::vector<const_iterator> to_remove;

        if (it == cend()) { return 0; }

        bool res = true;
        try {
            for (; std::next(it) != cend(); ++it) {
                if (p(*it, *std::next(it))) {
                    res = base_class::erase_called(std::next(it), std::next(it, 2));
                    to_remove.push_back(std::next(it));
                    ++ret_val;
                }
            }
        } catch (...) {
            for (auto erased_it : helper_ranges::reverse_range(to_remove)) {
                base_class::erase_undo(erased_it, std::next(erased_it));
            }

            throw;
        }

        if (!res) {
            for (auto erased_it : helper_ranges::reverse_range(to_remove)) {
                base_class::erase_undo(erased_it, std::next(erased_it));
            }

            return 0;
        }

        for (auto erase_it : to_remove) { m_data.erase(erase_it.get_iterator()); }
        return ret_val;
    }

    void sort() { m_data.sort(); }

    template<typename Compare>
    void sort(Compare comp) {
        m_data.sort(comp);
    }

    /**
     * @brief Returns reference to the underlying container
     * Useful for operations that should be done without notifications and for compability with older code
     *
     * @return wrapped_type& Reference to the underlying container
     */
    wrapped_type& container() { return m_data; }

    friend bool operator==(const cne::list<wrapped_type>& lhs, const cne::list<wrapped_type>& rhs) {
        return lhs.m_data == rhs.m_data;
    }

    friend bool operator!=(const cne::list<wrapped_type>& lhs, const cne::list<wrapped_type>& rhs) {
        return lhs.m_data != rhs.m_data;
    }

    friend bool operator<(const cne::list<wrapped_type>& lhs, const cne::list<wrapped_type>& rhs) {
        return lhs.m_data < rhs.m_data;
    }

    friend bool operator<=(const cne::list<wrapped_type>& lhs, const cne::list<wrapped_type>& rhs) {
        return lhs.m_data <= rhs.m_data;
    }

    friend bool operator>(const cne::list<wrapped_type>& lhs, const cne::list<wrapped_type>& rhs) {
        return lhs.m_data > rhs.m_data;
    }

    friend bool operator>=(const cne::list<wrapped_type>& lhs, const cne::list<wrapped_type>& rhs) {
        return lhs.m_data >= rhs.m_data;
    }

    friend bool swap(cne::list<wrapped_type>& lhs, cne::list<wrapped_type>& rhs) { return lhs.swap(rhs); }

private:
    iterator non_const_iterator(const_iterator it) { return *erase(it, it); }

    /**
     * @brief Erases last elements in containers to resize it to count
     * Notifies erase listeners
     *
     * @param count New size of container
     * @return true Notifications succeeded, erase happened
     * @return false Notifications failed, erase didn't happen
     */
    bool resize_erase(size_type count) {
        auto            it = cend();
        difference_type to_erase_cnt = size() - count;
        for (difference_type i = 0; i != to_erase_cnt; ++i, --it) {}

        bool res = base_class::erase_called(it, cend());
        if (res) { m_data.resize(count); }

        return res;
    }

    /**
     * @brief Erases last elements in containers to resize it to count
     * Notifies erase listeners
     *
     * @param count New size of container
     * @param value Needed to invoke the right overload of resize
     * @return true Notifications succeeded, erase happened
     * @return false Notifications failed, erase didn't happen
     */
    bool resize_erase(size_type count, const value_type& value) {
        auto            it = cend();
        difference_type to_erase_cnt = size() - count;
        for (difference_type i = 0; i != to_erase_cnt; ++i, --it) {}

        bool res = base_class::erase_called(it, cend());
        if (res) { m_data.resize(count, value); }

        return res;
    }

    /**
     * @brief Notifies insertion of count copies of value before pos
     * Notifies insert listeners
     *
     * @param pos Iterator before which the values will be inserted
     * @param count Number of copies
     * @param value Value to be inserted
     * @return true Notifications succeeded
     * @return false Notifications failed
     */
    bool notify_count_insert(const_iterator pos, size_type count, const value_type& value = value_type()) {
        return base_class::insert_called(count_ref_iterator{0, value}, count_ref_iterator{count, value},
                                         constant_iterator_wrapper{pos});
    }

    /**
     * @brief Transfers all elements from other to this before pos
     * Notifies insert listeners
     *
     * @param pos Iterator before which elements will be transfered
     * @param data Container to transfer elements from
     * @return iterator Iterator to first inserted element or end iterator if notifications failed
     */
    iterator p_insert_splice(const_iterator pos, wrapped_type& data) {
        if (data.size() == 0) { return non_const_iterator(pos); }
        // use this to create return value, the iterator doesn't get invalidated
        auto it = data.begin();

        if (base_class::insert_called(data.begin(), data.end(), constant_iterator_wrapper{pos})) {
            m_data.splice(pos.get_iterator(), data);
            return iterator(m_proxy, it);
        } else {
            return end();
        }
    }

    /**
     * @brief Perfect forwarding of value for insert
     * Notifies insert listeners
     *
     * @tparam V Only const value_type& or value_type&&
     * @param pos Iterator before which the values will be inserted
     * @param value Value to be forwarded
     * @return iterator Iterator to the inserted element or end iterator if notifications failed
     */
    template<typename V>
    iterator p_insert(const_iterator pos, V&& value) {
        if (!base_class::insert_called(value, pos)) { return end(); }

        return iterator(m_proxy, m_data.insert(pos.get_iterator(), std::forward<V>(value)));
    }

    /**
     * @brief Inserts elements from range [first, last) before pos
     * Notifies insert listeners
     *
     * @tparam BidirectionalIt Type of iterator to values, must satisfy at least BidirectionalIterator
     * @param pos Iterator before which the values will be inserted
     * @param first First iterator of range
     * @param last Last iterator of range
     * @return iterator Iterator to the first inserted element or end iterator if notifications failed
     */
    template<typename BidirectionalIt>
    iterator p_insert(const_iterator pos, BidirectionalIt first, BidirectionalIt last) {
        if (!base_class::insert_called(first, last, constant_iterator_wrapper{pos})) { return end(); }

        return iterator(m_proxy, m_data.insert(pos.get_iterator(), first, last));
    }

    /**
     * @brief Notifies replace and either insert or erase listeners depending on size of data
     *
     * @tparam Ty Type of container or range to assign from
     * Iterators of Ty must satisfy BidirectionalIterator
     * Iterators of Ty must be dereferenceable to value_type or type convertible to value_type
     *
     * @param data Container or range of elements to assign
     * @return true Notifications succeeded
     * @return false Notifications failed
     */
    template<typename Ty>
    bool notify_assign(const Ty& data) {
        auto it = cbegin();
        auto data_it = data.begin();
        bool res = true;

        for (size_type i = 0; i < data.size() && i < size(); ++i) {
            ++it;
            ++data_it;
        }

        if (!base_class::replace_called(iterator_wrapper{cbegin()}, data.begin(), data_it)) { return false; }

        try {
            if (size() > data.size()) {
                res = res && base_class::erase_called(it, cend());
            } else if (size() < data.size()) {
                res = res && base_class::insert_called(data_it, data.end(), constant_iterator_wrapper{cend()});
            }
        } catch (...) { base_class::replace_undo(iterator_wrapper{it}, data_it, data.begin()); }

        if (!res) { base_class::replace_undo(iterator_wrapper{it}, data_it, data.begin()); }
        return res;
    }

    /**
     * @brief Rollbacks notifications called by notify_assign
     * Iterators of Ty must satisfy RandomAccessIterator
     * Iterators of Ty must be dereferenceable to value_type or type convertible to value_type
     *
     * @tparam Ty Type of container or range to assign from
     * @param data Container or range of elements to assign
     */
    template<typename Ty>
    void notify_undo_assign(const Ty& data) {
        auto it = cbegin();
        auto data_it = data.begin();

        for (size_type i = 0; i < data.size() && i < size(); ++i) {
            ++it;
            ++data_it;
        }

        if (size() > data.size()) {
            base_class::erase_undo(it, cend());
        } else if (size() < data.size()) {
            base_class::insert_undo(data.end(), data_it, constant_iterator_wrapper{cend()});
        }

        base_class::replace_undo(iterator_wrapper{it}, data_it, data.begin());
    }

    void initialize_proxy() { m_proxy = std::make_shared<list*>(this); }

    shared_proxy m_proxy;
    wrapped_type m_data;
};
} // namespace cne

#endif // CNE_LIST_H
