#include "catch2.h"

#include <forward_list>
#include <initializer_list>
#include <forward_list>

#include "../headers/containers/forward_list.h"
#include "types.h"
#include "utils.h"

using namespace cne::notifications;
using namespace cne::tests;


TEST_CASE("forward_list.front", "[forward_list][front][element_access]") {
    using l = cne::forward_list<std::forward_list<int>>;

    l   test_list = {3, 5};
    int value;

    SECTION("Const") {
        const l& const_list = test_list;
        value = const_list.front();
        REQUIRE(value == 3);
    }

    SECTION("Non const") {
        value = test_list.front();
        REQUIRE(value == 3);
    }
}

TEST_CASE("forward_list.clear", "[forward_list][clear][modifiers][erase_notification]") {
    int size = 5;

    SECTION("Success") {
        using l = cne::forward_list<std::forward_list<default_constr_true>>;
        l test_list(size);

        ADD_ERASE_LISTENERS(test_list, l, default_constr_true)

        bool res = test_list.clear();
        REQUIRE(res == true);
        REQUIRE(test_list.container().begin() == test_list.container().end());
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        using l = cne::forward_list<std::forward_list<default_constr_false>>;
        l test_list(size);

        ADD_ERASE_LISTENERS(test_list, l, default_constr_false)

        bool res = test_list.clear();
        REQUIRE(res == false);
        REQUIRE(test_list.container().begin() != test_list.container().end());
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("forward_list.insert_after", "[forward_list][insert_after][modifiers][insert_notification]") {
    SECTION("Copy insertable") {
        using l = cne::forward_list<std::forward_list<copy_constr>>;
        l test_list;

        auto to_copy_s = copy_constr::make(true);
        auto to_copy_f = copy_constr::make(false);

        ADD_INSERT_LISTENERS(test_list, l, copy_constr)

        SECTION("const_iterator, const value_type&") {
            SECTION("Success") {
                auto res = test_list.insert_after(test_list.cbefore_begin(), to_copy_s);
                REQUIRE(res != test_list.end());
                REQUIRE(test_list.container().begin() != test_list.container().end());
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_list.insert_after(test_list.cbefore_begin(), to_copy_f);
                REQUIRE(res == test_list.end());
                REQUIRE(test_list.container().begin() == test_list.container().end());
                REQUIRE(insert_count == 0);
            }
        }
    }

    SECTION("Copy insertable and copy assignable") {
        using l = cne::forward_list<std::forward_list<copy_constr_assign>>;
        l test_list;

        auto to_copy_s = copy_constr_assign::make(true);
        auto to_copy_f = copy_constr_assign::make(false);

        ADD_INSERT_LISTENERS(test_list, l, copy_constr_assign)

        SECTION("const_iterator, size_type, const value_type&") {
            l::size_type count = 3;
            SECTION("Success") {
                auto res = test_list.insert_after(test_list.cbefore_begin(), count, to_copy_s);
                REQUIRE(res != test_list.end());
                REQUIRE(test_list.container().begin() != test_list.container().end());
                REQUIRE(insert_call_cnt == count);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_list.insert_after(test_list.cbefore_begin(), count, to_copy_f);
                REQUIRE(res == test_list.end());
                REQUIRE(test_list.container().begin() == test_list.container().end());
                REQUIRE(insert_count == 0);
            }
        }
    }

    SECTION("Move insertable") {
        using l = cne::forward_list<std::forward_list<move_constr>>;
        l test_list;

        ADD_INSERT_LISTENERS(test_list, l, move_constr)

        SECTION("const_iterator, value_type&&") {
            SECTION("Success") {
                auto res = test_list.insert_after(test_list.cbefore_begin(), move_constr::make(true));
                REQUIRE(res != test_list.end());
                REQUIRE(test_list.container().begin() != test_list.container().end());
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_list.insert_after(test_list.cbefore_begin(), move_constr::make(false));
                REQUIRE(res == test_list.end());
                REQUIRE(test_list.container().begin() == test_list.container().end());
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count == 0);
            }
        }
    }

    SECTION("Emplace constructible") {
        using l = cne::forward_list<std::forward_list<emplace_constr>>;
        l test_list;

        ADD_INSERT_LISTENERS(test_list, l, emplace_constr)

        SECTION("const_iterator, InputIt, InputIt") {
            using src_t = std::forward_list<bool>;
            src_t source;

            SECTION("Success") {
                source = {true, true, true};
                auto res = test_list.insert_after(test_list.cbefore_begin(), source.begin(), source.end());
                REQUIRE(res != test_list.end());
                REQUIRE(test_list.container().begin() != test_list.container().end());
                REQUIRE(insert_call_cnt == 3);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                source = {true, false, true};
                auto res = test_list.insert_after(test_list.cbefore_begin(), source.begin(), source.end());
                REQUIRE(res == test_list.end());
                REQUIRE(test_list.container().begin() == test_list.container().end());
                REQUIRE(insert_call_cnt == 2);
                REQUIRE(insert_count == 0);
            }
        }

        SECTION("initializer_list") {
            SECTION("Success") {
                auto res = test_list.insert_after(test_list.cbefore_begin(), {true, true, true});
                REQUIRE(res != test_list.end());
                REQUIRE(test_list.container().begin() != test_list.container().end());
                REQUIRE(insert_call_cnt == 3);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_list.insert_after(test_list.cbefore_begin(), {true, false, true});
                REQUIRE(res == test_list.end());
                REQUIRE(test_list.container().begin() == test_list.container().end());
                REQUIRE(insert_call_cnt == 2);
                REQUIRE(insert_count == 0);
            }
        }
    }
}

TEST_CASE("forward_list.emplace_after", "[forward_list][emplace_after][modifiers][insert_notification]") {
    using l = cne::forward_list<std::forward_list<emplace_constr>>;
    l test_list;

    ADD_INSERT_LISTENERS(test_list, l, emplace_constr)

    SECTION("Success") {
        auto res = test_list.emplace_after(test_list.cbefore_begin(), true);
        REQUIRE(res != test_list.end());
        REQUIRE(test_list.container().begin() != test_list.container().end());
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count != 0);
    }
    SECTION("Fail") {
        auto res = test_list.emplace_after(test_list.cbefore_begin(), false);
        REQUIRE(res == test_list.end());
        REQUIRE(test_list.container().begin() == test_list.container().end());
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count == 0);
    }
}

/**
 * @brief Can't be tested with minimal type requirements, because all insert operations impose more
 * requirements
 */
TEST_CASE("forward_list.erase_after", "[forward_list][erase_after][modifiers][erase_notification]") {
    using l = cne::forward_list<std::forward_list<move_constr_assign>>;
    l            test_list;
    l::size_type size = 4;

    ADD_ERASE_LISTENERS(test_list, l, move_constr_assign)

    SECTION("Success") {
        for (l::size_type i = 0; i < size; ++i) { test_list.container().emplace_front(move_constr_assign::make(true)); }

        SECTION("const_iterator") {
            auto res = test_list.erase_after(test_list.cbefore_begin());
            REQUIRE(res.has_value());
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count != 0);
        }

        SECTION("const_iterator, const_iterator") {
            l::size_type to_erase = 2;

            auto res = test_list.erase_after(test_list.cbefore_begin(), std::next(test_list.cbegin(), to_erase));
            REQUIRE(res.has_value());
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count != 0);
        }
    }

    SECTION("Fail") {
        int fail_pos = 2;
        for (l::size_type i = 0; i < size; ++i) {
            test_list.container().emplace_front(move_constr_assign::make(i != 1));
        }

        SECTION("const_iterator") {
            auto res = test_list.erase_after(std::next(test_list.cbefore_begin(), fail_pos));
            REQUIRE_FALSE(res.has_value());
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count == 0);
        }

        SECTION("const_iterator, const_iterator") {
            l::size_type to_erase = 2;
            auto         first_erase = std::next(test_list.cbefore_begin());

            auto res = test_list.erase_after(first_erase, std::next(first_erase, to_erase + 1));
            REQUIRE_FALSE(res.has_value());
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count == 0);
        }
    }
}

TEST_CASE("forward_list.push_front", "[forward_list][push_front][modifiers][insert_notification]") {
    SECTION("const value_type&") {
        using l = cne::forward_list<std::forward_list<copy_constr>>;
        l test_list;

        ADD_INSERT_LISTENERS(test_list, l, copy_constr);

        SECTION("Success") {
            auto to_copy = copy_constr::make(true);
            bool res = test_list.push_front(to_copy);
            REQUIRE(res);
            REQUIRE(test_list.container().begin() != test_list.container().end());
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count != 0);
        }

        SECTION("Fail") {
            auto to_copy = copy_constr::make(false);
            bool res = test_list.push_front(to_copy);
            REQUIRE_FALSE(res);
            REQUIRE(test_list.container().begin() == test_list.container().end());
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count == 0);
        }
    }

    SECTION("value_type&&") {
        using l = cne::forward_list<std::forward_list<move_constr>>;
        l test_list;

        ADD_INSERT_LISTENERS(test_list, l, move_constr);

        SECTION("Success") {
            bool res = test_list.push_front(move_constr::make(true));
            REQUIRE(res == true);
            REQUIRE(test_list.container().begin() != test_list.container().end());
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count != 0);
        }

        SECTION("Fail") {
            bool res = test_list.push_front(move_constr::make(false));
            REQUIRE_FALSE(res);
            REQUIRE(test_list.container().begin() == test_list.container().end());
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count == 0);
        }
    }
}

TEST_CASE("forward_list.emplace_front", "[forward_list][emplace_front][modifiers][insert_notification]") {
    using l = cne::forward_list<std::forward_list<emplace_constr>>;
    l test_list;

    ADD_INSERT_LISTENERS(test_list, l, emplace_constr);

    SECTION("Success") {
        auto res = test_list.emplace_front(true);
        REQUIRE(res.has_value());
        REQUIRE(res->data().get_value() == true);
        REQUIRE(test_list.container().begin() != test_list.container().end());
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count != 0);
    }

    SECTION("Fail") {
        auto res = test_list.emplace_front(false);
        REQUIRE_FALSE(res.has_value());
        REQUIRE(test_list.container().begin() == test_list.container().end());
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count == 0);
    }
}

/**
 * @brief Doesn't impose any type requirements other than Erasable.
 */
TEST_CASE("forward_list.pop_front", "[forward_list][pop_front][modifiers][erase_notification]") {
    using l = cne::forward_list<std::forward_list<no_restrictions>>;
    l test_list;

    ADD_ERASE_LISTENERS(test_list, l, no_restrictions);

    SECTION("Success") {
        test_list.container().emplace_front(true);
        bool res = test_list.pop_front();
        REQUIRE(res);
        REQUIRE(test_list.container().begin() == test_list.container().end());
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        test_list.container().emplace_front(false);
        bool res = test_list.pop_front();
        REQUIRE_FALSE(res);
        REQUIRE(test_list.container().begin() != test_list.container().end());
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("forward_list.resize", "[forward_list][resize][modifiers][insert_notification][erase_notification]") {
    SECTION("size_type") {
        SECTION("Success") {
            using l = cne::forward_list<std::forward_list<default_constr_true>>;
            l::size_type orig_size = 3;
            l            test_list(orig_size);

            SECTION("Enlarge") {
                ADD_INSERT_LISTENERS(test_list, l, default_constr_true)

                l::size_type new_size = 4;
                auto         res = test_list.resize(new_size);
                REQUIRE(res == true);
                REQUIRE(insert_call_cnt == (new_size - orig_size));
                REQUIRE(insert_count != 0);
            }

            SECTION("Shrink") {
                ADD_ERASE_LISTENERS(test_list, l, default_constr_true)

                l::size_type new_size = 2;
                auto         res = test_list.resize(new_size);
                REQUIRE(res == true);
                REQUIRE(erase_call_cnt == orig_size - new_size);
                REQUIRE(erase_count != 0);
            }
        }

        SECTION("Fail") {
            using l = cne::forward_list<std::forward_list<default_constr_false>>;
            l::size_type orig_size = 3;
            l            test_list(orig_size);

            SECTION("Enlarge") {
                ADD_INSERT_LISTENERS(test_list, l, default_constr_false)

                l::size_type new_size = 4;
                auto         res = test_list.resize(new_size);
                REQUIRE(res == false);
                REQUIRE(insert_count == 0);
            }

            SECTION("Shrink") {
                ADD_ERASE_LISTENERS(test_list, l, default_constr_false)

                l::size_type new_size = 2;
                auto         res = test_list.resize(new_size);
                REQUIRE(res == false);
                REQUIRE(erase_call_cnt == 1);
                REQUIRE(erase_count == 0);
            }
        }
    }

    SECTION("size_type, const value_type&") {
        using l = cne::forward_list<std::forward_list<copy_constr>>;
        l::size_type orig_size = 3;
        l            test_list;
        auto         to_copy_s = copy_constr::make(true);
        auto         to_copy_f = copy_constr::make(false);

        SECTION("Success") {
            test_list.container().resize(orig_size, to_copy_s);

            SECTION("Enlarge") {
                ADD_INSERT_LISTENERS(test_list, l, copy_constr)

                l::size_type new_size = 4;
                auto         res = test_list.resize(new_size, to_copy_s);
                REQUIRE(res == true);
                REQUIRE(insert_call_cnt == (new_size - orig_size));
                REQUIRE(insert_count != 0);
            }

            SECTION("Shrink") {
                ADD_ERASE_LISTENERS(test_list, l, copy_constr)

                l::size_type new_size = 2;
                auto         res = test_list.resize(new_size, to_copy_s);
                REQUIRE(res == true);
                REQUIRE(erase_call_cnt == orig_size - new_size);
                REQUIRE(erase_count != 0);
            }
        }

        SECTION("Fail") {
            test_list.container().resize(orig_size, to_copy_f);

            SECTION("Enlarge") {
                ADD_INSERT_LISTENERS(test_list, l, copy_constr)

                l::size_type new_size = 4;
                auto         res = test_list.resize(new_size, to_copy_f);
                REQUIRE(res == false);
                REQUIRE(insert_count == 0);
            }

            SECTION("Shrink") {
                ADD_ERASE_LISTENERS(test_list, l, copy_constr)

                l::size_type new_size = 2;
                auto         res = test_list.resize(new_size, to_copy_f);
                REQUIRE(res == false);
                REQUIRE(erase_call_cnt == 1);
                REQUIRE(erase_count == 0);
            }
        }
    }
}

TEST_CASE("forward_list.merge", "[forward_list][merge][operations][insert_notification][erase_notification]") {
    using l = cne::forward_list<std::forward_list<emplace_constr>>;
    l            dest;
    l::size_type dest_size = 3;
    l            src;
    l::size_type src_size = 5;
    for (l::size_type i = 0; i < dest_size; ++i) { dest.container().emplace_front(true); }

    ADD_INSERT_LISTENERS(dest, l, emplace_constr)
    ADD_ERASE_LISTENERS(src, l, emplace_constr)

    SECTION("Success") {
        for (l::size_type i = 0; i < src_size; ++i) { src.container().emplace_front(true); }

        bool res = dest.merge(src, std::greater<l::value_type>());
        REQUIRE(res == true);
        REQUIRE(src.container().begin() == src.container().end());
        REQUIRE(insert_count != 0);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        for (l::size_type i = 0; i < src_size; ++i) { src.container().emplace_front(false); }

        bool res = dest.merge(src, std::greater<l::value_type>());
        REQUIRE(res != true);
        REQUIRE(src.container().begin() != src.container().end());
        REQUIRE(insert_count == 0);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("forward_list.splice_after", "[forward_list][splice_after][operations][insert_notification][erase_notification]") {
    using l = cne::forward_list<std::forward_list<emplace_constr>>;
    l            dest;
    l::size_type dest_size = 3;
    l            src;
    l::size_type src_size = 5;
    for (l::size_type i = 0; i < dest_size; ++i) { dest.container().emplace_front(true); }

    ADD_INSERT_LISTENERS(dest, l, emplace_constr)
    ADD_ERASE_LISTENERS(src, l, emplace_constr)

    SECTION("Success") {
        for (l::size_type i = 0; i < src_size; ++i) { src.container().emplace_front(true); }

        bool res = dest.splice_after(dest.cbefore_begin(), src);
        REQUIRE(res == true);
        REQUIRE(src.container().begin() == src.container().end());
        REQUIRE(insert_count != 0);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        for (l::size_type i = 0; i < src_size; ++i) { src.container().emplace_front(false); }

        bool res = dest.splice_after(dest.cbefore_begin(), src);
        REQUIRE(res != true);
        REQUIRE(src.container().begin() != src.container().end());
        REQUIRE(insert_count == 0);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("forward_list.remove", "[forward_list][remove][operations][erase_notification]") {
    using l = cne::forward_list<std::forward_list<emplace_constr>>;
    l            test_list;
    l::size_type count_t = 5;
    l::size_type count_f = 3;
    for (l::size_type i = 0; i < count_t; ++i) { test_list.container().emplace_front(true); }
    for (l::size_type i = 0; i < count_f; ++i) { test_list.container().emplace_front(false); }

    ADD_ERASE_LISTENERS(test_list, l, emplace_constr)

    SECTION("Success") {
        auto res = test_list.remove(emplace_constr(true));
        REQUIRE(res == count_t);
        REQUIRE(erase_count != 0);
	}

    SECTION("Fail") {
        auto res = test_list.remove(emplace_constr(false));
        REQUIRE(res == 0);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("forward_list.unique", "[forward_list][unique][operations][erase_notification]") {
    using l = cne::forward_list<std::forward_list<emplace_constr>>;
    l            test_list;
    l::size_type count = 3;

    ADD_ERASE_LISTENERS(test_list, l, emplace_constr)

    SECTION("Success") {
        for (l::size_type i = 0; i < count; ++i) { test_list.container().emplace_front(true); }

        auto res = test_list.unique();
        REQUIRE(res == 2);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        for (l::size_type i = 0; i < count; ++i) { test_list.container().emplace_front(false); }

        auto res = test_list.remove(emplace_constr(false));
        REQUIRE(res == 0);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("forward_list.swap", "[forward_list][swap][modifiers][insert_notification][erase_notification][replace_notification]") {
    using l = cne::forward_list<std::forward_list<no_restrictions>>;
    l            src;
    l::size_type src_size = 5;
    l            dest;
    l::size_type dest_size = 3;

    ADD_INSERT_LISTENERS(dest, l, no_restrictions)
    ADD_REPLACE_LISTENERS(dest, l, no_restrictions)
    ADD_ERASE_LISTENERS(src, l, no_restrictions)

    SECTION("Success") {
        src.container().insert_after(src.container().cbefore_begin(), src_size, true);
        dest.container().insert_after(dest.container().cbefore_begin(), dest_size, true);

        bool res = dest.swap(src);
        REQUIRE(res == true);
        REQUIRE(insert_count != 0);
        REQUIRE(replace_count != 0);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        src.container().insert_after(src.container().cbefore_begin(), src_size, false);
        dest.container().insert_after(dest.container().cbefore_begin(), dest_size, true);

        bool res = dest.swap(src);
        REQUIRE(res == false);
        REQUIRE(insert_count == 0);
        REQUIRE(replace_count == 0);
        REQUIRE(erase_count == 0);
    }
}
