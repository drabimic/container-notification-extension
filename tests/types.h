#pragma once
#include <cstdint>
#include <functional>

#include "../headers/notifications/notifications.h"

using namespace cne::notifications;

namespace cne { namespace tests {
    class base
    {
    public:
        base(bool b) : m_val(b) {
            static int64_t counter = 1;
            m_cnt = counter++;
        }

        bool&    get_value() { return m_val; }
        bool     get_value() const { return m_val; }
        int64_t& get_count() { return m_cnt; }
        int64_t  get_count() const { return m_cnt; }

        friend bool operator<(const base& lhs, const base& rhs) { return lhs.m_cnt < rhs.m_cnt; }
        friend bool operator>(const base& lhs, const base& rhs) { return lhs.m_cnt > rhs.m_cnt; }
        friend bool operator==(const base& lhs, const base& rhs) { return lhs.m_val == rhs.m_val; }

    private:
        bool    m_val;
        int64_t m_cnt;
    };

    class copy_constr : public base
    {
    public:
        copy_constr(const copy_constr&) = default;
        copy_constr& operator=(const copy_constr&) = delete;

        static copy_constr make(bool b) { return copy_constr(b); }

    private:
        copy_constr(bool b) : base(b) {}
    };

    class copy_constr_assign : public base
    {
    public:
        copy_constr_assign(const copy_constr_assign&) = default;
        copy_constr_assign& operator=(const copy_constr_assign&) = default;

        static copy_constr_assign make(bool b) { return copy_constr_assign(b); }

    private:
        copy_constr_assign(bool b) : base(b) {}
    };

    class move_constr : public base
    {
    public:
        move_constr(move_constr&&) = default;
        move_constr& operator=(move_constr&&) = delete;

        move_constr(const move_constr&) = delete;
        move_constr& operator=(const move_constr&) = delete;

        static move_constr make(bool b) { return move_constr(b); }

    private:
        move_constr(bool b) : base(b) {}
    };

    class move_constr_emplace : public base
    {
    public:
        move_constr_emplace(bool b) : base(b) {}
        move_constr_emplace(const move_constr_emplace&) = delete;
        move_constr_emplace& operator=(const move_constr_emplace&) = delete;

        move_constr_emplace(move_constr_emplace&&) = default;
        move_constr_emplace& operator=(move_constr_emplace&&) = delete;
    };

    class move_constr_assign : public base
    {
    public:
        move_constr_assign(move_constr_assign&&) = default;
        move_constr_assign& operator=(move_constr_assign&&) = default;

        move_constr_assign(const move_constr_assign&) = delete;
        move_constr_assign& operator=(const move_constr_assign&) = delete;

        static move_constr_assign make(bool b) { return move_constr_assign(b); }

    private:
        move_constr_assign(bool b) : base(b) {}
    };

    class move_constr_assign_emplace : public base
    {
    public:
        move_constr_assign_emplace(bool b) : base(b) {}
        move_constr_assign_emplace(const move_constr_assign_emplace&) = default;
        move_constr_assign_emplace& operator=(const move_constr_assign_emplace&) = delete;

        move_constr_assign_emplace(move_constr_assign_emplace&&) = default;
        move_constr_assign_emplace& operator=(move_constr_assign_emplace&&) = default;
    };

    class move_default_constr_true : public base
    {
    public:
        move_default_constr_true() : base(true) {}

        move_default_constr_true(move_default_constr_true&&) = default;
        move_default_constr_true& operator=(move_default_constr_true&&) = delete;

        move_default_constr_true(const move_default_constr_true&) = delete;
        move_default_constr_true& operator=(const move_default_constr_true&) = delete;
    };

    class move_default_constr_false : public base
    {
    public:
        move_default_constr_false() : base(false) {}

        move_default_constr_false(move_default_constr_false&&) = default;
        move_default_constr_false& operator=(move_default_constr_false&&) = delete;

        move_default_constr_false(const move_default_constr_false&) = delete;
        move_default_constr_false& operator=(const move_default_constr_false&) = delete;
    };

    class default_constr_true : public base
    {
    public:
        default_constr_true() : base(true) {}

        default_constr_true(const default_constr_true&) = delete;
        default_constr_true(default_constr_true&&) = delete;

        default_constr_true& operator=(const default_constr_true&) = delete;
        default_constr_true& operator=(default_constr_true&&) = delete;
    };

    class default_constr_false : public base
    {
    public:
        default_constr_false() : base(false) {}

        default_constr_false(const default_constr_false&) = delete;
        default_constr_false(default_constr_false&&) = delete;

        default_constr_false& operator=(const default_constr_false&) = delete;
        default_constr_false& operator=(default_constr_false&&) = delete;
    };

    class emplace_constr : public base
    {
    public:
        emplace_constr(bool b) : base(b) {}
        emplace_constr(const emplace_constr&) = default;
        emplace_constr(emplace_constr&&) = delete;

        emplace_constr& operator=(const emplace_constr&) = delete;
        emplace_constr& operator=(emplace_constr&&) = delete;
    };

    class no_restrictions : public base
    {
    public:
        no_restrictions(bool b) : base(b) {}
        static no_restrictions make(bool b) { return no_restrictions(b); }
    };

}} // namespace cne::tests
