#pragma once

#define ADD_INSERT_LISTENERS(CONTAINER, CONT_TYPE, TYPE)                                               \
    int64_t insert_count = 0;                                                                          \
    int64_t insert_call_cnt = 0;                                                                       \
    CONTAINER.insert_check(                                                                            \
      [&insert_count](const CONT_TYPE&, const TYPE& x, auto&&...) {                                    \
          insert_count += x.get_count();                                                               \
          return true;                                                                                 \
      },                                                                                               \
      [&insert_count](const CONT_TYPE&, const TYPE& x, auto&&...) { insert_count -= x.get_count(); }); \
                                                                                                       \
    CONTAINER.insert_check(                                                                            \
      [&insert_count, &insert_call_cnt](const CONT_TYPE&, const TYPE& x, auto&&...) {                  \
          if (x.get_value()) { insert_count += x.get_count(); }                                        \
          ++insert_call_cnt;                                                                           \
          return x.get_value();                                                                        \
      },                                                                                               \
      [&insert_count](const CONT_TYPE&, const TYPE& x, auto&&...) { insert_count -= x.get_count(); });

#define ADD_REPLACE_LISTENERS(CONTAINER, CONT_TYPE, TYPE)                                                        \
    int64_t replace_count = 0;                                                                                   \
    int64_t replace_call_cnt = 0;                                                                                \
    CONTAINER.replace_check(                                                                                     \
      [&replace_count](const CONT_TYPE&, typename CONT_TYPE::const_iterator, const TYPE& x) {                    \
          replace_count += x.get_count();                                                                        \
          return true;                                                                                           \
      },                                                                                                         \
      [&replace_count](const CONT_TYPE&, typename CONT_TYPE::const_iterator, const TYPE& x) {                    \
          replace_count -= x.get_count();                                                                        \
      });                                                                                                        \
                                                                                                                 \
    CONTAINER.replace_check(                                                                                     \
      [&replace_count, &replace_call_cnt](const CONT_TYPE&, typename CONT_TYPE::const_iterator, const TYPE& x) { \
          if (x.get_value()) { replace_count += x.get_count(); }                                                 \
          ++replace_call_cnt;                                                                                    \
          return x.get_value();                                                                                  \
      },                                                                                                         \
      [&replace_count](const CONT_TYPE&, typename CONT_TYPE::const_iterator, const TYPE& x) {                    \
          replace_count -= x.get_count();                                                                        \
      });

#define ADD_ERASE_LISTENERS(CONTAINER, CONT_TYPE, TYPE)                                           \
    int64_t erase_count = 0;                                                                      \
    int64_t erase_call_cnt = 0;                                                                   \
    CONTAINER.erase_check(                                                                        \
      [&erase_count](const CONT_TYPE&, typename CONT_TYPE::const_iterator first,                  \
                     typename CONT_TYPE::const_iterator last) {                                   \
          while (first != last) {                                                                 \
              erase_count += static_cast<const TYPE&>(*first).get_count();                        \
              ++first;                                                                            \
          }                                                                                       \
                                                                                                  \
          return true;                                                                            \
      },                                                                                          \
      [&erase_count](const CONT_TYPE&, typename CONT_TYPE::const_iterator first,                  \
                     typename CONT_TYPE::const_iterator last) {                                   \
          while (first != last) {                                                                 \
              erase_count -= static_cast<const TYPE&>(*first).get_count();                        \
              ++first;                                                                            \
          }                                                                                       \
      });                                                                                         \
                                                                                                  \
    CONTAINER.erase_check(                                                                        \
      [&erase_count, &erase_call_cnt](const CONT_TYPE&, typename CONT_TYPE::const_iterator first, \
                                      typename CONT_TYPE::const_iterator last) {                  \
          int64_t tmp_cnt = erase_count;                                                          \
          ++erase_call_cnt;                                                                       \
          while (first != last) {                                                                 \
              if (false == static_cast<const TYPE&>(*first).get_value()) {                        \
                  erase_count = tmp_cnt;                                                          \
                  return false;                                                                   \
              }                                                                                   \
                                                                                                  \
              erase_count += static_cast<const TYPE&>(*first).get_count();                        \
              ++first;                                                                            \
          }                                                                                       \
          return true;                                                                            \
      },                                                                                          \
      [&erase_count](const CONT_TYPE&, typename CONT_TYPE::const_iterator first,                  \
                     typename CONT_TYPE::const_iterator last) {                                   \
          while (first != last) {                                                                 \
              erase_count -= static_cast<const TYPE&>(*first).get_count();                        \
              ++first;                                                                            \
          }                                                                                       \
      });

#define ADD_MAP_INSERT_LISTENERS(CONTAINER, CONT_TYPE, TYPE)                                                 \
    int64_t insert_count = 0;                                                                                \
    int64_t insert_call_cnt = 0;                                                                             \
    CONTAINER.insert_check(                                                                                  \
      [&insert_count](const CONT_TYPE&, const TYPE& x, auto&&...) {                                          \
          insert_count += x.first.get_count();                                                               \
          return true;                                                                                       \
      },                                                                                                     \
      [&insert_count](const CONT_TYPE&, const TYPE& x, auto&&...) { insert_count -= x.first.get_count(); }); \
                                                                                                             \
    CONTAINER.insert_check(                                                                                  \
      [&insert_count, &insert_call_cnt](const CONT_TYPE&, const TYPE& x, auto&&...) {                        \
          if (x.first.get_value()) { insert_count += x.first.get_count(); }                                  \
          ++insert_call_cnt;                                                                                 \
          return x.first.get_value();                                                                        \
      },                                                                                                     \
      [&insert_count](const CONT_TYPE&, const TYPE& x, auto&&...) { insert_count -= x.first.get_count(); });

#define ADD_MAP_ERASE_LISTENERS(CONTAINER, CONT_TYPE, TYPE)                                       \
    int64_t erase_count = 0;                                                                      \
    int64_t erase_call_cnt = 0;                                                                   \
    CONTAINER.erase_check(                                                                        \
      [&erase_count](const CONT_TYPE&, typename CONT_TYPE::const_iterator first,                  \
                     typename CONT_TYPE::const_iterator last) {                                   \
          while (first != last) {                                                                 \
              erase_count += static_cast<const TYPE&>(*first).first.get_count();                  \
              ++first;                                                                            \
          }                                                                                       \
                                                                                                  \
          return true;                                                                            \
      },                                                                                          \
      [&erase_count](const CONT_TYPE&, typename CONT_TYPE::const_iterator first,                  \
                     typename CONT_TYPE::const_iterator last) {                                   \
          while (first != last) {                                                                 \
              erase_count -= static_cast<const TYPE&>(*first).first.get_count();                  \
              ++first;                                                                            \
          }                                                                                       \
      });                                                                                         \
                                                                                                  \
    CONTAINER.erase_check(                                                                        \
      [&erase_count, &erase_call_cnt](const CONT_TYPE&, typename CONT_TYPE::const_iterator first, \
                                      typename CONT_TYPE::const_iterator last) {                  \
          int64_t tmp_cnt = erase_count;                                                          \
          ++erase_call_cnt;                                                                       \
          while (first != last) {                                                                 \
              if (false == static_cast<const TYPE&>(*first).first.get_value()) {                  \
                  erase_count = tmp_cnt;                                                          \
                  return false;                                                                   \
              }                                                                                   \
                                                                                                  \
              erase_count += static_cast<const TYPE&>(*first).first.get_count();                  \
              ++first;                                                                            \
          }                                                                                       \
          return true;                                                                            \
      },                                                                                          \
      [&erase_count](const CONT_TYPE&, typename CONT_TYPE::const_iterator first,                  \
                     typename CONT_TYPE::const_iterator last) {                                   \
          while (first != last) {                                                                 \
              erase_count -= static_cast<const TYPE&>(*first).first.get_count();                  \
              ++first;                                                                            \
          }                                                                                       \
      });
