#include "catch2.h"

#include <forward_list>
#include <initializer_list>
#include <deque>

#include "../headers/containers/deque.h"
#include "types.h"
#include "utils.h"

using namespace cne::notifications;
using namespace cne::tests;

TEST_CASE("deque.operator[]", "[deque][operator[]][element_access]") {
    using d = cne::deque<std::deque<int>>;

    d   test_deque = {3, 5};
    int value;

    SECTION("Const") {
        const d& const_deque = test_deque;
        value = const_deque.at(0);
        REQUIRE(value == 3);
    }

    SECTION("Non const") {
        value = test_deque.at(0);
        REQUIRE(value == 3);
    }
}

TEST_CASE("deque.at", "[deque][at][element_access]") {
    using d = cne::deque<std::deque<int>>;

    d   test_deque = {3, 5};
    int value;

    SECTION("Const") {
        const d& const_deque = test_deque;
        value = const_deque[0];
        REQUIRE(value == 3);
    }

    SECTION("Non const") {
        value = test_deque[0];
        REQUIRE(value == 3);
    }
}

TEST_CASE("deque.front", "[deque][front][element_access]") {
    using d = cne::deque<std::deque<int>>;

    d   test_deque = {3, 5};
    int value;

    SECTION("Const") {
        const d& const_deque = test_deque;
        value = const_deque.front();
        REQUIRE(value == 3);
    }

    SECTION("Non const") {
        value = test_deque.front();
        REQUIRE(value == 3);
    }
}

TEST_CASE("deque.back", "[deque][back][element_access]") {
    using d = cne::deque<std::deque<int>>;

    d   test_deque = {3, 5};
    int value;

    SECTION("Const") {
        const d& const_deque = test_deque;
        value = const_deque.back();
        REQUIRE(value == 5);
    }

    SECTION("Non const") {
        value = test_deque.back();
        REQUIRE(value == 5);
    }
}

TEST_CASE("deque.clear", "[deque][clear][modifiers][erase_notification]") {
    int size = 5;

    SECTION("Success") {
        using d = cne::deque<std::deque<default_constr_true>>;
        d test_deque(size);

        ADD_ERASE_LISTENERS(test_deque, d, default_constr_true)

        bool res = test_deque.clear();
        REQUIRE(res == true);
        REQUIRE(test_deque.size() == 0);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        using d = cne::deque<std::deque<default_constr_false>>;
        d test_deque(size);

        ADD_ERASE_LISTENERS(test_deque, d, default_constr_false)

        bool res = test_deque.clear();
        REQUIRE(res == false);
        REQUIRE(test_deque.size() == size);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("deque.insert", "[deque][insert][modifiers][insert_notification]") {
    SECTION("Copy assignable and copy insertable") {
        using d = cne::deque<std::deque<copy_constr_assign>>;
        d test_deque;

        auto to_copy_s = copy_constr_assign::make(true);
        auto to_copy_f = copy_constr_assign::make(false);

        ADD_INSERT_LISTENERS(test_deque, d, copy_constr_assign)

        SECTION("const_iterator, const value_type&") {
            SECTION("Success") {
                auto res = test_deque.insert(test_deque.cend(), to_copy_s);
                REQUIRE(res != test_deque.end());
                REQUIRE(test_deque.size() == 1);
                REQUIRE(insert_call_cnt == 1);
            }
            SECTION("Fail") {
                auto res = test_deque.insert(test_deque.cend(), to_copy_f);
                REQUIRE(res == test_deque.end());
                REQUIRE(test_deque.size() == 0);
                REQUIRE(insert_count == 0);
            }
        }

        SECTION("const_iterator, size_type, const value_type&") {
            d::size_type count = 3;
            SECTION("Success") {
                auto res = test_deque.insert(test_deque.cend(), count, to_copy_s);
                REQUIRE(res != test_deque.end());
                REQUIRE(test_deque.size() == count);
                REQUIRE(insert_call_cnt == count);
            }
            SECTION("Fail") {
                auto res = test_deque.insert(test_deque.cend(), count, to_copy_f);
                REQUIRE(res == test_deque.end());
                REQUIRE(test_deque.size() == 0);
                REQUIRE(insert_count == 0);
            }
        }
    }

    SECTION("Move assignable and move insertable") {
        using d = cne::deque<std::deque<move_constr_assign>>;
        d test_deque;

        ADD_INSERT_LISTENERS(test_deque, d, move_constr_assign)

        SECTION("const_iterator, value_type&&") {
            SECTION("Success") {
                auto res = test_deque.insert(test_deque.cend(), move_constr_assign::make(true));
                REQUIRE(res != test_deque.end());
                REQUIRE(test_deque.size() == 1);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_deque.insert(test_deque.cend(), move_constr_assign::make(false));
                REQUIRE(res == test_deque.end());
                REQUIRE(test_deque.size() == 0);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count == 0);
            }
        }
    }

    SECTION("Move assignable, move insertable and emplace constructible") {
        using d = cne::deque<std::deque<move_constr_assign_emplace>>;
        d test_deque;

        ADD_INSERT_LISTENERS(test_deque, d, move_constr_assign_emplace)

        SECTION("const_iterator, InputIt, InputIt") {
            using src_t = std::forward_list<bool>;
            src_t source;

            SECTION("Success") {
                source = {true, true, true};
                auto res = test_deque.insert(test_deque.cend(), source.begin(), source.end());
                REQUIRE(res != test_deque.end());
                REQUIRE(test_deque.size() == 3);
                REQUIRE(insert_call_cnt == 3);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                source = {true, false, true};
                auto res = test_deque.insert(test_deque.cend(), source.begin(), source.end());
                REQUIRE(res == test_deque.end());
                REQUIRE(test_deque.size() == 0);
                REQUIRE(insert_call_cnt == 2);
                REQUIRE(insert_count == 0);
            }
        }

#ifndef __GNUC__
        SECTION("initializer_list") {
            SECTION("Success") {
                auto res = test_deque.insert(test_deque.cend(), {true, true, true});
                REQUIRE(res != test_deque.end());
                REQUIRE(test_deque.size() == 3);
                REQUIRE(insert_call_cnt == 3);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_deque.insert(test_deque.cend(), {true, false, true});
                REQUIRE(res == test_deque.end());
                REQUIRE(test_deque.size() == 0);
                REQUIRE(insert_call_cnt == 2);
                REQUIRE(insert_count == 0);
            }
        }
#endif
    }
}

TEST_CASE("deque.emplace", "[deque][emplace][modifiers][insert_notification]") {
    using d = cne::deque<std::deque<move_constr_assign_emplace>>;
    d test_deque;

    ADD_INSERT_LISTENERS(test_deque, d, move_constr_assign_emplace)

    SECTION("Success") {
        auto res = test_deque.emplace(test_deque.begin(), true);
        REQUIRE(res != test_deque.end());
        REQUIRE(test_deque.size() == 1);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count != 0);
    }
    SECTION("Fail") {
        auto res = test_deque.emplace(test_deque.begin(), false);
        REQUIRE(res == test_deque.end());
        REQUIRE(test_deque.size() == 0);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count == 0);
    }
}

/**
 * @brief Can't be tested with minimal type requirements, because all insert operations impose more
 * requirements
 */
TEST_CASE("deque.erase", "[deque][erase][modifiers][erase_notification]") {
    using d = cne::deque<std::deque<move_constr_assign>>;
    d            test_deque;
    d::size_type size = 4;

    ADD_ERASE_LISTENERS(test_deque, d, move_constr_assign)

    SECTION("Success") {
        for (d::size_type i = 0; i < size; ++i) {
            test_deque.container().emplace_back(move_constr_assign::make(true));
        }

        SECTION("const_iterator") {
            auto res = test_deque.erase(test_deque.begin());
            REQUIRE(res.has_value());
            REQUIRE(test_deque.size() == (size - 1));
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count != 0);
        }

        SECTION("const_iterator, const_iterator") {
            d::size_type to_erase = 2;

            auto res = test_deque.erase(test_deque.begin(), test_deque.begin() + to_erase);
            REQUIRE(res.has_value());
            REQUIRE(test_deque.size() == (size - to_erase));
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count != 0);
        }
    }

    SECTION("Fail") {
        d::size_type fail_pos = 2;
        for (d::size_type i = 0; i < size; ++i) {
            test_deque.container().emplace_back(move_constr_assign::make(i != fail_pos));
        }

        SECTION("const_iterator") {
            auto res = test_deque.erase(test_deque.begin() + fail_pos);
            REQUIRE_FALSE(res.has_value());
            REQUIRE(test_deque.size() == size);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count == 0);
        }

        SECTION("const_iterator, const_iterator") {
            d::size_type to_erase = 2;
            auto         first_erase = test_deque.begin() + 1;

            auto res = test_deque.erase(first_erase, first_erase + to_erase);
            REQUIRE_FALSE(res.has_value());
            REQUIRE(test_deque.size() == size);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count == 0);
        }
    }
}

TEST_CASE("deque.push_front", "[deque][push_front][modifiers][insert_notification]") {
    SECTION("const value_type&") {
        using d = cne::deque<std::deque<copy_constr>>;
        d test_deque;

        ADD_INSERT_LISTENERS(test_deque, d, copy_constr);

        SECTION("Success") {
            auto to_copy = copy_constr::make(true);
            bool res = test_deque.push_front(to_copy);
            REQUIRE(res);
            REQUIRE(test_deque.size() == 1);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count != 0);
        }

        SECTION("Fail") {
            auto to_copy = copy_constr::make(false);
            bool res = test_deque.push_front(to_copy);
            REQUIRE_FALSE(res);
            REQUIRE(test_deque.size() == 0);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count == 0);
        }
    }

    SECTION("value_type&&") {
        using d = cne::deque<std::deque<move_constr>>;
        d test_deque;

        ADD_INSERT_LISTENERS(test_deque, d, move_constr);

        SECTION("Success") {
            bool res = test_deque.push_front(move_constr::make(true));
            REQUIRE(res == true);
            REQUIRE(test_deque.size() == 1);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count != 0);
        }

        SECTION("Fail") {
            bool res = test_deque.push_front(move_constr::make(false));
            REQUIRE_FALSE(res);
            REQUIRE(test_deque.size() == 0);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count == 0);
        }
    }
}

TEST_CASE("deque.emplace_front", "[deque][emplace_front][modifiers][insert_notification]") {
    using d = cne::deque<std::deque<move_constr_emplace>>;
    d test_deque;

    ADD_INSERT_LISTENERS(test_deque, d, move_constr_emplace);

    SECTION("Success") {
        auto res = test_deque.emplace_front(true);
        REQUIRE(res.has_value());
        REQUIRE(res->data().get_value() == true);
        REQUIRE(test_deque.size() == 1);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count != 0);
    }

    SECTION("Fail") {
        auto res = test_deque.emplace_front(false);
        REQUIRE_FALSE(res.has_value());
        REQUIRE(test_deque.size() == 0);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count == 0);
    }
}

/**
 * @brief Doesn't impose any type requirements other than Erasable.
 */
TEST_CASE("deque.pop_front", "[deque][pop_front][modifiers][erase_notification]") {
    using d = cne::deque<std::deque<no_restrictions>>;
    d test_deque;

    ADD_ERASE_LISTENERS(test_deque, d, no_restrictions);

    SECTION("Success") {
        test_deque.container().emplace_back(true);
        bool res = test_deque.pop_front();
        REQUIRE(res);
        REQUIRE(test_deque.size() == 0);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        test_deque.container().emplace_back(false);
        bool res = test_deque.pop_front();
        REQUIRE_FALSE(res);
        REQUIRE(test_deque.size() == 1);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("deque.push_back", "[deque][push_back][modifiers][insert_notification]") {
    SECTION("const value_type&") {
        using d = cne::deque<std::deque<copy_constr>>;
        d test_deque;

        ADD_INSERT_LISTENERS(test_deque, d, copy_constr);

        SECTION("Success") {
            auto to_copy = copy_constr::make(true);
            bool res = test_deque.push_back(to_copy);
            REQUIRE(res);
            REQUIRE(test_deque.size() == 1);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count != 0);
        }

        SECTION("Fail") {
            auto to_copy = copy_constr::make(false);
            bool res = test_deque.push_back(to_copy);
            REQUIRE_FALSE(res);
            REQUIRE(test_deque.size() == 0);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count == 0);
        }
    }

    SECTION("value_type&&") {
        using d = cne::deque<std::deque<move_constr>>;
        d test_deque;

        ADD_INSERT_LISTENERS(test_deque, d, move_constr);

        SECTION("Success") {
            bool res = test_deque.push_back(move_constr::make(true));
            REQUIRE(res == true);
            REQUIRE(test_deque.size() == 1);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count != 0);
        }

        SECTION("Fail") {
            bool res = test_deque.push_back(move_constr::make(false));
            REQUIRE_FALSE(res);
            REQUIRE(test_deque.size() == 0);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count == 0);
        }
    }
}

TEST_CASE("deque.emplace_back", "[deque][emplace_back][modifiers][insert_notification]") {
    using d = cne::deque<std::deque<move_constr_emplace>>;
    d test_deque;

    ADD_INSERT_LISTENERS(test_deque, d, move_constr_emplace);

    SECTION("Success") {
        auto res = test_deque.emplace_back(true);
        REQUIRE(res.has_value());
        REQUIRE(res->data().get_value() == true);
        REQUIRE(test_deque.size() == 1);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count != 0);
    }

    SECTION("Fail") {
        auto res = test_deque.emplace_back(false);
        REQUIRE_FALSE(res.has_value());
        REQUIRE(test_deque.size() == 0);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count == 0);
    }
}

/**
 * @brief Doesn't impose any type requirements other than Erasable.
 */
TEST_CASE("deque.pop_back", "[deque][pop_back][modifiers][erase_notification]") {
    using d = cne::deque<std::deque<no_restrictions>>;
    d test_deque;

    ADD_ERASE_LISTENERS(test_deque, d, no_restrictions);

    SECTION("Success") {
        test_deque.container().emplace_back(true);
        bool res = test_deque.pop_back();
        REQUIRE(res);
        REQUIRE(test_deque.size() == 0);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        test_deque.container().emplace_back(false);
        bool res = test_deque.pop_back();
        REQUIRE_FALSE(res);
        REQUIRE(test_deque.size() == 1);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("deque.resize", "[deque][resize][modifiers][insert_notification][erase_notification]") {
    SECTION("size_type") {
        SECTION("Success") {
            using d = cne::deque<std::deque<move_default_constr_true>>;
            d::size_type orig_size = 3;
            d            test_deque(orig_size);

            SECTION("Enlarge") {
                ADD_INSERT_LISTENERS(test_deque, d, move_default_constr_true)

                d::size_type new_size = 4;
                auto         res = test_deque.resize(new_size);
                REQUIRE(res == true);
                REQUIRE(test_deque.size() == new_size);
                REQUIRE(insert_call_cnt == (new_size - orig_size));
                REQUIRE(insert_count != 0);
            }

            SECTION("Shrink") {
                ADD_ERASE_LISTENERS(test_deque, d, move_default_constr_true)

                d::size_type new_size = 2;
                auto         res = test_deque.resize(new_size);
                REQUIRE(res == true);
                REQUIRE(test_deque.size() == new_size);
                REQUIRE(erase_call_cnt == orig_size - new_size);
                REQUIRE(erase_count != 0);
            }
        }

        SECTION("Fail") {
            using d = cne::deque<std::deque<move_default_constr_false>>;
            d::size_type orig_size = 3;
            d            test_deque(orig_size);

            SECTION("Enlarge") {
                ADD_INSERT_LISTENERS(test_deque, d, move_default_constr_false)

                d::size_type new_size = 4;
                auto         res = test_deque.resize(new_size);
                REQUIRE(res == false);
                REQUIRE(test_deque.size() == orig_size);
                REQUIRE(insert_count == 0);
            }

            SECTION("Shrink") {
                ADD_ERASE_LISTENERS(test_deque, d, move_default_constr_false)

                d::size_type new_size = 2;
                auto         res = test_deque.resize(new_size);
                REQUIRE(res == false);
                REQUIRE(test_deque.size() == orig_size);
                REQUIRE(erase_call_cnt == 1);
                REQUIRE(erase_count == 0);
            }
        }
    }

#ifndef __GNUC__
    SECTION("size_type, const value_type&") {
        using d = cne::deque<std::deque<copy_constr>>;
        d::size_type orig_size = 3;
        d            test_deque;
        auto         to_copy_s = copy_constr::make(true);
        auto         to_copy_f = copy_constr::make(false);

        SECTION("Success") {
            test_deque.container().resize(orig_size, to_copy_s);

            SECTION("Enlarge") {
                ADD_INSERT_LISTENERS(test_deque, d, copy_constr)

                d::size_type new_size = 4;
                auto         res = test_deque.resize(new_size, to_copy_s);
                REQUIRE(res == true);
                REQUIRE(test_deque.size() == new_size);
                REQUIRE(insert_call_cnt == (new_size - orig_size));
                REQUIRE(insert_count != 0);
            }

            SECTION("Shrink") {
                ADD_ERASE_LISTENERS(test_deque, d, copy_constr)

                d::size_type new_size = 2;
                auto         res = test_deque.resize(new_size, to_copy_s);
                REQUIRE(res == true);
                REQUIRE(test_deque.size() == new_size);
                REQUIRE(erase_call_cnt == orig_size - new_size);
                REQUIRE(erase_count != 0);
            }
        }

        SECTION("Fail") {
            test_deque.container().resize(orig_size, to_copy_f);

            SECTION("Enlarge") {
                ADD_INSERT_LISTENERS(test_deque, d, copy_constr)

                d::size_type new_size = 4;
                auto         res = test_deque.resize(new_size, to_copy_f);
                REQUIRE(res == false);
                REQUIRE(test_deque.size() == orig_size);
                REQUIRE(insert_count == 0);
            }

            SECTION("Shrink") {
                ADD_ERASE_LISTENERS(test_deque, d, copy_constr)

                d::size_type new_size = 2;
                auto         res = test_deque.resize(new_size, to_copy_f);
                REQUIRE(res == false);
                REQUIRE(test_deque.size() == orig_size);
                REQUIRE(erase_call_cnt == 1);
                REQUIRE(erase_count == 0);
            }
        }
    }
#endif
}

TEST_CASE("deque.swap", "[deque][swap][modifiers][insert_notification][erase_notification][replace_notification]") {
    using d = cne::deque<std::deque<no_restrictions>>;
    d src;
    d::size_type src_size = 5;
    d            dest;
    d::size_type dest_size = 3;

	ADD_INSERT_LISTENERS(dest, d, no_restrictions)
    ADD_REPLACE_LISTENERS(dest, d, no_restrictions)
    ADD_ERASE_LISTENERS(src, d, no_restrictions)

    SECTION("Success") {
        src.container().insert(src.container().cend(), src_size, true);
        dest.container().insert(dest.container().cend(), dest_size, true);

		bool res = dest.swap(src);
        REQUIRE(res == true);
        REQUIRE(src.size() == dest_size);
        REQUIRE(dest.size() == src_size);
        REQUIRE(insert_count != 0);
        REQUIRE(replace_count != 0);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        src.container().insert(src.container().cend(), src_size, false);
        dest.container().insert(dest.container().cend(), dest_size, true);

        bool res = dest.swap(src);
        REQUIRE(res != true);
        REQUIRE(src.size() == src_size);
        REQUIRE(dest.size() == dest_size);
        REQUIRE(insert_count == 0);
        REQUIRE(replace_count == 0);
        REQUIRE(erase_count == 0);
    }
}