#include "catch2.h"

#include <forward_list>
#include <initializer_list>
#include <map>

#include "../headers/containers/map.h"
#include "types.h"
#include "utils.h"

using namespace cne::notifications;
using namespace cne::tests;

/**
 * @brief Can't be tested with minimal type requirements, because all insert operations impose more
 * requirements
 */
TEST_CASE("multimap.extract", "[multimap][extract][modifiers][erase_notification]") {
    using m = cne::multimap<std::multimap<move_constr_assign, move_constr_assign>>;
    m            test_map;
    m::size_type size = 4;

    ADD_MAP_ERASE_LISTENERS(test_map, m, typename m::value_type)

    SECTION("Success") {
        for (m::size_type i = 0; i < size; ++i) {
            test_map.container().emplace(move_constr_assign::make(true), move_constr_assign::make(true));
        }

        SECTION("const_iterator") {
            auto res = test_map.extract(test_map.begin());
            REQUIRE(!res.empty());
            REQUIRE(test_map.size() == (size - 1));
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count != 0);
        }

        SECTION("const key_type&") {
            auto res = test_map.extract(test_map.begin()->first);
            REQUIRE(res.has_value());
            REQUIRE(test_map.size() == (size - 1));
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count != 0);
        }
    }

    SECTION("Fail") {
        m::size_type fail_pos = 0;
        for (m::size_type i = 0; i < size; ++i) {
            test_map.container().emplace(move_constr_assign::make(i != fail_pos),
                                         move_constr_assign::make(i != fail_pos));
        }

        SECTION("const_iterator") {
            auto res = test_map.extract(test_map.begin());
            REQUIRE(res.empty());
            REQUIRE(test_map.size() == size);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count == 0);
        }

        SECTION("const key_type&") {
            test_map.container().emplace(move_constr_assign::make(false), move_constr_assign::make(false));
            auto res = test_map.extract(test_map.begin()->first);
            REQUIRE(!res.has_value());
            REQUIRE(test_map.size() == size + 1);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count == 0);
        }
    }
}

TEST_CASE("multimap.merge", "[multimap][merge][operations][insert_notification][erase_notification]") {
    using m = cne::multimap<std::multimap<emplace_constr, emplace_constr>>;
    m            dest;
    m::size_type dest_size = 3;
    m            src;
    m::size_type src_size = 5;
    for (m::size_type i = 0; i < dest_size; ++i) { dest.container().emplace(true, true); }

    ADD_MAP_INSERT_LISTENERS(dest, m, typename m::value_type)
    ADD_MAP_ERASE_LISTENERS(src, m, typename m::value_type)

    SECTION("Success") {
        for (m::size_type i = 0; i < src_size; ++i) { src.container().emplace(true, true); }

        bool res = dest.merge(src);
        REQUIRE(res == true);
        REQUIRE(src.size() == 0);
        REQUIRE(dest.size() == (dest_size + src_size));
        REQUIRE(insert_count != 0);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        for (m::size_type i = 0; i < src_size; ++i) { src.container().emplace(false, false); }

        bool res = dest.merge(src);
        REQUIRE(res != true);
        REQUIRE(src.size() == src_size);
        REQUIRE(dest.size() == dest_size);
        REQUIRE(insert_count == 0);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("multimap.swap", "[multimap][swap][modifiers][insert_notification][erase_notification][replace_notification]") {
    using m = cne::multimap<std::multimap<no_restrictions, no_restrictions>>;
    m            src;
    m::size_type src_size = 5;
    m            dest;
    m::size_type dest_size = 3;

    ADD_MAP_INSERT_LISTENERS(dest, m, typename m::value_type)
    ADD_REPLACE_LISTENERS(dest, m, no_restrictions)
    ADD_MAP_ERASE_LISTENERS(src, m, typename m::value_type)

    SECTION("Success") {
        for (m::size_type i = 0; i < src_size; ++i) { src.container().emplace(true, true); }
        for (m::size_type i = 0; i < dest_size; ++i) { dest.container().emplace(true, true); }

        bool res = dest.swap(src);
        REQUIRE(res == true);
        REQUIRE(src.size() == dest_size);
        REQUIRE(dest.size() == src_size);
        REQUIRE(insert_count != 0);
        REQUIRE(replace_count == 0);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        for (m::size_type i = 0; i < src_size; ++i) { src.container().emplace(false, false); }
        for (m::size_type i = 0; i < dest_size; ++i) { dest.container().emplace(true, true); }

        bool res = dest.swap(src);
        REQUIRE(res != true);
        REQUIRE(src.size() == src_size);
        REQUIRE(dest.size() == dest_size);
        REQUIRE(insert_count == 0);
        REQUIRE(replace_count == 0);
        REQUIRE(erase_count == 0);
    }
}