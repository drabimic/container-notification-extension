#include "catch2.h"

#include <forward_list>
#include <initializer_list>
#include <set>

#include "../headers/containers/set.h"
#include "types.h"
#include "utils.h"

using namespace cne::notifications;
using namespace cne::tests;

/**
 * @brief Can't be tested with minimal type requirements, because all insert operations impose more
 * requirements
 */
TEST_CASE("set.extract", "[set][extract][modifiers][erase_notification]") {
    using s = cne::set<std::set<move_constr_assign>>;
    s            test_set;
    s::size_type size = 4;

    ADD_ERASE_LISTENERS(test_set, s, move_constr_assign)

    SECTION("Success") {
        for (s::size_type i = 0; i < size; ++i) { test_set.container().emplace(move_constr_assign::make(true)); }

        SECTION("const_iterator") {
            auto res = test_set.extract(test_set.begin());
            REQUIRE(!res.empty());
            REQUIRE(test_set.size() == (size - 1));
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count != 0);
        }

        SECTION("const key_type&") {
            auto res = test_set.extract(*test_set.begin());
            REQUIRE(res.has_value());
            REQUIRE(test_set.size() == (size - 1));
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count != 0);
        }
    }

    SECTION("Fail") {
        s::size_type fail_pos = 0;
        for (s::size_type i = 0; i < size; ++i) {
            test_set.container().emplace(move_constr_assign::make(i != fail_pos));
        }

        SECTION("const_iterator") {
            auto res = test_set.extract(test_set.begin());
            REQUIRE(res.empty());
            REQUIRE(test_set.size() == size);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count == 0);
        }

        SECTION("const key_type&") {
            test_set.container().emplace(move_constr_assign::make(false));
            auto res = test_set.extract(*test_set.begin());
            REQUIRE(!res.has_value());
            REQUIRE(test_set.size() == size + 1);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count == 0);
        }
    }
}

TEST_CASE("set.merge", "[set][merge][operations][insert_notification][erase_notification]") {
    using s = cne::set<std::set<emplace_constr>>;
    s            dest;
    s::size_type dest_size = 3;
    s            src;
    s::size_type src_size = 5;
    for (s::size_type i = 0; i < dest_size; ++i) { dest.container().emplace(true); }

    ADD_INSERT_LISTENERS(dest, s, emplace_constr)
    ADD_ERASE_LISTENERS(src, s, emplace_constr)

    SECTION("Success") {
        for (s::size_type i = 0; i < src_size; ++i) { src.container().emplace(true); }

        bool res = dest.merge(src);
        REQUIRE(res == true);
        REQUIRE(src.size() == 0);
        REQUIRE(dest.size() == (dest_size + src_size));
        REQUIRE(insert_count != 0);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        for (s::size_type i = 0; i < src_size; ++i) { src.container().emplace(false); }

        bool res = dest.merge(src);
        REQUIRE(res != true);
        REQUIRE(src.size() == src_size);
        REQUIRE(dest.size() == dest_size);
        REQUIRE(insert_count == 0);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("set.swap", "[set][swap][modifiers][insert_notification][erase_notification][replace_notification]") {
    using s = cne::set<std::set<no_restrictions>>;
    s            src;
    s::size_type src_size = 5;
    s            dest;
    s::size_type dest_size = 3;

    ADD_INSERT_LISTENERS(dest, s, no_restrictions)
    ADD_REPLACE_LISTENERS(dest, s, no_restrictions)
    ADD_ERASE_LISTENERS(src, s, no_restrictions)

    SECTION("Success") {
        for (s::size_type i = 0; i < src_size; ++i) { src.container().emplace(true); }
        for (s::size_type i = 0; i < dest_size; ++i) { dest.container().emplace(true); }

        bool res = dest.swap(src);
        REQUIRE(res == true);
        REQUIRE(src.size() == dest_size);
        REQUIRE(dest.size() == src_size);
        REQUIRE(insert_count != 0);
        REQUIRE(replace_count == 0);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        for (s::size_type i = 0; i < src_size; ++i) { src.container().emplace(false); }
        for (s::size_type i = 0; i < dest_size; ++i) { dest.container().emplace(true); }

        bool res = dest.swap(src);
        REQUIRE(res != true);
        REQUIRE(src.size() == src_size);
        REQUIRE(dest.size() == dest_size);
        REQUIRE(insert_count == 0);
        REQUIRE(replace_count == 0);
        REQUIRE(erase_count == 0);
    }
}