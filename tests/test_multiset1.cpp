#include "catch2.h"

#include <initializer_list>
#include <list>
#include <set>
#include <vector>

#include "../headers/containers/set.h"
#include "types.h"
#include "utils.h"

using namespace cne::notifications;
using namespace cne::tests;

TEST_CASE("multiset.operator=",
          "[multiset][operator=][insert_notification][erase_notification][replace_notification]") {
    SECTION("const wrapped_type&") {
        using wrapped_class = std::multiset<copy_constr>;
        using v = std::vector<copy_constr>;
        using s = cne::multiset<wrapped_class>;

        v common = {copy_constr::make(true), copy_constr::make(true), copy_constr::make(true)};

        s             dest(common.begin(), common.end());
        wrapped_class src(common.begin(), common.end());

        ADD_INSERT_LISTENERS(dest, s, copy_constr)
        ADD_REPLACE_LISTENERS(dest, s, copy_constr)
        ADD_ERASE_LISTENERS(dest, s, copy_constr)

        SECTION("Success") {
            dest.container().insert(copy_constr::make(true));
            src.insert(copy_constr::make(true));
            src.insert(copy_constr::make(true));

            dest = src;
            REQUIRE(dest.size() == src.size());
            REQUIRE(insert_call_cnt == 2);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(replace_call_cnt == common.size());
            REQUIRE(insert_count != 0);
            REQUIRE(erase_count != 0);
            REQUIRE(replace_count != 0);
        }

        SECTION("Fail insert") {
            dest.container().insert(copy_constr::make(true));
            src.insert(copy_constr::make(true));
            src.insert(copy_constr::make(false));

            dest = src;
            REQUIRE(dest.size() == 4);
            REQUIRE(insert_count == 0);
            REQUIRE(erase_count == 0);
            REQUIRE(replace_count == 0);
        }

        SECTION("Fail erase") {
            dest.container().insert(copy_constr::make(false));
            src.insert(copy_constr::make(true));
            src.insert(copy_constr::make(true));

            dest = src;
            REQUIRE(dest.size() == 4);
            REQUIRE(insert_count == 0);
            REQUIRE(erase_count == 0);
            REQUIRE(replace_count == 0);
        }
    }

    SECTION("wrapped_type&&") {
        using wrapped_class = std::multiset<move_constr>;
        using v = std::vector<move_constr>;
        using s = cne::multiset<wrapped_class>;

        int64_t common_size = 3;
        v       src_v;
        for (int64_t i = 0; i < common_size; ++i) { src_v.emplace_back(move_constr::make(true)); }
        v dest_v;
        for (int64_t i = 0; i < common_size; ++i) {
            auto& x = dest_v.emplace_back(move_constr::make(true));
            x.get_count() = (src_v.begin() + i)->get_count();
        }

        s             dest(std::move_iterator(src_v.begin()), std::move_iterator(src_v.end()));
        wrapped_class src(std::move_iterator(dest_v.begin()), std::move_iterator(dest_v.end()));

        ADD_INSERT_LISTENERS(dest, s, move_constr)
        ADD_REPLACE_LISTENERS(dest, s, move_constr)
        ADD_ERASE_LISTENERS(dest, s, move_constr)

        SECTION("Success") {
            dest.container().insert(move_constr::make(true));
            src.insert(move_constr::make(true));
            src.insert(move_constr::make(true));

            dest = std::move(src);
            REQUIRE(dest.size() == 5);
            REQUIRE(insert_call_cnt == 2);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(replace_call_cnt == common_size);
            REQUIRE(insert_count != 0);
            REQUIRE(erase_count != 0);
            REQUIRE(replace_count != 0);
        }

        SECTION("Fail insert") {
            dest.container().insert(move_constr::make(true));
            src.insert(move_constr::make(true));
            src.insert(move_constr::make(false));

            dest = std::move(src);
            REQUIRE(dest.size() == 4);
            REQUIRE(insert_count == 0);
            REQUIRE(erase_count == 0);
            REQUIRE(replace_count == 0);
        }

        SECTION("Fail erase") {
            dest.container().insert(move_constr::make(false));
            src.insert(move_constr::make(true));
            src.insert(move_constr::make(true));

            dest = std::move(src);
            REQUIRE(dest.size() == 4);
            REQUIRE(insert_count == 0);
            REQUIRE(erase_count == 0);
            REQUIRE(replace_count == 0);
        }
    }

    SECTION("initializer_list") {
        using wrapped_class = std::multiset<copy_constr>;
        using i = std::initializer_list<copy_constr>;
        using v = std::vector<copy_constr>;
        using s = cne::multiset<wrapped_class>;

        SECTION("Success") {
            i src = {copy_constr::make(true), copy_constr::make(true), copy_constr::make(true), copy_constr::make(true),
                     copy_constr::make(true)};
            v common(src);
            common.pop_back();
            common.pop_back();
            s dest(common.begin(), common.end());
            dest.container().insert(copy_constr::make(true));

            ADD_INSERT_LISTENERS(dest, s, copy_constr)
            ADD_REPLACE_LISTENERS(dest, s, copy_constr)
            ADD_ERASE_LISTENERS(dest, s, copy_constr)

            dest = src;
            REQUIRE(dest.size() == src.size());
            REQUIRE(insert_call_cnt == 2);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(replace_call_cnt == common.size());
            REQUIRE(insert_count != 0);
            REQUIRE(erase_count != 0);
            REQUIRE(replace_count != 0);
        }

        SECTION("Fail insert") {
            i src = {copy_constr::make(true), copy_constr::make(true), copy_constr::make(true), copy_constr::make(true),
                     copy_constr::make(false)};
            v common(src);
            common.pop_back();
            common.pop_back();
            s dest(common.begin(), common.end());
            dest.container().insert(copy_constr::make(true));

            ADD_INSERT_LISTENERS(dest, s, copy_constr)
            ADD_REPLACE_LISTENERS(dest, s, copy_constr)
            ADD_ERASE_LISTENERS(dest, s, copy_constr)

            dest = src;
            REQUIRE(dest.size() == 4);
            REQUIRE(insert_count == 0);
            REQUIRE(erase_count == 0);
            REQUIRE(replace_count == 0);
        }

        SECTION("Fail erase") {
            i src = {copy_constr::make(true), copy_constr::make(true), copy_constr::make(true), copy_constr::make(true),
                     copy_constr::make(true)};
            v common(src);
            common.pop_back();
            common.pop_back();
            s dest(common.begin(), common.end());
            dest.container().insert(copy_constr::make(false));

            ADD_INSERT_LISTENERS(dest, s, copy_constr)
            ADD_REPLACE_LISTENERS(dest, s, copy_constr)
            ADD_ERASE_LISTENERS(dest, s, copy_constr)

            dest = src;
            REQUIRE(dest.size() == 4);
            REQUIRE(insert_count == 0);
            REQUIRE(erase_count == 0);
            REQUIRE(replace_count == 0);
        }
    }
}

TEST_CASE("multiset.clear", "[multiset][clear][modifiers][erase_notification]") {
    int size = 5;

    SECTION("Success") {
        using s = cne::multiset<std::multiset<default_constr_true>>;
        s test_set;
        for (int i = 0; i < size; ++i) { test_set.emplace(); }

        ADD_ERASE_LISTENERS(test_set, s, default_constr_true)

        bool res = test_set.clear();
        REQUIRE(res == true);
        REQUIRE(test_set.size() == 0);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        using s = cne::multiset<std::multiset<default_constr_false>>;
        s test_set;
        for (int i = 0; i < size; ++i) { test_set.emplace(); }

        ADD_ERASE_LISTENERS(test_set, s, default_constr_false)

        bool res = test_set.clear();
        REQUIRE(res == false);
        REQUIRE(test_set.size() == size);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("multiset.insert", "[multiset][insert][modifiers][insert_notification]") {
    SECTION("Copy constructable") {
        using s = cne::multiset<std::multiset<copy_constr>>;
        s           test_set;
        copy_constr to_copy_s(copy_constr::make(true));
        copy_constr to_copy_f(copy_constr::make(false));

        ADD_INSERT_LISTENERS(test_set, s, copy_constr)

        SECTION("const_value&") {
            SECTION("Success") {
                auto res = test_set.insert(to_copy_s);
                REQUIRE(res != test_set.end());
                REQUIRE(test_set.size() == 1);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_set.insert(to_copy_f);
                REQUIRE(res == test_set.end());
                REQUIRE(test_set.size() == 0);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count == 0);
            }
        }

        SECTION("const_iterator, const value&") {
            SECTION("Success") {
                auto res = test_set.insert(test_set.begin(), to_copy_s);
                REQUIRE(res != test_set.end());
                REQUIRE(test_set.size() == 1);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_set.insert(test_set.begin(), to_copy_f);
                REQUIRE(res == test_set.end());
                REQUIRE(test_set.size() == 0);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count == 0);
            }
        }
    }

    SECTION("Move constructable") {
        using s = cne::multiset<std::multiset<move_constr>>;
        s           test_set;
        move_constr to_move_s(move_constr::make(true));
        move_constr to_move_f(move_constr::make(false));

        ADD_INSERT_LISTENERS(test_set, s, move_constr)

        SECTION("const_value&") {
            SECTION("Success") {
                auto res = test_set.insert(std::move(to_move_s));
                REQUIRE(res != test_set.end());
                REQUIRE(test_set.size() == 1);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_set.insert(std::move(to_move_f));
                REQUIRE(res == test_set.end());
                REQUIRE(test_set.size() == 0);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count == 0);
            }
        }

        SECTION("const_iterator, const value&") {
            SECTION("Success") {
                auto res = test_set.insert(test_set.begin(), std::move(to_move_s));
                REQUIRE(res != test_set.end());
                REQUIRE(test_set.size() == 1);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_set.insert(test_set.begin(), std::move(to_move_f));
                REQUIRE(res == test_set.end());
                REQUIRE(test_set.size() == 0);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count == 0);
            }
        }
    }

    SECTION("Emplace constructible") {
        using s = cne::multiset<std::multiset<emplace_constr>>;
        s test_set;

        ADD_INSERT_LISTENERS(test_set, s, emplace_constr)

        SECTION("const_iterator, InputIt, InputIt") {
            using src_t = std::list<bool>;
            src_t source;

            SECTION("Success") {
                source = {true, true, true};
                auto res = test_set.insert(source.begin(), source.end());
                REQUIRE(res == true);
                REQUIRE(test_set.size() == 3);
                REQUIRE(insert_call_cnt == 3);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                source = {true, false, true};
                auto res = test_set.insert(source.begin(), source.end());
                REQUIRE(res == false);
                REQUIRE(test_set.size() == 0);
                REQUIRE(insert_call_cnt == 2);
                REQUIRE(insert_count == 0);
            }
        }

        SECTION("initializer_list") {
            SECTION("Success") {
                auto res = test_set.insert({true, true, true});
                REQUIRE(res == true);
                REQUIRE(test_set.size() == 3);
                REQUIRE(insert_call_cnt == 3);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_set.insert({true, false, true});
                REQUIRE(res == false);
                REQUIRE(test_set.size() == 0);
                REQUIRE(insert_call_cnt == 2);
                REQUIRE(insert_count == 0);
            }
        }
    }

    SECTION("No requirements") {
        SECTION("Success") {
            using wrapped_type = std::multiset<default_constr_true>;
            using s = cne::multiset<wrapped_type>;
            s            test_set;
            wrapped_type src;
            src.emplace();

            ADD_INSERT_LISTENERS(test_set, s, default_constr_true)

            SECTION("node_handle&&") {
                auto res = test_set.insert(src.extract(src.begin()));
                REQUIRE(res.has_value());
                REQUIRE(res != test_set.end());
                REQUIRE(test_set.size() == 1);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count != 0);
            }

            SECTION("const_iterator, node_handle&&") {
                auto res = test_set.insert(test_set.begin(), src.extract(src.begin()));
                REQUIRE(res.has_value());
                REQUIRE(*res != test_set.end());
                REQUIRE(test_set.size() == 1);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count != 0);
            }
        }

        SECTION("Fail") {
            using wrapped_type = std::multiset<default_constr_false>;
            using s = cne::multiset<wrapped_type>;
            s            test_set;
            wrapped_type src;
            src.emplace();

            ADD_INSERT_LISTENERS(test_set, s, default_constr_false)

            SECTION("node_handle&&") {
                auto res = test_set.insert(src.extract(src.begin()));
                REQUIRE(!res.has_value());
                REQUIRE(test_set.size() == 0);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count == 0);
            }

            SECTION("const_iterator, node_handle&&") {
                auto res = test_set.insert(test_set.begin(), src.extract(src.begin()));
                REQUIRE(!res.has_value());
                REQUIRE(test_set.size() == 0);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count == 0);
            }
        }
    }
}

TEST_CASE("multiset.emplace", "[multiset][emplace][modifiers][insert_notification]") {
    using s = cne::multiset<std::multiset<emplace_constr>>;
    s test_set;

    ADD_INSERT_LISTENERS(test_set, s, emplace_constr)

    SECTION("Success") {
        auto res = test_set.emplace(true);
        REQUIRE(res != test_set.end());
        REQUIRE(test_set.size() == 1);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count != 0);
    }
    SECTION("Fail") {
        auto res = test_set.emplace(false);
        REQUIRE(res == test_set.end());
        REQUIRE(test_set.size() == 0);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count == 0);
    }
}

TEST_CASE("multiset.emplace_hint", "[multiset][emplace_hint][modifiers][insert_notification]") {
    using s = cne::multiset<std::multiset<emplace_constr>>;
    s test_set;

    ADD_INSERT_LISTENERS(test_set, s, emplace_constr)

    SECTION("Success") {
        auto res = test_set.emplace_hint(test_set.begin(), true);
        REQUIRE(res != test_set.end());
        REQUIRE(test_set.size() == 1);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count != 0);
    }
    SECTION("Fail") {
        auto res = test_set.emplace_hint(test_set.begin(), false);
        REQUIRE(res == test_set.end());
        REQUIRE(test_set.size() == 0);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count == 0);
    }
}

/**
 * @brief Can't be tested with minimal type requirements, because all insert operations impose more
 * requirements
 */
TEST_CASE("multiset.erase", "[multiset][erase][modifiers][erase_notification]") {
    using s = cne::multiset<std::multiset<move_constr_assign>>;
    s            test_set;
    s::size_type size = 4;

    ADD_ERASE_LISTENERS(test_set, s, move_constr_assign)

    SECTION("Success") {
        for (s::size_type i = 0; i < size; ++i) { test_set.container().emplace(move_constr_assign::make(true)); }

        SECTION("const_iterator") {
            auto res = test_set.erase(test_set.begin());
            REQUIRE(res.has_value());
            REQUIRE(test_set.size() == (size - 1));
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count != 0);
        }

        SECTION("const_iterator, const_iterator") {
            s::size_type to_erase = 2;

            auto res = test_set.erase(test_set.begin(), std::next(test_set.begin(), to_erase));
            REQUIRE(res.has_value());
            REQUIRE(test_set.size() == (size - to_erase));
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count != 0);
        }

        SECTION("const key_type&") {
            auto res = test_set.erase(*test_set.begin());
            REQUIRE(res == 1);
            REQUIRE(test_set.size() == (size - 1));
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count != 0);
        }
    }

    SECTION("Fail") {
        s::size_type fail_pos = 2;
        for (s::size_type i = 0; i < size; ++i) {
            test_set.container().emplace(move_constr_assign::make(i != fail_pos));
        }

        SECTION("const_iterator") {
            auto res = test_set.erase(std::next(test_set.begin(), fail_pos));
            REQUIRE_FALSE(res.has_value());
            REQUIRE(test_set.size() == size);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count == 0);
        }

        SECTION("const_iterator, const_iterator") {
            s::size_type to_erase = 2;
            auto         first_erase = std::next(test_set.begin());

            auto res = test_set.erase(first_erase, std::next(first_erase, to_erase));
            REQUIRE_FALSE(res.has_value());
            REQUIRE(test_set.size() == size);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count == 0);
        }

        SECTION("const key_type&") {
            test_set.container().emplace(move_constr_assign::make(false));
            auto res = test_set.erase(*std::prev(test_set.end()));
            REQUIRE(res == 0);
            REQUIRE(test_set.size() == size + 1);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count == 0);
        }
    }
}