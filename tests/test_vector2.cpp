#include "catch2.h"

#include <forward_list>
#include <initializer_list>
#include <vector>

#include "../headers/containers/vector.h"
#include "types.h"
#include "utils.h"

using namespace cne::notifications;
using namespace cne::tests;

TEST_CASE("vector.operator[]", "[vector][operator[]][element_access]") {
    using v = cne::vector<std::vector<int>>;

    v   test_vector = {3, 5};
    int value;

    SECTION("Const") {
        const v& const_vector = test_vector;
        value = const_vector.at(0);
        REQUIRE(value == 3);
    }

    SECTION("Non const") {
        value = test_vector.at(0);
        REQUIRE(value == 3);
    }
}

TEST_CASE("vector.at", "[vector][at][element_access]") {
    using v = cne::vector<std::vector<int>>;

    v   test_vector = {3, 5};
    int value;

    SECTION("Const") {
        const v& const_vector = test_vector;
        value = const_vector[0];
        REQUIRE(value == 3);
    }

    SECTION("Non const") {
        value = test_vector[0];
        REQUIRE(value == 3);
    }
}

TEST_CASE("vector.front", "[vector][front][element_access]") {
    using v = cne::vector<std::vector<int>>;

    v   test_vector = {3, 5};
    int value;

    SECTION("Const") {
        const v& const_vector = test_vector;
        value = const_vector.front();
        REQUIRE(value == 3);
    }

    SECTION("Non const") {
        value = test_vector.front();
        REQUIRE(value == 3);
    }
}

TEST_CASE("vector.back", "[vector][back][element_access]") {
    using v = cne::vector<std::vector<int>>;

    v   test_vector = {3, 5};
    int value;

    SECTION("Const") {
        const v& const_vector = test_vector;
        value = const_vector.back();
        REQUIRE(value == 5);
    }

    SECTION("Non const") {
        value = test_vector.back();
        REQUIRE(value == 5);
    }
}

TEST_CASE("vector.clear", "[vector][clear][modifiers][erase_notification]") {
    int size = 5;

    SECTION("Success") {
        using v = cne::vector<std::vector<default_constr_true>>;
        v test_vector(size);

        ADD_ERASE_LISTENERS(test_vector, v, default_constr_true)

        bool res = test_vector.clear();
        REQUIRE(res == true);
        REQUIRE(test_vector.size() == 0);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        using v = cne::vector<std::vector<default_constr_false>>;
        v test_vector(size);

        ADD_ERASE_LISTENERS(test_vector, v, default_constr_false)

        bool res = test_vector.clear();
        REQUIRE(res == false);
        REQUIRE(test_vector.size() == size);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("vector.insert", "[vector][insert][modifiers][insert_notification]") {
    SECTION("Copy assignable and copy insertable") {
        using v = cne::vector<std::vector<copy_constr_assign>>;
        v test_vector;

        auto to_copy_s = copy_constr_assign::make(true);
        auto to_copy_f = copy_constr_assign::make(false);

        ADD_INSERT_LISTENERS(test_vector, v, copy_constr_assign)

        SECTION("const_iterator, const value_type&") {
            SECTION("Success") {
                auto res = test_vector.insert(test_vector.cend(), to_copy_s);
                REQUIRE(res != test_vector.end());
                REQUIRE(test_vector.size() == 1);
                REQUIRE(insert_call_cnt == 1);
            }
            SECTION("Fail") {
                auto res = test_vector.insert(test_vector.cend(), to_copy_f);
                REQUIRE(res == test_vector.end());
                REQUIRE(test_vector.size() == 0);
                REQUIRE(insert_count == 0);
            }
        }

        SECTION("const_iterator, size_type, const value_type&") {
            v::size_type count = 3;
            SECTION("Success") {
                auto res = test_vector.insert(test_vector.cend(), count, to_copy_s);
                REQUIRE(res != test_vector.end());
                REQUIRE(test_vector.size() == count);
                REQUIRE(insert_call_cnt == count);
            }
            SECTION("Fail") {
                auto res = test_vector.insert(test_vector.cend(), count, to_copy_f);
                REQUIRE(res == test_vector.end());
                REQUIRE(test_vector.size() == 0);
                REQUIRE(insert_count == 0);
            }
        }
    }

    SECTION("Move assignable and move insertable") {
        using v = cne::vector<std::vector<move_constr_assign>>;
        v test_vector;

        ADD_INSERT_LISTENERS(test_vector, v, move_constr_assign)

        SECTION("const_iterator, value_type&&") {
            SECTION("Success") {
                auto res = test_vector.insert(test_vector.cend(), move_constr_assign::make(true));
                REQUIRE(res != test_vector.end());
                REQUIRE(test_vector.size() == 1);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_vector.insert(test_vector.cend(), move_constr_assign::make(false));
                REQUIRE(res == test_vector.end());
                REQUIRE(test_vector.size() == 0);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count == 0);
            }
        }
    }

    SECTION("Move assignable, move insertable and emplace constructible") {
        using v = cne::vector<std::vector<move_constr_assign_emplace>>;
        v test_vector;

        ADD_INSERT_LISTENERS(test_vector, v, move_constr_assign_emplace)

        SECTION("const_iterator, InputIt, InputIt") {
            using src_t = std::forward_list<bool>;
            src_t source;

            SECTION("Success") {
                source = {true, true, true};
                auto res = test_vector.insert(test_vector.cend(), source.begin(), source.end());
                REQUIRE(res != test_vector.end());
                REQUIRE(test_vector.size() == 3);
                REQUIRE(insert_call_cnt == 3);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                source = {true, false, true};
                auto res = test_vector.insert(test_vector.cend(), source.begin(), source.end());
                REQUIRE(res == test_vector.end());
                REQUIRE(test_vector.size() == 0);
                REQUIRE(insert_call_cnt == 2);
                REQUIRE(insert_count == 0);
            }
        }

#ifndef __GNUC__
        SECTION("initializer_list") {
            SECTION("Success") {
                auto res = test_vector.insert(test_vector.cend(), {true, true, true});
                REQUIRE(res != test_vector.end());
                REQUIRE(test_vector.size() == 3);
                REQUIRE(insert_call_cnt == 3);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_vector.insert(test_vector.cend(), {true, false, true});
                REQUIRE(res == test_vector.end());
                REQUIRE(test_vector.size() == 0);
                REQUIRE(insert_call_cnt == 2);
                REQUIRE(insert_count == 0);
            }
        }
#endif
    }
}

TEST_CASE("vector.emplace", "[vector][emplace][modifiers][insert_notification]") {
    using v = cne::vector<std::vector<move_constr_assign_emplace>>;
    v test_vector;

    ADD_INSERT_LISTENERS(test_vector, v, move_constr_assign_emplace)

    SECTION("Success") {
        auto res = test_vector.emplace(test_vector.begin(), true);
        REQUIRE(res != test_vector.end());
        REQUIRE(test_vector.size() == 1);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count != 0);
    }
    SECTION("Fail") {
        auto res = test_vector.emplace(test_vector.begin(), false);
        REQUIRE(res == test_vector.end());
        REQUIRE(test_vector.size() == 0);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count == 0);
    }
}

/**
 * @brief Can't be tested with minimal type requirements, because all insert operations impose more
 * requirements
 */
TEST_CASE("vector.erase", "[vector][erase][modifiers][erase_notification]") {
    using v = cne::vector<std::vector<move_constr_assign>>;
    v            test_vector;
    v::size_type size = 4;

    ADD_ERASE_LISTENERS(test_vector, v, move_constr_assign)

    SECTION("Success") {
        for (v::size_type i = 0; i < size; ++i) {
            test_vector.container().emplace_back(move_constr_assign::make(true));
        }

        SECTION("const_iterator") {
            auto res = test_vector.erase(test_vector.begin());
            REQUIRE(res.has_value());
            REQUIRE(test_vector.size() == (size - 1));
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count != 0);
        }

        SECTION("const_iterator, const_iterator") {
            v::size_type to_erase = 2;

            auto res = test_vector.erase(test_vector.begin(), test_vector.begin() + to_erase);
            REQUIRE(res.has_value());
            REQUIRE(test_vector.size() == (size - to_erase));
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count != 0);
        }
    }

    SECTION("Fail") {
        v::size_type fail_pos = 2;
        for (v::size_type i = 0; i < size; ++i) {
            test_vector.container().emplace_back(move_constr_assign::make(i != fail_pos));
        }

        SECTION("const_iterator") {
            auto res = test_vector.erase(test_vector.begin() + fail_pos);
            REQUIRE_FALSE(res.has_value());
            REQUIRE(test_vector.size() == size);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count == 0);
        }

        SECTION("const_iterator, const_iterator") {
            v::size_type to_erase = 2;
            auto         first_erase = test_vector.begin() + 1;

            auto res = test_vector.erase(first_erase, first_erase + to_erase);
            REQUIRE_FALSE(res.has_value());
            REQUIRE(test_vector.size() == size);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count == 0);
        }
    }
}

TEST_CASE("vector.push_back", "[vector][push_back][modifiers][insert_notification]") {
    SECTION("const value_type&") {
        using v = cne::vector<std::vector<copy_constr>>;
        v test_vector;

        ADD_INSERT_LISTENERS(test_vector, v, copy_constr);

        SECTION("Success") {
            auto to_copy = copy_constr::make(true);
            bool res = test_vector.push_back(to_copy);
            REQUIRE(res);
            REQUIRE(test_vector.size() == 1);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count != 0);
        }

        SECTION("Fail") {
            auto to_copy = copy_constr::make(false);
            bool res = test_vector.push_back(to_copy);
            REQUIRE_FALSE(res);
            REQUIRE(test_vector.size() == 0);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count == 0);
        }
    }

    SECTION("value_type&&") {
        using v = cne::vector<std::vector<move_constr>>;
        v test_vector;

        ADD_INSERT_LISTENERS(test_vector, v, move_constr);

        SECTION("Success") {
            bool res = test_vector.push_back(move_constr::make(true));
            REQUIRE(res == true);
            REQUIRE(test_vector.size() == 1);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count != 0);
        }

        SECTION("Fail") {
            bool res = test_vector.push_back(move_constr::make(false));
            REQUIRE_FALSE(res);
            REQUIRE(test_vector.size() == 0);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count == 0);
        }
    }
}

TEST_CASE("vector.emplace_back", "[vector][emplace_back][modifiers][insert_notification]") {
    using v = cne::vector<std::vector<move_constr_emplace>>;
    v test_vector;

    ADD_INSERT_LISTENERS(test_vector, v, move_constr_emplace);

    SECTION("Success") {
        auto res = test_vector.emplace_back(true);
        REQUIRE(res.has_value());
        REQUIRE(res->data().get_value() == true);
        REQUIRE(test_vector.size() == 1);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count != 0);
    }

    SECTION("Fail") {
        auto res = test_vector.emplace_back(false);
        REQUIRE_FALSE(res.has_value());
        REQUIRE(test_vector.size() == 0);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count == 0);
    }
}

/**
 * @brief Doesn't impose any type requirements other than Erasable.
 */
TEST_CASE("vector.pop_back", "[vector][pop_back][modifiers][erase_notification]") {
    using v = cne::vector<std::vector<no_restrictions>>;
    v test_vector;

    ADD_ERASE_LISTENERS(test_vector, v, no_restrictions);

    SECTION("Success") {
        test_vector.container().emplace_back(true);
        bool res = test_vector.pop_back();
        REQUIRE(res);
        REQUIRE(test_vector.size() == 0);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        test_vector.container().emplace_back(false);
        bool res = test_vector.pop_back();
        REQUIRE_FALSE(res);
        REQUIRE(test_vector.size() == 1);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("vector.resize", "[vector][resize][modifiers][insert_notification][erase_notification]") {
    SECTION("size_type") {
        SECTION("Success") {
            using v = cne::vector<std::vector<move_default_constr_true>>;
            v::size_type orig_size = 3;
            v            test_vector(orig_size);

            SECTION("Enlarge") {
                ADD_INSERT_LISTENERS(test_vector, v, move_default_constr_true)

                v::size_type new_size = 4;
                auto         res = test_vector.resize(new_size);
                REQUIRE(res == true);
                REQUIRE(test_vector.size() == new_size);
                REQUIRE(insert_call_cnt == (new_size - orig_size));
                REQUIRE(insert_count != 0);
            }

            SECTION("Shrink") {
                ADD_ERASE_LISTENERS(test_vector, v, move_default_constr_true)

                v::size_type new_size = 2;
                auto         res = test_vector.resize(new_size);
                REQUIRE(res == true);
                REQUIRE(test_vector.size() == new_size);
                REQUIRE(erase_call_cnt == orig_size - new_size);
                REQUIRE(erase_count != 0);
            }
        }

        SECTION("Fail") {
            using v = cne::vector<std::vector<move_default_constr_false>>;
            v::size_type orig_size = 3;
            v            test_vector(orig_size);

            SECTION("Enlarge") {
                ADD_INSERT_LISTENERS(test_vector, v, move_default_constr_false)

                v::size_type new_size = 4;
                auto         res = test_vector.resize(new_size);
                REQUIRE(res == false);
                REQUIRE(test_vector.size() == orig_size);
                REQUIRE(insert_count == 0);
            }

            SECTION("Shrink") {
                ADD_ERASE_LISTENERS(test_vector, v, move_default_constr_false)

                v::size_type new_size = 2;
                auto         res = test_vector.resize(new_size);
                REQUIRE(res == false);
                REQUIRE(test_vector.size() == orig_size);
                REQUIRE(erase_call_cnt == 1);
                REQUIRE(erase_count == 0);
            }
        }
    }

#ifndef __GNUC__
    SECTION("size_type, const value_type&") {
        using v = cne::vector<std::vector<copy_constr>>;
        v::size_type orig_size = 3;
        v            test_vector;
        auto         to_copy_s = copy_constr::make(true);
        auto         to_copy_f = copy_constr::make(false);

        SECTION("Success") {
            test_vector.container().resize(orig_size, to_copy_s);

            SECTION("Enlarge") {
                ADD_INSERT_LISTENERS(test_vector, v, copy_constr)

                v::size_type new_size = 4;
                auto         res = test_vector.resize(new_size, to_copy_s);
                REQUIRE(res == true);
                REQUIRE(test_vector.size() == new_size);
                REQUIRE(insert_call_cnt == (new_size - orig_size));
                REQUIRE(insert_count != 0);
            }

            SECTION("Shrink") {
                ADD_ERASE_LISTENERS(test_vector, v, copy_constr)

                v::size_type new_size = 2;
                auto         res = test_vector.resize(new_size, to_copy_s);
                REQUIRE(res == true);
                REQUIRE(test_vector.size() == new_size);
                REQUIRE(erase_call_cnt == orig_size - new_size);
                REQUIRE(erase_count != 0);
            }
        }

        SECTION("Fail") {
            test_vector.container().resize(orig_size, to_copy_f);

            SECTION("Enlarge") {
                ADD_INSERT_LISTENERS(test_vector, v, copy_constr)

                v::size_type new_size = 4;
                auto         res = test_vector.resize(new_size, to_copy_f);
                REQUIRE(res == false);
                REQUIRE(test_vector.size() == orig_size);
                REQUIRE(insert_count == 0);
            }

            SECTION("Shrink") {
                ADD_ERASE_LISTENERS(test_vector, v, copy_constr)

                v::size_type new_size = 2;
                auto         res = test_vector.resize(new_size, to_copy_f);
                REQUIRE(res == false);
                REQUIRE(test_vector.size() == orig_size);
                REQUIRE(erase_call_cnt == 1);
                REQUIRE(erase_count == 0);
            }
        }
    }
#endif
}

TEST_CASE("vector.swap", "[vector][swap][modifiers][insert_notification][erase_notification][replace_notification]") {
    using v = cne::vector<std::vector<no_restrictions>>;
    v src;
    v::size_type src_size = 5;
    v            dest;
    v::size_type dest_size = 3;

	ADD_INSERT_LISTENERS(dest, v, no_restrictions)
    ADD_REPLACE_LISTENERS(dest, v, no_restrictions)
    ADD_ERASE_LISTENERS(src, v, no_restrictions)

    SECTION("Success") {
        src.container().insert(src.container().cend(), src_size, true);
        dest.container().insert(dest.container().cend(), dest_size, true);

		bool res = dest.swap(src);
        REQUIRE(res == true);
        REQUIRE(src.size() == dest_size);
        REQUIRE(dest.size() == src_size);
        REQUIRE(insert_count != 0);
        REQUIRE(replace_count != 0);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        src.container().insert(src.container().cend(), src_size, false);
        dest.container().insert(dest.container().cend(), dest_size, true);

        bool res = dest.swap(src);
        REQUIRE(res != true);
        REQUIRE(src.size() == src_size);
        REQUIRE(dest.size() == dest_size);
        REQUIRE(insert_count == 0);
        REQUIRE(replace_count == 0);
        REQUIRE(erase_count == 0);
    }
}