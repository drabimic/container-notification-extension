#if 0
#include "catch2.h"

#include "../headers/notifications/notifications.h"
#include "types.h"
#include "utils.h"

using namespace cne::notifications;
using namespace cne::tests;

#define TEST_NOTIFY(NOTIFIER, NOTIFY_METHOD, ...)       \
    SECTION("Notify") {                                 \
        bool res = NOTIFIER.NOTIFY_METHOD(__VA_ARGS__); \
        REQUIRE(first_listener == 1);                   \
        REQUIRE(second_listener == 1);                  \
        REQUIRE(undo == 0);                             \
        REQUIRE(res == true);                           \
                                                        \
        res = NOTIFIER.NOTIFY_METHOD(__VA_ARGS__);      \
        REQUIRE(first_listener == 2);                   \
        REQUIRE(second_listener == 2);                  \
        REQUIRE(undo == 1);                             \
        REQUIRE(res == false);                          \
    }

#define TEST_NOTIFY_UNDO(NOTIFIER, NOTIFY_METHOD, ...) \
    SECTION("Notify_undo") {                           \
        NOTIFIER.NOTIFY_METHOD(__VA_ARGS__);           \
        REQUIRE(first_listener == 0);                  \
        REQUIRE(second_listener == 0);                 \
        REQUIRE(undo == 1);                            \
    }

#define DEFINE_TEST_CONTAINER(NOTIFICATION_NAME)                                                                       \
    template<typename... Args>                                                                                         \
    class container : public NOTIFICATION_NAME<container, Args...>                                                     \
    {                                                                                                                  \
    public:                                                                                                            \
        bool notify(Args&&... args) { return NOTIFICATION_NAME::NOTIFICATION_NAME _called(std::forward<args>...); }    \
        bool notify_undo(Args&&... args) { return NOTIFICATION_NAME::NOTIFICATION_NAME _undo(std::forward<args>...); } \
    };

#define TEST_NOTIFICATION(NOTIFICATION_NAME) \
    TEST_CASE("Notification." #NOTIFICATION_NAME, "[notifications][" #NOTIFICATION_NAME "]")

TEST_CASE("Notifications.Notifier. Single operations", "[notifications][notifier][connection]") {
    using n = notifier<const only_default&>;

    n            test_notifier;
    only_default test_object;

    ADD_TESTING_LISTENERS(test_notifier, add_listener)

    REQUIRE(first_connection.active());
    REQUIRE(second_connection.active());

    TEST_NOTIFY(test_notifier, notify, test_object);

    SECTION("Notify parts") {
        INFO("Notify operation")
        second_listener = 1; /* To ensure failure would get called*/
        auto failed_check = test_notifier.notify_operation(test_object);
        REQUIRE(first_listener == 1);
        REQUIRE(second_listener == 2);
        REQUIRE(undo == 0);

        INFO("Notify undo");
        first_listener = second_listener = 0;
        test_notifier.notify_undo(test_object);
        REQUIRE(first_listener == 0);
        REQUIRE(second_listener == 0);
        REQUIRE(undo == 1);

        test_notifier.notify_undo(failed_check, test_object);
        REQUIRE(first_listener == 0);
        REQUIRE(second_listener == 0);
        REQUIRE(undo == 2);
    }

    SECTION("Connection") {
        SECTION("Disconnect") {
            first_connection.disconnect();
            REQUIRE_FALSE(first_connection.active());

            second_listener = 1; // To ensure undo would get called
            test_notifier.notify(test_object);
            REQUIRE(first_listener == 0);
            REQUIRE(second_listener == 2);
            REQUIRE(undo == 0);
        }

        SECTION("Change undo listener") {
            first_connection.change_undo_listener(nullptr);

            second_listener = 1; // To ensure undo would get called
            test_notifier.notify(test_object);
            REQUIRE(first_listener == 1);
            REQUIRE(second_listener == 2);
            REQUIRE(undo == 0);
        }

        SECTION("Synchronisation") {
            auto dc_and_test = [&test_notifier, &first_connection](n::connection_type& new_c) {
                first_connection.disconnect();

                REQUIRE_FALSE(first_connection.active());
                REQUIRE_FALSE(new_c.active());

                INFO("Checking validity of notifier after disconnecting one connection")
                auto connection = test_notifier.add_listener([](auto&&...) { return true; });
                REQUIRE(connection.active());
                connection.disconnect();
                REQUIRE_FALSE(connection.active());
            };

            SECTION("Copy operator=") {
                n::connection_type new_c;

                new_c = first_connection;
                dc_and_test(new_c);
            }

            SECTION("Copy constructor") {
                n::connection_type new_c = first_connection;
                dc_and_test(new_c);
            }
        }
    }
}

TEST_CASE("Notifications.Notifier. Class operations", "[notifications][notifier][connection]") {
    using n = notifier<const only_default&>;

    only_default test_object;

    SECTION("Swap") {
        n dest;
        DEFINE_TESTING_VARIABLES(n)

        {
            n source;

            ADD_TESTING_LISTENERS_WITHOUT_DEFINE(source, add_listener)
            dest.swap_listeners(source);
        }

        REQUIRE(first_connection.active());
        REQUIRE(second_connection.active());
    }

    SECTION("Merge") {}
}

TEST_NOTIFICATION(insert) {
    class container : public insert<container, every_operation, const only_default&>
    {
    public:
        bool notify(every_operation a, const only_default& b) const { return this->insert_called(a, b); }
        void notify_undo(every_operation a, const only_default& b) const { this->insert_undo(a, b); }
    };

    container test_insert;

    ADD_TESTING_LISTENERS(test_insert, insert_check)

    TEST_NOTIFY(test_insert, notify, every_operation(), only_default())
    TEST_NOTIFY_UNDO(test_insert, notify_undo, every_operation(), only_default())
}

TEST_NOTIFICATION(erase) {
    class container : public erase<container, every_operation>
    {
    public:
        bool notify(every_operation a, every_operation b) const { return this->erase_called(a, b); }
        void notify_undo(every_operation a, every_operation b) const { this->erase_undo(a, b); }
    };

    container test_erase;

    ADD_TESTING_LISTENERS(test_erase, erase_check)

    TEST_NOTIFY(test_erase, notify, every_operation(), every_operation())
    TEST_NOTIFY_UNDO(test_erase, notify_undo, every_operation(), every_operation())
}

TEST_NOTIFICATION(replace) {
    class container : public replace<container, every_operation, const only_default&>
    {
    public:
        bool notify(every_operation a, const only_default& b) const { return this->replace_called(a, b); }
        void notify_undo(every_operation a, const only_default& b) const { this->replace_undo(a, b); }
    };

    container test_replace;

    ADD_TESTING_LISTENERS(test_replace, replace_check)

    TEST_NOTIFY(test_replace, notify, every_operation(), only_default())
    TEST_NOTIFY_UNDO(test_replace, notify_undo, every_operation(), only_default())
}

TEST_NOTIFICATION(value_change) {
    class container : public value_change<container, only_copy>
    {
    public:
        bool notify(only_copy a) const {
            return this->value_changed(a, [](auto&&...) { SUCCEED(true); });
        }
    };

    container test_value_change;

    ADD_TESTING_LISTENERS(test_value_change, value_change_check)

    TEST_NOTIFY(test_value_change, notify, only_copy::make())
}

TEST_CASE("Notifications.Notification mixin", "[notifications]") {
    dummy_container source;
    size_t          insert = 0, erase = 0, replace = 0, clear = 0, value_change = 0;

    source.insert_check([&insert](const dummy_container&, const only_default&) {
        ++insert;
        return true;
    });
    source.erase_check([&erase](const dummy_container&, every_operation) {
        ++erase;
        return true;
    });
    source.replace_check([&replace](const dummy_container&, const only_default&) {
        ++replace;
        return true;
    });
    source.clear_check([&clear](const dummy_container&) {
        ++clear;
        return true;
    });
    source.value_change_check([&value_change](const dummy_container&, every_operation) {
        ++value_change;
        return true;
    });

    SECTION("Swap") {
        dummy_container destination;
        // same as source.swap_listeners(destination)
        destination.swap_listeners(source);

        /*source.insert();
        REQUIRE(insert == 0);
        destination.insert();
        REQUIRE(insert == 1);

        source.erase();
        REQUIRE(erase == 0);
        destination.erase();
        REQUIRE(erase == 1);

        source.swap();
        REQUIRE(swap == 0);
        destination.swap();
        REQUIRE(swap == 1);

        source.clear();
        REQUIRE(clear == 0);
        destination.clear();
        REQUIRE(clear == 1);

        source.value_change();
        REQUIRE(value_change == 0);
        destination.value_change();
        REQUIRE(value_change == 1);*/
    }

    SECTION("Clear") {
        dummy_container destination;
        destination.insert_check([&insert](const dummy_container&, const only_default&) {
            ++insert;
            return true;
        });
        source.merge_listeners(destination);
        source.clear_listeners();

        /*source.insert();
        REQUIRE(insert == 0);

        source.erase();
        REQUIRE(erase == 0);

        source.swap();
        REQUIRE(swap == 0);

        source.clear();
        REQUIRE(clear == 0);

        source.value_change();
        REQUIRE(value_change == 0);*/
    }
}
#endif