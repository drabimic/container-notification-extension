#include "catch2.h"

#include <initializer_list>
#include <map>
#include <utility>
#include <vector>

#include "../headers/containers/map.h"
#include "types.h"
#include "utils.h"

using namespace cne::notifications;
using namespace cne::tests;

TEST_CASE("multimap.operator=", "[multimap][operator=][insert_notification][erase_notification][replace_notification]") {
    SECTION("const wrapped_type&") {
        using wrapped_class = std::multimap<copy_constr, copy_constr>;
        using v = std::vector<typename wrapped_class::value_type>;
        using m = cne::multimap<wrapped_class>;

        v common = {std::make_pair(copy_constr::make(true), copy_constr::make(true)),
                    std::make_pair(copy_constr::make(true), copy_constr::make(true)),
                    std::make_pair(copy_constr::make(true), copy_constr::make(true))};

        m             dest(common.begin(), common.end());
        wrapped_class src(common.begin(), common.end());

        ADD_MAP_INSERT_LISTENERS(dest, m, typename m::value_type)
        ADD_REPLACE_LISTENERS(dest, m, typename m::mapped_type)
        ADD_MAP_ERASE_LISTENERS(dest, m, typename m::value_type)

        SECTION("Success") {
            dest.container().emplace(copy_constr::make(true), copy_constr::make(true));
            src.emplace(copy_constr::make(true), copy_constr::make(true));
            src.emplace(copy_constr::make(true), copy_constr::make(true));

            dest = src;
            REQUIRE(dest.size() == src.size());
            REQUIRE(insert_call_cnt == 2);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(replace_call_cnt == common.size());
            REQUIRE(insert_count != 0);
            REQUIRE(erase_count != 0);
            REQUIRE(replace_count != 0);
        }

        SECTION("Fail insert") {
            dest.container().emplace(copy_constr::make(true), copy_constr::make(true));
            src.emplace(copy_constr::make(true), copy_constr::make(true));
            src.emplace(copy_constr::make(false), copy_constr::make(false));

            dest = src;
            REQUIRE(dest.size() == 4);
            REQUIRE(insert_count == 0);
            REQUIRE(erase_count == 0);
            REQUIRE(replace_count == 0);
        }

        SECTION("Fail erase") {
            dest.container().emplace(copy_constr::make(false), copy_constr::make(true));
            src.emplace(copy_constr::make(true), copy_constr::make(true));
            src.emplace(copy_constr::make(true), copy_constr::make(true));

            dest = src;
            REQUIRE(dest.size() == 4);
            REQUIRE(insert_count == 0);
            REQUIRE(erase_count == 0);
            REQUIRE(replace_count == 0);
        }
    }
    SECTION("wrapped_type&&") {
        using wrapped_class = std::multimap<move_constr, move_constr>;
        using v = std::vector<std::pair<move_constr, move_constr>>;
        using m = cne::multimap<wrapped_class>;

        int64_t common_size = 3;
        v       src_v;
        for (int64_t i = 0; i < common_size; ++i) {
            src_v.emplace_back(move_constr::make(true), move_constr::make(true));
        }
        v dest_v;
        for (int64_t i = 0; i < common_size; ++i) {
            auto& x = dest_v.emplace_back(move_constr::make(true), move_constr::make(true));
            x.first.get_count() = (src_v.begin() + i)->first.get_count();
        }

        m             dest(std::move_iterator(src_v.begin()), std::move_iterator(src_v.end()));
        wrapped_class src(std::move_iterator(dest_v.begin()), std::move_iterator(dest_v.end()));

        ADD_MAP_INSERT_LISTENERS(dest, m, typename m::value_type)
        ADD_REPLACE_LISTENERS(dest, m, typename m::mapped_type)
        ADD_MAP_ERASE_LISTENERS(dest, m, typename m::value_type)

        SECTION("Success") {
            dest.container().emplace(move_constr::make(true), move_constr::make(true));
            src.emplace(move_constr::make(true), move_constr::make(true));
            src.emplace(move_constr::make(true), move_constr::make(true));

            dest = std::move(src);
            REQUIRE(dest.size() == 5);
            REQUIRE(insert_call_cnt == 2);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(replace_call_cnt == common_size);
            REQUIRE(insert_count != 0);
            REQUIRE(erase_count != 0);
            REQUIRE(replace_count != 0);
        }

        SECTION("Fail insert") {
            dest.container().emplace(move_constr::make(true), move_constr::make(true));
            src.emplace(move_constr::make(true), move_constr::make(true));
            src.emplace(move_constr::make(false), move_constr::make(false));

            dest = std::move(src);
            REQUIRE(dest.size() == 4);
            REQUIRE(insert_count == 0);
            REQUIRE(erase_count == 0);
            REQUIRE(replace_count == 0);
        }

        SECTION("Fail erase") {
            dest.container().emplace(move_constr::make(false), move_constr::make(false));
            src.emplace(move_constr::make(true), move_constr::make(true));
            src.emplace(move_constr::make(true), move_constr::make(true));

            dest = std::move(src);
            REQUIRE(dest.size() == 4);
            REQUIRE(insert_count == 0);
            REQUIRE(erase_count == 0);
            REQUIRE(replace_count == 0);
        }
    }

    SECTION("initializer_list") {
        using wrapped_class = std::multimap<copy_constr, copy_constr>;
        using i = std::initializer_list<std::pair<const copy_constr, copy_constr>>;
        using v = std::vector<std::pair<const copy_constr, copy_constr>>;
        using m = cne::multimap<wrapped_class>;

        SECTION("Success") {
            i src = {std::make_pair(copy_constr::make(true), copy_constr::make(true)),
                     std::make_pair(copy_constr::make(true), copy_constr::make(true)),
                     std::make_pair(copy_constr::make(true), copy_constr::make(true)),
                     std::make_pair(copy_constr::make(true), copy_constr::make(true))};
            v common(src);
            common.pop_back();
            common.pop_back();
            m dest(common.begin(), common.end());
            dest.container().emplace(copy_constr::make(true), copy_constr::make(true));

            ADD_MAP_INSERT_LISTENERS(dest, m, typename m::value_type)
            ADD_REPLACE_LISTENERS(dest, m, typename m::mapped_type)
            ADD_MAP_ERASE_LISTENERS(dest, m, typename m::value_type)

            dest = src;
            REQUIRE(dest.size() == src.size());
            REQUIRE(insert_call_cnt == 2);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(replace_call_cnt == common.size());
            REQUIRE(insert_count != 0);
            REQUIRE(erase_count != 0);
            REQUIRE(replace_count != 0);
        }

        SECTION("Fail insert") {
            i src = {std::make_pair(copy_constr::make(true), copy_constr::make(true)),
                     std::make_pair(copy_constr::make(true), copy_constr::make(true)),
                     std::make_pair(copy_constr::make(true), copy_constr::make(true)),
                     std::make_pair(copy_constr::make(true), copy_constr::make(true)),
                     std::make_pair(copy_constr::make(false), copy_constr::make(false))};
            v common(src);
            common.pop_back();
            common.pop_back();
            m dest(common.begin(), common.end());
            dest.container().emplace(copy_constr::make(true), copy_constr::make(true));

            ADD_MAP_INSERT_LISTENERS(dest, m, typename m::value_type)
            ADD_REPLACE_LISTENERS(dest, m, typename m::mapped_type)
            ADD_MAP_ERASE_LISTENERS(dest, m, typename m::value_type)

            dest = src;
            REQUIRE(dest.size() == 4);
            REQUIRE(insert_count == 0);
            REQUIRE(erase_count == 0);
            REQUIRE(replace_count == 0);
        }

        SECTION("Fail erase") {
            i src = {std::make_pair(copy_constr::make(true), copy_constr::make(true)),
                     std::make_pair(copy_constr::make(true), copy_constr::make(true)),
                     std::make_pair(copy_constr::make(true), copy_constr::make(true)),
                     std::make_pair(copy_constr::make(true), copy_constr::make(true)),
                     std::make_pair(copy_constr::make(true), copy_constr::make(true))};
            v common(src);
            common.pop_back();
            common.pop_back();
            m dest(common.begin(), common.end());
            dest.container().emplace(copy_constr::make(false), copy_constr::make(false));

            ADD_MAP_INSERT_LISTENERS(dest, m, typename m::value_type)
            ADD_REPLACE_LISTENERS(dest, m, typename m::mapped_type)
            ADD_MAP_ERASE_LISTENERS(dest, m, typename m::value_type)

            dest = src;
            REQUIRE(dest.size() == 4);
            REQUIRE(insert_count == 0);
            REQUIRE(erase_count == 0);
            REQUIRE(replace_count == 0);
        }
    }
}

TEST_CASE("multimap.clear", "[multimap][clear][modifiers][erase_notification]") {
    int size = 5;

    SECTION("Success") {
        using m = cne::multimap<std::multimap<default_constr_true, default_constr_true>>;
        m test_map;
        for (int i = 0; i < size; ++i) { test_map.emplace(); }

        ADD_MAP_ERASE_LISTENERS(test_map, m, typename m::value_type)

        bool res = test_map.clear();
        REQUIRE(res == true);
        REQUIRE(test_map.size() == 0);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        using m = cne::multimap<std::multimap<default_constr_false, default_constr_false>>;
        m test_map;
        for (int i = 0; i < size; ++i) { test_map.emplace(); }

        ADD_MAP_ERASE_LISTENERS(test_map, m, typename m::value_type)

        bool res = test_map.clear();
        REQUIRE(res == false);
        REQUIRE(test_map.size() == size);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("multimap.insert", "[multimap][insert][modifiers][insert_notification]") {
    SECTION("Copy constructable") {
        using m = cne::multimap<std::multimap<copy_constr, copy_constr>>;
        m           test_map;
        std::pair<copy_constr, copy_constr> to_copy_s
          = std::make_pair(copy_constr::make(true), copy_constr::make(true));
        std::pair<copy_constr, copy_constr> to_copy_f
          = std::make_pair(copy_constr::make(false), copy_constr::make(false));

        ADD_MAP_INSERT_LISTENERS(test_map, m, typename m::value_type)

        SECTION("const_value&") {
            SECTION("Success") {
                auto res = test_map.insert(to_copy_s);
                REQUIRE(res != test_map.end());
                REQUIRE(test_map.size() == 1);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_map.insert(to_copy_f);
                REQUIRE(res == test_map.end());
                REQUIRE(test_map.size() == 0);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count == 0);
            }
        }

        SECTION("const_iterator, const value&") {
            SECTION("Success") {
                auto res = test_map.insert(test_map.begin(), to_copy_s);
                REQUIRE(res != test_map.end());
                REQUIRE(test_map.size() == 1);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_map.insert(test_map.begin(), to_copy_f);
                REQUIRE(res == test_map.end());
                REQUIRE(test_map.size() == 0);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count == 0);
            }
        }
    }

    SECTION("Move constructable") {
        using m = cne::multimap<std::multimap<copy_constr, move_constr>>;
        m                                   test_map;
        std::pair<copy_constr, move_constr> to_move_s
          = std::make_pair(copy_constr::make(true), move_constr::make(true));
        std::pair<copy_constr, move_constr> to_move_f
          = std::make_pair(copy_constr::make(false), move_constr::make(false));
		
        ADD_MAP_INSERT_LISTENERS(test_map, m, typename m::value_type)

        SECTION("const_value&") {
            SECTION("Success") {
                auto res = test_map.insert(std::move(to_move_s));
                REQUIRE(res != test_map.end());
                REQUIRE(test_map.size() == 1);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_map.insert(std::move(to_move_f));
                REQUIRE(res == test_map.end());
                REQUIRE(test_map.size() == 0);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count == 0);
            }
        }

        SECTION("const_iterator, const value&") {
            SECTION("Success") {
                auto res = test_map.insert(test_map.begin(), std::move(to_move_s));
                REQUIRE(res != test_map.end());
                REQUIRE(test_map.size() == 1);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_map.insert(test_map.begin(), std::move(to_move_f));
                REQUIRE(res == test_map.end());
                REQUIRE(test_map.size() == 0);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count == 0);
            }
        }
    }

    SECTION("Emplace constructible") {
        using m = cne::multimap<std::multimap<emplace_constr, emplace_constr>>;
        m test_map;

        ADD_MAP_INSERT_LISTENERS(test_map, m, typename m::value_type)

        SECTION("const_iterator, InputIt, InputIt") {
            using src_t = std::vector<std::pair<bool, bool>>;
            src_t source;

            SECTION("Success") {
                source = {{true, true}, {true, true}, {true, true}};
                auto res = test_map.insert(source.begin(), source.end());
                REQUIRE(res == true);
                REQUIRE(test_map.size() == 3);
                REQUIRE(insert_call_cnt == 3);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                source = {{{true, true}, {false, false}, {true, true}}};
                auto res = test_map.insert(source.begin(), source.end());
                REQUIRE(res == false);
                REQUIRE(test_map.size() == 0);
                REQUIRE(insert_call_cnt == 2);
                REQUIRE(insert_count == 0);
            }
        }

        SECTION("initializer_list") {
            SECTION("Success") {
                auto res = test_map.insert({{true, true}, {true, true}, {true, true}});
                REQUIRE(res == true);
                REQUIRE(test_map.size() == 3);
                REQUIRE(insert_call_cnt == 3);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_map.insert({{true, true}, {false, false}, {true, true}});
                REQUIRE(res == false);
                REQUIRE(test_map.size() == 0);
                REQUIRE(insert_call_cnt == 2);
                REQUIRE(insert_count == 0);
            }
        }
    }

    SECTION("No requirements") {
        SECTION("Success") {
            using wrapped_type = std::multimap<default_constr_true, default_constr_true>;
            using m = cne::multimap<wrapped_type>;
            m            test_map;
            wrapped_type src;
            src.emplace();

            ADD_MAP_INSERT_LISTENERS(test_map, m, typename m::value_type)

            SECTION("node_handle&&") {
                auto res = test_map.insert(src.extract(src.begin()));
                REQUIRE(res.has_value());
                REQUIRE(*res != test_map.end());
                REQUIRE(test_map.size() == 1);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count != 0);
            }

            SECTION("const_iterator, node_handle&&") {
                auto res = test_map.insert(test_map.begin(), src.extract(src.begin()));
                REQUIRE(res.has_value());
                REQUIRE(*res != test_map.end());
                REQUIRE(test_map.size() == 1);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count != 0);
            }
        }

        SECTION("Fail") {
            using wrapped_type = std::multimap<default_constr_false, default_constr_false>;
            using m = cne::multimap<wrapped_type>;
            m            test_map;
            wrapped_type src;
            src.emplace();

            ADD_MAP_INSERT_LISTENERS(test_map, m, typename m::value_type)

            SECTION("node_handle&&") {
                auto res = test_map.insert(src.extract(src.begin()));
                REQUIRE(!res.has_value());
                REQUIRE(test_map.size() == 0);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count == 0);
            }

            SECTION("const_iterator, node_handle&&") {
                auto res = test_map.insert(test_map.begin(), src.extract(src.begin()));
                REQUIRE(!res.has_value());
                REQUIRE(test_map.size() == 0);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count == 0);
            }
        }
    }
}

TEST_CASE("multimap.emplace", "[multimap][emplace][modifiers][insert_notification]") {
    using m = cne::multimap<std::multimap<emplace_constr, emplace_constr>>;
    m test_map;

    ADD_MAP_INSERT_LISTENERS(test_map, m, typename m::value_type)

    SECTION("Success") {
        auto res = test_map.emplace(true, true);
        REQUIRE(res != test_map.end());
        REQUIRE(test_map.size() == 1);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count != 0);
    }
    SECTION("Fail") {
        auto res = test_map.emplace(false, false);
        REQUIRE(res == test_map.end());
        REQUIRE(test_map.size() == 0);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count == 0);
    }
}

TEST_CASE("multimap.emplace_hint", "[multimap][emplace_hint][modifiers][insert_notification]") {
    using m = cne::multimap<std::multimap<emplace_constr, emplace_constr>>;
    m test_map;

    ADD_MAP_INSERT_LISTENERS(test_map, m, typename m::value_type)

    SECTION("Success") {
        auto res = test_map.emplace_hint(test_map.begin(), true, true);
        REQUIRE(res != test_map.end());
        REQUIRE(test_map.size() == 1);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count != 0);
    }
    SECTION("Fail") {
        auto res = test_map.emplace_hint(test_map.begin(), false, false);
        REQUIRE(res == test_map.end());
        REQUIRE(test_map.size() == 0);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count == 0);
    }
}

/**
 * @brief Can't be tested with minimal type requirements, because all insert operations impose more
 * requirements
 */
TEST_CASE("multimap.erase", "[multimap][erase][modifiers][erase_notification]") {
    using m = cne::multimap<std::multimap<move_constr_assign, move_constr_assign>>;
    m            test_map;
    m::size_type size = 4;

    ADD_MAP_ERASE_LISTENERS(test_map, m, typename m::value_type)

    SECTION("Success") {
        for (m::size_type i = 0; i < size; ++i) {
            test_map.container().emplace(move_constr_assign::make(true), move_constr_assign::make(true));
        }

        SECTION("const_iterator") {
            auto res = test_map.erase(test_map.begin());
            REQUIRE(res.has_value());
            REQUIRE(test_map.size() == (size - 1));
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count != 0);
        }

        SECTION("const_iterator, const_iterator") {
            m::size_type to_erase = 2;

            auto res = test_map.erase(test_map.begin(), std::next(test_map.begin(), to_erase));
            REQUIRE(res.has_value());
            REQUIRE(test_map.size() == (size - to_erase));
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count != 0);
        }

        SECTION("const key_type&") {
            auto res = test_map.erase(test_map.begin()->first);
            REQUIRE(res == 1);
            REQUIRE(test_map.size() == (size - 1));
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count != 0);
        }
    }

    SECTION("Fail") {
        m::size_type fail_pos = 2;
        for (m::size_type i = 0; i < size; ++i) {
            test_map.container().emplace(move_constr_assign::make(i != fail_pos),
                                         move_constr_assign::make(i != fail_pos));
        }

        SECTION("const_iterator") {
            auto res = test_map.erase(std::next(test_map.begin(), fail_pos));
            REQUIRE_FALSE(res.has_value());
            REQUIRE(test_map.size() == size);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count == 0);
        }

        SECTION("const_iterator, const_iterator") {
            m::size_type to_erase = 2;
            auto         first_erase = std::next(test_map.begin());

            auto res = test_map.erase(first_erase, std::next(first_erase, to_erase));
            REQUIRE_FALSE(res.has_value());
            REQUIRE(test_map.size() == size);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count == 0);
        }

        SECTION("const key_type&") {
            test_map.container().emplace(move_constr_assign::make(false), move_constr_assign::make(false));
            auto res = test_map.erase(std::prev(test_map.end())->first);
            REQUIRE(res == 0);
            REQUIRE(test_map.size() == size + 1);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count == 0);
        }
    }
}