#include "catch2.h"

#include <initializer_list>
#include <forward_list>

#include "../headers/containers/forward_list.h"
#include "types.h"
#include "utils.h"

using namespace cne::notifications;
using namespace cne::tests;

TEST_CASE("forward_list.operator=", "[forward_list][operator=][insert_notification][erase_notification][replace_notification]") {
    SECTION("const wrapped_type&") {
        using wrapped_class = std::forward_list<copy_constr_assign>;
        using l = cne::forward_list<wrapped_class>;

        l    dest{copy_constr_assign::make(true), copy_constr_assign::make(true), copy_constr_assign::make(true)};
        l::size_type orig_size = 3;

        ADD_INSERT_LISTENERS(dest, l, copy_constr_assign)
        ADD_REPLACE_LISTENERS(dest, l, copy_constr_assign)
        ADD_ERASE_LISTENERS(dest, l, copy_constr_assign)

        SECTION("Assign smaller") {
            wrapped_class src_smaller_s{copy_constr_assign::make(true), copy_constr_assign::make(true)};
            l::size_type  smaller_size = 2;

            SECTION("Success") {
                dest = src_smaller_s;
                REQUIRE(insert_call_cnt == 0);
                REQUIRE(erase_call_cnt == (orig_size - smaller_size));
                REQUIRE(replace_call_cnt == smaller_size);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_count != 0);
                REQUIRE(replace_count != 0);
            }

            SECTION("Fail") {
                SECTION("Erase fail") {
                    *std::next(dest.container().begin(), 2) = copy_constr_assign::make(false);

                    dest = src_smaller_s;
                    REQUIRE(insert_call_cnt == 0);
                    REQUIRE(erase_call_cnt == 1);
                    REQUIRE(insert_count == 0);
                    REQUIRE(erase_count == 0);
                    REQUIRE(replace_count == 0);
                }

                SECTION("Replace fail") {
                    wrapped_class src_smaller_f{copy_constr_assign::make(true), copy_constr_assign::make(false)};

                    dest = src_smaller_f;
                    REQUIRE(insert_call_cnt == 0);
                    REQUIRE(replace_call_cnt == smaller_size);
                    REQUIRE(insert_count == 0);
                    REQUIRE(erase_count == 0);
                    REQUIRE(replace_count == 0);
                }
            }
        }

        SECTION("Assign bigger") {
            l::size_type bigger_size = 4;
            SECTION("Success") {
                wrapped_class src_bigger_s{copy_constr_assign::make(true), copy_constr_assign::make(true),
                                           copy_constr_assign::make(true), copy_constr_assign::make(true)};

                dest = src_bigger_s;
                REQUIRE(insert_call_cnt == (bigger_size - orig_size));
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_call_cnt == orig_size);
                REQUIRE(insert_count != 0);
                REQUIRE(erase_count == 0);
                REQUIRE(replace_count != 0);
            }

            SECTION("Fail") {
                SECTION("Insert fail") {
                    wrapped_class src_bigger_f{copy_constr_assign::make(true), copy_constr_assign::make(true),
                                               copy_constr_assign::make(true), copy_constr_assign::make(false)};

                    dest = src_bigger_f;
                    REQUIRE(insert_call_cnt == 1);
                    REQUIRE(erase_call_cnt == 0);
                    REQUIRE(insert_count == 0);
                    REQUIRE(erase_call_cnt == 0);
                    REQUIRE(replace_count == 0);
                }

                SECTION("Replace fail") {
                    wrapped_class src_bigger_f{copy_constr_assign::make(true), copy_constr_assign::make(true),
                                               copy_constr_assign::make(false), copy_constr_assign::make(true)};

                    dest = src_bigger_f;
                    REQUIRE(erase_call_cnt == 0);
                    REQUIRE(replace_call_cnt == 3);
                    REQUIRE(insert_count == 0);
                    REQUIRE(erase_call_cnt == 0);
                    REQUIRE(replace_count == 0);
                }
            }
        }

        SECTION("Assign same size") {
            SECTION("Success") {
                wrapped_class src_same_s{copy_constr_assign::make(true), copy_constr_assign::make(true),
                                         copy_constr_assign::make(true)};

                dest = src_same_s;
                REQUIRE(insert_call_cnt == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_call_cnt == orig_size);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_count != 0);
            }

            SECTION("Fail") {
                wrapped_class src_same_f{copy_constr_assign::make(true), copy_constr_assign::make(false),
                                         copy_constr_assign::make(true)};

                dest = src_same_f;
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(insert_call_cnt == 0);
                REQUIRE(replace_call_cnt == 2);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_count == 0);
            }
        }
    }

    SECTION("wrapped_type&&") {
        using wrapped_class = std::forward_list<move_constr>;
        using l = cne::forward_list<wrapped_class>;

        l            dest;
        l::size_type orig_size = 3;
        for (l::size_type i = 0; i < orig_size; ++i) { dest.container().emplace_front(move_constr::make(true)); }

        ADD_INSERT_LISTENERS(dest, l, move_constr)
        ADD_REPLACE_LISTENERS(dest, l, move_constr)
        ADD_ERASE_LISTENERS(dest, l, move_constr)

        SECTION("Assign smaller") {
            wrapped_class            src_smaller_s;
            wrapped_class            src_smaller_f;
            wrapped_class::size_type smaller_size = 2;

            for (l::size_type i = 0; i < smaller_size; ++i) {
                src_smaller_s.emplace_front(move_constr::make(true));
                src_smaller_f.emplace_front(move_constr::make(i != 0));
            }

            SECTION("Success") {
                dest = std::move(src_smaller_s);
                REQUIRE(insert_call_cnt == 0);
                REQUIRE(erase_call_cnt == (orig_size - smaller_size));
                REQUIRE(replace_call_cnt == smaller_size);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_count != 0);
                REQUIRE(replace_count != 0);
            }

            SECTION("Fail") {
                SECTION("Erase fail") {
                    l dest2;
                    for (l::size_type i = 0; i < orig_size; ++i) {
                        dest2.container().emplace_front(move_constr::make(i != 0));
                    }

                    ADD_INSERT_LISTENERS(dest2, l, move_constr)
                    ADD_REPLACE_LISTENERS(dest2, l, move_constr)
                    ADD_ERASE_LISTENERS(dest2, l, move_constr)

                    dest2 = std::move(src_smaller_s);
                    REQUIRE(insert_call_cnt == 0);
                    REQUIRE(erase_call_cnt == 1);
                    REQUIRE(insert_count == 0);
                    REQUIRE(erase_count == 0);
                    REQUIRE(replace_count == 0);
                }

                SECTION("Replace fail") {
                    dest = std::move(src_smaller_f);
                    REQUIRE(insert_call_cnt == 0);
                    REQUIRE(replace_call_cnt == smaller_size);
                    REQUIRE(insert_count == 0);
                    REQUIRE(erase_count == 0);
                    REQUIRE(replace_count == 0);
                }
            }
        }

        SECTION("Assign bigger") {
            wrapped_class            src_bigger_s;
            wrapped_class            src_bigger_fr;
            wrapped_class            src_bigger_fi;
            wrapped_class::size_type bigger_size = 4;

            for (l::size_type i = 0; i < bigger_size; ++i) {
                src_bigger_s.emplace_front(move_constr::make(true));
                src_bigger_fr.emplace_front(move_constr::make(i != (bigger_size - orig_size)));
                src_bigger_fi.emplace_front(move_constr::make(i != 0));
            }

            SECTION("Success") {
                dest = std::move(src_bigger_s);
                REQUIRE(insert_call_cnt == (bigger_size - orig_size));
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_call_cnt == orig_size);
                REQUIRE(insert_count != 0);
                REQUIRE(erase_count == 0);
                REQUIRE(replace_count != 0);
            }

            SECTION("Fail") {
                SECTION("Insert fail") {
                    dest = std::move(src_bigger_fi);
                    REQUIRE(insert_call_cnt == (bigger_size - orig_size));
                    REQUIRE(erase_call_cnt == 0);
                    REQUIRE(insert_count == 0);
                    REQUIRE(erase_call_cnt == 0);
                    REQUIRE(replace_count == 0);
                }

                SECTION("Replace fail") {
                    dest = std::move(src_bigger_fr);
                    REQUIRE(erase_call_cnt == 0);
                    REQUIRE(replace_call_cnt == orig_size);
                    REQUIRE(insert_count == 0);
                    REQUIRE(erase_call_cnt == 0);
                    REQUIRE(replace_count == 0);
                }
            }
        }

        SECTION("Assign same size") {
            wrapped_class src_same_s;
            wrapped_class src_same_f;

            for (l::size_type i = 0; i < orig_size; ++i) {
                src_same_s.emplace_front(move_constr::make(true));
                src_same_f.emplace_front(move_constr::make(i != 0));
            }

            SECTION("Success") {
                dest = std::move(src_same_s);
                REQUIRE(insert_call_cnt == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_call_cnt == orig_size);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_count != 0);
            }

            SECTION("Fail") {
                dest = std::move(src_same_f);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(insert_call_cnt == 0);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_count == 0);
            }
        }
    }
    
    SECTION("initializer_list") {
        using wrapped_class = std::forward_list<copy_constr_assign>;
        using l = cne::forward_list<wrapped_class>;

        l dest{copy_constr_assign::make(true), copy_constr_assign::make(true), copy_constr_assign::make(true)};
        l::size_type orig_size = 3;

        ADD_INSERT_LISTENERS(dest, l, copy_constr_assign)
        ADD_REPLACE_LISTENERS(dest, l, copy_constr_assign)
        ADD_ERASE_LISTENERS(dest, l, copy_constr_assign)

        SECTION("Assign smaller") {
            std::initializer_list<copy_constr_assign> src_smaller_s{copy_constr_assign::make(true),
                                                                    copy_constr_assign::make(true)};
            l::size_type                              smaller_size = 2;

            SECTION("Success") {
                dest = src_smaller_s;
                REQUIRE(insert_call_cnt == 0);
                REQUIRE(erase_call_cnt == (orig_size - smaller_size));
                REQUIRE(replace_call_cnt == smaller_size);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_count != 0);
                REQUIRE(replace_count != 0);
            }

            SECTION("Fail") {
                SECTION("Erase fail") {
                    *std::next(dest.container().begin(), 2) = copy_constr_assign::make(false);

                    dest = src_smaller_s;
                    REQUIRE(insert_call_cnt == 0);
                    REQUIRE(erase_call_cnt == 1);
                    REQUIRE(insert_count == 0);
                    REQUIRE(erase_count == 0);
                    REQUIRE(replace_count == 0);
                }

                SECTION("Replace fail") {
                    std::initializer_list<copy_constr_assign> src_smaller_f{copy_constr_assign::make(true),
                                                                            copy_constr_assign::make(false)};

                    dest = src_smaller_f;
                    REQUIRE(insert_call_cnt == 0);
                    REQUIRE(replace_call_cnt == smaller_size);
                    REQUIRE(insert_count == 0);
                    REQUIRE(erase_count == 0);
                    REQUIRE(replace_count == 0);
                }
            }
        }

        SECTION("Assign bigger") {
            l::size_type bigger_size = 4;

            SECTION("Success") {
                std::initializer_list<copy_constr_assign> src_bigger_s{
                  copy_constr_assign::make(true), copy_constr_assign::make(true), copy_constr_assign::make(true),
                  copy_constr_assign::make(true)};

                dest = src_bigger_s;
                REQUIRE(insert_call_cnt == (bigger_size - orig_size));
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_call_cnt == orig_size);
                REQUIRE(insert_count != 0);
                REQUIRE(erase_count == 0);
                REQUIRE(replace_count != 0);
            }

            SECTION("Fail") {
                std::initializer_list<copy_constr_assign> src_bigger_f{
                  copy_constr_assign::make(true), copy_constr_assign::make(true), copy_constr_assign::make(false),
                  copy_constr_assign::make(true)};

                dest = src_bigger_f;
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_count == 0);
            }
        }

        SECTION("Assign same size") {
            SECTION("Success") {
                std::initializer_list<copy_constr_assign> src_same_s{
                  copy_constr_assign::make(true), copy_constr_assign::make(true), copy_constr_assign::make(true)};

                dest = src_same_s;
                REQUIRE(insert_call_cnt == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_call_cnt == orig_size);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_count != 0);
            }

            SECTION("Fail") {
                std::initializer_list<copy_constr_assign> src_same_f{
                  copy_constr_assign::make(true), copy_constr_assign::make(false), copy_constr_assign::make(true)};

                dest = src_same_f;
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(insert_call_cnt == 0);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_count == 0);
            }
        }
    }
}

TEST_CASE("forward_list.assign", "[forward_list][assign][insert_notification][erase_notification][replace_notification]") {
    using wrapped_class = std::forward_list<copy_constr_assign>;
    using l = cne::forward_list<wrapped_class>;

    l            dest{copy_constr_assign::make(true), copy_constr_assign::make(true), copy_constr_assign::make(true)};
    l::size_type orig_size = 3;

    ADD_INSERT_LISTENERS(dest, l, copy_constr_assign)
    ADD_REPLACE_LISTENERS(dest, l, copy_constr_assign)
    ADD_ERASE_LISTENERS(dest, l, copy_constr_assign)

    SECTION("size_type, const value_type&") {
        auto to_copy_s = copy_constr_assign::make(true);
        auto to_copy_f = copy_constr_assign::make(false);

        SECTION("Assign smaller") {
            auto new_size = orig_size - 1;

            SECTION("Success") {
                bool res = dest.assign(new_size, to_copy_s);
                REQUIRE(res == true);
                REQUIRE(insert_call_cnt == 0);
                REQUIRE(erase_call_cnt == (orig_size - new_size));
                REQUIRE(replace_call_cnt == new_size);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_count != 0);
                REQUIRE(replace_count != 0);
            }

            SECTION("Fail") {
                SECTION("Erase fail") {
                    *std::next(dest.container().begin(), 2) = copy_constr_assign::make(false);

                    bool res = dest.assign(new_size, to_copy_s);
                    REQUIRE(res == false);
                    REQUIRE(insert_call_cnt == 0);
                    REQUIRE(insert_count == 0);
                    REQUIRE(erase_count == 0);
                    REQUIRE(replace_count == 0);
                }

                SECTION("Replace fail") {
                    bool res = dest.assign(new_size, to_copy_f);
                    REQUIRE(res == false);
                    REQUIRE(insert_call_cnt == 0);
                    REQUIRE(insert_count == 0);
                    REQUIRE(erase_count == 0);
                    REQUIRE(replace_count == 0);
                }
            }
        }

        SECTION("Assign bigger") {
            auto new_size = orig_size + 1;
            SECTION("Success") {
                bool res = dest.assign(new_size, to_copy_s);
                REQUIRE(res == true);
                REQUIRE(insert_call_cnt == (new_size - orig_size));
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_call_cnt == orig_size);
                REQUIRE(insert_count != 0);
                REQUIRE(erase_count == 0);
                REQUIRE(replace_count != 0);
            }

            SECTION("Fail") {
                bool res = dest.assign(new_size, to_copy_f);
                REQUIRE(res == false);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_count == 0);
            }
        }

        SECTION("Assign same size") {
            SECTION("Success") {
                bool res = dest.assign(orig_size, to_copy_s);
                REQUIRE(res == true);
                REQUIRE(insert_call_cnt == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_call_cnt == orig_size);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_count != 0);
            }

            SECTION("Fail") {
                bool res = dest.assign(orig_size, to_copy_f);
                REQUIRE(res == false);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(insert_call_cnt == 0);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_count == 0);
            }
        }
    }

    SECTION("InputIt, InputIt") {
        SECTION("Assign smaller") {
            l src_smaller_s{copy_constr_assign::make(true), copy_constr_assign::make(true)};
            l::size_type smaller_size = 2;

            SECTION("Success") {
                bool res = dest.assign(src_smaller_s.begin(), src_smaller_s.end());
                REQUIRE(res == true);
                REQUIRE(insert_call_cnt == 0);
                REQUIRE(erase_call_cnt == (orig_size - smaller_size));
                REQUIRE(replace_call_cnt == smaller_size);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_count != 0);
                REQUIRE(replace_count != 0);
            }

            SECTION("Fail") {
                SECTION("Erase fail") {
                    *std::next(dest.container().begin(), 2) = copy_constr_assign::make(false);

                    bool res = dest.assign(src_smaller_s.begin(), src_smaller_s.end());
                    REQUIRE(res == false);
                    REQUIRE(insert_call_cnt == 0);
                    REQUIRE(insert_count == 0);
                    REQUIRE(erase_count == 0);
                    REQUIRE(replace_count == 0);
                }

                SECTION("Replace fail") {
                    l src_smaller_f{copy_constr_assign::make(true), copy_constr_assign::make(false)};

                    bool res = dest.assign(src_smaller_f.begin(), src_smaller_f.end());
                    REQUIRE(res == false);
                    REQUIRE(insert_call_cnt == 0);
                    REQUIRE(insert_count == 0);
                    REQUIRE(erase_count == 0);
                    REQUIRE(replace_count == 0);
                }
            }
        }

        SECTION("Assign bigger") {
            l::size_type bigger_size = 4;

            SECTION("Success") {
                l src_bigger_s{copy_constr_assign::make(true), copy_constr_assign::make(true),
                               copy_constr_assign::make(true), copy_constr_assign::make(true)};

                bool res = dest.assign(src_bigger_s.begin(), src_bigger_s.end());
                REQUIRE(res == true);
                REQUIRE(insert_call_cnt == (bigger_size - orig_size));
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_call_cnt == orig_size);
                REQUIRE(insert_count != 0);
                REQUIRE(erase_count == 0);
                REQUIRE(replace_count != 0);
            }

            SECTION("Fail") {
                l src_bigger_f{copy_constr_assign::make(true), copy_constr_assign::make(true),
                               copy_constr_assign::make(false), copy_constr_assign::make(true)};

                bool res = dest.assign(src_bigger_f.begin(), src_bigger_f.end());
                REQUIRE(res == false);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_count == 0);
            }
        }

        SECTION("Assign same size") {
            SECTION("Success") {
                l src_same_s{copy_constr_assign::make(true), copy_constr_assign::make(true),
                             copy_constr_assign::make(true)};

                bool res = dest.assign(src_same_s.begin(), src_same_s.end());
                REQUIRE(res == true);
                REQUIRE(insert_call_cnt == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_call_cnt == orig_size);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_count != 0);
            }

            SECTION("Fail") {
                l src_same_f{copy_constr_assign::make(true), copy_constr_assign::make(false),
                             copy_constr_assign::make(true)};

                bool res = dest.assign(src_same_f.begin(), src_same_f.end());
                REQUIRE(res == false);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(insert_call_cnt == 0);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_count == 0);
            }
        }
    }

    SECTION("initializer_list") {
        SECTION("Assign smaller") {
            std::initializer_list<copy_constr_assign> src_smaller_s{copy_constr_assign::make(true),
                                                                    copy_constr_assign::make(true)};
            l::size_type                              smaller_size = 2;

            SECTION("Success") {
                bool res = dest.assign(src_smaller_s);
                REQUIRE(res == true);
                REQUIRE(insert_call_cnt == 0);
                REQUIRE(erase_call_cnt == (orig_size - smaller_size));
                REQUIRE(replace_call_cnt == smaller_size);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_count != 0);
                REQUIRE(replace_count != 0);
            }

            SECTION("Fail") {
                SECTION("Erase fail") {
                    *std::next(dest.container().begin(), 2) = copy_constr_assign::make(false);

                    bool res = dest.assign(src_smaller_s);
                    REQUIRE(res == false);
                    REQUIRE(insert_call_cnt == 0);
                    REQUIRE(insert_count == 0);
                    REQUIRE(erase_count == 0);
                    REQUIRE(replace_count == 0);
                }

                SECTION("Replace fail") {
                    std::initializer_list<copy_constr_assign> src_smaller_f{copy_constr_assign::make(true),
                                                                            copy_constr_assign::make(false)};

                    bool res = dest.assign(src_smaller_f);
                    REQUIRE(res == false);
                    REQUIRE(insert_call_cnt == 0);
                    REQUIRE(insert_count == 0);
                    REQUIRE(erase_count == 0);
                    REQUIRE(replace_count == 0);
                }
            }
        }

        SECTION("Assign bigger") {
            l::size_type bigger_size = 4;

            SECTION("Success") {
                std::initializer_list<copy_constr_assign> src_bigger_s{
                  copy_constr_assign::make(true), copy_constr_assign::make(true), copy_constr_assign::make(true),
                  copy_constr_assign::make(true)};

                bool res = dest.assign(src_bigger_s);
                REQUIRE(res == true);
                REQUIRE(insert_call_cnt == (bigger_size - orig_size));
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_call_cnt == orig_size);
                REQUIRE(insert_count != 0);
                REQUIRE(erase_count == 0);
                REQUIRE(replace_count != 0);
            }

            SECTION("Fail") {
                std::initializer_list<copy_constr_assign> src_bigger_f{
                  copy_constr_assign::make(true), copy_constr_assign::make(true), copy_constr_assign::make(false),
                  copy_constr_assign::make(true)};

                bool res = dest.assign(src_bigger_f);
                REQUIRE(res == false);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_count == 0);
            }
        }

        SECTION("Assign same size") {
            SECTION("Success") {
                std::initializer_list<copy_constr_assign> src_same_s{
                  copy_constr_assign::make(true), copy_constr_assign::make(true), copy_constr_assign::make(true)};

                bool res = dest.assign(src_same_s);
                REQUIRE(res == true);
                REQUIRE(insert_call_cnt == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_call_cnt == orig_size);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_count != 0);
            }

            SECTION("Fail") {
                std::initializer_list<copy_constr_assign> src_same_f{
                  copy_constr_assign::make(true), copy_constr_assign::make(false), copy_constr_assign::make(true)};

                bool res = dest.assign(src_same_f);
                REQUIRE(res == false);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(insert_call_cnt == 0);
                REQUIRE(insert_count == 0);
                REQUIRE(erase_call_cnt == 0);
                REQUIRE(replace_count == 0);
            }
        }
    }
}
