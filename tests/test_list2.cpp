#include "catch2.h"

#include <forward_list>
#include <initializer_list>
#include <list>

#include "../headers/containers/list.h"
#include "types.h"
#include "utils.h"

using namespace cne::notifications;
using namespace cne::tests;

TEST_CASE("list.front", "[list][front][element_access]") {
    using l = cne::list<std::list<int>>;

    l   test_list = {3, 5};
    int value;

    SECTION("Const") {
        const l& const_list = test_list;
        value = const_list.front();
        REQUIRE(value == 3);
    }

    SECTION("Non const") {
        value = test_list.front();
        REQUIRE(value == 3);
    }
}

TEST_CASE("list.back", "[list][back][element_access]") {
    using l = cne::list<std::list<int>>;

    l   test_list = {3, 5};
    int value;

    SECTION("Const") {
        const l& const_list = test_list;
        value = const_list.back();
        REQUIRE(value == 5);
    }

    SECTION("Non const") {
        value = test_list.back();
        REQUIRE(value == 5);
    }
}

TEST_CASE("list.clear", "[list][clear][modifiers][erase_notification]") {
    int size = 5;

    SECTION("Success") {
        using l = cne::list<std::list<default_constr_true>>;
        l test_list(size);

        ADD_ERASE_LISTENERS(test_list, l, default_constr_true)

        bool res = test_list.clear();
        REQUIRE(res == true);
        REQUIRE(test_list.size() == 0);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        using l = cne::list<std::list<default_constr_false>>;
        l test_list(size);

        ADD_ERASE_LISTENERS(test_list, l, default_constr_false)

        bool res = test_list.clear();
        REQUIRE(res == false);
        REQUIRE(test_list.size() == size);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("list.insert", "[list][insert][modifiers][insert_notification]") {
    SECTION("Copy insertable") {
        using l = cne::list<std::list<copy_constr>>;
        l test_list;

        auto to_copy_s = copy_constr::make(true);
        auto to_copy_f = copy_constr::make(false);

        ADD_INSERT_LISTENERS(test_list, l, copy_constr)

        SECTION("const_iterator, const value_type&") {
            SECTION("Success") {
                auto res = test_list.insert(test_list.cend(), to_copy_s);
                REQUIRE(res != test_list.end());
                REQUIRE(test_list.size() == 1);
                REQUIRE(insert_call_cnt == 1);
            }
            SECTION("Fail") {
                auto res = test_list.insert(test_list.cend(), to_copy_f);
                REQUIRE(res == test_list.end());
                REQUIRE(test_list.size() == 0);
                REQUIRE(insert_count == 0);
            }
        }
    }

    SECTION("Copy insertable and copy assignable") {
        using l = cne::list<std::list<copy_constr_assign>>;
        l test_list;

        auto to_copy_s = copy_constr_assign::make(true);
        auto to_copy_f = copy_constr_assign::make(false);

        ADD_INSERT_LISTENERS(test_list, l, copy_constr_assign)

        SECTION("const_iterator, size_type, const value_type&") {
            l::size_type count = 3;
            SECTION("Success") {
                auto res = test_list.insert(test_list.cend(), count, to_copy_s);
                REQUIRE(res != test_list.end());
                REQUIRE(test_list.size() == count);
                REQUIRE(insert_call_cnt == count);
            }
            SECTION("Fail") {
                auto res = test_list.insert(test_list.cend(), count, to_copy_f);
                REQUIRE(res == test_list.end());
                REQUIRE(test_list.size() == 0);
                REQUIRE(insert_count == 0);
            }
        }
    }

    SECTION("Move insertable") {
        using l = cne::list<std::list<move_constr>>;
        l test_list;

        ADD_INSERT_LISTENERS(test_list, l, move_constr)

        SECTION("const_iterator, value_type&&") {
            SECTION("Success") {
                auto res = test_list.insert(test_list.cend(), move_constr::make(true));
                REQUIRE(res != test_list.end());
                REQUIRE(test_list.size() == 1);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_list.insert(test_list.cend(), move_constr::make(false));
                REQUIRE(res == test_list.end());
                REQUIRE(test_list.size() == 0);
                REQUIRE(insert_call_cnt == 1);
                REQUIRE(insert_count == 0);
            }
        }
    }

    SECTION("Emplace constructible") {
        using l = cne::list<std::list<emplace_constr>>;
        l test_list;

        ADD_INSERT_LISTENERS(test_list, l, emplace_constr)

        SECTION("const_iterator, InputIt, InputIt") {
            using src_t = std::forward_list<bool>;
            src_t source;

            SECTION("Success") {
                source = {true, true, true};
                auto res = test_list.insert(test_list.cend(), source.begin(), source.end());
                REQUIRE(res != test_list.end());
                REQUIRE(test_list.size() == 3);
                REQUIRE(insert_call_cnt == 3);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                source = {true, false, true};
                auto res = test_list.insert(test_list.cend(), source.begin(), source.end());
                REQUIRE(res == test_list.end());
                REQUIRE(test_list.size() == 0);
                REQUIRE(insert_call_cnt == 2);
                REQUIRE(insert_count == 0);
            }
        }

        SECTION("initializer_list") {
            SECTION("Success") {
                auto res = test_list.insert(test_list.cend(), {true, true, true});
                REQUIRE(res != test_list.end());
                REQUIRE(test_list.size() == 3);
                REQUIRE(insert_call_cnt == 3);
                REQUIRE(insert_count != 0);
            }
            SECTION("Fail") {
                auto res = test_list.insert(test_list.cend(), {true, false, true});
                REQUIRE(res == test_list.end());
                REQUIRE(test_list.size() == 0);
                REQUIRE(insert_call_cnt == 2);
                REQUIRE(insert_count == 0);
            }
        }
    }
}

TEST_CASE("list.emplace", "[list][emplace][modifiers][insert_notification]") {
    using l = cne::list<std::list<emplace_constr>>;
    l test_list;

    ADD_INSERT_LISTENERS(test_list, l, emplace_constr)

    SECTION("Success") {
        auto res = test_list.emplace(test_list.begin(), true);
        REQUIRE(res != test_list.end());
        REQUIRE(test_list.size() == 1);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count != 0);
    }
    SECTION("Fail") {
        auto res = test_list.emplace(test_list.begin(), false);
        REQUIRE(res == test_list.end());
        REQUIRE(test_list.size() == 0);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count == 0);
    }
}

/**
 * @brief Can't be tested with minimal type requirements, because all insert operations impose more
 * requirements
 */
TEST_CASE("list.erase", "[list][erase][modifiers][erase_notification]") {
    using l = cne::list<std::list<move_constr_assign>>;
    l            test_list;
    l::size_type size = 4;

    ADD_ERASE_LISTENERS(test_list, l, move_constr_assign)

    SECTION("Success") {
        for (l::size_type i = 0; i < size; ++i) { test_list.container().emplace_back(move_constr_assign::make(true)); }

        SECTION("const_iterator") {
            auto res = test_list.erase(test_list.begin());
            REQUIRE(res.has_value());
            REQUIRE(test_list.size() == (size - 1));
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count != 0);
        }

        SECTION("const_iterator, const_iterator") {
            l::size_type to_erase = 2;

            auto res = test_list.erase(test_list.begin(), std::next(test_list.begin(), to_erase));
            REQUIRE(res.has_value());
            REQUIRE(test_list.size() == (size - to_erase));
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count != 0);
        }
    }

    SECTION("Fail") {
        l::size_type fail_pos = 2;
        for (l::size_type i = 0; i < size; ++i) {
            test_list.container().emplace_back(move_constr_assign::make(i != fail_pos));
        }

        SECTION("const_iterator") {
            auto res = test_list.erase(std::next(test_list.begin(), fail_pos));
            REQUIRE_FALSE(res.has_value());
            REQUIRE(test_list.size() == size);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count == 0);
        }

        SECTION("const_iterator, const_iterator") {
            l::size_type to_erase = 2;
            auto         first_erase = std::next(test_list.begin());

            auto res = test_list.erase(first_erase, std::next(first_erase, to_erase));
            REQUIRE_FALSE(res.has_value());
            REQUIRE(test_list.size() == size);
            REQUIRE(erase_call_cnt == 1);
            REQUIRE(erase_count == 0);
        }
    }
}

TEST_CASE("list.push_front", "[list][push_front][modifiers][insert_notification]") {
    SECTION("const value_type&") {
        using l = cne::list<std::list<copy_constr>>;
        l test_list;

        ADD_INSERT_LISTENERS(test_list, l, copy_constr);

        SECTION("Success") {
            auto to_copy = copy_constr::make(true);
            bool res = test_list.push_front(to_copy);
            REQUIRE(res);
            REQUIRE(test_list.size() == 1);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count != 0);
        }

        SECTION("Fail") {
            auto to_copy = copy_constr::make(false);
            bool res = test_list.push_front(to_copy);
            REQUIRE_FALSE(res);
            REQUIRE(test_list.size() == 0);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count == 0);
        }
    }

    SECTION("value_type&&") {
        using l = cne::list<std::list<move_constr>>;
        l test_list;

        ADD_INSERT_LISTENERS(test_list, l, move_constr);

        SECTION("Success") {
            bool res = test_list.push_front(move_constr::make(true));
            REQUIRE(res == true);
            REQUIRE(test_list.size() == 1);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count != 0);
        }

        SECTION("Fail") {
            bool res = test_list.push_front(move_constr::make(false));
            REQUIRE_FALSE(res);
            REQUIRE(test_list.size() == 0);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count == 0);
        }
    }
}

TEST_CASE("list.emplace_front", "[list][emplace_front][modifiers][insert_notification]") {
    using l = cne::list<std::list<emplace_constr>>;
    l test_list;

    ADD_INSERT_LISTENERS(test_list, l, emplace_constr);

    SECTION("Success") {
        auto res = test_list.emplace_front(true);
        REQUIRE(res.has_value());
        REQUIRE(res->data().get_value() == true);
        REQUIRE(test_list.size() == 1);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count != 0);
    }

    SECTION("Fail") {
        auto res = test_list.emplace_front(false);
        REQUIRE_FALSE(res.has_value());
        REQUIRE(test_list.size() == 0);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count == 0);
    }
}

/**
 * @brief Doesn't impose any type requirements other than Erasable.
 */
TEST_CASE("list.pop_front", "[list][pop_front][modifiers][erase_notification]") {
    using l = cne::list<std::list<no_restrictions>>;
    l test_list;

    ADD_ERASE_LISTENERS(test_list, l, no_restrictions);

    SECTION("Success") {
        test_list.container().emplace_back(true);
        bool res = test_list.pop_front();
        REQUIRE(res);
        REQUIRE(test_list.size() == 0);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        test_list.container().emplace_back(false);
        bool res = test_list.pop_front();
        REQUIRE_FALSE(res);
        REQUIRE(test_list.size() == 1);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("list.push_back", "[list][push_back][modifiers][insert_notification]") {
    SECTION("const value_type&") {
        using l = cne::list<std::list<copy_constr>>;
        l test_list;

        ADD_INSERT_LISTENERS(test_list, l, copy_constr);

        SECTION("Success") {
            auto to_copy = copy_constr::make(true);
            bool res = test_list.push_back(to_copy);
            REQUIRE(res);
            REQUIRE(test_list.size() == 1);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count != 0);
        }

        SECTION("Fail") {
            auto to_copy = copy_constr::make(false);
            bool res = test_list.push_back(to_copy);
            REQUIRE_FALSE(res);
            REQUIRE(test_list.size() == 0);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count == 0);
        }
    }

    SECTION("value_type&&") {
        using l = cne::list<std::list<move_constr>>;
        l test_list;

        ADD_INSERT_LISTENERS(test_list, l, move_constr);

        SECTION("Success") {
            bool res = test_list.push_back(move_constr::make(true));
            REQUIRE(res == true);
            REQUIRE(test_list.size() == 1);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count != 0);
        }

        SECTION("Fail") {
            bool res = test_list.push_back(move_constr::make(false));
            REQUIRE_FALSE(res);
            REQUIRE(test_list.size() == 0);
            REQUIRE(insert_call_cnt == 1);
            REQUIRE(insert_count == 0);
        }
    }
}

TEST_CASE("list.emplace_back", "[list][emplace_back][modifiers][insert_notification]") {
    using l = cne::list<std::list<move_constr_emplace>>;
    l test_list;

    ADD_INSERT_LISTENERS(test_list, l, move_constr_emplace);

    SECTION("Success") {
        auto res = test_list.emplace_back(true);
        REQUIRE(res.has_value());
        REQUIRE(res->data().get_value() == true);
        REQUIRE(test_list.size() == 1);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count != 0);
    }

    SECTION("Fail") {
        auto res = test_list.emplace_back(false);
        REQUIRE_FALSE(res.has_value());
        REQUIRE(test_list.size() == 0);
        REQUIRE(insert_call_cnt == 1);
        REQUIRE(insert_count == 0);
    }
}

/**
 * @brief Doesn't impose any type requirements other than Erasable.
 */
TEST_CASE("list.pop_back", "[list][pop_back][modifiers][erase_notification]") {
    using l = cne::list<std::list<no_restrictions>>;
    l test_list;

    ADD_ERASE_LISTENERS(test_list, l, no_restrictions);

    SECTION("Success") {
        test_list.container().emplace_back(true);
        bool res = test_list.pop_back();
        REQUIRE(res);
        REQUIRE(test_list.size() == 0);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        test_list.container().emplace_back(false);
        bool res = test_list.pop_back();
        REQUIRE_FALSE(res);
        REQUIRE(test_list.size() == 1);
        REQUIRE(erase_call_cnt == 1);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("list.resize", "[list][resize][modifiers][insert_notification][erase_notification]") {
    SECTION("size_type") {
        SECTION("Success") {
            using l = cne::list<std::list<default_constr_true>>;
            l::size_type orig_size = 3;
            l            test_list(orig_size);

            SECTION("Enlarge") {
                ADD_INSERT_LISTENERS(test_list, l, default_constr_true)

                l::size_type new_size = 4;
                auto         res = test_list.resize(new_size);
                REQUIRE(res == true);
                REQUIRE(test_list.size() == new_size);
                REQUIRE(insert_call_cnt == (new_size - orig_size));
                REQUIRE(insert_count != 0);
            }

            SECTION("Shrink") {
                ADD_ERASE_LISTENERS(test_list, l, default_constr_true)

                l::size_type new_size = 2;
                auto         res = test_list.resize(new_size);
                REQUIRE(res == true);
                REQUIRE(test_list.size() == new_size);
                REQUIRE(erase_call_cnt == orig_size - new_size);
                REQUIRE(erase_count != 0);
            }
        }

        SECTION("Fail") {
            using l = cne::list<std::list<default_constr_false>>;
            l::size_type orig_size = 3;
            l            test_list(orig_size);

            SECTION("Enlarge") {
                ADD_INSERT_LISTENERS(test_list, l, default_constr_false)

                l::size_type new_size = 4;
                auto         res = test_list.resize(new_size);
                REQUIRE(res == false);
                REQUIRE(test_list.size() == orig_size);
                REQUIRE(insert_count == 0);
            }

            SECTION("Shrink") {
                ADD_ERASE_LISTENERS(test_list, l, default_constr_false)

                l::size_type new_size = 2;
                auto         res = test_list.resize(new_size);
                REQUIRE(res == false);
                REQUIRE(test_list.size() == orig_size);
                REQUIRE(erase_call_cnt == 1);
                REQUIRE(erase_count == 0);
            }
        }
    }

    SECTION("size_type, const value_type&") {
        using l = cne::list<std::list<copy_constr>>;
        l::size_type orig_size = 3;
        l            test_list;
        auto         to_copy_s = copy_constr::make(true);
        auto         to_copy_f = copy_constr::make(false);

        SECTION("Success") {
            test_list.container().resize(orig_size, to_copy_s);

            SECTION("Enlarge") {
                ADD_INSERT_LISTENERS(test_list, l, copy_constr)

                l::size_type new_size = 4;
                auto         res = test_list.resize(new_size, to_copy_s);
                REQUIRE(res == true);
                REQUIRE(test_list.size() == new_size);
                REQUIRE(insert_call_cnt == (new_size - orig_size));
                REQUIRE(insert_count != 0);
            }

            SECTION("Shrink") {
                ADD_ERASE_LISTENERS(test_list, l, copy_constr)

                l::size_type new_size = 2;
                auto         res = test_list.resize(new_size, to_copy_s);
                REQUIRE(res == true);
                REQUIRE(test_list.size() == new_size);
                REQUIRE(erase_call_cnt == orig_size - new_size);
                REQUIRE(erase_count != 0);
            }
        }

        SECTION("Fail") {
            test_list.container().resize(orig_size, to_copy_f);

            SECTION("Enlarge") {
                ADD_INSERT_LISTENERS(test_list, l, copy_constr)

                l::size_type new_size = 4;
                auto         res = test_list.resize(new_size, to_copy_f);
                REQUIRE(res == false);
                REQUIRE(test_list.size() == orig_size);
                REQUIRE(insert_count == 0);
            }

            SECTION("Shrink") {
                ADD_ERASE_LISTENERS(test_list, l, copy_constr)

                l::size_type new_size = 2;
                auto         res = test_list.resize(new_size, to_copy_f);
                REQUIRE(res == false);
                REQUIRE(test_list.size() == orig_size);
                REQUIRE(erase_call_cnt == 1);
                REQUIRE(erase_count == 0);
            }
        }
    }
}

TEST_CASE("list.merge", "[list][merge][operations][insert_notification][erase_notification]") {
    using l = cne::list<std::list<emplace_constr>>;
    l            dest;
    l::size_type dest_size = 3;
    l            src;
    l::size_type src_size = 5;
    for (l::size_type i = 0; i < dest_size; ++i) { dest.container().emplace_back(true); }

    ADD_INSERT_LISTENERS(dest, l, emplace_constr)
    ADD_ERASE_LISTENERS(src, l, emplace_constr)

    SECTION("Success") {
        for (l::size_type i = 0; i < src_size; ++i) { src.container().emplace_back(true); }

        bool res = dest.merge(src);
        REQUIRE(res == true);
        REQUIRE(src.size() == 0);
        REQUIRE(dest.size() == (dest_size + src_size));
        REQUIRE(insert_count != 0);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        for (l::size_type i = 0; i < src_size; ++i) { src.container().emplace_back(false); }

        bool res = dest.merge(src);
        REQUIRE(res != true);
        REQUIRE(src.size() == src_size);
        REQUIRE(dest.size() == dest_size);
        REQUIRE(insert_count == 0);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("list.splice", "[list][splice][operations][insert_notification][erase_notification]") {
    using l = cne::list<std::list<emplace_constr>>;
    l            dest;
    l::size_type dest_size = 3;
    l            src;
    l::size_type src_size = 5;
    for (l::size_type i = 0; i < dest_size; ++i) { dest.container().emplace_back(true); }

    ADD_INSERT_LISTENERS(dest, l, emplace_constr)
    ADD_ERASE_LISTENERS(src, l, emplace_constr)

    SECTION("Success") {
        for (l::size_type i = 0; i < src_size; ++i) { src.container().emplace_back(true); }

        bool res = dest.splice(dest.end(), src);
        REQUIRE(res == true);
        REQUIRE(src.size() == 0);
        REQUIRE(dest.size() == (dest_size + src_size));
        REQUIRE(insert_count != 0);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        for (l::size_type i = 0; i < src_size; ++i) { src.container().emplace_back(false); }

        bool res = dest.splice(dest.end(), src);
        REQUIRE(res != true);
        REQUIRE(src.size() == src_size);
        REQUIRE(dest.size() == dest_size);
        REQUIRE(insert_count == 0);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("list.remove", "[list][remove][operations][erase_notification]") {
    using l = cne::list<std::list<emplace_constr>>;
    l            test_list;
    l::size_type count_t = 5;
    l::size_type count_f = 3;
    for (l::size_type i = 0; i < count_t; ++i) { test_list.container().emplace_back(true); }
    for (l::size_type i = 0; i < count_f; ++i) { test_list.container().emplace_back(false); }

    ADD_ERASE_LISTENERS(test_list, l, emplace_constr)

    SECTION("Success") {
        auto res = test_list.remove(emplace_constr(true));
        REQUIRE(res == count_t);
        REQUIRE(test_list.size() == count_f);
        REQUIRE(erase_count != 0);
	}

    SECTION("Fail") {
        auto res = test_list.remove(emplace_constr(false));
        REQUIRE(res == 0);
        REQUIRE(test_list.size() == (count_t + count_f));
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("list.unique", "[list][unique][operations][erase_notification]") {
    using l = cne::list<std::list<emplace_constr>>;
    l            test_list;
    l::size_type count = 3;

    ADD_ERASE_LISTENERS(test_list, l, emplace_constr)

    SECTION("Success") {
        for (l::size_type i = 0; i < count; ++i) { test_list.container().emplace_back(true); }

        auto res = test_list.unique();
        REQUIRE(res == 2);
        REQUIRE(test_list.size() == 1);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        for (l::size_type i = 0; i < count; ++i) { test_list.container().emplace_back(false); }

        auto res = test_list.remove(emplace_constr(false));
        REQUIRE(res == 0);
        REQUIRE(test_list.size() == count);
        REQUIRE(erase_count == 0);
    }
}

TEST_CASE("list.swap", "[list][swap][modifiers][insert_notification][erase_notification][replace_notification]") {
    using l = cne::list<std::list<no_restrictions>>;
    l            src;
    l::size_type src_size = 5;
    l            dest;
    l::size_type dest_size = 3;

    ADD_INSERT_LISTENERS(dest, l, no_restrictions)
    ADD_REPLACE_LISTENERS(dest, l, no_restrictions)
    ADD_ERASE_LISTENERS(src, l, no_restrictions)

    SECTION("Success") {
        src.container().insert(src.container().cend(), src_size, true);
        dest.container().insert(dest.container().cend(), dest_size, true);

        bool res = dest.swap(src);
        REQUIRE(res == true);
        REQUIRE(src.size() == dest_size);
        REQUIRE(dest.size() == src_size);
        REQUIRE(insert_count != 0);
        REQUIRE(replace_count != 0);
        REQUIRE(erase_count != 0);
    }

    SECTION("Fail") {
        src.container().insert(src.container().cend(), src_size, false);
        dest.container().insert(dest.container().cend(), dest_size, true);

        bool res = dest.swap(src);
        REQUIRE(res != true);
        REQUIRE(src.size() == src_size);
        REQUIRE(dest.size() == dest_size);
        REQUIRE(insert_count == 0);
        REQUIRE(replace_count == 0);
        REQUIRE(erase_count == 0);
    }
}