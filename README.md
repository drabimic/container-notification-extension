# Container notification extension
Header only library extending stl containers with notifications on attempts to change its content.


## Tests
Library Catch2 is used for testing.
Command line interface:
https://github.com/catchorg/Catch2/blob/edde6f473604c6364476d2963939272ac7c795d4/docs/command-line.md#top

To run all test, just execute the binary.

When compiling with gcc, some tests are excluded from compilation due to bug in gcc - some methods impose more type requirements than they are supposed to according to standard.

## Sample
Trivial example of usage.