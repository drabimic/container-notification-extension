#include "../headers/containers/map.h"
#include "../headers/containers/set.h"

#include <map>
#include <set>

using state = int;
using set = cne::set<std::set<state>>;
using multimap = cne::multimap<std::multimap<state, state>>;

int main() {
    set      states;
    multimap transitions;

    transitions.insert_check(
      [&states](const multimap& transitions, const multimap::value_type& to_add, multimap::const_iterator where) {
          // States of transition must exist in set of states
          return states.count(to_add.first) && states.count(to_add.second);
      });

    transitions.insert_check(
      [&states](const multimap& transitions, const multimap::value_type& to_add, multimap::const_iterator where) {
          // The transition mustn't exist
          auto range = transitions.equal_range(to_add.first);
          for (auto it = range.first; it != range.second; ++it) {
              if (it->second == to_add.second) { return false; }
          }

          return true;
      });

    transitions.replace_check(
      [&states](auto& transitions, auto iterator, auto& new_to) { return states.count(new_to); });

    transitions.replace_check(
      [&states](const multimap& transitions, multimap::const_iterator iterator, const multimap::mapped_type& new_to) {
          auto range = transitions.equal_range(iterator->first);
          for (auto it = range.first; it != range.second; ++it) {
              if (it->second == new_to) { return false; }
          }

          return true;
      });

    // Add states
    for (int i = 0; i < 6; ++i) { states.emplace(i); }

    // Add transitions - only valid are added
    transitions.emplace(0, 5);
    transitions.emplace(0, 3);
    transitions.emplace(0, 9);
    transitions.emplace(0, 5);
    transitions.emplace(5, 5);

    auto it = transitions.find(0);
    // Replace mapped value
    it->second = 4;

    // Change mapped value - this won't work as there already is (0,3) transition in the map
    it->second.invoke([](state& x) { x--; }, [](state& x) { x++; });

    // Two ways to work with const&
    state start;
    start = it->first;
    it->first.invoke([](state x) { });
    return 0;
}